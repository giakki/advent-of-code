/** Correct results:
 * - 3239890
 * - 4856963
 */
use crate::common::errors::AocError;
use std::str::FromStr;

const fn fuel_req(fuel: i32) -> i32 {
    fuel / 3 - 2
}

const fn fuel_req_total(fuel: i32) -> i32 {
    let mut iter = fuel_req(fuel);
    let mut total = 0;

    while iter > 0 {
        total += iter;
        iter = fuel_req(iter);
    }

    total
}

#[aoc_generator(day1)]
fn generator(input: &str) -> Result<Vec<i32>, AocError> {
    input
        .lines()
        .map(FromStr::from_str)
        .collect::<Result<Vec<_>, _>>()
        .map_err(|_| AocError::ParseError("Invalid input".to_owned()))
}

#[aoc(day1, part1)]
fn part01(input: &[i32]) -> i32 {
    input.iter().copied().map(fuel_req).sum()
}

#[aoc(day1, part2)]
fn part02(input: &[i32]) -> i32 {
    input.iter().copied().map(fuel_req_total).sum()
}

#[cfg(test)]
mod tests {

    #[test]
    fn test0101() {
        let inputs = vec!["12", "14", "1969", "100756"];
        for input in inputs {
            let gen = super::generator(input).unwrap();
            let res = super::part01(&gen);
            println!("-- {} --", res)
        }
    }

    #[test]
    fn test0102() {
        let inputs = vec!["14", "1969", "100756"];
        for input in inputs {
            let gen = super::generator(input).unwrap();
            let res = super::part02(&gen);
            println!("-- {} --", res)
        }
    }
}
