/** Correct results:
 * - 1097
 * - 1188
 */
use crate::common::{errors::AocError, parsers::input_parser};
use itertools::Itertools;
use nom::{character::complete::anychar, combinator::map_opt, multi::many0};

#[aoc_generator(day1)]
fn generator(input: &str) -> Result<Vec<u32>, AocError> {
    input_parser(many0(map_opt(anychar, |c| c.to_digit(10))))(input)
}

#[aoc(day1, part1)]
fn part01(original_input: &[u32]) -> u32 {
    let mut input = original_input.iter().collect::<Vec<_>>();
    input.push(input[0]);

    input
        .into_iter()
        .tuple_windows()
        .filter_map(|(a, b)| if a == b { Some(a) } else { None })
        .sum()
}

#[aoc(day1, part2)]
fn part02(input: &[u32]) -> u32 {
    let mut sum = 0;
    for i in 0..input.len() {
        let ix = i;
        let iy = (i + input.len() / 2) % input.len();
        if input[ix] == input[iy] {
            sum += input[ix];
        }
    }

    sum
}

#[cfg(test)]
mod tests {
    #[test]
    fn test0101() {
        let inputs = vec!["1122", "1111", "1234", "91212129"];
        for input in inputs {
            let gen = super::generator(input).unwrap();
            let res = super::part01(&gen);

            println!("-- {} --", res);
        }
    }

    #[test]
    fn test0102() {
        let inputs = vec!["1212", "1221", "123425", "123123", "12131415"];

        for input in inputs {
            let gen = super::generator(input).unwrap();
            let res = super::part02(&gen);

            println!("-- {} --", res);
        }
    }
}
