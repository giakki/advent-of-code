/** Correct results:
 * - c6697b55
 * - 8c35d1ab
 */
use crate::common::errors::AocError;
use itertools::Itertools;
use md5::{Digest, Md5};
use rayon::iter::{IntoParallelIterator, ParallelIterator};
use std::collections::HashMap;

#[inline]
const fn hex_to_char(byte: u8) -> char {
    if byte < 10 {
        (byte + b'0') as char
    } else {
        (byte + b'a' - 10) as char
    }
}

struct Md5Iterator<'a> {
    base: &'a str,
    n: usize,
}

impl<'a> Md5Iterator<'a> {
    pub const fn new(base: &'a str) -> Self {
        Self { base, n: 0 }
    }
}

impl<'a> Iterator for Md5Iterator<'a> {
    type Item = [u8; 16];

    fn next(&mut self) -> Option<Self::Item> {
        if let Some((n, result)) = (self.n..usize::MAX).into_par_iter().find_map_first(|n| {
            let result = Md5::new()
                .chain_update(self.base)
                .chain_update(n.to_string())
                .finalize();

            if let Some((a, b, c)) = result.iter().take(3).collect_tuple() {
                if *a == 0 && *b == 0 && (*c & 0xF0) == 0 {
                    return Some((n, result.into()));
                }
            }
            None
        }) {
            self.n = n + 1;
            Some(result)
        } else {
            None
        }
    }
}

#[aoc(day5, part1)]
fn part01(input: &str) -> String {
    Md5Iterator::new(input)
        .take(8)
        .map(|digest| hex_to_char(digest[2] & 0x0F))
        .collect::<String>()
}

#[aoc(day5, part2)]
fn part02(input: &str) -> Result<String, AocError> {
    let mut password = HashMap::with_capacity(8);

    for (idx, chr) in Md5Iterator::new(input).filter_map(|digest| {
        let idx = digest[2] & 0x0F;
        if idx < 8 {
            Some((idx, hex_to_char(digest[3] >> 4)))
        } else {
            None
        }
    }) {
        password.entry(idx).or_insert(chr);
        if password.len() == 8 {
            return Ok(password
                .into_iter()
                .sorted_by_key(|(k, _)| *k)
                .map(|(_, chr)| chr)
                .collect());
        }
    }

    Err(AocError::NoSolution)
}

#[cfg(test)]
mod tests {

    use super::*;

    const BUILD_INPUT: &str = r"abc";

    #[test]
    fn test0501() {
        let res = part01(BUILD_INPUT);

        assert_eq!(res, "18f47a30")
    }

    #[test]
    fn test0502() {
        let res = part02(BUILD_INPUT).unwrap();

        assert_eq!(res, "05ace8e3")
    }
}
