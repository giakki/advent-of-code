/** Correct results:
 * - 105
 * - 258
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser},
};
use itertools::Itertools;
use nom::{
    branch::alt,
    character::complete::{alpha1, char},
    combinator::map,
    multi::many0,
    sequence::delimited,
    IResult,
};

#[derive(Debug, PartialEq, Eq)]
enum IPV7PartKind {
    Supernet,
    Hypernet,
}

#[derive(Debug, PartialEq, Eq)]
struct Aba(u8, u8);

impl Aba {
    pub const fn invert(&self) -> Self {
        Self(self.1, self.0)
    }
}

#[derive(Debug)]
struct IPV7Part<'a> {
    kind: IPV7PartKind,
    value: &'a str,
}

impl<'a> IPV7Part<'a> {
    pub fn parse(s: &'a str) -> IResult<&'a str, Self> {
        alt((
            map(alpha1, |value| Self {
                kind: IPV7PartKind::Supernet,
                value,
            }),
            map(delimited(char('['), alpha1, char(']')), |value| Self {
                kind: IPV7PartKind::Hypernet,
                value,
            }),
        ))(s)
    }

    pub fn has_abba(&self) -> bool {
        for (a, b, c, d) in self.value.as_bytes().iter().tuple_windows() {
            if a == d && b == c && a != b {
                return true;
            }
        }

        false
    }

    pub fn abas(&self) -> Vec<Aba> {
        self.value
            .as_bytes()
            .iter()
            .tuple_windows()
            .filter_map(|(a, b, c)| {
                if a == c && a != b {
                    Some(Aba(*a, *b))
                } else {
                    None
                }
            })
            .collect()
    }
}

type IPV7<'a> = Vec<IPV7Part<'a>>;

fn parse_ipv7(s: &str) -> IResult<&str, IPV7> {
    many0(IPV7Part::parse)(s)
}

fn generator(input: &str) -> Result<Vec<IPV7>, AocError> {
    input_parser(by_lines(parse_ipv7))(input)
}

#[aoc(day7, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    Ok(generator(input)?
        .into_iter()
        .filter(|ip| {
            let mut supports_tls = false;
            for part in ip {
                let has_abba = part.has_abba();
                if part.kind == IPV7PartKind::Hypernet && has_abba {
                    supports_tls = false;
                    break;
                }
                if part.kind == IPV7PartKind::Supernet && has_abba {
                    supports_tls = true;
                }
            }
            supports_tls
        })
        .count())
}

#[aoc(day7, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    Ok(generator(input)?
        .into_iter()
        .filter(|ip| {
            let (abas, babs) =
                ip.iter()
                    .fold((Vec::new(), Vec::new()), |(mut abas, mut babs), part| {
                        if part.kind == IPV7PartKind::Supernet {
                            abas.extend(part.abas());
                        }
                        if part.kind == IPV7PartKind::Hypernet {
                            babs.extend(part.abas());
                        }

                        (abas, babs)
                    });

            abas.into_iter().any(|aba| babs.contains(&aba.invert()))
        })
        .count())
}

#[cfg(test)]
mod tests {

    use super::*;

    const BUILD_INPUT_1: &str = r"abba[mnop]qrst
abcd[bddb]xyyx
aaaa[qwer]tyui
ioxxoj[asdfgh]zxcvbn";

    const BUILD_INPUT_2: &str = r"aba[bab]xyz
xyx[xyx]xyx
aaa[kek]eke
zazbz[bzb]cdb";

    #[test]
    fn test07_gen() {
        let input = generator(BUILD_INPUT_1).unwrap();

        println!("-- {:?} --", input)
    }

    #[test]
    fn test0701() {
        let res = part01(BUILD_INPUT_1).unwrap();

        assert_eq!(res, 2)
    }

    #[test]
    fn test0702() {
        let res = part02(BUILD_INPUT_2).unwrap();

        assert_eq!(res, 3)
    }
}
