/** Correct results:
 * - qrqlznrl
 * - kgzdfaon
 */

fn frequencies(input: &str) -> Vec<Vec<u32>> {
    input.lines().fold(vec![], |mut freq, line| {
        for (idx, char) in line.as_bytes().iter().enumerate() {
            while idx + 1 > freq.len() {
                freq.push(vec![0; 26]);
            }

            freq[idx][(char - b'a') as usize] += 1;
        }
        freq
    })
}

#[aoc(day6, part1)]
fn part01(input: &str) -> String {
    frequencies(input)
        .into_iter()
        .fold(vec![], |mut chars, freq| {
            chars.push(
                freq.into_iter()
                    .enumerate()
                    .fold((0, 0), |(max_char, max_n), (chr, n)| {
                        if n > max_n {
                            (chr, n)
                        } else {
                            (max_char, max_n)
                        }
                    }),
            );
            chars
        })
        .into_iter()
        .map(|(chr, _)| ((chr as u8) + b'a') as char)
        .collect()
}

#[aoc(day6, part2)]
fn part02(input: &str) -> String {
    frequencies(input)
        .into_iter()
        .fold(vec![], |mut chars, freq| {
            chars.push(freq.into_iter().enumerate().fold(
                (usize::MAX, u32::MAX),
                |(min_char, min_n), (chr, n)| {
                    if n < min_n {
                        (chr, n)
                    } else {
                        (min_char, min_n)
                    }
                },
            ));
            chars
        })
        .into_iter()
        .map(|(chr, _)| ((chr as u8) + b'a') as char)
        .collect()
}

#[cfg(test)]
mod tests {

    use super::*;

    const BUILD_INPUT: &str = r"eedadn
drvtee
eandsr
raavrd
atevrs
tsrnev
sdttsa
rasrtv
nssdts
ntnada
svetve
tesnvt
vntsnd
vrdear
dvrsen
enarar";

    #[test]
    fn test0601() {
        let res = part01(BUILD_INPUT);

        assert_eq!(res, "easter")
    }

    #[test]
    fn test0602() {
        let res = part02(BUILD_INPUT);

        assert_eq!(res, "advent")
    }
}
