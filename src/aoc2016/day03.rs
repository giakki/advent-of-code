/** Correct results:
 * - 993
 * - 1849
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use nom::{
    character::complete::{space0, space1},
    sequence::{preceded, tuple},
};

type MaybeTriangle = (u32, u32, u32);

#[aoc_generator(day3)]
fn generator(input: &str) -> Result<Vec<MaybeTriangle>, AocError> {
    input_parser(by_lines(tuple((
        preceded(space0, parse_int),
        preceded(space1, parse_int),
        preceded(space1, parse_int),
    ))))(input)
}

#[inline]
const fn is_valid_triangle(a: u32, b: u32, c: u32) -> bool {
    a + b > c && a + c > b && b + c > a
}

#[aoc(day3, part1)]
fn part01(input: &[MaybeTriangle]) -> usize {
    input
        .iter()
        .filter(|(a, b, c)| is_valid_triangle(*a, *b, *c))
        .count()
}

#[aoc(day3, part2)]
fn part02(input: &[MaybeTriangle]) -> usize {
    input
        .chunks_exact(3)
        .flat_map(|chunk| {
            [
                (chunk[0].0, chunk[1].0, chunk[2].0),
                (chunk[0].1, chunk[1].1, chunk[2].1),
                (chunk[0].2, chunk[1].2, chunk[2].2),
            ]
        })
        .filter(|(a, b, c)| is_valid_triangle(*a, *b, *c))
        .count()
}

#[cfg(test)]
mod tests {

    use super::*;

    const BUILD_INPUT: &str = r"101 301 501
102 302 502
103 303 503
201 401 601
202 402 602
203 403 603";

    #[test]
    fn test03_gen() {
        let input = generator(BUILD_INPUT).unwrap();

        println!("-- {:?} --", input)
    }

    #[test]
    fn test0301() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 3);
    }

    #[test]
    fn test0302() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, 6);
    }
}
