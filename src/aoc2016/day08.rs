/** Correct results:
 * - 123
 * - AFBUPZBJPS
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use itertools::Itertools;
use ndarray::{s, Array, Array2, Axis};
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::char,
    combinator::map,
    sequence::{preceded, separated_pair},
    IResult,
};

#[derive(Debug)]
enum Command {
    Rect(usize, usize),
    RotateRow(usize, usize),
    RotateCol(usize, usize),
}

impl Command {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        alt((
            map(
                preceded(
                    tag("rect "),
                    separated_pair(parse_int, char('x'), parse_int),
                ),
                |(a, b)| Self::Rect(a, b),
            ),
            map(
                preceded(
                    tag("rotate row y="),
                    separated_pair(parse_int, tag(" by "), parse_int),
                ),
                |(a, b)| Self::RotateRow(a, b),
            ),
            map(
                preceded(
                    tag("rotate column x="),
                    separated_pair(parse_int, tag(" by "), parse_int),
                ),
                |(a, b)| Self::RotateCol(a, b),
            ),
        ))(s)
    }
}

#[aoc_generator(day8)]
fn generator(input: &str) -> Result<Vec<Command>, AocError> {
    input_parser(by_lines(Command::parse))(input)
}

fn run_commands(commands: &[Command]) -> Array2<bool> {
    const WIDTH: usize = 50;
    const HEIGHT: usize = 6;
    let mut display = Array2::from_elem((HEIGHT, WIDTH), false).reversed_axes();

    for command in commands {
        match command {
            Command::Rect(x, y) => {
                display.slice_mut(s![..*x, ..*y]).fill(true);
            }
            Command::RotateRow(y, amount) => {
                let mut slice = display.column(*y).iter().copied().collect_vec();
                slice.rotate_right(amount % WIDTH);
                display.column_mut(*y).assign(&Array::from_iter(slice));
            }
            Command::RotateCol(x, amount) => {
                let mut slice = display.row(*x).iter().copied().collect_vec();
                slice.rotate_right(amount % HEIGHT);
                display.row_mut(*x).assign(&Array::from_iter(slice));
            }
        }
    }

    display
}

#[aoc(day8, part1)]
fn part01(input: &[Command]) -> usize {
    let display = run_commands(input);

    display.into_iter().filter(|x| *x).count()
}

#[aoc(day8, part2)]
fn part02(input: &[Command]) -> usize {
    let display = run_commands(input);
    let prnt = display
        .lanes(Axis(0))
        .into_iter()
        .map(|row| {
            row.into_iter()
                .map(|b| if *b { "#" } else { "." })
                .collect::<Vec<_>>()
                .join("")
        })
        .collect::<Vec<_>>()
        .join("\n");
    eprintln!("{prnt}\n");
    0
}

#[cfg(test)]
mod tests {

    use super::*;

    const BUILD_INPUT: &str = r"rect 3x2
rotate column x=1 by 1
rotate row y=0 by 4
rotate column x=1 by 1";

    #[test]
    fn test08_gen() {
        let input = generator(BUILD_INPUT).unwrap();

        println!("{:?}", input);
    }

    #[test]
    fn test0801() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 6)
    }

    #[test]
    fn test0802() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, 3)
    }
}
