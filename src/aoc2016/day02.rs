/** Correct results:
 * - 24862
 * - 46C91
 */
use crate::common::{
    direction::Direction,
    errors::AocError,
    parsers::{by_lines, input_parser},
    point::{Point, PointOps},
};
use nom::{multi::many0, IResult};

fn parse_line(s: &str) -> IResult<&str, Vec<Direction>> {
    many0(Direction::parse_from_initial)(s)
}

fn is_valid_position_1(pos: Point<i32>) -> bool {
    pos.x() > -1 && pos.x() < 3 && pos.y() > -1 && pos.y() < 3
}

fn solve<F: Fn(Point<i32>) -> bool>(
    mut position: Point<i32>,
    directions: &[Direction],
    pos_validator: F,
) -> Point<i32> {
    for dir in directions {
        let mut new_pos = position;
        new_pos.move_by(1, dir);
        if pos_validator(new_pos) {
            position = new_pos;
        }
    }

    position
}

fn is_valid_position_2(pos: Point<i32>) -> bool {
    match pos.x() {
        0 | 4 => pos.y() == 2,
        1 | 3 => pos.y() > 0 && pos.y() < 4,
        2 => pos.y() > -1 && pos.y() < 5,
        _ => false,
    }
}

const fn pos_to_char_1(pos: Point<i32>) -> Result<char, AocError> {
    match pos {
        (0, 0) => Ok('1'),
        (1, 0) => Ok('2'),
        (2, 0) => Ok('3'),
        (0, 1) => Ok('4'),
        (1, 1) => Ok('5'),
        (2, 1) => Ok('6'),
        (0, 2) => Ok('7'),
        (1, 2) => Ok('8'),
        (2, 2) => Ok('9'),
        _ => Err(AocError::NoSolution),
    }
}

const fn pos_to_char_2(pos: Point<i32>) -> Result<char, AocError> {
    match pos {
        (2, 0) => Ok('1'),
        (1, 1) => Ok('2'),
        (2, 1) => Ok('3'),
        (3, 1) => Ok('4'),
        (0, 2) => Ok('5'),
        (1, 2) => Ok('6'),
        (2, 2) => Ok('7'),
        (3, 2) => Ok('8'),
        (4, 2) => Ok('9'),
        (1, 3) => Ok('A'),
        (2, 3) => Ok('B'),
        (3, 3) => Ok('C'),
        (2, 4) => Ok('D'),
        _ => Err(AocError::NoSolution),
    }
}

#[aoc_generator(day2)]
fn generator(input: &str) -> Result<Vec<Vec<Direction>>, AocError> {
    input_parser(by_lines(parse_line))(input)
}

#[aoc(day2, part1)]
fn part01(input: &[Vec<Direction>]) -> Result<String, AocError> {
    let mut res = Vec::new();
    let mut position = (1, 1);

    for line in input {
        position = solve(position, line, is_valid_position_1);
        res.push(position);
    }

    res.into_iter().map(pos_to_char_1).collect()
}

#[aoc(day2, part2)]
fn part02(input: &[Vec<Direction>]) -> Result<String, AocError> {
    let mut res = Vec::new();
    let mut position = (0, 2);

    for line in input {
        position = solve(position, line, is_valid_position_2);
        res.push(position);
    }

    res.into_iter().map(pos_to_char_2).collect()
}

#[cfg(test)]
mod tests {

    use super::{is_valid_position_1, is_valid_position_2};

    const BUILD_INPUT: &str = r"ULL
RRDDD
LURDL
UUUUD";

    #[test]
    fn test_solve_1() {
        let dirs = super::generator("LURDL").unwrap();
        let res = super::solve((2, 2), dirs.first().unwrap(), is_valid_position_1);
        println!("{:?}", res);
    }

    #[test]
    fn test0001() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::part01(&input);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test_solve_2() {
        let dirs = super::generator("UUUUD").unwrap();
        let res = super::solve((2, 3), dirs.first().unwrap(), is_valid_position_2);
        println!("{:?}", res);
    }

    #[test]
    fn test0002() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::part02(&input);

        println!("-- {:?} --", res)
    }
}
