/** Correct results:
 * - 181
 * - 12567
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use nom::{
    branch::alt,
    bytes::complete::tag,
    combinator::map,
    sequence::{delimited, preceded, tuple},
    IResult,
};
use std::collections::HashMap;

#[derive(Debug, Default)]
struct Bot {
    low: Option<u32>,
    high: Option<u32>,
}

impl Bot {
    pub fn give(&mut self) -> Option<(u32, u32)> {
        if let Some(low) = self.low {
            if let Some(high) = self.high {
                self.low = None;
                self.high = None;

                return Some((low, high));
            }
        }
        None
    }

    pub fn take(&mut self, value: u32) {
        if let Some(low) = self.low {
            self.low = Some(u32::min(low, value));
            self.high = Some(u32::max(low, value));
        } else {
            self.low = Some(value);
            self.high = None;
        }
    }
}

#[derive(Debug, Clone, Copy)]
enum Destination {
    Bot(usize),
    Output(usize),
}

impl Destination {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        alt((
            map(preceded(tag("bot "), parse_int), Self::Bot),
            map(preceded(tag("output "), parse_int), Self::Output),
        ))(s)
    }
}

#[derive(Debug)]
enum Instruction {
    Value(u32, usize),
    Exchange(usize, Destination, Destination),
}

impl Instruction {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        alt((
            map(
                tuple((
                    preceded(tag("value "), parse_int),
                    preceded(tag(" goes to bot "), parse_int),
                )),
                |(value, bot)| Self::Value(value, bot),
            ),
            map(
                tuple((
                    delimited(tag("bot "), parse_int, tag(" gives ")),
                    preceded(tag("low to "), Destination::parse),
                    preceded(tag(" and high to "), Destination::parse),
                )),
                |(value, out1, out2)| Self::Exchange(value, out1, out2),
            ),
        ))(s)
    }
}

struct Factory {
    pub bots: HashMap<usize, Bot>,
    pub outputs: HashMap<usize, u32>,
    pub mappings: HashMap<usize, (Destination, Destination)>,
}

impl Factory {
    pub fn new(instructions: &[Instruction]) -> Self {
        let mut bots: HashMap<usize, Bot> = HashMap::new();
        let mut mappings = HashMap::new();

        for instruction in instructions {
            match instruction {
                Instruction::Value(value, bot) => {
                    bots.entry(*bot).or_default().take(*value);
                }
                Instruction::Exchange(from, low, high) => {
                    mappings.insert(*from, (*low, *high));
                }
            }
        }

        Self {
            bots,
            outputs: HashMap::new(),
            mappings,
        }
    }

    pub fn step(&mut self) -> Option<(usize, (u32, u32))> {
        if let Some((source_id, (low, high))) = self
            .bots
            .iter_mut()
            .find_map(|(id, bot)| bot.give().map(|values| (*id, values)))
        {
            if let Some((destination_low, destination_high)) =
                self.mappings.get(&source_id).copied()
            {
                self.do_exchange(destination_low, low);
                self.do_exchange(destination_high, high);

                Some((source_id, (low, high)))
            } else {
                None
            }
        } else {
            None
        }
    }

    fn do_exchange(&mut self, destination: Destination, value: u32) {
        match destination {
            Destination::Bot(id) => {
                self.bots.entry(id).or_default().take(value);
            }
            Destination::Output(id) => {
                *self.outputs.entry(id).or_default() = value;
            }
        }
    }
}

#[aoc_generator(day10)]
fn generator(input: &str) -> Result<Vec<Instruction>, AocError> {
    input_parser(by_lines(Instruction::parse))(input)
}

#[aoc(day10, part1)]
fn part01(input: &[Instruction]) -> Result<usize, AocError> {
    let mut factory = Factory::new(input);

    while let Some((id, (low, high))) = factory.step() {
        if low == 17 && high == 61 {
            return Ok(id);
        }
    }

    Err(AocError::NoSolution)
}

#[aoc(day10, part2)]
fn part02(input: &[Instruction]) -> Result<u32, AocError> {
    let mut factory = Factory::new(input);

    while factory.step().is_some() {
        if let Some(zero) = factory.outputs.get(&0) {
            if let Some(one) = factory.outputs.get(&1) {
                if let Some(two) = factory.outputs.get(&2) {
                    return Ok(zero * one * two);
                }
            }
        }
    }

    Err(AocError::NoSolution)
}

#[cfg(test)]
mod tests {

    use super::*;

    const BUILD_INPUT: &str = r"value 5 goes to bot 2
bot 2 gives low to bot 1 and high to bot 0
value 3 goes to bot 1
bot 1 gives low to output 1 and high to bot 0
bot 0 gives low to output 2 and high to output 0
value 2 goes to bot 2";

    #[rustfmt::skip]
    #[test]
    fn test10_gen() {
        println!("{:?}", generator(BUILD_INPUT).unwrap());
    }

    #[test]
    fn test1001() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input).unwrap();

        assert_eq!(res, 0)
    }

    #[test]
    fn test1002() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input).unwrap();

        assert_eq!(res, 0)
    }
}
