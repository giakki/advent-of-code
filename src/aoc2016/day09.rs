/** Correct results:
 * - 120765
 * - 11658395076
 */
use crate::common::{errors::AocError, parsers::parse_int};
use nom::{
    branch::alt,
    character::complete::{alpha1, char},
    combinator::map,
    sequence::{delimited, separated_pair},
    IResult,
};

#[derive(Debug)]
enum Segment<'a> {
    Marker(usize, usize),
    Data(&'a str),
}

impl<'a> Segment<'a> {
    pub fn parse(s: &'a str) -> IResult<&'a str, Self> {
        alt((
            map(alpha1, Self::Data),
            map(
                delimited(
                    char('('),
                    separated_pair(parse_int, char('x'), parse_int),
                    char(')'),
                ),
                |(x, y)| Self::Marker(x, y),
            ),
        ))(s)
    }
}

fn decompress(mut s: &str, rec: bool) -> Result<usize, AocError> {
    let mut result = 0;

    while !s.is_empty() {
        let segment;
        (s, segment) = Segment::parse(s)?;

        match segment {
            Segment::Marker(num_chars, times) => {
                let inner = if rec {
                    decompress(&s[0..num_chars], true)?
                } else {
                    num_chars
                };

                result += inner * times;
                s = &s[num_chars..];
            }
            Segment::Data(str) => {
                result += str.len();
            }
        }
    }

    Ok(result)
}

#[aoc(day9, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    decompress(input, false)
}

#[aoc(day9, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    decompress(input, true)
}

#[cfg(test)]
mod tests {

    use super::*;

    const BUILD_INPUT_1: &str = r"ADVENT";
    const BUILD_INPUT_2: &str = r"A(1x5)BC";
    const BUILD_INPUT_3: &str = r"(3x3)XYZ";
    const BUILD_INPUT_4: &str = r"A(2x2)BCD(2x2)EFG";
    const BUILD_INPUT_5: &str = r"(6x1)(1x3)A";
    const BUILD_INPUT_6: &str = r"X(8x2)(3x3)ABCY";

    #[test]
    fn test0901() {
        assert_eq!(part01(BUILD_INPUT_1).unwrap(), 6);
        assert_eq!(part01(BUILD_INPUT_2).unwrap(), 7);
        assert_eq!(part01(BUILD_INPUT_3).unwrap(), 9);
        assert_eq!(part01(BUILD_INPUT_4).unwrap(), 11);
        assert_eq!(part01(BUILD_INPUT_5).unwrap(), 6);
        assert_eq!(part01(BUILD_INPUT_6).unwrap(), 18);
    }

    #[test]
    fn test0902() {
        assert_eq!(part02(BUILD_INPUT_1).unwrap(), 6);
        assert_eq!(part02(BUILD_INPUT_2).unwrap(), 7);
        assert_eq!(part02(BUILD_INPUT_3).unwrap(), 9);
        assert_eq!(part02(BUILD_INPUT_4).unwrap(), 11);
        assert_eq!(part02(BUILD_INPUT_5).unwrap(), 3);
        assert_eq!(part02(BUILD_INPUT_6).unwrap(), 20);
    }
}
