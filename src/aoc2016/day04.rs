/** Correct results:
 * - 245102
 * - 324
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use itertools::Itertools;
use nom::{
    character::complete::{alpha1, char},
    combinator::{map, recognize},
    multi::separated_list0,
    sequence::{delimited, preceded, tuple},
    IResult,
};
use std::{cmp::Ordering, collections::HashMap};

#[derive(Debug)]
struct Room<'a> {
    name: &'a str,
    sector_id: u32,
    checksum: &'a str,
}

impl<'a> Room<'a> {
    pub fn parse(s: &'a str) -> IResult<&'a str, Self> {
        map(
            tuple((
                recognize(separated_list0(char('-'), alpha1)),
                preceded(char('-'), parse_int),
                delimited(char('['), alpha1, char(']')),
            )),
            |(name, sector_id, checksum)| Self {
                name,
                sector_id,
                checksum,
            },
        )(s)
    }
}

fn generator(input: &str) -> Result<Vec<Room>, AocError> {
    input_parser(by_lines(Room::parse))(input)
}

#[aoc(day4, part1)]
fn part01(input: &str) -> Result<u32, AocError> {
    Ok(generator(input)?
        .into_iter()
        .filter_map(|room| {
            let mut letters: HashMap<char, u32> = HashMap::with_capacity(26);
            for letter in room.name.chars() {
                if letter != '-' {
                    *letters.entry(letter).or_default() += 1;
                }
            }

            if letters
                .into_iter()
                .sorted_by(|a, b| match b.1.cmp(&a.1) {
                    Ordering::Equal => a.0.cmp(&b.0),
                    a => a,
                })
                .take(room.checksum.len())
                .all(|(letter, _)| room.checksum.contains(letter))
            {
                Some(room.sector_id)
            } else {
                None
            }
        })
        .sum())
}

#[aoc(day4, part2)]
fn part02(input: &str) -> Result<u32, AocError> {
    generator(input)?
        .into_iter()
        .find_map(|room| {
            let name: String = room
                .name
                .as_bytes()
                .iter()
                .filter_map(|byte| {
                    if *byte == b'-' {
                        None
                    } else {
                        Some(
                            (((u32::from(byte - b'a') + room.sector_id) % 26) as u8 + b'a') as char,
                        )
                    }
                })
                .collect();

            if name.contains("northpoleobjectstorage") {
                Some(room.sector_id)
            } else {
                None
            }
        })
        .ok_or(AocError::NoSolution)
}

#[cfg(test)]
mod tests {

    use super::*;

    const BUILD_INPUT: &str = r"aaaaa-bbb-z-y-x-123[abxyz]
a-b-c-d-e-f-g-h-987[abcde]
not-a-real-room-404[oarel]
totally-real-room-200[decoy]";

    #[test]
    fn test04_gen() {
        let input = generator(BUILD_INPUT).unwrap();

        println!("-- {:?} --", input)
    }

    #[test]
    fn test0401() {
        let res = part01(BUILD_INPUT).unwrap();

        assert_eq!(res, 1514)
    }

    #[test]
    fn test0402() {
        let res = part02(BUILD_INPUT).unwrap();

        assert_eq!(res, 1514)
    }
}
