/** Correct results:
* - 332
* - 166
*/
use crate::common::{
    direction::{Direction, Turn},
    errors::AocError,
    parsers::input_parser,
    parsers::parse_int,
    point::PointOps,
};
use nom::{
    branch::alt, bytes::complete::tag, character::complete::char, combinator::map,
    multi::separated_list0, sequence::tuple, IResult,
};
use std::collections::HashSet;

struct Command {
    turn: Turn,
    distance: i32,
}

fn parse_turn(s: &str) -> IResult<&str, Turn> {
    map(alt((char('L'), char('R'))), |c| match c {
        'L' => Turn::Ccw,
        'R' => Turn::Cw,
        _ => unreachable!(),
    })(s)
}

fn parse_one_input(s: &str) -> IResult<&str, Command> {
    map(tuple((parse_turn, parse_int)), |(turn, distance)| Command {
        turn,
        distance,
    })(s)
}

#[aoc_generator(day1)]
fn generator(input: &str) -> Result<Vec<Command>, AocError> {
    input_parser(separated_list0(tag(", "), parse_one_input))(input)
}

#[aoc(day1, part1)]
fn part01(input: &[Command]) -> i32 {
    let mut pos = (0i32, 0);
    let mut dir = Direction::Up;
    for command in input {
        dir = dir.rotate(&command.turn);
        pos.move_by(command.distance, &dir);
    }

    pos.x().abs() + pos.y().abs()
}

#[aoc(day1, part2)]
fn part02(input: &[Command]) -> Result<i32, AocError> {
    let mut pos = (0i32, 0);
    let mut dir = Direction::Up;

    let mut visited = HashSet::new();
    visited.insert(pos);
    for command in input {
        dir = dir.rotate(&command.turn);
        for _ in 0..command.distance {
            pos.move_by(1, &dir);

            if visited.contains(&pos) {
                return Ok(pos.x().abs() + pos.y().abs());
            }
            visited.insert(pos);
        }
    }

    Err(AocError::NoSolution)
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT_1: &str = r"R2, L3";
    const BUILD_INPUT_2: &str = r"R2, R2, R2";
    const BUILD_INPUT_3: &str = r"R5, L5, R5, R3";
    const BUILD_INPUT_4: &str = r"R8, R4, R4, R8";

    #[test]
    fn test0101_1() {
        let input = super::generator(BUILD_INPUT_1).unwrap();
        let res = super::part01(&input);

        println!("-- {} --", res)
    }

    #[test]
    fn test0101_2() {
        let input = super::generator(BUILD_INPUT_2).unwrap();
        let res = super::part01(&input);

        println!("-- {} --", res)
    }

    #[test]
    fn test0101_3() {
        let input = super::generator(BUILD_INPUT_3).unwrap();
        let res = super::part01(&input);

        println!("-- {} --", res)
    }

    #[test]
    fn test0102() {
        let input = super::generator(BUILD_INPUT_4).unwrap();
        let res = super::part02(&input);

        println!("-- {:?} --", res)
    }
}
