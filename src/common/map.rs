use super::{chr::FromChar, point::PointOps};
use ndarray::{Array2, Axis, Dim, ShapeError, StrideShape};
use std::fmt;
use std::iter;
use std::str::FromStr;

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Map<T> {
    pub tiles: Array2<T>,
}

impl<T> Map<T> {
    #[must_use]
    pub const fn from_ndarray(tiles: Array2<T>) -> Self {
        Self { tiles }
    }

    pub fn from_shape_vec<S: Into<StrideShape<Dim<[usize; 2]>>>>(
        shape: S,
        v: Vec<T>,
    ) -> Result<Self, ShapeError> {
        Ok(Self {
            tiles: Array2::from_shape_vec(shape, v)?,
        })
    }

    #[must_use]
    pub fn at(&self, index: (usize, usize)) -> &T {
        &self.tiles[index]
    }

    #[must_use]
    pub fn at_mut(&mut self, index: (usize, usize)) -> &mut T {
        &mut self.tiles[index]
    }

    #[must_use]
    pub fn is_empty(&self) -> bool {
        self.tiles.shape()[0] == 0
    }

    #[must_use]
    pub fn height(&self) -> usize {
        self.tiles.shape()[1]
    }

    #[must_use]
    pub fn width(&self) -> usize {
        self.tiles.shape()[0]
    }

    pub fn iter_positions(&self) -> impl Iterator<Item = (usize, usize)> {
        let shape = self.tiles.shape();
        let s0 = shape[0];
        let s1 = shape[1];

        (0..s1).flat_map(move |e| (0..s0).zip(iter::repeat(e)))
    }
}

impl<T: Clone + Default> Map<T> {
    #[must_use]
    pub fn at_checked(&self, index: (isize, isize)) -> T {
        if index.x() < 0
            || index.y() < 0
            || index.x() as usize >= self.width()
            || index.y() as usize >= self.height()
        {
            return T::default();
        }

        self.at((index.x() as usize, index.y() as usize)).clone()
    }
}

impl<T: Copy> Map<T> {
    #[must_use]
    pub fn from_vec2(tiles: &[Vec<T>]) -> Self {
        Self {
            tiles: Array2::from_shape_fn((tiles[0].len(), tiles.len()), |(x, y)| tiles[y][x]),
        }
    }

    #[must_use]
    pub fn iter_neighbors_fn<U, F: Fn(T, Vec<T>) -> U>(&self, fun: F) -> NeighborIter<T, U, F> {
        NeighborIter::new(fun, self)
    }
}

pub struct NeighborIter<'a, T, U, F: Fn(T, Vec<T>) -> U> {
    fun: F,
    iter: Box<dyn Iterator<Item = (usize, usize)> + 'a>,
    map: &'a Map<T>,
}

impl<'a, T, U, F: Fn(T, Vec<T>) -> U> NeighborIter<'a, T, U, F> {
    fn new(fun: F, map: &'a Map<T>) -> Self {
        Self {
            fun,
            map,
            iter: Box::new(map.iter_positions()),
        }
    }
}

impl<'a, T: Copy, U, F: Fn(T, Vec<T>) -> U> Iterator for NeighborIter<'a, T, U, F> {
    type Item = U;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(pos) = self.iter.next() {
            let tile = *self.map.at(pos);
            let neighbors = pos
                .all_neighbors_checked(self.map.width() - 1, self.map.height() - 1)
                .iter()
                .map(|p| *self.map.at(*p))
                .collect();
            Some((self.fun)(tile, neighbors))
        } else {
            None
        }
    }
}

impl<T: Copy + FromChar> FromStr for Map<T> {
    type Err = T::Err;

    fn from_str(input: &str) -> Result<Self, T::Err> {
        Ok(Self::from_vec2(
            &input
                .lines()
                .map(|line| {
                    line.chars()
                        .map(|c| T::from_char(&c))
                        .collect::<Result<Vec<T>, T::Err>>()
                })
                .collect::<Result<Vec<Vec<T>>, T::Err>>()?,
        ))
    }
}

impl<T: fmt::Display> fmt::Display for Map<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let display = self
            .tiles
            .lanes(Axis(0))
            .into_iter()
            .map(|row| row.into_iter().map(T::to_string).collect::<String>())
            .collect::<Vec<_>>()
            .join("\n");

        f.write_str(&display)
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn test_iter_positions() {
        let map = super::Map::from_vec2(&vec![vec![0, 1, 2], vec![3, 4, 5]]);
        println!("{}", map);
        for pos in map.iter_positions() {
            println!("{:?}: {}", pos, map.at(pos));
        }
    }

    #[test]
    fn test_iter_neighbors() {
        let map = super::Map::from_vec2(&vec![vec![0, 1, 2], vec![3, 4, 5]]);
        println!("{}", map);

        let tiles = map
            .iter_neighbors_fn(|a, b| {
                println!("{:?}: {:?}", a, b);
                a
            })
            .collect::<Vec<_>>();

        assert!(tiles == vec![0, 1, 2, 3, 4, 5]);
    }
}
