use super::errors::AocError;
use nom::{character::complete::anychar, combinator::map_res, IResult};
use num_traits::Num;
use std::ops::Neg;

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Turn {
    Cw,
    Ccw,
}

impl Turn {
    #[must_use]
    pub const fn invert(&self) -> Self {
        match self {
            Self::Cw => Self::Ccw,
            Self::Ccw => Self::Cw,
        }
    }

    pub fn from_lr(c: &char) -> Result<Self, AocError> {
        match c {
            'R' | 'r' => Ok(Self::Cw),
            'L' | 'l' => Ok(Self::Ccw),
            _ => Err(AocError::ParseError(format!("Cannot parse Turn '{c}'"))),
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash, PartialOrd, Ord)]
pub enum Direction {
    Up,
    Right,
    Down,
    Left,
}

impl Direction {
    #[must_use]
    pub fn as_diff<T: Num + Neg<Output = T>>(&self) -> (T, T) {
        match self {
            Self::Up => (T::zero(), -T::one()),
            Self::Down => (T::zero(), T::one()),
            Self::Left => (-T::one(), T::zero()),
            Self::Right => (T::one(), T::zero()),
        }
    }

    #[must_use]
    pub const fn flip(&self) -> Self {
        match self {
            Self::Up => Self::Down,
            Self::Down => Self::Up,
            Self::Left => Self::Right,
            Self::Right => Self::Left,
        }
    }

    #[must_use]
    pub const fn rotate(&self, turn: &Turn) -> Self {
        match turn {
            Turn::Cw => self.rotate_cw(),
            Turn::Ccw => self.rotate_ccw(),
        }
    }

    #[must_use]
    pub const fn rotate_cw(&self) -> Self {
        match self {
            Self::Up => Self::Right,
            Self::Right => Self::Down,
            Self::Down => Self::Left,
            Self::Left => Self::Up,
        }
    }

    #[must_use]
    pub const fn rotate_ccw(&self) -> Self {
        match self {
            Self::Up => Self::Left,
            Self::Left => Self::Down,
            Self::Down => Self::Right,
            Self::Right => Self::Up,
        }
    }

    pub fn try_from_arrow(b: &u8) -> Result<Self, AocError> {
        match b {
            b'^' => Ok(Self::Up),
            b'>' => Ok(Self::Right),
            b'v' => Ok(Self::Down),
            b'<' => Ok(Self::Left),
            _ => Err(AocError::ParseError(format!("Invalid direction: {b}"))),
        }
    }

    pub fn from_arrow(c: &char) -> Result<Self, AocError> {
        match c {
            '^' => Ok(Self::Up),
            '>' => Ok(Self::Right),
            'v' => Ok(Self::Down),
            '<' => Ok(Self::Left),
            _ => Err(AocError::ParseError(format!("Invalid direction: {c}"))),
        }
    }

    pub fn parse_from_arrow(s: &str) -> IResult<&str, Self> {
        map_res(anychar, |c| Self::from_arrow(&c))(s)
    }

    pub fn parse_from_initial(s: &str) -> IResult<&str, Self> {
        map_res(anychar, |c| match c {
            'U' => Ok(Self::Up),
            'R' => Ok(Self::Right),
            'D' => Ok(Self::Down),
            'L' => Ok(Self::Left),
            _ => Err(AocError::ParseError(format!("Invalid direction: {c}"))),
        })(s)
    }
}
