use super::direction::{Direction, Turn};

use num::{Num, Signed};
use std::{
    cmp::Ordering,
    ops::{Add, AddAssign},
};

pub type Point<T> = (T, T);

pub trait PointOps<T: Num> {
    fn default() -> Self;

    fn x(&self) -> T;
    fn y(&self) -> T;

    fn set_x(&mut self, x: T);
    fn set_y(&mut self, y: T);

    fn move_by(&mut self, amount: T, direction: &Direction);

    #[must_use]
    fn moved(&self, amount: T, direction: &Direction) -> Self;

    #[must_use]
    fn flip(&self) -> Self;

    fn cmp_reading(&self, b: &Self) -> Ordering
    where
        T: Ord;

    fn manhattan_distance(&self, b: &Self) -> T
    where
        T: Ord;

    fn neighbors(&self) -> [Self; 4]
    where
        Self: Sized;

    fn all_neighbors(&self) -> [Self; 8]
    where
        Self: Sized;

    fn cardinal_neighbors_checked(&self, max_x: T, max_y: T) -> Vec<Self>
    where
        Self: Sized,
        T: Ord;

    fn all_neighbors_checked(&self, max_x: T, max_y: T) -> Vec<Self>
    where
        Self: Sized,
        T: Ord;

    fn rotate(&mut self, turn: &Turn)
    where
        T: Signed;

    fn add_x(&mut self, x: T)
    where
        T: AddAssign;

    fn add_y(&mut self, y: T)
    where
        T: AddAssign;

    #[must_use]
    fn add(&self, other: &Self) -> Self
    where
        T: Add;
}

impl<T: Copy + Num> PointOps<T> for Point<T> {
    fn default() -> Self {
        (T::zero(), T::zero())
    }

    fn x(&self) -> T {
        self.0
    }

    fn y(&self) -> T {
        self.1
    }

    fn set_x(&mut self, x: T) {
        self.0 = x;
    }

    fn set_y(&mut self, y: T) {
        self.1 = y;
    }

    fn moved(&self, amount: T, direction: &Direction) -> Self {
        let mut copy = *self;
        copy.move_by(amount, direction);

        copy
    }

    fn move_by(&mut self, amount: T, direction: &Direction) {
        match direction {
            Direction::Up => self.set_y(self.y() - amount),
            Direction::Right => self.set_x(self.x() + amount),
            Direction::Down => self.set_y(self.y() + amount),
            Direction::Left => self.set_x(self.x() - amount),
        }
    }

    fn flip(&self) -> Self {
        (self.y(), self.x())
    }

    fn neighbors(&self) -> [Self; 4] {
        [
            (self.x() - T::one(), self.y()),
            (self.x() + T::one(), self.y()),
            (self.x(), self.y() - T::one()),
            (self.x(), self.y() + T::one()),
        ]
    }

    fn all_neighbors(&self) -> [Self; 8] {
        [
            (self.x() - T::one(), self.y() - T::one()),
            (self.x() - T::one(), self.y()),
            (self.x() - T::one(), self.y() + T::one()),
            (self.x(), self.y() - T::one()),
            (self.x(), self.y() + T::one()),
            (self.x() + T::one(), self.y() - T::one()),
            (self.x() + T::one(), self.y()),
            (self.x() + T::one(), self.y() + T::one()),
        ]
    }

    fn cardinal_neighbors_checked(&self, max_x: T, max_y: T) -> Vec<Self>
    where
        T: Ord,
    {
        let mut res = Vec::with_capacity(4);

        if self.x() > T::zero() {
            res.push((self.x() - T::one(), self.y()));
        }
        if self.x() < max_x {
            res.push((self.x() + T::one(), self.y()));
        }
        if self.y() > T::zero() {
            res.push((self.x(), self.y() - T::one()));
        }
        if self.y() < max_y {
            res.push((self.x(), self.y() + T::one()));
        }

        res
    }

    fn all_neighbors_checked(&self, max_x: T, max_y: T) -> Vec<Self>
    where
        T: Ord,
    {
        let mut res = Vec::with_capacity(8);
        res.extend(self.cardinal_neighbors_checked(max_x, max_y));

        if self.x() > T::zero() && self.y() > T::zero() {
            res.push((self.x() - T::one(), self.y() - T::one()));
        }
        if self.x() < max_x && self.y() > T::zero() {
            res.push((self.x() + T::one(), self.y() - T::one()));
        }
        if self.x() > T::zero() && self.y() < max_y {
            res.push((self.x() - T::one(), self.y() + T::one()));
        }
        if self.x() < max_x && self.y() < max_y {
            res.push((self.x() + T::one(), self.y() + T::one()));
        }

        res
    }

    fn cmp_reading(&self, b: &Self) -> Ordering
    where
        T: Ord,
    {
        self.x().cmp(&b.x()).then(self.y().cmp(&b.y()))
    }

    fn manhattan_distance(&self, b: &Self) -> T
    where
        T: Ord,
    {
        let (x1, x2) = if self.x() >= b.x() {
            (self.x(), b.x())
        } else {
            (b.x(), self.x())
        };
        let (y1, y2) = if self.y() >= b.y() {
            (self.y(), b.y())
        } else {
            (b.y(), self.y())
        };

        (x1 - x2) + (y1 - y2)
    }

    fn rotate(&mut self, turn: &Turn)
    where
        T: Signed,
    {
        let orig_x = self.x();
        let orig_y = self.y();

        match turn {
            Turn::Cw => {
                self.set_x(orig_y);
                self.set_y(orig_x.neg());
            }
            Turn::Ccw => {
                self.set_x(orig_y.neg());
                self.set_y(orig_x);
            }
        }
    }

    fn add_x(&mut self, x: T)
    where
        T: AddAssign,
    {
        self.0 += x;
    }

    fn add_y(&mut self, y: T)
    where
        T: AddAssign,
    {
        self.1 += y;
    }

    fn add(&self, other: &Self) -> Self
    where
        T: Add,
    {
        (self.x() + other.x(), self.y() + other.y())
    }
}

pub fn ray_iter<T: Copy + Num + Ord>(
    from: &Point<T>,
    to: &Point<T>,
) -> impl Iterator<Item = Point<T>> {
    Rayterator::new(*from, *to)
}

struct Rayterator<T> {
    pos: (T, T),
    dest: (T, T),
    dir: (bool, bool),
}

impl<T: Copy + Ord> Rayterator<T> {
    pub fn new(pos: (T, T), dest: (T, T)) -> Self {
        Self {
            pos,
            dest,
            dir: (pos.0 < dest.0, pos.1 < dest.1),
        }
    }
}

impl<T: Copy + Num> Iterator for Rayterator<T> {
    type Item = (T, T);

    fn next(&mut self) -> Option<Self::Item> {
        if self.pos == self.dest {
            return None;
        }

        if self.pos.0 != self.dest.0 {
            if self.dir.0 {
                self.pos.0 = self.pos.0 + T::one();
            } else {
                self.pos.0 = self.pos.0 - T::one();
            }
        }

        if self.pos.1 != self.dest.1 {
            if self.dir.1 {
                self.pos.1 = self.pos.1 + T::one();
            } else {
                self.pos.1 = self.pos.1 - T::one();
            }
        }

        Some(self.pos)
    }
}

#[cfg(test)]
mod tests {
    use super::PointOps;

    #[test]
    fn test_ray_iter_0() {
        let point = (2, 3);
        println!("{:?}", point);

        for pos in super::ray_iter(&point, &(0, 0)) {
            println!("{:?}", pos);
        }
    }

    #[test]
    fn test_ray_iter_1() {
        let point = (0, 0);
        println!("{:?}", point);

        for pos in super::ray_iter(&point, &(5, 0)) {
            println!("{:?}", pos);
        }
    }

    #[test]
    fn test_all_neighbors() {
        let point = (1, 1);

        assert_eq!(
            point.all_neighbors(),
            vec![
                (0, 0),
                (0, 1),
                (0, 2),
                (1, 0),
                (1, 2),
                (2, 0),
                (2, 1),
                (2, 2),
            ]
            .as_slice()
        )
    }
}
