pub trait FromChar: Sized {
    type Err;

    fn from_char(c: &char) -> Result<Self, Self::Err>;
}

pub trait FromAsciiDigit {
    fn from_ascii_digit(digit: u8) -> Self;
}

impl<T: From<u8>> FromAsciiDigit for T {
    fn from_ascii_digit(digit: u8) -> Self {
        Self::from(digit - b'0')
    }
}
