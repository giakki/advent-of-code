use ndarray::ShapeError;
use std::fmt;
use std::num::ParseIntError;
use std::str::Utf8Error;
use std::{array::TryFromSliceError, error::Error};

#[derive(Debug)]
pub enum AocError {
    Generic(String),
    Input,
    NoSolution,
    ParseError(String),
    Unspecified,
}

impl AocError {
    #[must_use]
    pub fn new_generic(s: &str) -> Self {
        Self::Generic(s.to_owned())
    }

    #[must_use]
    pub fn new_parse(s: &str) -> Self {
        Self::ParseError(s.to_owned())
    }
}

impl fmt::Display for AocError {
    #[allow(clippy::use_debug)]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{self:?}")
    }
}

impl Error for AocError {}

impl From<&str> for AocError {
    fn from(err: &str) -> Self {
        Self::Generic(err.to_owned())
    }
}

impl From<ParseIntError> for AocError {
    fn from(err: ParseIntError) -> Self {
        Self::ParseError(err.to_string())
    }
}

impl From<ShapeError> for AocError {
    fn from(err: ShapeError) -> Self {
        Self::Generic(err.to_string())
    }
}

impl From<Utf8Error> for AocError {
    fn from(err: Utf8Error) -> Self {
        Self::ParseError(err.to_string())
    }
}

impl From<TryFromSliceError> for AocError {
    fn from(err: TryFromSliceError) -> Self {
        Self::Generic(err.to_string())
    }
}

impl<'a> From<nom::Err<nom::error::Error<&'a str>>> for AocError {
    fn from(err: nom::Err<nom::error::Error<&'a str>>) -> Self {
        Self::ParseError(err.to_string())
    }
}
