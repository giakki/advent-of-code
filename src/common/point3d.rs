use std::ops::AddAssign;

use num::Num;

pub type Point3D<T> = (T, T, T);

pub trait Point3dOps<T> {
    fn default() -> Self;

    fn x(&self) -> T;
    fn y(&self) -> T;
    fn z(&self) -> T;

    fn is_inside(&self, min: &Self, max: &Self) -> bool
    where
        T: Ord;

    fn neighbors(&self) -> [Self; 6]
    where
        Self: Sized;
}

impl<T: Copy + Num + AddAssign> Point3dOps<T> for Point3D<T> {
    fn default() -> Self {
        (T::zero(), T::zero(), T::zero())
    }

    fn x(&self) -> T {
        self.0
    }

    fn y(&self) -> T {
        self.1
    }

    fn z(&self) -> T {
        self.2
    }

    fn is_inside(&self, min: &Self, max: &Self) -> bool
    where
        T: Ord,
    {
        self.x() >= min.x()
            && self.x() <= max.x()
            && self.y() >= min.y()
            && self.y() <= max.y()
            && self.z() >= min.z()
            && self.z() <= max.z()
    }

    fn neighbors(&self) -> [Self; 6] {
        [
            (self.x() - T::one(), self.y(), self.z()),
            (self.x() + T::one(), self.y(), self.z()),
            (self.x(), self.y() + T::one(), self.z()),
            (self.x(), self.y() - T::one(), self.z()),
            (self.x(), self.y(), self.z() + T::one()),
            (self.x(), self.y(), self.z() - T::one()),
        ]
    }
}
