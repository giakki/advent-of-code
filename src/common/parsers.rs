use super::{errors::AocError, point::Point, point3d::Point3D};
use nom::{
    branch::alt,
    character::complete::{char, digit1},
    combinator::{map_res, opt, recognize},
    error::ParseError,
    multi::separated_list0,
    sequence::{preceded, tuple},
    IResult, InputIter, InputLength, Parser, Slice,
};
use num::Num;
use std::{ops::RangeFrom, str::FromStr};

pub fn input_parser<'a, O, F>(mut parser: F) -> impl FnMut(&'a str) -> Result<O, AocError>
where
    F: Parser<&'a str, O, nom::error::Error<&'a str>>,
{
    move |s| {
        let (remaining, parsed) = parser.parse(s)?;
        if remaining.is_empty() {
            Ok(parsed)
        } else {
            Err(AocError::ParseError(format!(
                "Incomplete parsing: `{remaining}`"
            )))
        }
    }
}

pub fn parse_int<T: FromStr>(s: &str) -> IResult<&str, T> {
    map_res(digit1, FromStr::from_str)(s)
}

pub fn parse_binary_u32(s: &str) -> IResult<&str, u32> {
    map_res(digit1, |src| u32::from_str_radix(src, 2))(s)
}

pub fn parse_signed_int<T: FromStr>(s: &str) -> IResult<&str, T> {
    map_res(
        recognize(tuple((opt(alt((char('+'), char('-')))), digit1))),
        FromStr::from_str,
    )(s)
}

pub fn parse_point_comma<T: Copy + FromStr + Num + Ord>(s: &str) -> IResult<&str, Point<T>> {
    tuple((parse_int, preceded(char(','), parse_int)))(s)
}

pub fn parse_point3d_comma<T: Copy + FromStr + Num + Ord>(s: &str) -> IResult<&str, Point3D<T>> {
    tuple((
        parse_int,
        preceded(char(','), parse_int),
        preceded(char(','), parse_int),
    ))(s)
}

pub fn by_lines<I, O, E, F>(f: F) -> impl FnMut(I) -> IResult<I, Vec<O>, E>
where
    E: ParseError<I>,
    I: InputIter + Clone + InputLength + Slice<RangeFrom<usize>>,
    <I as nom::InputIter>::Item: nom::AsChar,
    F: Parser<I, O, E>,
{
    separated_list0(char('\n'), f)
}

#[cfg(test)]
mod tests {

    #[test]
    fn test_parse_signed_int() {
        let strings = ["1", "-1", "+1"];
        let parsed = strings
            .iter()
            .map(|s| super::input_parser(super::parse_signed_int)(s))
            .collect::<Result<Vec<i32>, _>>()
            .unwrap();

        assert_eq!(parsed, vec![1, -1, 1]);
    }
}
