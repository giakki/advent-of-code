use super::JsonIterator;
use std::collections::HashMap;

#[derive(Debug)]
pub enum Json<'a> {
    Null,
    Boolean(bool),
    Number(f64),
    String(&'a str),
    Array(Vec<Json<'a>>),
    Object(HashMap<&'a str, Json<'a>>),
}

impl<'a> Json<'a> {
    #[must_use]
    pub fn iter(&'a self) -> JsonIterator<'a> {
        JsonIterator::new(self)
    }
}
