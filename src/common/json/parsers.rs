use super::Json;
use nom::{
    branch::alt,
    bytes::complete::{escaped, tag},
    character::complete::{char, none_of, one_of, space0},
    combinator::{cut, map, value},
    error::ParseError,
    multi::separated_list0,
    number::complete::double,
    sequence::{delimited, preceded, separated_pair, terminated},
    IResult, Parser,
};
use std::collections::HashMap;

fn wrapped<'a, F, O, E>(fun: F) -> impl FnMut(&'a str) -> IResult<&'a str, O, E>
where
    E: ParseError<&'a str>,
    F: Parser<&'a str, O, E>,
{
    delimited(space0, fun, space0)
}

fn string0(i: &str) -> IResult<&str, &str> {
    escaped(none_of(r#""\"#), '\\', one_of(r#""n\"#))(i)
}

fn parse_json_array(s: &str) -> IResult<&str, Vec<Json>> {
    preceded(
        char('['),
        cut(terminated(
            separated_list0(char(','), parse_json),
            char(']'),
        )),
    )(s)
}

fn parse_json_boolean(s: &str) -> IResult<&str, bool> {
    alt((value(true, tag("true")), value(false, tag("false"))))(s)
}

fn parse_json_null(s: &str) -> IResult<&str, ()> {
    value((), tag("null"))(s)
}

fn parse_json_number(s: &str) -> IResult<&str, f64> {
    double(s)
}

fn kvp(s: &str) -> IResult<&str, (&str, Json)> {
    separated_pair(wrapped(parse_json_string), cut(char(':')), parse_json)(s)
}

fn parse_json_object(s: &str) -> IResult<&str, HashMap<&str, Json>> {
    preceded(
        char('{'),
        cut(wrapped(terminated(
            map(separated_list0(char(','), kvp), |tuple_vec| {
                tuple_vec.into_iter().collect()
            }),
            char('}'),
        ))),
    )(s)
}

fn parse_json_string(s: &str) -> IResult<&str, &str> {
    preceded(char('"'), cut(terminated(string0, char('"'))))(s)
}

fn parse_json_value(s: &str) -> IResult<&str, Json<'_>> {
    alt((
        map(parse_json_array, Json::Array),
        map(parse_json_boolean, Json::Boolean),
        map(parse_json_null, |()| Json::Null),
        map(parse_json_number, Json::Number),
        map(parse_json_object, Json::Object),
        map(parse_json_string, Json::String),
    ))(s)
}

pub fn parse_json(s: &str) -> IResult<&str, Json> {
    wrapped(parse_json_value)(s)
}

#[cfg(test)]
mod tests {

    #[test]
    fn test_parse_string() {
        println!("{:?}", super::parse_json("\"😋😋😋\n\\\"\""))
    }

    #[test]
    fn test_parse_string_escaped() {
        println!("{:?}", super::parse_json(r#"{"string":1}"#))
    }

    #[test]
    fn test_parse_json() {
        println!(
            "{:?}",
            super::parse_json(r#" { "root" : [ 1 , { "child": 3 } ] } "#)
        )
    }
}
