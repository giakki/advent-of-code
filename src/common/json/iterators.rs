use super::Json;

trait RecursiveIterator<'a> {
    fn recurse(&mut self, json: &'a Json<'a>);
}

pub struct JsonIterator<'a> {
    stack: Vec<&'a Json<'a>>,
}

impl<'a> JsonIterator<'a> {
    #[must_use]
    pub fn new(json: &'a Json) -> Self {
        Self { stack: vec![json] }
    }

    #[must_use]
    pub fn recurse_if<F>(&self, pred: F) -> JsonFilteredIterator<'a>
    where
        F: 'static + FnMut(&'a Json) -> bool,
    {
        JsonFilteredIterator {
            stack: self.stack.clone(),
            filterer: Box::new(pred),
        }
    }
}

impl<'a> RecursiveIterator<'a> for JsonIterator<'a> {
    fn recurse(&mut self, json: &'a Json) {
        if let Json::Array(arr) = json {
            self.stack.extend(arr);
        } else if let Json::Object(map) = json {
            self.stack.extend(map.values());
        } else {
            // noop
        }
    }
}

impl<'a> Iterator for JsonIterator<'a> {
    type Item = &'a Json<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        self.stack.pop().map(|value| {
            self.recurse(value);

            value
        })
    }
}

pub struct JsonFilteredIterator<'a> {
    stack: Vec<&'a Json<'a>>,
    filterer: Box<dyn FnMut(&'a Json) -> bool>,
}

impl<'a> RecursiveIterator<'a> for JsonFilteredIterator<'a> {
    fn recurse(&mut self, json: &'a Json) {
        if let Json::Array(arr) = json {
            if (self.filterer)(json) {
                self.stack.extend(arr);
            }
        } else if let Json::Object(map) = json {
            if (self.filterer)(json) {
                self.stack.extend(map.values());
            }
        } else {
            // noop
        }
    }
}

impl<'a> Iterator for JsonFilteredIterator<'a> {
    type Item = &'a Json<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        self.stack.pop().map(|value| {
            self.recurse(value);

            value
        })
    }
}
