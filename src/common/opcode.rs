#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum OpCode {
    Addr,
    Addi,
    Banr,
    Bani,
    Borr,
    Bori,
    Eqir,
    Eqri,
    Eqrr,
    Gtir,
    Gtri,
    Gtrr,
    Mulr,
    Muli,
    Setr,
    Seti,
    Undefined,
}

impl From<&str> for OpCode {
    fn from(other: &str) -> Self {
        match other.as_bytes() {
            b"addi" => Self::Addi,
            b"addr" => Self::Addr,
            b"bani" => Self::Bani,
            b"banr" => Self::Banr,
            b"bori" => Self::Bori,
            b"borr" => Self::Borr,
            b"eqir" => Self::Eqir,
            b"eqri" => Self::Eqri,
            b"eqrr" => Self::Eqrr,
            b"gtir" => Self::Gtir,
            b"gtri" => Self::Gtri,
            b"gtrr" => Self::Gtrr,
            b"muli" => Self::Muli,
            b"mulr" => Self::Mulr,
            b"seti" => Self::Seti,
            b"setr" => Self::Setr,
            _ => Self::Undefined,
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
pub struct Instruction {
    pub opcode: OpCode,
    pub a: usize,
    pub b: usize,
    pub c: usize,
}

impl Instruction {
    pub fn run(&self, reg: &mut [usize]) {
        let a = self.a;
        let b = self.b;
        let c = self.c;

        reg[c] = match self.opcode {
            OpCode::Addi => reg[a] + b,
            OpCode::Addr => reg[a] + reg[b],
            OpCode::Bani => reg[a] & b,
            OpCode::Banr => reg[a] & reg[b],
            OpCode::Bori => reg[a] | b,
            OpCode::Borr => reg[a] | reg[b],
            OpCode::Eqir => usize::from(reg[b] == a),
            OpCode::Eqri => usize::from(reg[a] == b),
            OpCode::Eqrr => usize::from(reg[a] == reg[b]),
            OpCode::Gtir => usize::from(a > reg[b]),
            OpCode::Gtri => usize::from(b < reg[a]),
            OpCode::Gtrr => usize::from(reg[a] > reg[b]),
            OpCode::Muli => reg[a] * b,
            OpCode::Mulr => reg[a] * reg[b],
            OpCode::Seti => a,
            OpCode::Setr => reg[a],
            OpCode::Undefined => panic!("Undefined instruction."),
        };
    }
}
