/** Correct results:
 * - 900
 * - 1216
 */
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use itertools::Itertools;
use nom::{
    bytes::complete::tag,
    character::complete::char,
    sequence::{delimited, preceded, tuple},
};
use std::fmt::Debug;

const MAGIC_MISSILE_COST: i32 = 53;
const DRAIN_COST: i32 = 73;
const SHIELD_COST: i32 = 113;
const POISON_COST: i32 = 173;
const RECHARGE_COST: i32 = 229;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Spell {
    MagicMissile,
    Drain,
    Shield,
    Poison,
    Recharge,
}

#[derive(Clone)]
struct Game {
    player_hp: i32,
    player_mana: i32,
    mana_spent: i32,
    boss_hp: i32,
    boss_attack: i32,
    active_spells: Vec<(Spell, u32)>,
    is_hard_mode: bool,
}

impl Debug for Game {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "- Player has {} hit points, {} armor, {} mana\n",
            self.player_hp,
            self.player_armor(),
            self.player_mana
        ))?;

        f.write_fmt(format_args!("- Boss has {} hit points\n", self.boss_hp))?;

        f.write_fmt(format_args!(
            "- active spells: {:?}\n\n",
            self.active_spells
        ))
    }
}

impl Game {
    pub const fn from_input(boss_hp: i32, boss_attack: i32, is_hard_mode: bool) -> Self {
        Self {
            player_hp: 50,
            player_mana: 500,
            mana_spent: 0,
            boss_hp,
            boss_attack,
            active_spells: Vec::new(),
            is_hard_mode,
        }
    }

    pub fn autoplay(mut self) -> Option<i32> {
        if self.is_hard_mode {
            self.player_hp -= 1;
        }
        if let Some(result) = self.is_game_over().or_else(|| self.tick_spell()) {
            if result {
                return Some(self.mana_spent);
            }
            return None;
        }

        let mut min_mana_spent = None;

        for spell in self.castable_spells() {
            let mut next_state = self.clone();

            if let Some(result) = next_state
                .cast_spell(spell)
                .or_else(|| next_state.boss_turn())
            {
                if result {
                    return Some(next_state.mana_spent);
                }
                return None;
            }

            if let Some(subgame_mana_spent) = next_state.autoplay() {
                min_mana_spent = min_mana_spent.map_or(Some(subgame_mana_spent), |mana| {
                    Some(i32::min(mana, subgame_mana_spent))
                });
            }
        }

        min_mana_spent
    }

    pub fn can_cast(&self, spell: Spell) -> bool {
        match spell {
            Spell::MagicMissile => self.player_mana >= MAGIC_MISSILE_COST,
            Spell::Drain => self.player_mana >= DRAIN_COST,
            Spell::Shield => self.player_mana >= SHIELD_COST && !self.has_active_spell(spell),
            Spell::Poison => self.player_mana >= POISON_COST && !self.has_active_spell(spell),
            Spell::Recharge => self.player_mana >= RECHARGE_COST && !self.has_active_spell(spell),
        }
    }

    const fn is_game_over(&self) -> Option<bool> {
        if self.boss_hp <= 0 {
            Some(true)
        } else if self.player_hp <= 0 {
            Some(false)
        } else {
            None
        }
    }

    fn castable_spells(&self) -> impl Iterator<Item = Spell> + '_ {
        [
            Spell::MagicMissile,
            Spell::Drain,
            Spell::Shield,
            Spell::Poison,
            Spell::Recharge,
        ]
        .into_iter()
        .filter(|s| self.can_cast(*s))
    }

    fn has_active_spell(&self, spell: Spell) -> bool {
        self.active_spells.iter().map(|a| a.0).contains(&spell)
    }

    fn player_armor(&self) -> i32 {
        self.active_spells
            .iter()
            .find(|(s, _)| *s == Spell::Shield)
            .map_or(0, |_| 7)
    }

    fn cast_spell(&mut self, spell: Spell) -> Option<bool> {
        match spell {
            Spell::MagicMissile => {
                self.player_mana -= MAGIC_MISSILE_COST;
                self.mana_spent += MAGIC_MISSILE_COST;
                self.boss_hp -= 4;
            }
            Spell::Drain => {
                self.player_mana -= DRAIN_COST;
                self.mana_spent += DRAIN_COST;
                self.boss_hp -= 2;
                self.player_hp += 2;
            }
            Spell::Shield => {
                self.player_mana -= SHIELD_COST;
                self.mana_spent += SHIELD_COST;
                self.active_spells.push((spell, 6));
            }
            Spell::Poison => {
                self.player_mana -= POISON_COST;
                self.mana_spent += POISON_COST;
                self.active_spells.push((spell, 6));
            }
            Spell::Recharge => {
                self.player_mana -= RECHARGE_COST;
                self.mana_spent += RECHARGE_COST;
                self.active_spells.push((spell, 5));
            }
        }

        self.is_game_over()
    }

    fn boss_turn(&mut self) -> Option<bool> {
        self.tick_spell();

        if let Some(winner) = self.is_game_over() {
            return Some(winner);
        }

        self.player_hp -= i32::max(self.boss_attack - self.player_armor(), 1);

        self.is_game_over()
    }

    fn tick_spell(&mut self) -> Option<bool> {
        self.active_spells.retain_mut(|(spell, duration)| {
            match *spell {
                Spell::Poison => {
                    self.boss_hp -= 3;
                }
                Spell::Recharge => {
                    self.player_mana += 101;
                }
                _ => {}
            }
            *duration -= 1;
            *duration != 0
        });

        self.is_game_over()
    }
}

#[aoc_generator(day22)]
fn generator(input: &str) -> Result<(i32, i32), AocError> {
    input_parser(tuple((
        delimited(tag("Hit Points: "), parse_int, char('\n')),
        preceded(tag("Damage: "), parse_int),
    )))(input)
}

#[aoc(day22, part1)]
fn part01(input: &(i32, i32)) -> Option<i32> {
    Game::from_input(input.0, input.1, false).autoplay()
}

#[aoc(day22, part2)]
fn part02(input: &(i32, i32)) -> Option<i32> {
    Game::from_input(input.0, input.1, true).autoplay()
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"Hit Points: 51
Damage: 9";

    #[test]
    fn test00_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test_game_1() {
        let mut game = Game::from_input(13, 8, false);

        println!("-- Player turn --\n{:?}", game);
        assert!(game.can_cast(Spell::Poison));
        game.cast_spell(Spell::Poison);

        println!("-- Boss turn --\n{:?}", game);
        assert_eq!(game.boss_turn(), None);

        println!("-- Player turn --\n{:?}", game);
        assert!(game.can_cast(Spell::MagicMissile));
        game.cast_spell(Spell::MagicMissile);

        println!("-- Boss turn --\n{:?}", game);
        assert_eq!(game.boss_turn(), Some(true));
    }
}
