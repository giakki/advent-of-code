/** Correct results:
 * - 141
 * - 736
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use nom::{
    bytes::complete::tag, character::complete::alpha1, combinator::map, sequence::separated_pair,
    IResult,
};
use petgraph::prelude::NodeIndex;
use petgraph::visit::EdgeRef;
use petgraph::visit::IntoNodeReferences;
use petgraph::{Graph, Undirected};

struct TravelDistance {
    from: String,
    to: String,
    distance: usize,
}

impl TravelDistance {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        map(
            separated_pair(
                separated_pair(alpha1, tag(" to "), alpha1),
                tag(" = "),
                parse_int,
            ),
            |((from, to), distance)| Self {
                from: from.to_owned(),
                to: to.to_owned(),
                distance,
            },
        )(s)
    }
}

fn walk_rec<N, Ty: petgraph::EdgeType, Ix: petgraph::csr::IndexType>(
    next: NodeIndex<Ix>,
    target: NodeIndex<Ix>,
    len: usize,
    cost: usize,
    path: &[NodeIndex<Ix>],
    graph: &Graph<N, usize, Ty, Ix>,
) -> Vec<usize> {
    let mut solutions = Vec::new();

    for edge in graph.edges(next) {
        let neighbor = edge.target();
        if neighbor == target && path.len() == len {
            solutions.push(cost);
        } else if !path.contains(&neighbor) {
            let neighbor_cost = edge.weight() + cost;
            let mut new_path = path.to_owned();
            new_path.push(neighbor);
            let solutions_for_neighbor =
                walk_rec(neighbor, target, len, neighbor_cost, &new_path, graph);
            solutions.extend(solutions_for_neighbor);
        }
    }

    solutions
}

fn find_all_solutions<N, Ty: petgraph::EdgeType, Ix: petgraph::csr::IndexType>(
    cities: &[&String],
    graph: &Graph<N, usize, Ty, Ix>,
) -> Vec<usize> {
    let mut solutions = Vec::new();
    for (index, (start, _)) in graph.node_references().enumerate() {
        for (end, _) in graph.node_references().skip(index + 1) {
            if start != end {
                let tmp_solutions = walk_rec(start, end, cities.len(), 0, &[start], graph);
                solutions.extend(tmp_solutions);
            }
        }
    }

    solutions
}

fn build_graph(
    travel_distances: &[TravelDistance],
) -> (Vec<&String>, Graph<(), usize, Undirected>) {
    let cities = {
        let (mut departures, arrivals): (Vec<_>, Vec<_>) = travel_distances
            .iter()
            .map(|trip| (&trip.from, &trip.to))
            .unzip();
        departures.extend(arrivals);
        departures.sort();
        departures.dedup();
        departures
    };

    let iter = travel_distances.iter().map(|trip| {
        (
            cities.iter().position(|x| *x == &trip.from).unwrap() as u32,
            cities.iter().position(|x| *x == &trip.to).unwrap() as u32,
            trip.distance,
        )
    });

    let graph = Graph::<(), usize, Undirected>::from_edges(iter);

    (cities, graph)
}

#[aoc_generator(day9)]
fn generator(input: &str) -> Result<Vec<TravelDistance>, AocError> {
    input_parser(by_lines(TravelDistance::parse))(input)
}

#[aoc(day9, part1)]
fn part01(input: &[TravelDistance]) -> Option<usize> {
    let (cities, graph) = build_graph(input);

    find_all_solutions(&cities, &graph).into_iter().min()
}

#[aoc(day9, part2)]
fn part02(input: &[TravelDistance]) -> Option<usize> {
    let (cities, graph) = build_graph(input);

    find_all_solutions(&cities, &graph).into_iter().max()
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r"London to Dublin = 464
London to Belfast = 518
Dublin to Belfast = 141";

    #[test]
    fn test0901() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::part01(&input);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test0902() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::part02(&input);

        println!("-- {:?} --", res)
    }
}
