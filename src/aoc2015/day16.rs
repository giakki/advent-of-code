/** Correct results:
 * - 213
 * - 323
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use nom::{
    branch::alt,
    bytes::complete::tag,
    combinator::map,
    multi::separated_list0,
    sequence::{preceded, separated_pair, tuple},
    IResult,
};

struct AuntId {
    children: Option<usize>,
    cats: Option<usize>,
    samoyeds: Option<usize>,
    pomeranians: Option<usize>,
    akitas: Option<usize>,
    vizslas: Option<usize>,
    goldfish: Option<usize>,
    trees: Option<usize>,
    cars: Option<usize>,
    perfumes: Option<usize>,
}

impl AuntId {
    const fn new() -> Self {
        Self {
            children: None,
            cats: None,
            samoyeds: None,
            pomeranians: None,
            akitas: None,
            vizslas: None,
            goldfish: None,
            trees: None,
            cars: None,
            perfumes: None,
        }
    }

    const fn matches_message_1(&self, msg: &Self) -> bool {
        #[rustfmt::skip]
        let children_matches = if let Some(children) = self.children { if let Some(msg_children) = msg.children { children == msg_children } else { true } } else { true };
        #[rustfmt::skip]
        let cats_matches = if let Some(cats) = self.cats { if let Some(msg_cats) = msg.cats { cats == msg_cats } else { true } } else { true };
        #[rustfmt::skip]
        let samoyeds_matches = if let Some(samoyeds) = self.samoyeds { if let Some(msg_samoyeds) = msg.samoyeds { samoyeds == msg_samoyeds } else { true } } else { true };
        #[rustfmt::skip]
        let pomeranians_matches = if let Some(pomeranians) = self.pomeranians { if let Some(msg_pomeranians) = msg.pomeranians { pomeranians == msg_pomeranians } else { true } } else { true };
        #[rustfmt::skip]
        let akitas_matches = if let Some(akitas) = self.akitas { if let Some(msg_akitas) = msg.akitas { akitas == msg_akitas } else { true } } else { true };
        #[rustfmt::skip]
        let vizslas_matches = if let Some(vizslas) = self.vizslas { if let Some(msg_vizslas) = msg.vizslas { vizslas == msg_vizslas } else { true } } else { true };
        #[rustfmt::skip]
        let goldfish_matches = if let Some(goldfish) = self.goldfish { if let Some(msg_goldfish) = msg.goldfish { goldfish == msg_goldfish } else { true } } else { true };
        #[rustfmt::skip]
        let trees_matches = if let Some(trees) = self.trees { if let Some(msg_trees) = msg.trees { trees == msg_trees } else { true } } else { true };
        #[rustfmt::skip]
        let cars_matches = if let Some(cars) = self.cars { if let Some(msg_cars) = msg.cars { cars == msg_cars } else { true } } else { true };
        #[rustfmt::skip]
        let perfumes_matches = if let Some(perfumes) = self.perfumes { if let Some(msg_perfumes) = msg.perfumes { perfumes == msg_perfumes } else { true } } else { true };

        children_matches
            && cats_matches
            && samoyeds_matches
            && pomeranians_matches
            && akitas_matches
            && vizslas_matches
            && goldfish_matches
            && trees_matches
            && cars_matches
            && perfumes_matches
    }

    const fn matches_message_2(&self, msg: &Self) -> bool {
        #[rustfmt::skip]
        let children_matches = if let Some(children) = self.children { if let Some(msg_children) = msg.children { children == msg_children } else { true } } else { true };
        #[rustfmt::skip]
        let cats_matches = if let Some(cats) = self.cats { if let Some(msg_cats) = msg.cats { cats > msg_cats } else { true } } else { true };
        #[rustfmt::skip]
        let samoyeds_matches = if let Some(samoyeds) = self.samoyeds { if let Some(msg_samoyeds) = msg.samoyeds { samoyeds == msg_samoyeds } else { true } } else { true };
        #[rustfmt::skip]
        let pomeranians_matches = if let Some(pomeranians) = self.pomeranians { if let Some(msg_pomeranians) = msg.pomeranians { pomeranians < msg_pomeranians } else { true } } else { true };
        #[rustfmt::skip]
        let akitas_matches = if let Some(akitas) = self.akitas { if let Some(msg_akitas) = msg.akitas { akitas == msg_akitas } else { true } } else { true };
        #[rustfmt::skip]
        let vizslas_matches = if let Some(vizslas) = self.vizslas { if let Some(msg_vizslas) = msg.vizslas { vizslas == msg_vizslas } else { true } } else { true };
        #[rustfmt::skip]
        let goldfish_matches = if let Some(goldfish) = self.goldfish { if let Some(msg_goldfish) = msg.goldfish { goldfish < msg_goldfish } else { true } } else { true };
        #[rustfmt::skip]
        let trees_matches = if let Some(trees) = self.trees { if let Some(msg_trees) = msg.trees { trees > msg_trees } else { true } } else { true };
        #[rustfmt::skip]
        let cars_matches = if let Some(cars) = self.cars { if let Some(msg_cars) = msg.cars { cars == msg_cars } else { true } } else { true };
        #[rustfmt::skip]
        let perfumes_matches = if let Some(perfumes) = self.perfumes { if let Some(msg_perfumes) = msg.perfumes { perfumes == msg_perfumes } else { true } } else { true };

        children_matches
            && cats_matches
            && samoyeds_matches
            && pomeranians_matches
            && akitas_matches
            && vizslas_matches
            && goldfish_matches
            && trees_matches
            && cars_matches
            && perfumes_matches
    }
}

fn parse_pet(s: &str) -> IResult<&str, (&str, usize)> {
    alt((
        separated_pair(tag("children"), tag(": "), parse_int),
        separated_pair(tag("cats"), tag(": "), parse_int),
        separated_pair(tag("samoyeds"), tag(": "), parse_int),
        separated_pair(tag("pomeranians"), tag(": "), parse_int),
        separated_pair(tag("akitas"), tag(": "), parse_int),
        separated_pair(tag("vizslas"), tag(": "), parse_int),
        separated_pair(tag("goldfish"), tag(": "), parse_int),
        separated_pair(tag("trees"), tag(": "), parse_int),
        separated_pair(tag("cars"), tag(": "), parse_int),
        separated_pair(tag("perfumes"), tag(": "), parse_int),
    ))(s)
}

fn parse_aunt_pets(s: &str) -> IResult<&str, AuntId> {
    // Sue 1: children: 1, cars: 8, vizslas: 7
    map(
        preceded(
            tuple((tag("Sue "), parse_int::<usize>, tag(": "))),
            separated_list0(tag(", "), parse_pet),
        ),
        |tags| {
            tags.into_iter()
                .fold(AuntId::new(), |mut id, (tag, amount)| {
                    match tag {
                        "children" => id.children = Some(amount),
                        "cats" => id.cats = Some(amount),
                        "samoyeds" => id.samoyeds = Some(amount),
                        "pomeranians" => id.pomeranians = Some(amount),
                        "akitas" => id.akitas = Some(amount),
                        "vizslas" => id.vizslas = Some(amount),
                        "goldfish" => id.goldfish = Some(amount),
                        "trees" => id.trees = Some(amount),
                        "cars" => id.cars = Some(amount),
                        "perfumes" => id.perfumes = Some(amount),
                        _ => println!("Warning: Unrecognized input: {tag}"),
                    }
                    id
                })
        },
    )(s)
}

#[aoc_generator(day16)]
fn generator(input: &str) -> Result<Vec<AuntId>, AocError> {
    input_parser(by_lines(parse_aunt_pets))(input)
}

const MESSAGE: AuntId = AuntId {
    children: Some(3),
    cats: Some(7),
    samoyeds: Some(2),
    pomeranians: Some(3),
    akitas: Some(0),
    vizslas: Some(0),
    goldfish: Some(5),
    trees: Some(3),
    cars: Some(2),
    perfumes: Some(1),
};

#[aoc(day16, part1)]
fn part01(input: &[AuntId]) -> Option<usize> {
    input
        .iter()
        .position(|id| id.matches_message_1(&MESSAGE))
        .map(|i| i + 1)
}

#[aoc(day16, part2)]
fn part02(input: &[AuntId]) -> Option<usize> {
    input
        .iter()
        .position(|id| id.matches_message_2(&MESSAGE))
        .map(|i| i + 1)
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r"";

    #[test]
    fn test1601() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::part01(&input);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test1602() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::part02(&input);

        println!("-- {:?} --", res)
    }
}
