/** Correct results:
 * - 255
 * - 334
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_signed_int},
};
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::anychar,
    combinator::map_res,
    sequence::{preceded, separated_pair},
};

#[derive(Debug)]
enum Register {
    A,
    B,
}

impl TryFrom<char> for Register {
    type Error = AocError;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            'a' => Ok(Self::A),
            'b' => Ok(Self::B),
            _ => Err(AocError::ParseError(format!(
                "Cannot parse register '{value}'",
            ))),
        }
    }
}

#[derive(Debug)]
enum Instruction {
    Half(Register),
    Triple(Register),
    Increment(Register),
    Jump(i32),
    JumpIfEven(Register, i32),
    JumpIfOne(Register, i32),
}

#[aoc_generator(day23)]
fn generator(input: &str) -> Result<Vec<Instruction>, AocError> {
    input_parser(by_lines(alt((
        map_res(preceded(tag("hlf "), anychar), |reg| {
            Ok::<Instruction, AocError>(Instruction::Half(Register::try_from(reg)?))
        }),
        map_res(preceded(tag("tpl "), anychar), |reg| {
            Ok::<Instruction, AocError>(Instruction::Triple(Register::try_from(reg)?))
        }),
        map_res(preceded(tag("inc "), anychar), |reg| {
            Ok::<Instruction, AocError>(Instruction::Increment(Register::try_from(reg)?))
        }),
        map_res(preceded(tag("jmp "), parse_signed_int), |offset| {
            Ok::<Instruction, AocError>(Instruction::Jump(offset))
        }),
        map_res(
            preceded(
                tag("jie "),
                separated_pair(anychar, tag(", "), parse_signed_int),
            ),
            |(reg, offset)| {
                Ok::<Instruction, AocError>(Instruction::JumpIfEven(
                    Register::try_from(reg)?,
                    offset,
                ))
            },
        ),
        map_res(
            preceded(
                tag("jio "),
                separated_pair(anychar, tag(", "), parse_signed_int),
            ),
            |(reg, offset)| {
                Ok::<Instruction, AocError>(Instruction::JumpIfOne(
                    Register::try_from(reg)?,
                    offset,
                ))
            },
        ),
    ))))(input)
}

fn run_program(instructions: &[Instruction], a: &mut i32, b: &mut i32) {
    let mut ip = 0;
    while ip < instructions.len() {
        match &instructions[ip] {
            Instruction::Half(reg) => match reg {
                Register::A => *a /= 2,
                Register::B => *b /= 2,
            },
            Instruction::Triple(reg) => match reg {
                Register::A => *a *= 3,
                Register::B => *b *= 3,
            },
            Instruction::Increment(reg) => match reg {
                Register::A => *a += 1,
                Register::B => *b += 1,
            },
            Instruction::Jump(offset) => ip = (ip as i32 + offset - 1) as usize,
            Instruction::JumpIfEven(reg, offset) => match reg {
                Register::A => {
                    if *a % 2 == 0 {
                        ip = (ip as i32 + offset - 1) as usize;
                    }
                }
                Register::B => {
                    if *b % 2 == 0 {
                        ip = (ip as i32 + offset - 1) as usize;
                    }
                }
            },
            Instruction::JumpIfOne(reg, offset) => match reg {
                Register::A => {
                    if *a == 1 {
                        ip = (ip as i32 + offset - 1) as usize;
                    }
                }
                Register::B => {
                    if *b == 1 {
                        ip = (ip as i32 + offset - 1) as usize;
                    }
                }
            },
        }

        ip += 1;
    }
}

#[aoc(day23, part1)]
fn part01(input: &[Instruction]) -> i32 {
    let mut a = 0;
    let mut b = 0;

    run_program(input, &mut a, &mut b);

    b
}

#[aoc(day23, part2)]
fn part02(input: &[Instruction]) -> i32 {
    let mut a = 1;
    let mut b = 0;

    run_program(input, &mut a, &mut b);

    b
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"inc a
jio a, +2
tpl a
inc a";

    #[test]
    fn test23_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test2301() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 0);
    }

    #[test]
    fn test2302() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, 0);
    }
}
