/** Correct results:
* - 222870
* - 117936
*/
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_signed_int},
};
use nom::{
    bytes::complete::tag,
    character::complete::alpha1,
    combinator::map,
    sequence::{preceded, terminated, tuple},
    IResult,
};

type Ingredient = [i64; 5];

fn parse_prop<'a>(s: &'a str) -> impl FnMut(&'a str) -> IResult<&'a str, i64> {
    preceded(tag(s), parse_signed_int)
}

fn parse_line(s: &str) -> IResult<&str, Ingredient> {
    map(
        tuple((
            terminated(alpha1, tag(": ")),
            terminated(parse_prop("capacity "), tag(", ")),
            terminated(parse_prop("durability "), tag(", ")),
            terminated(parse_prop("flavor "), tag(", ")),
            terminated(parse_prop("texture "), tag(", ")),
            parse_prop("calories "),
        )),
        |(_, a, b, c, d, e)| [a, b, c, d, e],
    )(s)
}

#[aoc_generator(day15)]
fn generator(input: &str) -> Result<Vec<Ingredient>, AocError> {
    input_parser(by_lines(parse_line))(input)
}

fn recipe_score(ingredients: &[Ingredient], amounts: &[i64]) -> (i64, i64) {
    let mut score = 1;

    for property_index in 0..4 {
        let mut property_sum = 0;

        for i in 0..ingredients.len() {
            property_sum += ingredients[i][property_index] * amounts[i];
        }

        if property_sum < 0 {
            return (0, 0);
        }

        score *= property_sum;
    }

    let mut calories = 0;
    for i in 0..ingredients.len() {
        calories += ingredients[i][4] * amounts[i];
    }

    (score, calories)
}

fn find_max_score(ingredients: &[Ingredient], amounts: &[i64]) -> i64 {
    if amounts.len() == ingredients.len() {
        return recipe_score(ingredients, amounts).0;
    }

    let max_amount = 100 - amounts.iter().sum::<i64>();
    if amounts.len() == ingredients.len() - 1 {
        let mut new_amounts = amounts.to_vec();
        new_amounts.push(max_amount);
        return recipe_score(ingredients, &new_amounts).0;
    }

    let mut max_score = 0;
    for i in 0..=max_amount {
        let mut new_amounts = amounts.to_vec();
        new_amounts.push(i);

        let max = find_max_score(ingredients, &new_amounts);
        if max > max_score {
            // println!("New max: {} ({:?})", max, new_amounts);
            max_score = max;
        }
    }

    max_score
}

fn find_by_calories(
    ingredients: &[Ingredient],
    amounts: &[i64],
    required_calories: i64,
) -> Vec<i64> {
    if amounts.len() == ingredients.len() {
        let (score, cals) = recipe_score(ingredients, amounts);
        if cals == required_calories {
            return vec![score];
        }
        return Vec::with_capacity(0);
    }

    let max_amount = 100 - amounts.iter().sum::<i64>();
    if amounts.len() == ingredients.len() - 1 {
        let mut new_amounts = amounts.to_vec();
        new_amounts.push(max_amount);
        let (score, cals) = recipe_score(ingredients, &new_amounts);
        if cals == required_calories {
            return vec![score];
        }
        return Vec::with_capacity(0);
    }

    let mut valid_recipes = Vec::with_capacity(3);
    for i in 0..=max_amount {
        let mut new_amounts = amounts.to_vec();
        new_amounts.push(i);

        let valid_rec = find_by_calories(ingredients, &new_amounts, required_calories);
        valid_recipes.extend(valid_rec);
    }

    valid_recipes
}

#[aoc(day15, part1)]
fn part01(input: &[Ingredient]) -> i64 {
    find_max_score(input, &Vec::new())
}

#[aoc(day15, part2)]
fn part02(input: &[Ingredient]) -> Result<i64, AocError> {
    find_by_calories(input, &Vec::new(), 500)
        .into_iter()
        .max()
        .ok_or(AocError::NoSolution)
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r"Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3";

    #[test]
    fn test15_gen() {
        let input = super::generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test15_max_score_1() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::find_max_score(&input, &[44, 56]);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test1501() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::part01(&input);

        println!("-- {} --", res)
    }

    #[test]
    fn test1502() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::part02(&input);

        println!("-- {:?} --", res)
    }
}
