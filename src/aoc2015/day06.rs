/** Correct results:
* - 377891
* - 14110788
*/
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_point_comma},
    point::{Point, PointOps},
};
use ndarray::{s, Array2};
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::char,
    combinator::{map, value},
    sequence::separated_pair,
    IResult,
};

#[derive(Clone, Copy)]
enum Switch {
    On,
    Off,
    Toggle,
}

impl Switch {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        alt((
            value(Self::On, tag("turn on")),
            value(Self::Off, tag("turn off")),
            value(Self::Toggle, tag("toggle")),
        ))(s)
    }
}

struct Command {
    pub switch: Switch,
    pub start: Point<usize>,
    pub end: Point<usize>,
}

impl Command {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        map(
            separated_pair(
                Switch::parse,
                char(' '),
                separated_pair(parse_point_comma, tag(" through "), parse_point_comma),
            ),
            |(switch, (start, end))| Self { switch, start, end },
        )(s)
    }
}

#[aoc_generator(day6)]
fn generator(input: &str) -> Result<Vec<Command>, AocError> {
    input_parser(by_lines(Command::parse))(input)
}

#[aoc(day6, part1)]
fn part01(input: &[Command]) -> usize {
    let mut map = Array2::<u8>::zeros((1000, 1000));

    for command in input {
        let slice = s![
            command.start.x()..=command.end.x(),
            command.start.y()..=command.end.y()
        ];
        for cell in &mut map.slice_mut(slice) {
            match &command.switch {
                Switch::On => *cell = 1,
                Switch::Off => *cell = 0,
                Switch::Toggle => *cell = u8::from(*cell == 0),
            }
        }
    }

    map.into_iter().filter(|c| *c == 1).count()
}

#[aoc(day6, part2)]
fn part02(input: &[Command]) -> usize {
    let mut map = Array2::<usize>::zeros((1000, 1000));

    for command in input {
        let slice = s![
            command.start.x()..=command.end.x(),
            command.start.y()..=command.end.y()
        ];
        for cell in &mut map.slice_mut(slice) {
            match &command.switch {
                Switch::On => *cell += 1,
                Switch::Off => {
                    if *cell > 0 {
                        *cell -= 1;
                    }
                }
                Switch::Toggle => *cell += 2,
            }
        }
    }

    map.into_iter().sum()
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT_1: &str = r"turn on 0,0 through 999,999
toggle 0,0 through 999,0
turn off 499,499 through 500,500";

    #[test]
    fn test0601_1() {
        let input = super::generator(BUILD_INPUT_1).unwrap();
        let res = super::part01(&input);

        println!("-- {} --", res)
    }

    #[test]
    fn test0002() {
        let input = super::generator(BUILD_INPUT_1).unwrap();
        let res = super::part02(&input);

        println!("-- {} --", res)
    }
}
