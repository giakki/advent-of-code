/** Correct results:
* - 1638
* - 17
*/
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use std::cmp::Ordering;

struct SubslicesIterator<'a, T> {
    index: usize,
    slice: &'a [T],
}

impl<'a, T> SubslicesIterator<'a, T> {
    const fn new(ts: &'a [T]) -> Self {
        Self {
            index: 0,
            slice: ts,
        }
    }
}

impl<'a, T> Iterator for SubslicesIterator<'a, T>
where
    T: Copy,
{
    type Item = (T, &'a [T]);

    fn next(&mut self) -> Option<Self::Item> {
        if self.index >= self.slice.len() {
            return None;
        }
        let res = Some((self.slice[self.index], &self.slice[self.index + 1..]));
        self.index += 1;

        res
    }
}

fn generator(input: &str) -> Result<Vec<usize>, AocError> {
    input_parser(by_lines(parse_int))(input)
}

fn knapsack(containers: &[usize], max: usize) -> usize {
    if max == 0 {
        return 1;
    }

    let mut combinations_found = 0;

    for (value, rest) in SubslicesIterator::new(containers) {
        if value <= max {
            combinations_found += knapsack(rest, max - value);
        }
    }

    combinations_found
}

fn knapsack_combinations(containers: &[usize], max: usize) -> (usize, usize) {
    if max == 0 {
        return (0, 1);
    }

    let mut min_num_containers = std::usize::MAX;
    let mut combinations_found = 0;
    for (value, rest) in SubslicesIterator::new(containers) {
        if value <= max {
            let (mut nested_num_containers, nested_combinations) =
                knapsack_combinations(rest, max - value);

            nested_num_containers = nested_num_containers.saturating_add(1);

            match nested_num_containers.cmp(&min_num_containers) {
                Ordering::Equal => combinations_found += nested_combinations,
                Ordering::Less => {
                    min_num_containers = nested_num_containers;
                    combinations_found = nested_combinations;
                }
                Ordering::Greater => {}
            }
        }
    }

    (min_num_containers, combinations_found)
}

#[aoc(day17, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    let mut weights = generator(input)?;
    weights.sort_unstable();

    Ok(knapsack(&weights, 150))
}

#[aoc(day17, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    let mut weights = generator(input)?;
    weights.sort_unstable();

    Ok(knapsack_combinations(&weights, 150).1)
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r"20
15
10
5
5";

    #[test]
    fn test17_gen() {
        let input = super::generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test17_knap() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::knapsack(&input, 25);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test17_knap_comb() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::knapsack_combinations(&input, 25);

        println!("-- {:?} --", res)
    }
}
