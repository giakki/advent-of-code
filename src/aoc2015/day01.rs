/** Correct results:
 * - 138
 * - 1771
 */
use crate::common::errors::AocError;

#[aoc(day1, part1)]
fn part01(input: &str) -> isize {
    let mut floor = 0;
    for next in input.as_bytes() {
        floor += if *next == b'(' { 1 } else { -1 };
    }

    floor
}

#[aoc(day1, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    let mut floor = 0;
    for (i, next) in input.as_bytes().iter().enumerate() {
        floor += if *next == b'(' { 1 } else { -1 };

        if floor < 0 {
            return Ok(i + 1);
        }
    }

    Err(AocError::NoSolution)
}
