/** Correct results:
* - 618
* - 601
*/
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use itertools::Itertools;
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{alpha1, char},
    combinator::value,
    sequence::{delimited, preceded, tuple},
    IResult,
};
use std::{cmp::max, collections::HashMap};

fn parse_line(s: &str) -> IResult<&str, (&str, i32, i32, &str)> {
    tuple((
        alpha1,
        preceded(
            tag(" would "),
            alt((value(1, tag("gain")), value(-1, tag("lose")))),
        ),
        preceded(char(' '), parse_int),
        delimited(
            tag(" happiness units by sitting next to "),
            alpha1,
            char('.'),
        ),
    ))(s)
}

fn generator(input: &str) -> Result<HashMap<&str, HashMap<&str, i32>>, AocError> {
    input_parser(by_lines(parse_line))(input).map(|res| {
        let num_entries = res.len();

        res.into_iter().fold(
            HashMap::with_capacity(num_entries),
            |mut accum, (a, sign, amount, b)| {
                let map = accum
                    .entry(a)
                    .or_insert_with(|| HashMap::with_capacity(num_entries));
                map.insert(b, amount * sign);
                accum
            },
        )
    })
}

fn optimal_happiness(map: &[Vec<i32>]) -> i32 {
    let num_people = map.len();

    let mut max_res = i32::MIN;
    for arrangement in (0..num_people).permutations(num_people) {
        let mut res = 0i32;
        for (a, b, c) in std::iter::once(&arrangement[arrangement.len() - 1])
            .chain(arrangement.iter())
            .chain(std::iter::once(&arrangement[0]))
            .tuple_windows()
        {
            res += map[*b][*a];
            res += map[*b][*c];
        }

        max_res = max(max_res, res);
    }

    max_res
}

fn to_adjacency_map(graph: HashMap<&str, HashMap<&str, i32>>) -> Result<Vec<Vec<i32>>, AocError> {
    let names = {
        let mut n = graph.keys().copied().collect::<Vec<_>>();
        n.sort_unstable();
        n.dedup();
        n
    };

    let size = names.len();

    let mut map = vec![vec![0; size]; size];
    for (name, friends) in graph {
        let x = names
            .iter()
            .position(|n| *n == name)
            .ok_or(AocError::Unspecified)?;
        for (friend, happiness) in friends {
            let y = names
                .iter()
                .position(|n| *n == friend)
                .ok_or(AocError::Unspecified)?;
            map[x][y] = happiness;
        }
    }

    Ok(map)
}

#[aoc(day13, part1)]
fn part01(input: &str) -> Result<i32, AocError> {
    let map = to_adjacency_map(generator(input)?)?;

    Ok(optimal_happiness(&map))
}

#[aoc(day13, part2)]
fn part02(input: &str) -> Result<i32, AocError> {
    let mut graph = generator(input)?;
    graph.insert("Me", HashMap::new());
    let map = to_adjacency_map(graph)?;

    Ok(optimal_happiness(&map))
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r"Alice would gain 54 happiness units by sitting next to Bob.
Alice would lose 79 happiness units by sitting next to Carol.
Alice would lose 2 happiness units by sitting next to David.
Bob would gain 83 happiness units by sitting next to Alice.
Bob would lose 7 happiness units by sitting next to Carol.
Bob would lose 63 happiness units by sitting next to David.
Carol would lose 62 happiness units by sitting next to Alice.
Carol would gain 60 happiness units by sitting next to Bob.
Carol would gain 55 happiness units by sitting next to David.
David would gain 46 happiness units by sitting next to Alice.
David would lose 7 happiness units by sitting next to Bob.
David would gain 41 happiness units by sitting next to Carol.";

    #[test]
    fn test0001() {
        let res = super::part01(BUILD_INPUT);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test0002() {
        let res = super::part02(BUILD_INPUT);

        println!("-- {:?} --", res)
    }
}
