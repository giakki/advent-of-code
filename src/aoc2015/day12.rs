/** Correct results:
* - 111754
* - 65402
*/
use crate::common::{
    errors::AocError,
    json::{parse_json, Json},
    parsers::input_parser,
};

fn generator(input: &str) -> Result<Json, AocError> {
    input_parser(parse_json)(input)
}

#[aoc(day12, part1)]
fn part01(input: &str) -> Result<f64, AocError> {
    Ok(generator(input)?
        .iter()
        .filter_map(|v| {
            if let Json::Number(n) = v {
                Some(n)
            } else {
                None
            }
        })
        .sum())
}

fn has_red_key(json: &Json) -> bool {
    if let Json::Object(map) = json {
        map.values().any(|v| {
            if let Json::String(s) = v {
                *s == "red"
            } else {
                false
            }
        })
    } else {
        false
    }
}

#[aoc(day12, part2)]
fn part02(input: &str) -> Result<f64, AocError> {
    Ok(generator(input)?
        .iter()
        .recurse_if(|v| !has_red_key(v))
        .filter_map(|v| {
            if let Json::Number(n) = v {
                Some(n)
            } else {
                None
            }
        })
        .sum())
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT_1: &str = r#"[1,2,3]"#;
    const BUILD_INPUT_2: &str = r#"[[[3]]]"#;
    const BUILD_INPUT_3: &str = r#"{"a":[-1,1]}"#;
    const BUILD_INPUT_4: &str = r#"[]"#;
    const BUILD_INPUT_5: &str = r#"[1,{"c":"red","b":2},3]"#;
    const BUILD_INPUT_6: &str = r#"{"d":"red","e":[1,2,3,4],"f":5}"#;
    const BUILD_INPUT_7: &str = r#"[1,"red",5]"#;

    #[test]
    fn test1201() {
        for input in &[BUILD_INPUT_1, BUILD_INPUT_2, BUILD_INPUT_3, BUILD_INPUT_4] {
            let res = super::part01(input);

            println!("-- {:?} --", res)
        }
    }

    #[test]
    fn test1202() {
        for input in &[BUILD_INPUT_1, BUILD_INPUT_5, BUILD_INPUT_6, BUILD_INPUT_7] {
            let res = super::part02(input);

            println!("-- {:?} --", res)
        }
    }
}
