/** Correct results:
 * - 776160
 * - 786240
 */
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};

#[aoc_generator(day20)]
fn generator(input: &str) -> Result<usize, AocError> {
    input_parser(parse_int)(input)
}

#[aoc(day20, part1)]
fn part01(input: &usize) -> Result<usize, AocError> {
    let max = input / 10;
    let mut house_gifts = vec![10; max];

    for elf_num in 2..max {
        for house_num in (elf_num..max).step_by(elf_num) {
            house_gifts[house_num] += elf_num * 10;
        }
    }

    for (idx, gifts) in house_gifts.into_iter().enumerate() {
        if gifts > *input {
            return Ok(idx);
        }
    }

    Err(AocError::NoSolution)
}

#[aoc(day20, part2)]
fn part02(input: &usize) -> Result<usize, AocError> {
    let max = input / 10;
    let mut house_gifts = vec![10; max];

    for elf_num in 2..max {
        for house_num in (elf_num..max).step_by(elf_num).take(50) {
            house_gifts[house_num] += elf_num * 11;
        }
    }

    for (idx, gifts) in house_gifts.into_iter().enumerate() {
        if gifts > *input {
            return Ok(idx);
        }
    }

    Err(AocError::NoSolution)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"100";

    #[test]
    fn test20_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test2001() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input).unwrap();

        assert_eq!(res, 6);
    }
}
