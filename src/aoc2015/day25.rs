/** Correct result:
 * - 9132360
 */
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use nom::{
    bytes::complete::tag,
    character::complete::char,
    sequence::{preceded, separated_pair, terminated},
};

/*
 * 1   3   6   10  15  21  28  36  45    = binomial(n+0, 2) + n
 * 2   5   9   14  20  27  35  44        = binomial(n+1, 2) + n
 * 4   8   13  19  26  34  43            = binomial(n+2, 2) + n
 * 7   12  18  25  33  42                = binomial(n+3, 2) + n
 * 11  17  24  32  41
 * 16  23  31  40
 * 22  30  39
 * 29  38
 * 37
*/

#[aoc_generator(day25)]
fn generator(input: &str) -> Result<(usize, usize), AocError> {
    input_parser(separated_pair(
        preceded(
            tag("To continue, please consult the code grid in the manual.  Enter the code at row "),
            parse_int,
        ),
        tag(", column "),
        terminated(parse_int, char('.')),
    ))(input)
}

const fn fast_binomial_2(n: usize) -> usize {
    // (0..n).into_iter().sum::<usize>()
    n * (n - 1) / 2
}

// From: https://en.wikipedia.org/wiki/Modular_exponentiation#Pseudocode
const fn fast_exp(mut base: usize, mut exponent: usize, modulus: usize) -> usize {
    if modulus == 1 {
        return 0;
    }

    let mut result = 1;
    base %= modulus;
    while exponent > 0 {
        if exponent % 2 == 1 {
            result = (result * base) % modulus;
        }
        exponent >>= 1;
        base = (base * base) % modulus;
    }

    result
}

#[aoc(day25, part1)]
fn part01((row, column): &(usize, usize)) -> usize {
    let num_iterations = fast_binomial_2(column + row - 1) + column - 1;

    (20_151_125 * fast_exp(252_533, num_iterations, 33_554_393)) % 33_554_393
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"To continue, please consult the code grid in the manual.  Enter the code at row 5, column 6.";

    #[test]
    fn test25_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test_binomial() {
        assert_eq!(fast_binomial_2(10), 45);
        assert_eq!(fast_binomial_2(11), 55);
    }

    #[test]
    fn test2501() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 31663883);
    }
}
