/** Correct results:
* - hepxxyzz
* - heqaabcc
*/
use itertools::Itertools;
use std::string::FromUtf8Error;

fn is_valid(pass: &[u8]) -> bool {
    let mut has_triplet = false;
    for (a, b, c) in pass.iter().tuple_windows() {
        if *b == a + 1 && *c == b + 1 {
            has_triplet = true;
        }
    }
    if !has_triplet {
        return false;
    }

    for c in pass {
        if matches!(c, b'i' | b'o' | b'l') {
            return false;
        }
    }

    let mut num_doubles = 0;
    let mut i = 1;
    while i < pass.len() {
        if pass[i - 1] == pass[i] {
            num_doubles += 1;
            i += 2;
        } else {
            i += 1;
        }
    }

    num_doubles == 2
}

fn next_password(pass: &[u8]) -> Vec<u8> {
    let mut result = pass.iter().map(u8::clone).collect::<Vec<_>>();
    for i in result.iter_mut().rev() {
        if *i < b'z' {
            *i += 1;
            return result;
        }

        *i = b'a';
    }

    std::iter::repeat(b'a').take(result.len() + 1).collect()
}

#[aoc(day11, part1)]
fn part01(input: &str) -> Result<String, FromUtf8Error> {
    let mut iter = input.as_bytes().iter().map(u8::clone).collect::<Vec<_>>();
    while !is_valid(&iter) {
        iter = next_password(&iter);
    }

    String::from_utf8(iter)
}

#[aoc(day11, part2)]
fn part02(input: &str) -> Result<String, FromUtf8Error> {
    let mut iter = next_password(part01(input)?.as_bytes());
    while !is_valid(&iter) {
        iter = next_password(&iter);
    }

    String::from_utf8(iter)
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r"hepxcrrq";

    #[test]
    fn test_is_valid() {
        let passes = vec![b"hijklmmn", b"abbceffg", b"abbcegjk", b"abbcddef"];
        for pass in passes {
            println!("{}", super::is_valid(pass))
        }
    }

    #[test]
    fn test_next_password() {
        let passes = vec![b"aa", b"ab", b"az", b"zz"];
        for pass in passes {
            println!("{}", String::from_utf8_lossy(&super::next_password(pass)))
        }
    }

    #[test]
    fn test1101() {
        let res = super::part01(BUILD_INPUT);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test1102() {
        let res = super::part02(BUILD_INPUT);

        println!("-- {:?} --", res)
    }
}
