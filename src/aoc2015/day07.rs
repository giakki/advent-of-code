/** Correct results:
* - 16076
* - 2797
*/
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{alpha1, digit1},
    combinator::{map, map_res},
    sequence::{preceded, separated_pair, tuple},
    IResult,
};
use std::collections::HashMap;

#[derive(Debug)]
enum LogicGateLiteral<'a> {
    Const(usize),
    Named(&'a str),
}

impl<'a> LogicGateLiteral<'a> {
    pub fn parse(s: &'a str) -> IResult<&'a str, Self> {
        alt((
            map_res(digit1, |num: &str| {
                Ok::<Self, AocError>(Self::Const(num.parse()?))
            }),
            map(alpha1, Self::Named),
        ))(s)
    }
}

#[derive(Debug)]
enum LogicGate<'a> {
    Literal(LogicGateLiteral<'a>),
    And((LogicGateLiteral<'a>, LogicGateLiteral<'a>)),
    Or((LogicGateLiteral<'a>, LogicGateLiteral<'a>)),
    LShift((LogicGateLiteral<'a>, usize)),
    RShift((LogicGateLiteral<'a>, usize)),
    Not(LogicGateLiteral<'a>),
}

impl<'a> LogicGate<'a> {
    pub fn parse(s: &'a str) -> IResult<&'a str, (String, Self)> {
        map(
            tuple((
                alt((
                    Self::parse_and_lg,
                    Self::parse_or_lg,
                    Self::parse_lshift_lg,
                    Self::parse_rshift_lg,
                    Self::parse_not_lg,
                    map(LogicGateLiteral::parse, Self::Literal),
                )),
                preceded(tag(" -> "), alpha1),
            )),
            |(a, b)| (b.to_owned(), a),
        )(s)
    }

    fn parse_and_lg(s: &'a str) -> IResult<&'a str, Self> {
        map(
            separated_pair(
                LogicGateLiteral::parse,
                tag(" AND "),
                LogicGateLiteral::parse,
            ),
            Self::And,
        )(s)
    }

    fn parse_or_lg(s: &'a str) -> IResult<&'a str, Self> {
        map(
            separated_pair(
                LogicGateLiteral::parse,
                tag(" OR "),
                LogicGateLiteral::parse,
            ),
            Self::Or,
        )(s)
    }

    fn parse_lshift_lg(s: &'a str) -> IResult<&'a str, Self> {
        map(
            separated_pair(LogicGateLiteral::parse, tag(" LSHIFT "), parse_int),
            Self::LShift,
        )(s)
    }

    fn parse_rshift_lg(s: &'a str) -> IResult<&'a str, Self> {
        map(
            separated_pair(LogicGateLiteral::parse, tag(" RSHIFT "), parse_int),
            Self::RShift,
        )(s)
    }

    fn parse_not_lg(s: &'a str) -> IResult<&'a str, Self> {
        map(preceded(tag("NOT "), LogicGateLiteral::parse), Self::Not)(s)
    }
}

struct Circuit<'a> {
    map: HashMap<&'a str, &'a LogicGate<'a>>,
    memo: HashMap<&'a str, usize>,
}

impl<'a> Circuit<'a> {
    fn new(input: &'a [(String, LogicGate)]) -> Self {
        let mut map = HashMap::new();
        for (output, gate) in input {
            map.insert(output as &str, gate);
        }

        Self {
            map,
            memo: HashMap::new(),
        }
    }

    fn eval(&mut self, name: &'a str) -> Result<usize, AocError> {
        if let Some(memo) = self.memo.get(name) {
            return Ok(*memo);
        }

        let gate = self.map.get(name).ok_or(AocError::NoSolution)?;

        let res = match gate {
            LogicGate::And((a, b)) => self.eval_literal(a)? & self.eval_literal(b)?,
            LogicGate::Or((a, b)) => self.eval_literal(a)? | self.eval_literal(b)?,
            LogicGate::LShift((a, n)) => self.eval_literal(a)? << n,
            LogicGate::RShift((a, n)) => self.eval_literal(a)? >> n,
            LogicGate::Not(a) => !self.eval_literal(a)?,
            LogicGate::Literal(a) => self.eval_literal(a)?,
        };

        self.memo.insert(name, res);

        Ok(res)
    }

    fn eval_literal(&mut self, literal: &'a LogicGateLiteral) -> Result<usize, AocError> {
        match literal {
            LogicGateLiteral::Const(n) => Ok(*n),
            LogicGateLiteral::Named(n) => self.eval(n),
        }
    }

    fn set(&mut self, name: &'a str, gate: &'a LogicGate) {
        self.map.insert(name, gate);
        self.memo = HashMap::new();
    }
}

fn generator(input: &str) -> Result<Vec<(String, LogicGate)>, AocError> {
    input_parser(by_lines(LogicGate::parse))(input)
}

#[aoc(day7, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    let parsed = generator(input)?;
    let mut circuit = Circuit::new(&parsed);

    circuit.eval("a")
}

#[aoc(day7, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    let parsed = generator(input)?;
    let mut circuit = Circuit::new(&parsed);

    let p1 = circuit.eval("a")?;

    let new_b = LogicGate::Literal(LogicGateLiteral::Const(p1));
    circuit.set("b", &new_b);

    circuit.eval("a")
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r"123 -> b
111 AND g -> x
456 -> y
x AND y -> d
x OR y -> e
x LSHIFT 2 -> f
y RSHIFT 2 -> g
NOT f -> h
NOT g -> i
f OR g -> a";

    #[test]
    fn test0701() {
        let res = super::part01(BUILD_INPUT);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test0702() {
        let res = super::part02(BUILD_INPUT);

        println!("-- {:?} --", res)
    }
}
