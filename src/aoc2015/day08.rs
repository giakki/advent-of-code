/** Correct results:
* - 1333
* - 2046
*/
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser},
};
use nom::{bytes::complete::take_while, IResult};

fn parse_line(s: &str) -> IResult<&str, &str> {
    take_while(|c| c != '\n')(s)
}

fn generator(input: &str) -> Result<Vec<&str>, AocError> {
    input_parser(by_lines(parse_line))(input)
}

#[aoc(day8, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    let mut total_chars = 0;
    let mut str_chars = 0;
    for line in generator(input)? {
        total_chars += line.len();
        let bytes = line.as_bytes();

        let mut i = 1;
        while i < bytes.len() - 1 {
            if bytes[i] == b'\\' {
                if bytes[i + 1] == b'x' {
                    i += 3;
                } else {
                    i += 1;
                }
            }

            i += 1;
            str_chars += 1;
        }
    }

    Ok(total_chars - str_chars)
}

#[aoc(day8, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    Ok(generator(input)?
        .iter()
        .map(|line| {
            2 + line
                .as_bytes()
                .iter()
                .filter(|c| matches!(c, b'"' | b'\\'))
                .count()
        })
        .sum())
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r#"""
"abc"
"aaa\"aaa"
"\x27""#;

    #[test]
    fn test0801() {
        let res = super::part01(BUILD_INPUT);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test0802() {
        let res = super::part02(BUILD_INPUT);

        println!("-- {:?} --", res)
    }
}
