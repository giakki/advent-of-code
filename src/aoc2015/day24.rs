/** Correct results:
 * - 11266889531
 * - 77387711
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use itertools::Itertools;

#[aoc_generator(day24)]
fn generator(input: &str) -> Result<Vec<u64>, AocError> {
    input_parser(by_lines(parse_int))(input)
}

fn group_packages(packages: &[u64], num_groups: u64) -> Option<u64> {
    let group_sum = packages.iter().sum::<u64>() / num_groups;

    for i in 1..packages.len() {
        let quantum_entanglements = packages
            .iter()
            .combinations(i)
            .filter_map(|combination| {
                let sum = combination.iter().copied().sum::<u64>();
                if sum == group_sum {
                    Some(combination.iter().copied().product())
                } else {
                    None
                }
            })
            .collect_vec();

        if let Some(min) = quantum_entanglements.into_iter().min() {
            return Some(min);
        }
    }

    None
}

#[aoc(day24, part1)]
fn part01(input: &[u64]) -> Option<u64> {
    group_packages(input, 3)
}

#[aoc(day24, part2)]
fn part02(input: &[u64]) -> Option<u64> {
    group_packages(input, 4)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"1
2
3
4
5
7
8
9
10
11";

    #[test]
    fn test24_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test2401() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, Some(99));
    }

    #[test]
    fn test2402() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, Some(44));
    }
}
