/** Correct results:
 * - 576
 * - 207
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser},
};
use itertools::Itertools;
use nom::{
    bytes::complete::tag,
    character::complete::alpha1,
    combinator::map,
    sequence::{preceded, tuple},
};
use rand::{seq::SliceRandom, thread_rng};
use std::collections::BTreeSet;

#[aoc_generator(day19)]
fn generator(input: &str) -> Result<(Vec<(String, String)>, String), AocError> {
    input_parser(tuple((
        by_lines(map(
            tuple((alpha1, preceded(tag(" => "), alpha1))),
            |(a, b): (&str, &str)| (a.to_owned(), b.to_owned()),
        )),
        preceded(tag("\n\n"), map(alpha1, |a: &str| a.to_owned())),
    )))(input)
}

#[aoc(day19, part1)]
fn part01((replacements, molecule): &(Vec<(String, String)>, String)) -> usize {
    let mut new_molecules = BTreeSet::new();

    for (from, to) in replacements {
        for (idx, substr) in molecule.as_bytes().windows(from.len()).enumerate() {
            if &String::from_utf8_lossy(substr) == from {
                new_molecules
                    .insert(molecule[..idx].to_string() + to + &molecule[idx + substr.len()..]);
            }
        }
    }

    new_molecules.len()
}

#[aoc(day19, part2)]
fn part02((replacements, molecule): &(Vec<(String, String)>, String)) -> usize {
    let mut replacements = replacements.iter().map(|(a, b)| (b, a)).collect_vec();
    let mut steps: usize = 0;
    let mut target = molecule.clone();

    while target != "e" {
        let mut changed = false;

        for (replacement, source) in &replacements {
            if target.contains(*replacement) {
                target = target.replacen(*replacement, source, 1);
                steps += 1;
                changed = true;
            }
        }
        if !changed {
            target = molecule.clone();
            replacements.shuffle(&mut thread_rng());
            steps = 0;
        }
    }

    steps
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT_1: &str = r"H => HO
H => OH
O => HH

HOH";

    const BUILD_INPUT_2: &str = r"e => H
e => O
H => HO
H => OH
O => HH

HOH";

    #[test]
    fn test19_gen() {
        let input = generator(BUILD_INPUT_1);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test1901() {
        let input = generator(BUILD_INPUT_1).unwrap();
        let res = part01(&input);

        assert_eq!(res, 4);
    }

    #[test]
    fn test1902() {
        let input = generator(BUILD_INPUT_2).unwrap();
        let res = part02(&input);

        assert_eq!(res, 3);
    }
}
