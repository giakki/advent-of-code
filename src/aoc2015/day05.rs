/** Correct results:
 * - 236
 * - 51
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser},
};
use itertools::Itertools;
use nom::character::complete::alpha1;

fn is_nice_part_1(s: &str) -> bool {
    let mut found_pair = false;
    for (a, b) in s.as_bytes().iter().tuple_windows() {
        if (*a == b'a' && *b == b'b')
            || (*a == b'c' && *b == b'd')
            || (*a == b'p' && *b == b'q')
            || (*a == b'x' && *b == b'y')
        {
            return false;
        }
        if a == b {
            found_pair = true;
        }
    }

    found_pair
        && s.as_bytes()
            .iter()
            .filter(|c| matches!(c, b'a' | b'e' | b'i' | b'o' | b'u'))
            .take(3)
            .count()
            == 3
}

fn is_nice_part_2(s: &str) -> bool {
    let mut rule_1_valid = false;
    for (index, w1) in s.as_bytes().windows(2).enumerate() {
        for w2 in s.as_bytes().windows(2).skip(index + 2) {
            if w1 == w2 {
                rule_1_valid = true;
            }
        }
    }

    if !rule_1_valid {
        return false;
    }

    let mut rule_2_valid = false;
    for (a, _, b) in s.as_bytes().iter().tuple_windows() {
        if a == b {
            rule_2_valid = true;
        }
    }

    rule_2_valid
}

fn generator(input: &str) -> Result<Vec<&str>, AocError> {
    input_parser(by_lines(alpha1))(input)
}

#[aoc(day5, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    Ok(generator(input)?
        .iter()
        .filter(|line| is_nice_part_1(line))
        .count())
}

#[aoc(day5, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    Ok(generator(input)?
        .iter()
        .filter(|line| is_nice_part_2(line))
        .count())
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT_1: &str = r"ugknbfddgicrmopn
aaa
jchzalrnumimnmhp
haegwjzuvuyypxyu
dvszwmarrgswjxmb";

    const BUILD_INPUT_2: &str = r"qjhvhtzxzqqjkmpb
xxyxx
uurcxstgmygtbstg
ieodomkazucvgmuy
aaa";

    #[test]
    fn test0001() {
        let res = super::part01(BUILD_INPUT_1);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test0002() {
        let res = super::part02(BUILD_INPUT_2);

        println!("-- {:?} --", res)
    }
}
