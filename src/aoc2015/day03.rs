/** Correct results:
* - 2572
* - 2631
*/
use crate::common::{
    direction::Direction, errors::AocError, parsers::input_parser, point::PointOps,
};
use itertools::Itertools;
use nom::multi::many0;
use std::collections::HashSet;

#[aoc_generator(day3)]
fn generator(input: &str) -> Result<Vec<Direction>, AocError> {
    input_parser(many0(Direction::parse_from_arrow))(input)
}

#[aoc(day3, part1)]
fn part01(input: &[Direction]) -> usize {
    input
        .iter()
        .scan((0, 0), |pos, direction| {
            pos.move_by(1, direction);

            Some(*pos)
        })
        .unique()
        .count()
}

#[aoc(day3, part2)]
fn part02(input: &[Direction]) -> usize {
    // Essentially the same as before, but in imperative form.
    let mut visited = HashSet::new();
    let mut santa_pos = (0, 0);
    let mut robot_pos = (0, 0);

    visited.insert(santa_pos);
    for dir in input.chunks_exact(2) {
        santa_pos.move_by(1, &dir[0]);
        robot_pos.move_by(1, &dir[1]);
        visited.insert(santa_pos);
        visited.insert(robot_pos);
    }

    visited.len()
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT_1_1: &str = r">";
    const BUILD_INPUT_1_2: &str = r"^>v<";
    const BUILD_INPUT_1_3: &str = r"^v^v^v^v^v";

    const BUILD_INPUT_2_1: &str = r"^v";
    const BUILD_INPUT_2_2: &str = r"^>v<";
    const BUILD_INPUT_2_3: &str = r"^v^v^v^v^v";

    #[test]
    fn test0301_1() {
        let input = super::generator(BUILD_INPUT_1_1).unwrap();
        let res = super::part01(&input);

        println!("-- {} --", res)
    }

    #[test]
    fn test0301_2() {
        let input = super::generator(BUILD_INPUT_1_2).unwrap();
        let res = super::part01(&input);

        println!("-- {} --", res)
    }

    #[test]
    fn test0301_3() {
        let input = super::generator(BUILD_INPUT_1_3).unwrap();
        let res = super::part01(&input);

        println!("-- {} --", res)
    }

    #[test]
    fn test0302_1() {
        let input = super::generator(BUILD_INPUT_2_1).unwrap();
        let res = super::part02(&input);

        println!("-- {} --", res)
    }

    #[test]
    fn test0302_2() {
        let input = super::generator(BUILD_INPUT_2_2).unwrap();
        let res = super::part02(&input);

        println!("-- {} --", res)
    }

    #[test]
    fn test0302_3() {
        let input = super::generator(BUILD_INPUT_2_3).unwrap();
        let res = super::part02(&input);

        println!("-- {} --", res)
    }
}
