/** Correct results:
 * - 1588178
 * - 3783758
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use nom::{
    character::complete::char,
    combinator::map,
    sequence::{terminated, tuple},
    IResult,
};
use std::cmp;

struct Rect {
    pub width: u32,
    pub height: u32,
    pub length: u32,
}

impl Rect {
    pub const fn surface_area(&self) -> u32 {
        (self.front_area() + self.side_area() + self.top_area()) * 2
    }

    pub const fn volume(&self) -> u32 {
        self.width * self.height * self.length
    }

    pub const fn front_area(&self) -> u32 {
        self.height * self.width
    }

    pub const fn front_perimeter(&self) -> u32 {
        (self.height + self.width) * 2
    }

    pub const fn top_area(&self) -> u32 {
        self.width * self.length
    }

    pub const fn top_perimeter(&self) -> u32 {
        (self.width + self.length) * 2
    }

    pub const fn side_area(&self) -> u32 {
        self.height * self.length
    }

    pub const fn side_perimeter(&self) -> u32 {
        (self.height + self.length) * 2
    }
}

fn parse_rect(s: &str) -> IResult<&str, Rect> {
    map(
        tuple((
            terminated(parse_int, char('x')),
            terminated(parse_int, char('x')),
            parse_int,
        )),
        |(width, height, length)| Rect {
            width,
            height,
            length,
        },
    )(s)
}

#[aoc_generator(day2)]
fn generator(input: &str) -> Result<Vec<Rect>, AocError> {
    input_parser(by_lines(parse_rect))(input)
}

#[aoc(day2, part1)]
fn part01(input: &[Rect]) -> u32 {
    input
        .iter()
        .map(|gift| {
            let smallest_side_area = cmp::min(
                cmp::min(gift.front_area(), gift.side_area()),
                gift.top_area(),
            );

            gift.surface_area() + smallest_side_area
        })
        .sum()
}

#[aoc(day2, part2)]
fn part02(input: &[Rect]) -> u32 {
    input
        .iter()
        .map(|gift| {
            let smallest_side_perimeter = cmp::min(
                cmp::min(gift.front_perimeter(), gift.side_perimeter()),
                gift.top_perimeter(),
            );

            gift.volume() + smallest_side_perimeter
        })
        .sum()
}
