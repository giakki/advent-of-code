/** Correct results:
* - 2655
* - 1059
*/
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use nom::{
    bytes::complete::tag,
    character::complete::alpha1,
    combinator::map,
    sequence::{delimited, preceded, tuple},
    IResult,
};
use num::Integer;
use std::cmp::min;

struct Rule {
    speed: usize,
    run_time: usize,
    rest_time: usize,
}

impl Rule {
    fn run_for(&self, time: usize) -> usize {
        let loop_time = self.run_time + self.rest_time;
        let (num_loops, remaining) = time.div_rem(&loop_time);
        let last_run_time = min(remaining, self.run_time);

        self.speed * (self.run_time * num_loops + last_run_time)
    }
}

fn parse_line(s: &str) -> IResult<&str, Rule> {
    map(
        tuple((
            alpha1,
            preceded(tag(" can fly "), parse_int),
            preceded(tag(" km/s for "), parse_int),
            delimited(
                tag(" seconds, but then must rest for "),
                parse_int,
                tag(" seconds."),
            ),
        )),
        |(_, speed, run_time, rest_time)| Rule {
            speed,
            run_time,
            rest_time,
        },
    )(s)
}

fn generator(input: &str) -> Result<Vec<Rule>, AocError> {
    input_parser(by_lines(parse_line))(input)
}

#[aoc(day14, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    generator(input).and_then(|rules| {
        rules
            .into_iter()
            .map(|rule| rule.run_for(2503))
            .max()
            .ok_or(AocError::NoSolution)
    })
}

#[aoc(day14, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    let rules = generator(input)?;
    let mut points = vec![0; rules.len()];

    for time in 1..=2503 {
        let positions = rules
            .iter()
            .map(|rule| rule.run_for(time))
            .collect::<Vec<_>>();
        let max = positions.iter().max().ok_or(AocError::NoSolution)?;

        for (i, pos) in positions.iter().enumerate() {
            if pos == max {
                points[i] += 1;
            }
        }
    }

    points.into_iter().max().ok_or(AocError::NoSolution)
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r"Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.";

    #[test]
    fn test_run() {
        let rules = super::generator(BUILD_INPUT).unwrap();
        let time_steps = vec![1, 10, 11, 12, 174, 1000];

        for step in time_steps {
            println!(
                "{:?}",
                rules.iter().map(|r| r.run_for(step)).collect::<Vec<_>>()
            );
        }
    }

    #[test]
    fn test1401() {
        let res = super::part01(BUILD_INPUT);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test1402() {
        let res = super::part02(BUILD_INPUT);

        println!("-- {:?} --", res)
    }
}
