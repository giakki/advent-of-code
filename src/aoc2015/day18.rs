/** Correct results:
 * - 1061
 * - 1006
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser},
    point::PointOps,
};
use itertools::Itertools;
use ndarray::{s, Array2};
use nom::{
    branch::alt,
    character::complete::char,
    combinator::{map, map_res},
    multi::many0,
};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Light {
    On,
    Off,
}

impl TryFrom<char> for Light {
    type Error = AocError;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            '.' => Ok(Self::Off),
            '#' => Ok(Self::On),
            _ => Err(AocError::ParseError(format!(
                "Cannot parse light: '{value}'"
            ))),
        }
    }
}
impl From<Light> for char {
    fn from(value: Light) -> Self {
        match value {
            Light::Off => '.',
            Light::On => '#',
        }
    }
}

type Board = Array2<Light>;

#[allow(dead_code)]
fn print_board(board: &Board) {
    for row in board.rows() {
        println!("{}", row.iter().copied().map(char::from).join(""));
    }
}

fn step_lights_part_1(orig_board: &Board) -> Board {
    let mut new_board = orig_board.clone();
    let (y_max, x_max) = orig_board.dim();
    for y in 1..(y_max - 1) {
        for x in 1..(x_max - 1) {
            let num_neighbors_alight = (y, x)
                .all_neighbors()
                .iter()
                .filter(|pos| orig_board[**pos] == Light::On)
                .count();

            let light = &mut new_board[(y, x)];
            if *light == Light::On && !(2..=3).contains(&num_neighbors_alight) {
                *light = Light::Off;
            } else if *light == Light::Off && num_neighbors_alight == 3 {
                *light = Light::On;
            }
        }
    }

    new_board
}

fn reset_corner_lights(board: &mut Board) {
    let dims = board.dim();

    board[(1, 1)] = Light::On;
    board[(dims.0 - 2, 1)] = Light::On;
    board[(1, dims.1 - 2)] = Light::On;
    board[(dims.0 - 2, dims.1 - 2)] = Light::On;
}

#[aoc_generator(day18)]
fn generator(input: &str) -> Result<Board, AocError> {
    input_parser(map(
        by_lines(many0(map_res(alt((char('.'), char('#'))), Light::try_from))),
        |l| {
            let mut board = Array2::from_elem((l.len() + 2, l[0].len() + 2), Light::Off);
            for (cell, light) in board
                .slice_mut(s![1..=l.len(), 1..=l[0].len(),])
                .iter_mut()
                .zip(l.into_iter().flatten().collect_vec())
            {
                *cell = light;
            }

            board
        },
    ))(input)
}

#[aoc(day18, part1)]
fn part01(input: &Board) -> usize {
    let mut board = input.clone();

    for _ in 0..100 {
        board = step_lights_part_1(&board);
    }

    board.map(|l| usize::from(*l == Light::On)).sum()
}

#[aoc(day18, part2)]
fn part02(input: &Board) -> usize {
    let mut board = input.clone();
    reset_corner_lights(&mut board);

    for _ in 0..100 {
        board = step_lights_part_1(&board);
        reset_corner_lights(&mut board);
    }

    board.map(|l| usize::from(*l == Light::On)).sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r".#.#.#
...##.
#....#
..#...
#.#..#
####..";

    #[test]
    fn test18_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test_step_1() {
        let mut board = generator(BUILD_INPUT).unwrap();

        println!("Initial state:");
        print_board(&board);

        for i in 1..5 {
            board = step_lights_part_1(&board);
            println!("After {} step:", i);
            print_board(&board);
            println!();
        }
    }

    #[test]
    fn test_step_2() {
        let mut board = generator(BUILD_INPUT).unwrap();
        reset_corner_lights(&mut board);

        println!("Initial state:");
        print_board(&board);

        for i in 1..6 {
            board = step_lights_part_1(&board);
            reset_corner_lights(&mut board);
            println!("After {} step:", i);
            print_board(&board);
            println!();
        }
    }
}
