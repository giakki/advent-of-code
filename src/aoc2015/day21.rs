/** Correct results:
 * - 121
 * - 201
 */
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use itertools::iproduct;
use nom::{
    bytes::complete::tag,
    character::complete::char,
    sequence::{delimited, preceded, tuple},
};

const SHOP_WEAPONS: &[(i32, i32, i32)] =
    &[(8, 4, 0), (10, 5, 0), (25, 6, 0), (40, 7, 0), (74, 8, 0)];

const SHOP_ARMOR: &[(i32, i32, i32)] = &[
    (0, 0, 0),
    (13, 0, 1),
    (31, 0, 2),
    (53, 0, 3),
    (75, 0, 4),
    (102, 0, 5),
];

const SHOP_RINGS: &[(i32, i32, i32)] = &[
    (0, 0, 0),
    (0, 0, 0),
    (25, 1, 0),
    (50, 2, 0),
    (100, 3, 0),
    (20, 0, 1),
    (40, 0, 2),
    (80, 0, 3),
];

fn num_rounds_to_kill(attacker_atk: i32, defender_hp: i32, defender_armor: i32) -> i32 {
    (defender_hp as f32 / i32::max(attacker_atk - defender_armor, 1) as f32).ceil() as i32
}

#[aoc_generator(day21)]
fn generator(input: &str) -> Result<(i32, i32, i32), AocError> {
    input_parser(tuple((
        delimited(tag("Hit Points: "), parse_int, char('\n')),
        delimited(tag("Damage: "), parse_int, char('\n')),
        preceded(tag("Armor: "), parse_int),
    )))(input)
}

#[aoc(day21, part1)]
fn part01(boss: &(i32, i32, i32)) -> i32 {
    let mut min_gold = i32::MAX;
    for (weapon, armor, ring1, ring2) in iproduct!(SHOP_WEAPONS, SHOP_ARMOR, SHOP_RINGS, SHOP_RINGS)
    {
        if *ring1 != (0, 0, 0) && ring1 == ring2 {
            continue;
        }
        let player = (
            100,
            weapon.1 + ring1.1 + ring2.1,
            armor.2 + ring1.2 + ring2.2,
        );
        if num_rounds_to_kill(player.1, boss.0, boss.2)
            <= num_rounds_to_kill(boss.1, player.0, player.2)
        {
            min_gold = i32::min(min_gold, weapon.0 + armor.0 + ring1.0 + ring2.0);
        }
    }

    min_gold
}

#[aoc(day21, part2)]
fn part02(boss: &(i32, i32, i32)) -> i32 {
    let mut max_gold = 0;
    for (weapon, armor, ring1, ring2) in iproduct!(SHOP_WEAPONS, SHOP_ARMOR, SHOP_RINGS, SHOP_RINGS)
    {
        if *ring1 != (0, 0, 0) && ring1 == ring2 {
            continue;
        }
        let player = (
            100,
            weapon.1 + ring1.1 + ring2.1,
            armor.2 + ring1.2 + ring2.2,
        );
        if num_rounds_to_kill(player.1, boss.0, boss.2)
            > num_rounds_to_kill(boss.1, player.0, player.2)
        {
            max_gold = i32::max(max_gold, weapon.0 + armor.0 + ring1.0 + ring2.0);
        }
    }

    max_gold
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"Hit Points: 103
Damage: 9
Armor: 2";

    #[test]
    fn test21_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }
}
