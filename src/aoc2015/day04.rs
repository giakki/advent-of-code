/** Correct results:
 * - 254575
 * - 1038736
 */
use crate::common::errors::AocError;
use itertools::Itertools;
use md5::{Digest, Md5};
use rayon::prelude::{IntoParallelIterator, ParallelIterator};

#[aoc(day4, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    (0..std::usize::MAX)
        .into_par_iter()
        .find_first(|n: &usize| {
            let mut hasher = Md5::new();
            hasher.update(input);
            hasher.update(n.to_string());

            let result = hasher.finalize();
            if let Some((a, b, c)) = result.iter().take(3).collect_tuple() {
                if *a == 0 && *b == 0 && (*c & 0xF0) == 0 {
                    return true;
                }
            }
            false
        })
        .ok_or(AocError::NoSolution)
}

#[aoc(day4, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    (0..std::usize::MAX)
        .into_par_iter()
        .find_first(|n: &usize| {
            let mut hasher = Md5::new();
            hasher.update(input);
            hasher.update(n.to_string());

            let result = hasher.finalize();
            if let Some((a, b, c)) = result.iter().take(3).collect_tuple() {
                if *a == 0 && *b == 0 && *c == 0 {
                    return true;
                }
            }
            false
        })
        .ok_or(AocError::NoSolution)
}

#[cfg(test)]
mod tests {
    const BUILD_INPUT_1: &str = r"abcdef";
    const BUILD_INPUT_2: &str = r"pqrstuv";

    #[test]
    fn test0401_1() {
        let res = super::part01(BUILD_INPUT_1);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test0401_2() {
        let res = super::part01(BUILD_INPUT_2);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test0402() {
        let res = super::part02(BUILD_INPUT_1);

        println!("-- {:?} --", res)
    }
}
