#![deny(clippy::all)]
#![deny(clippy::cargo)]
#![deny(clippy::complexity)]
#![deny(clippy::correctness)]
#![deny(clippy::nursery)]
#![deny(clippy::pedantic)]
#![deny(clippy::perf)]
#![deny(clippy::restriction)]
#![deny(clippy::style)]
#![deny(clippy::suspicious)]
#![allow(clippy::missing_errors_doc)]
#![allow(clippy::missing_panics_doc)]
#![allow(clippy::module_name_repetitions)]
#![allow(clippy::cast_sign_loss)]
#![allow(clippy::cast_possible_truncation)]
#![allow(clippy::iter_without_into_iter)]
#![allow(clippy::cast_possible_wrap)]
#![allow(clippy::similar_names)]
#![allow(clippy::implicit_return)]
#![allow(clippy::single_call_fn)]
#![allow(clippy::exhaustive_structs)]
#![allow(clippy::arithmetic_side_effects)]
#![allow(clippy::missing_inline_in_public_items)]
#![allow(clippy::as_conversions)]
#![allow(clippy::std_instead_of_core)]
#![allow(clippy::missing_trait_methods)]
#![allow(clippy::missing_docs_in_private_items)]
#![allow(clippy::min_ident_chars)]
#![allow(clippy::question_mark_used)]
#![allow(clippy::absolute_paths)]
#![allow(clippy::indexing_slicing)]
#![allow(clippy::panic)]
#![allow(clippy::exhaustive_enums)]
#![allow(clippy::needless_lifetimes)]
#![allow(clippy::missing_asserts_for_indexing)]
#![allow(clippy::std_instead_of_alloc)]
#![allow(clippy::unused_trait_names)]
#![allow(clippy::allow_attributes_without_reason)]
#![allow(clippy::allow_attributes)]
#![allow(clippy::manual_inspect)]
#![allow(clippy::pattern_type_mismatch)]
#![allow(clippy::integer_division_remainder_used)]
#![allow(clippy::wildcard_enum_match_arm)]
#![allow(clippy::integer_division)]
#![allow(clippy::default_numeric_fallback)]
#![allow(clippy::iter_over_hash_type)]
#![allow(clippy::let_underscore_untyped)]
#![allow(clippy::map_err_ignore)]
#![allow(clippy::single_char_lifetime_names)]
#![allow(clippy::pub_use)]
#![allow(clippy::separated_literal_suffix)]
#![allow(clippy::mod_module_files)]
#![allow(clippy::blanket_clippy_restriction_lints)]
#![allow(clippy::empty_structs_with_brackets)]
#![allow(clippy::float_arithmetic)]

extern crate aoc_runner;

#[macro_use]
extern crate aoc_runner_derive;

// pub mod aoc2015;

// pub mod aoc2016;

// pub mod aoc2017;

// pub mod aoc2018;

// pub mod aoc2019;

// pub mod aoc2020;

// pub mod aoc2021;

// pub mod aoc2022;

// pub mod aoc2023;

pub mod aoc2024;

pub mod common;

aoc_lib! { year = 2024 }
