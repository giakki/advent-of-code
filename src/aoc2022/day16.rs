/** Correct results:
 * - 1944
 * - 2679
 */
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::alpha1,
    combinator::map,
    multi::separated_list1,
    sequence::{preceded, tuple},
    IResult,
};
use pathfinding::prelude::{astar, dijkstra_all};
use std::collections::{hash_map::Entry, HashMap};
use std::hash::Hash;

use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};

#[derive(Debug)]
struct Valve<'a> {
    name: &'a str,
    flow_rate: i32,
    connections: Vec<&'a str>,
}

fn parse_valve(input: &str) -> IResult<&str, Valve> {
    map(
        tuple((
            preceded(tag("Valve "), alpha1),
            preceded(tag(" has flow rate="), parse_int),
            preceded(
                alt((
                    tag("; tunnels lead to valves "),
                    tag("; tunnel leads to valve "),
                )),
                separated_list1(tag(", "), alpha1),
            ),
        )),
        |(name, flow_rate, connections)| Valve {
            name,
            flow_rate,
            connections,
        },
    )(input)
}

fn generator(input: &str) -> Result<Vec<Valve>, AocError> {
    input_parser(by_lines(parse_valve))(input)
}

#[derive(Debug)]
struct CompressedValve<'a> {
    name: &'a str,
    flow_rate: i32,
    connections: Vec<(&'a str, i32)>,
}

struct IdCache<T> {
    cache: HashMap<T, usize>,
}

impl<T: PartialEq + Eq + Hash> IdCache<T> {
    fn new() -> Self {
        Self {
            cache: HashMap::new(),
        }
    }

    fn id(&mut self, item: T) -> usize {
        let len = self.cache.len();
        match self.cache.entry(item) {
            Entry::Vacant(e) => {
                e.insert(len);
                len
            }
            Entry::Occupied(e) => *e.get(),
        }
    }
}

fn compress<'a>(valves: &'a [Valve]) -> HashMap<&'a str, CompressedValve<'a>> {
    let valves: HashMap<&str, &Valve> = valves.iter().map(|v| (v.name, v)).collect();
    let mut compressed = HashMap::new();

    for &k in valves
        .keys()
        .filter(|&&k| k == "AA" || valves[k].flow_rate > 0)
    {
        let predecessors = dijkstra_all(&k, |n| {
            if *n != k && valves[n].flow_rate > 0 {
                return vec![];
            }

            valves[n].connections.iter().map(|e| (*e, 1)).collect()
        });

        let exit = predecessors
            .iter()
            .filter(|(n, _)| valves[**n].flow_rate > 0)
            .map(|(n, (_, c))| (*n, *c))
            .collect();

        compressed.insert(
            k,
            CompressedValve {
                name: k,
                flow_rate: valves[k].flow_rate,
                connections: exit,
            },
        );
    }

    compressed
}

struct IdValve {
    name: usize,
    flow_rate: i32,
    connections: Vec<(usize, i32)>,
}

fn identifiers(valves: &HashMap<&str, CompressedValve>) -> (HashMap<usize, IdValve>, usize) {
    let mut cache = IdCache::new();

    for &name in valves.keys() {
        cache.id(name);
    }

    let id_valves = valves
        .values()
        .map(|v| {
            (
                cache.id(v.name),
                IdValve {
                    name: cache.id(v.name),
                    flow_rate: v.flow_rate,
                    connections: v
                        .connections
                        .iter()
                        .map(|(n, c)| (cache.id(n), *c))
                        .collect(),
                },
            )
        })
        .collect();

    let start = cache.id("AA");

    (id_valves, start)
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
struct State {
    remaining: i32,
    current: (usize, i32),
    opened: BitSet,
}

impl State {
    const fn new(start: usize) -> Self {
        Self {
            remaining: 30,
            current: (start, 0),
            opened: BitSet::new(),
        }
    }

    const fn opened(&self, valve: usize) -> bool {
        self.opened.contains(valve)
    }

    fn moves(&self, valves: &HashMap<usize, IdValve>) -> Vec<(Self, i32)> {
        let mut nexts = vec![];
        let (dest, distance) = self.current;

        if distance > 0 {
            let mut next = self.clone();
            next.remaining -= 1;
            next.current = (dest, distance - 1);
            nexts.push((next, 0));
            return nexts;
        }

        if !self.opened(dest) {
            let mut next = self.clone();
            next.remaining -= 1;
            next.opened.insert(dest);
            let cost = next.remaining * valves[&dest].flow_rate;
            nexts.push((next, -cost));
        }

        for (next_dest, next_distance) in &valves[&dest].connections {
            let mut next = self.clone();
            next.remaining -= 1;
            next.current = (*next_dest, next_distance - 1);
            nexts.push((next, 0));
        }

        nexts
    }

    fn finished(&self, valves: &HashMap<usize, IdValve>) -> bool {
        assert!(self.remaining >= 0);
        self.remaining == 0 || self.opened.len() == valves.len()
    }

    fn heuristic(&self, sorted_by_flow: &[(usize, i32)]) -> i32 {
        let mut h = 0;
        let mut r = self.remaining;
        let mut i = 0;

        while r >= 1 && i < sorted_by_flow.len() {
            let inc = sorted_by_flow[i..]
                .iter()
                .position(|(name, _)| !self.opened(*name));

            if let Some(inc) = inc {
                i += inc;
            } else {
                break;
            }

            r -= 1;
            h += sorted_by_flow[i].1 * r;
            i += 1;
            r -= 1;
        }

        -h
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct BitSet(u64);

impl BitSet {
    const fn new() -> Self {
        Self(0)
    }

    fn insert(&mut self, k: usize) {
        self.0 |= 0b1 << k;
    }

    const fn contains(self, k: usize) -> bool {
        self.0 & (0b1 << k) != 0
    }

    const fn len(self) -> usize {
        self.0.count_ones() as usize
    }
}

#[derive(Clone, PartialEq, Eq, Hash)]
struct ElephantState {
    remaining: i32,
    you: (usize, i32),
    elephant: (usize, i32),
    opened: BitSet,
}

impl ElephantState {
    const fn new(start: usize) -> Self {
        Self {
            remaining: 26,
            you: (start, 0),
            elephant: (start, 0),
            opened: BitSet::new(),
        }
    }

    const fn opened(&self, valve: usize) -> bool {
        self.opened.contains(valve)
    }

    fn finished(&self, valves: &HashMap<usize, IdValve>) -> bool {
        assert!(self.remaining >= 0);
        self.remaining == 0 || self.opened.len() == valves.len()
    }

    fn you_moves(&self, valves: &HashMap<usize, IdValve>) -> Vec<(Self, i32)> {
        let mut nexts = vec![];
        let (dest, distance) = self.you;

        if distance > 0 {
            let mut next = self.clone();
            next.you = (dest, distance - 1);
            nexts.push((next, 0));
            return nexts;
        }

        if !self.opened(dest) {
            let mut next = self.clone();
            next.opened.insert(dest);
            let cost = next.remaining * valves[&dest].flow_rate;
            nexts.push((next, -cost));
        }

        for (next_dest, next_distance) in &valves[&dest].connections {
            let mut next = self.clone();
            next.you = (*next_dest, next_distance - 1);
            nexts.push((next, 0));
        }

        nexts
    }

    fn elephant_moves(&self, valves: &HashMap<usize, IdValve>) -> Vec<(Self, i32)> {
        let mut nexts = vec![];
        let (dest, distance) = self.elephant;

        if distance > 0 {
            let mut next = self.clone();
            next.elephant = (dest, distance - 1);
            nexts.push((next, 0));
            return nexts;
        }

        if !self.opened(dest) {
            let mut next = self.clone();
            next.opened.insert(dest);
            let cost = next.remaining * valves[&dest].flow_rate;
            nexts.push((next, -cost));
        }

        for (next_dest, next_distance) in &valves[&dest].connections {
            let mut next = self.clone();
            next.elephant = (*next_dest, next_distance - 1);
            nexts.push((next, 0));
        }

        nexts
    }

    fn moves(&self, valves: &HashMap<usize, IdValve>) -> Vec<(Self, i32)> {
        let mut moved = self.clone();
        moved.remaining -= 1;

        moved
            .you_moves(valves)
            .into_iter()
            .flat_map(|(n, c0)| {
                n.elephant_moves(valves)
                    .into_iter()
                    .map(move |(n, c1)| (n, c0 + c1))
            })
            .collect()
    }

    fn heuristic(&self, sorted_by_flow: &[(usize, i32)], valves: &HashMap<usize, IdValve>) -> i32 {
        let state = State {
            remaining: self.remaining,
            current: self.you,
            opened: self.opened,
        };

        let (_, you_cost) = astar(
            &state,
            |state| state.moves(valves),
            |state| state.heuristic(sorted_by_flow),
            |state| state.finished(valves),
        )
        .unwrap();

        let state = State {
            remaining: self.remaining,
            current: self.elephant,
            opened: self.opened,
        };

        let (_, elephant_cost) = astar(
            &state,
            |state| state.moves(valves),
            |state| state.heuristic(sorted_by_flow),
            |state| state.finished(valves),
        )
        .unwrap();

        you_cost + elephant_cost
    }
}

fn part_one(valves: &HashMap<usize, IdValve>, start: usize) -> i32 {
    let mut sorted_by_flow: Vec<(usize, i32)> =
        valves.values().map(|v| (v.name, v.flow_rate)).collect();
    sorted_by_flow.sort_by_key(|(_, f)| *f);
    sorted_by_flow.reverse();

    let (_, cost) = astar(
        &State::new(start),
        |state| state.moves(valves),
        |state| state.heuristic(&sorted_by_flow),
        |state| state.finished(valves),
    )
    .unwrap();

    -cost
}

fn part_two(valves: &HashMap<usize, IdValve>, start: usize) -> i32 {
    let mut sorted_by_flow: Vec<(usize, i32)> =
        valves.values().map(|v| (v.name, v.flow_rate)).collect();
    sorted_by_flow.sort_by_key(|(_, f)| *f);
    sorted_by_flow.reverse();

    let (_, cost) = astar(
        &ElephantState::new(start),
        |state| state.moves(valves),
        |state| state.heuristic(&sorted_by_flow, valves),
        |state| state.finished(valves),
    )
    .unwrap();

    -cost
}

#[aoc(day16, part1)]
fn part01(input: &str) -> Result<i32, AocError> {
    let valves = generator(input)?;
    let compressed = compress(&valves);
    let (ids, start) = identifiers(&compressed);

    Ok(part_one(&ids, start))
}

#[aoc(day16, part2)]
fn part02(input: &str) -> Result<i32, AocError> {
    let valves = generator(input)?;
    let compressed = compress(&valves);
    let (ids, start) = identifiers(&compressed);

    Ok(part_two(&ids, start))
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"Valve AA has flow rate=0; tunnels lead to valves DD, II, BB
Valve BB has flow rate=13; tunnels lead to valves CC, AA
Valve CC has flow rate=2; tunnels lead to valves DD, BB
Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE
Valve EE has flow rate=3; tunnels lead to valves FF, DD
Valve FF has flow rate=0; tunnels lead to valves EE, GG
Valve GG has flow rate=0; tunnels lead to valves FF, HH
Valve HH has flow rate=22; tunnel leads to valve GG
Valve II has flow rate=0; tunnels lead to valves AA, JJ
Valve JJ has flow rate=21; tunnel leads to valve II";

    #[test]
    fn test16_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test1601() {
        let valves = generator(BUILD_INPUT).unwrap();
        let compressed = compress(&valves);
        let (ids, start) = identifiers(&compressed);
        assert_eq!(part_one(&ids, start), 1651);
    }

    #[test]
    fn test1602() {
        let valves = generator(BUILD_INPUT).unwrap();
        let compressed = compress(&valves);
        let (ids, start) = identifiers(&compressed);
        assert_eq!(part_two(&ids, start), 1707);
    }
}
