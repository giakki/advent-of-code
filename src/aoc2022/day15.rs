/** Correct results:
 * - 4876693
 * - 11645454855041
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_signed_int},
    point::{Point, PointOps},
};
use itertools::Itertools;
use nom::{
    bytes::complete::tag,
    sequence::{preceded, separated_pair, tuple},
    IResult,
};
use std::collections::HashSet;

type SensorAndBeacon = (Point<isize>, Point<isize>);

fn parse_coord(s: &str) -> IResult<&str, Point<isize>> {
    separated_pair(
        preceded(tag("x="), parse_signed_int),
        tag(", "),
        preceded(tag("y="), parse_signed_int),
    )(s)
}

#[aoc_generator(day15)]
fn generator(input: &str) -> Result<Vec<SensorAndBeacon>, AocError> {
    input_parser(by_lines(tuple((
        preceded(tag("Sensor at "), parse_coord),
        preceded(tag(": closest beacon is at "), parse_coord),
    ))))(input)
}

fn run_part_1(input: &[SensorAndBeacon], row: isize) -> isize {
    let mut ranges = Vec::new();

    let sorted_ranges = input
        .iter()
        .filter_map(|(sensor, beacon)| {
            let beacon_dist = sensor.manhattan_distance(beacon);
            let range_width = beacon_dist - sensor.y().abs_diff(row) as isize;
            if range_width < 0 {
                None
            } else {
                Some((sensor.x() - range_width, sensor.x() + range_width))
            }
        })
        .sorted();

    for (start, end) in sorted_ranges {
        if ranges.is_empty() {
            ranges.push((start, end));
        } else {
            let last_range = ranges.last_mut().unwrap();
            if start <= last_range.1 + 1 {
                last_range.1 = isize::max(last_range.1, end);
            } else {
                ranges.push((start, end));
            }
        }
    }

    let beacons_in_row = input
        .iter()
        .filter_map(|(_, beacon)| {
            if beacon.y() == row {
                Some(beacon.x())
            } else {
                None
            }
        })
        .collect::<HashSet<_>>()
        .len() as isize;

    let row_coverage = ranges
        .into_iter()
        .map(|range| range.1 - range.0 + 1)
        .sum::<isize>();

    row_coverage - beacons_in_row
}

fn run_part_2(input: &[SensorAndBeacon], n: isize) -> Option<isize> {
    let sensors_and_distances = input
        .iter()
        .map(|(s, b)| (s, s.manhattan_distance(b)))
        .collect_vec();

    let mut a_coeffs = HashSet::new();
    let mut b_coeffs = HashSet::new();

    for (sensor, distance) in &sensors_and_distances {
        a_coeffs.insert(sensor.y() - sensor.x() + distance + 1);
        a_coeffs.insert(sensor.y() - sensor.x() - distance - 1);
        b_coeffs.insert(sensor.y() + sensor.x() + distance + 1);
        b_coeffs.insert(sensor.y() + sensor.x() - distance - 1);
    }

    for a in a_coeffs {
        for b in &b_coeffs {
            if a - b % 2 == 1 || a >= *b {
                continue;
            }

            let point = ((b - a) / 2, (a + b) / 2);
            if point.x() > 0
                && point.y() > 0
                && point.x() <= n
                && point.y() <= n
                && sensors_and_distances
                    .iter()
                    .all(|(sensor, distance)| sensor.manhattan_distance(&point) > *distance)
            {
                return Some(point.x() * 4_000_000 + point.y());
            }
        }
    }

    None
}

#[aoc(day15, part1)]
fn part01(input: &[SensorAndBeacon]) -> isize {
    run_part_1(input, 2_000_000)
}

#[aoc(day15, part2)]
fn part02(input: &[SensorAndBeacon]) -> Option<isize> {
    run_part_2(input, 4_000_000)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"Sensor at x=2, y=18: closest beacon is at x=-2, y=15
Sensor at x=9, y=16: closest beacon is at x=10, y=16
Sensor at x=13, y=2: closest beacon is at x=15, y=3
Sensor at x=12, y=14: closest beacon is at x=10, y=16
Sensor at x=10, y=20: closest beacon is at x=10, y=16
Sensor at x=14, y=17: closest beacon is at x=10, y=16
Sensor at x=8, y=7: closest beacon is at x=2, y=10
Sensor at x=2, y=0: closest beacon is at x=2, y=10
Sensor at x=0, y=11: closest beacon is at x=2, y=10
Sensor at x=20, y=14: closest beacon is at x=25, y=17
Sensor at x=17, y=20: closest beacon is at x=21, y=22
Sensor at x=16, y=7: closest beacon is at x=15, y=3
Sensor at x=14, y=3: closest beacon is at x=15, y=3
Sensor at x=20, y=1: closest beacon is at x=15, y=3";

    #[test]
    fn test015_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input);
    }

    #[test]
    fn test01501() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = run_part_1(&input, 10);

        assert_eq!(res, 26);
    }

    #[test]
    fn test01502() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = run_part_2(&input, 20);

        assert_eq!(res, Some(56000011));
    }
}
