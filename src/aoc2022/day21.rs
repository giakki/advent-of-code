/** Correct results:
 * - 21208142603224
 * - 3882224466191
 */
use crate::common::{
    chr::FromChar,
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use itertools::Itertools;
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{alpha1, char, one_of},
    combinator::{map, map_res},
    sequence::{delimited, separated_pair, tuple},
    IResult,
};
use std::collections::HashMap;

#[derive(Debug, Clone, Copy)]
enum Operation {
    Add,
    Sub,
    Mul,
    Div,
}

impl FromChar for Operation {
    type Err = AocError;

    fn from_char(c: &char) -> Result<Self, Self::Err> {
        match c {
            '+' => Ok(Self::Add),
            '-' => Ok(Self::Sub),
            '*' => Ok(Self::Mul),
            '/' => Ok(Self::Div),
            _ => Err(AocError::ParseError(format!(
                "Cannot parse Operation '{c}'"
            ))),
        }
    }
}

#[derive(Debug, Clone, Copy)]
enum Job<T> {
    Variable,
    Literal(i64),
    Rec(T, Operation, T),
}

impl<T> Job<T> {
    const fn get_as_literal(&self) -> Result<i64, AocError> {
        match self {
            Self::Literal(n) => Ok(*n),
            _ => Err(AocError::Unspecified),
        }
    }

    fn get_as_rec(&self) -> Result<(T, Operation, T), AocError>
    where
        T: Clone,
    {
        match self {
            Self::Rec(lhs, op, rhs) => Ok((lhs.clone(), *op, rhs.clone())),
            _ => Err(AocError::Unspecified),
        }
    }
}

fn parse_job(s: &str) -> IResult<&str, Job<&str>> {
    alt((
        map(parse_int, Job::Literal),
        map(
            tuple((
                alpha1,
                delimited(
                    char(' '),
                    map_res(one_of("+-*/"), |c| Operation::from_char(&c)),
                    char(' '),
                ),
                alpha1,
            )),
            |(n1, op, n2)| Job::Rec(n1, op, n2),
        ),
    ))(s)
}

fn generator(input: &str) -> Result<(Vec<Job<usize>>, usize, usize), AocError> {
    let str_monkes = input_parser(by_lines(separated_pair(alpha1, tag(": "), parse_job)))(input)?;
    let lookup = str_monkes
        .iter()
        .enumerate()
        .map(|(i, (m, _))| (*m, i))
        .collect::<HashMap<_, _>>();

    let int_monkes = str_monkes
        .into_iter()
        .map(|(_, j)| match j {
            Job::Literal(n) => Job::Literal(n),
            Job::Rec(a, op, b) => Job::Rec(lookup[a], op, lookup[b]),
            Job::Variable => unreachable!(),
        })
        .collect_vec();

    Ok((int_monkes, lookup["root"], lookup["humn"]))
}

fn eval_monke(monke: usize, monkes: &[Job<usize>]) -> i64 {
    match &monkes[monke] {
        Job::Literal(n) => *n,
        Job::Rec(a, op, b) => {
            let a = eval_monke(*a, monkes);
            let b = eval_monke(*b, monkes);
            match op {
                Operation::Add => a + b,
                Operation::Sub => a - b,
                Operation::Mul => a * b,
                Operation::Div => a / b,
            }
        }
        Job::Variable => unreachable!(),
    }
}

fn simplify_monkes((idx, job): (usize, Job<usize>), monkes: &mut [Job<usize>]) {
    if let Job::Rec(lhs_idx, op, rhs_idx) = job {
        let lhs = monkes[lhs_idx];
        let rhs = monkes[rhs_idx];
        simplify_monkes((lhs_idx, lhs), monkes);
        simplify_monkes((rhs_idx, rhs), monkes);

        if let (Job::Literal(a), Job::Literal(b)) = (monkes[lhs_idx], monkes[rhs_idx]) {
            let result = match op {
                Operation::Add => a + b,
                Operation::Sub => a - b,
                Operation::Mul => a * b,
                Operation::Div => a / b,
            };

            monkes[idx] = Job::Literal(result);
        }
    }
}

fn solve_for_monke(idx: usize, monkes: &mut [Job<usize>]) -> Result<i64, AocError> {
    simplify_monkes((idx, monkes[idx]), monkes);

    let (lhs, _, rhs) = monkes[idx].get_as_rec()?;

    let mut value_on_lhs = matches!(monkes[lhs], Job::Literal(_));
    let mut value = monkes[if value_on_lhs { lhs } else { rhs }].get_as_literal()?;
    let mut monke = if value_on_lhs { rhs } else { lhs };

    while !matches!(monkes[monke], Job::Variable) {
        let (lhs, op, rhs) = monkes[monke].get_as_rec()?;
        value_on_lhs = matches!(monkes[lhs], Job::Literal(_));
        let paired = monkes[if value_on_lhs { lhs } else { rhs }].get_as_literal()?;

        value = match op {
            Operation::Add => value - paired,
            Operation::Sub => (if value_on_lhs { -value } else { value }) + paired,
            Operation::Mul => value / paired,
            Operation::Div => value * paired,
        };

        // update the index to traverse "down" the tree
        monke = if value_on_lhs { rhs } else { lhs };
    }

    Ok(value)
}

#[aoc(day21, part1)]
fn part01(input: &str) -> Result<i64, AocError> {
    let (monkes, root_idx, _) = generator(input)?;

    Ok(eval_monke(root_idx, &monkes))
}

#[aoc(day21, part2)]
fn part02(input: &str) -> Result<i64, AocError> {
    let (mut monkes, root_idx, humn_idx) = generator(input)?;

    monkes[humn_idx] = Job::Variable;

    solve_for_monke(root_idx, &mut monkes)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"root: pppw + sjmn
dbpl: 5
cczh: sllz + lgvd
zczc: 2
ptdq: humn - dvpt
dvpt: 3
lfqf: 4
humn: 5
ljgn: 2
sjmn: drzm * dbpl
sllz: 4
pppw: cczh / lfqf
lgvd: ljgn * ptdq
drzm: hmdt - zczc
hmdt: 32";

    #[test]
    fn test21_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test2101() {
        let res = part01(BUILD_INPUT).unwrap();

        assert_eq!(res, 152);
    }

    #[test]
    fn test2102() {
        let res = part02(BUILD_INPUT).unwrap();

        assert_eq!(res, 301);
    }
}
