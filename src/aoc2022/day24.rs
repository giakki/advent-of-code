/** Correct results:
 * - 247
 * - 728
 */
use crate::common::{
    direction::Direction,
    errors::AocError,
    point::{Point, PointOps},
};
use num::Integer;
use std::{
    cmp::Ordering,
    collections::{HashMap, HashSet, VecDeque},
};

type PuzzleInput = (Point<i32>, HashSet<(Direction, Point<i32>)>);

#[aoc_generator(day24)]
fn generator(input: &str) -> Result<PuzzleInput, AocError> {
    let size_y = input.lines().count() - 2;
    let size_x = input.lines().map(str::len).max().ok_or(AocError::Input)? - 2;

    let blizzards = input
        .lines()
        .skip(1)
        .take(size_y)
        .enumerate()
        .flat_map(|(y, line)| {
            line.chars()
                .skip(1)
                .take(size_x)
                .enumerate()
                .filter_map(move |(x, c)| {
                    if c == '.' {
                        Direction::from_arrow(&c)
                            .map_or(None, |dir| Some((dir, (x as i32, y as i32))))
                    } else {
                        None
                    }
                })
        })
        .collect::<HashSet<_>>();

    Ok(((size_x as i32, size_y as i32), blizzards))
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
struct State {
    minutes_elapsed: i32,
    position: Point<i32>,
}

impl Ord for State {
    fn cmp(&self, other: &Self) -> Ordering {
        (other.minutes_elapsed)
            .cmp(&self.minutes_elapsed)
            .then_with(|| self.position.cmp(&other.position))
    }
}

impl PartialOrd for State {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn has_blizzard(
    state: &State,
    map_size: Point<i32>,
    blizzards: &HashSet<(Direction, (i32, i32))>,
) -> bool {
    blizzards.contains(&(
        Direction::Up,
        (
            state.position.x(),
            (state.position.y() + state.minutes_elapsed).rem_euclid(map_size.y()),
        ),
    )) || blizzards.contains(&(
        Direction::Down,
        (
            state.position.x(),
            (state.position.y() - state.minutes_elapsed).rem_euclid(map_size.y()),
        ),
    )) || blizzards.contains(&(
        Direction::Left,
        (
            (state.position.x() + state.minutes_elapsed).rem_euclid(map_size.x()),
            state.position.y(),
        ),
    )) || blizzards.contains(&(
        Direction::Right,
        (
            (state.position.x() - state.minutes_elapsed).rem_euclid(map_size.x()),
            state.position.y(),
        ),
    ))
}

fn pathfind(
    start: State,
    end: Point<i32>,
    map_size: Point<i32>,
    blizzards: &HashSet<(Direction, Point<i32>)>,
) -> Result<usize, AocError> {
    let entrance = (0, -1);
    let exit = map_size.add(&(-1, 0));
    let size_mod = map_size.x().lcm(&map_size.y());

    let mut queue = VecDeque::new();
    queue.push_back(start);

    let mut cache = HashMap::new();
    cache.insert(
        (start.position, start.minutes_elapsed % size_mod),
        start.minutes_elapsed,
    );

    let mut states_to_from = HashMap::<State, State>::new();

    while let Some(current) = queue.pop_front() {
        if current.position == end {
            let mut path_len = 1;
            let mut iter = current;
            while let Some(s) = states_to_from.get(&iter) {
                path_len += 1;
                iter = *s;
            }

            return Ok(path_len);
        }

        if let Some(time) = cache.get(&(current.position, current.minutes_elapsed % size_mod)) {
            if current.minutes_elapsed > *time {
                continue;
            }
        }

        for next_position in current
            .position
            .neighbors()
            .into_iter()
            .chain(std::iter::once(current.position))
        {
            if (next_position.x() < 0
                || next_position.x() == map_size.x()
                || next_position.y() < 0
                || next_position.y() == map_size.y())
                && next_position != entrance
                && next_position != exit
            {
                continue;
            }

            let next_state = State {
                minutes_elapsed: current.minutes_elapsed + 1,
                position: next_position,
            };

            if has_blizzard(&next_state, map_size, blizzards) {
                continue;
            }

            let time = cache
                .entry((next_state.position, next_state.minutes_elapsed % size_mod))
                .or_insert(i32::MAX);

            if next_state.minutes_elapsed < *time {
                queue.push_back(next_state);
                states_to_from.insert(next_state, current);
                *time = next_state.minutes_elapsed;
            }
        }
    }
    Err(AocError::NoSolution)
}

#[aoc(day24, part1)]
fn part01((map_size, blizzards): &PuzzleInput) -> Result<usize, AocError> {
    pathfind(
        State {
            minutes_elapsed: 0,
            position: (0, -1),
        },
        (map_size.x() - 1, map_size.y() - 1),
        *map_size,
        blizzards,
    )
}

#[aoc(day24, part2)]
fn part02((map_size, blizzards): &PuzzleInput) -> Result<usize, AocError> {
    let minutes_1 = pathfind(
        State {
            minutes_elapsed: 0,
            position: (0, -1),
        },
        (map_size.x() - 1, map_size.y() - 1),
        *map_size,
        blizzards,
    )?;

    let minutes_2 = pathfind(
        State {
            minutes_elapsed: minutes_1 as i32,
            position: (map_size.x() - 1, map_size.y()),
        },
        (0, 0),
        *map_size,
        blizzards,
    )?;

    let minutes_3 = pathfind(
        State {
            minutes_elapsed: (minutes_1 + minutes_2) as i32,
            position: (0, -1),
        },
        (map_size.x() - 1, map_size.y() - 1),
        *map_size,
        blizzards,
    )?;

    Ok(minutes_1 + minutes_2 + minutes_3)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"#.######
#>>.<^<#
#.<..<<#
#>v.><>#
#<^v^^>#
######.#";

    #[test]
    fn test240_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test24001() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input).unwrap();

        assert_eq!(res, 18);
    }

    #[test]
    fn test24002() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input).unwrap();

        assert_eq!(res, 54);
    }
}
