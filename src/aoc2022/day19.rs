/** Correct results:
 * - 1466
 * - 8250
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use nom::{
    bytes::complete::tag,
    combinator::map,
    sequence::{delimited, separated_pair, tuple},
};
use std::collections::HashSet;

macro_rules! min {
    ($x: expr) => ($x);
    ($x: expr, $($z: expr),+) => (::std::cmp::min($x, min!($($z),*)));
}

macro_rules! max {
    ($x: expr) => ($x);
    ($x: expr, $($z: expr),+) => (::std::cmp::max($x, max!($($z),*)));
}

#[derive(Debug)]
struct Blueprint {
    id: u32,
    recipes: [[u32; 3]; 4],
}

impl Blueprint {
    pub fn max_robots(&self) -> [u32; 3] {
        [
            max!(
                self.recipes[0][0],
                self.recipes[1][0],
                self.recipes[2][0],
                self.recipes[3][0]
            ),
            self.recipes[2][1],
            self.recipes[3][2],
        ]
    }
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
struct State {
    robots: [u32; 4],
    inventory: [u32; 4],
    minutes_remaining: u32,
}

impl State {
    const fn initial(minutes_remaining: u32) -> Self {
        Self {
            robots: [1, 0, 0, 0],
            inventory: [0, 0, 0, 0],
            minutes_remaining,
        }
    }

    #[allow(clippy::nonminimal_bool)]
    const fn can_build(&self, recipe: &[u32; 3]) -> bool {
        self.inventory[0] >= recipe[0]
            && self.inventory[1] >= recipe[1]
            && self.inventory[2] >= recipe[2]
    }

    fn prune_resources(&mut self, max_robots: &[u32]) {
        self.inventory[0] = min!(self.inventory[0], max_robots[0] * 2);
        self.inventory[1] = min!(self.inventory[1], max_robots[1] * 2);
        self.inventory[2] = min!(self.inventory[2], max_robots[2] * 2);
    }

    const fn max_theoretical_possible(&self) -> u32 {
        let with_current_robots = self.robots[3] * self.minutes_remaining;
        let building_a_bot_every_turn = ((self.minutes_remaining - 1) * self.minutes_remaining) / 2;

        with_current_robots + building_a_bot_every_turn + self.inventory[3]
    }

    const fn mine(&self) -> Self {
        Self {
            minutes_remaining: self.minutes_remaining - 1,
            robots: self.robots,
            inventory: [
                self.inventory[0] + self.robots[0],
                self.inventory[1] + self.robots[1],
                self.inventory[2] + self.robots[2],
                self.inventory[3] + self.robots[3],
            ],
        }
    }

    const fn build_robot_and_mine(&self, recipe: &[u32; 3], robot_idx: usize) -> Self {
        let mut new_self = self.mine();

        new_self.inventory[0] -= recipe[0];
        new_self.inventory[1] -= recipe[1];
        new_self.inventory[2] -= recipe[2];

        new_self.robots[robot_idx] += 1;

        new_self
    }
}

fn run_blueprint(blueprint: &Blueprint, total_minutes: u32) -> u32 {
    let max_robots = blueprint.max_robots();

    let mut seen = HashSet::new();
    let mut queue = Vec::new();
    let mut max = 0;
    queue.push(State::initial(total_minutes));

    while let Some(mut s) = queue.pop() {
        if s.minutes_remaining == 0 {
            max = u32::max(max, s.inventory[3]);
            continue;
        }
        if s.max_theoretical_possible() < max {
            continue;
        }

        s.prune_resources(&max_robots);

        if seen.contains(&s) {
            continue;
        }

        seen.insert(s.clone());

        if s.can_build(&blueprint.recipes[3]) {
            queue.push(s.build_robot_and_mine(&blueprint.recipes[3], 3));
            continue;
        }

        for (robot_idx, recipe) in blueprint.recipes.iter().take(3).enumerate() {
            if s.robots[robot_idx] >= max_robots[robot_idx] {
                continue;
            }

            if s.can_build(recipe) {
                queue.push(s.build_robot_and_mine(recipe, robot_idx));
                continue;
            }
        }

        queue.push(s.mine());
    }

    max
}

#[aoc_generator(day19)]
fn generator(input: &str) -> Result<Vec<Blueprint>, AocError> {
    input_parser(by_lines(map(
        tuple((
            (delimited(tag("Blueprint "), parse_int, tag(": "))),
            (delimited(tag("Each ore robot costs "), parse_int, tag(" ore. "))),
            (delimited(tag("Each clay robot costs "), parse_int, tag(" ore. "))),
            (delimited(
                tag("Each obsidian robot costs "),
                separated_pair(parse_int, tag(" ore and "), parse_int),
                tag(" clay. "),
            )),
            (delimited(
                tag("Each geode robot costs "),
                separated_pair(parse_int, tag(" ore and "), parse_int),
                tag(" obsidian."),
            )),
        )),
        |(id, ore_cost, clay_cost, obsidian_cost, geode_cost)| Blueprint {
            id,
            recipes: [
                [ore_cost, 0, 0],
                [clay_cost, 0, 0],
                [obsidian_cost.0, obsidian_cost.1, 0],
                [geode_cost.0, 0, geode_cost.1],
            ],
        },
    )))(input)
}

#[aoc(day19, part1)]
fn part01(input: &[Blueprint]) -> u32 {
    input.iter().map(|b| b.id * run_blueprint(b, 24)).sum()
}

#[aoc(day19, part2)]
fn part02(input: &[Blueprint]) -> u32 {
    input.iter().take(3).map(|b| run_blueprint(b, 32)).product()
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 2 ore. Each obsidian robot costs 3 ore and 14 clay. Each geode robot costs 2 ore and 7 obsidian.
Blueprint 2: Each ore robot costs 2 ore. Each clay robot costs 3 ore. Each obsidian robot costs 3 ore and 8 clay. Each geode robot costs 3 ore and 12 obsidian.";

    #[test]
    fn test19_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test1901() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 33);
    }

    #[test]
    fn test1902() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, 62);
    }
}
