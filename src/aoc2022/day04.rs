/** Correct results:
 * - 595
 * - 952
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
    point::Point,
};
use nom::{
    character::complete::char,
    sequence::{preceded, tuple},
    IResult,
};

type Elves = (Point<u32>, Point<u32>);

fn parse_interval(s: &str) -> IResult<&str, (u32, u32)> {
    tuple((parse_int, preceded(char('-'), parse_int)))(s)
}

const fn interval_is_contained(a: Point<u32>, b: Point<u32>) -> bool {
    (a.0 <= b.0 && a.1 >= b.1) || (b.0 <= a.0 && b.1 >= a.1)
}

const fn interval_overlaps(a: Point<u32>, b: Point<u32>) -> bool {
    a.0 <= b.1 && a.1 >= b.0
}

#[aoc_generator(day4)]
fn generator(input: &str) -> Result<Vec<Elves>, AocError> {
    input_parser(by_lines(tuple((
        parse_interval,
        preceded(char(','), parse_interval),
    ))))(input)
}

#[aoc(day4, part1)]
fn part01(input: &[Elves]) -> usize {
    input
        .iter()
        .filter(|(a, b)| interval_is_contained(*a, *b))
        .count()
}

#[aoc(day4, part2)]
fn part02(input: &[Elves]) -> usize {
    input
        .iter()
        .filter(|(a, b)| interval_overlaps(*a, *b))
        .count()
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8";

    #[test]
    fn test04_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test0401() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 2);
    }

    #[test]
    fn test0402() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, 4);
    }
}
