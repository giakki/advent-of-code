/** Correct results:
 * - 4364
 * - 2508
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
    point3d::{Point3D, Point3dOps},
};
use nom::{
    character::complete::char,
    sequence::{terminated, tuple},
};
use std::collections::{HashSet, VecDeque};

#[aoc_generator(day18)]
fn generator(input: &str) -> Result<Vec<Point3D<i32>>, AocError> {
    input_parser(by_lines(tuple((
        terminated(parse_int, char(',')),
        terminated(parse_int, char(',')),
        parse_int,
    ))))(input)
}

#[aoc(day18, part1)]
fn part01(input: &[Point3D<i32>]) -> usize {
    let points = HashSet::<&Point3D<i32>>::from_iter(input);
    let mut covered_faces = 0;

    for point in &points {
        for neighbor in point.neighbors() {
            if points.contains(&neighbor) {
                covered_faces += 1;
            }
        }
    }

    input.len() * 6 - covered_faces
}

#[aoc(day18, part2)]
fn part02(input: &[Point3D<i32>]) -> usize {
    let (min, max) = input.iter().fold(
        (
            (i32::MAX, i32::MAX, i32::MAX),
            (i32::MIN, i32::MIN, i32::MIN),
        ),
        |(min, max), point| {
            (
                (
                    min.x().min(point.x()),
                    min.y().min(point.y()),
                    min.z().min(point.z()),
                ),
                (
                    max.x().max(point.x()),
                    max.y().max(point.y()),
                    max.z().max(point.z()),
                ),
            )
        },
    );

    let min = (min.x() - 1, min.y() - 1, min.z() - 1);
    let max = (max.x() + 1, max.y() + 1, max.z() + 1);

    let points = HashSet::<&Point3D<i32>>::from_iter(input);
    let mut fill = HashSet::<Point3D<i32>>::new();
    let mut queue = VecDeque::new();

    fill.insert(min);
    queue.push_back(min);

    while let Some(coord) = queue.pop_front() {
        fill.insert(coord);

        for neighbor in coord.neighbors() {
            if !neighbor.is_inside(&min, &max) {
                continue;
            }

            if !fill.contains(&neighbor) && !points.contains(&neighbor) {
                fill.insert(neighbor);
                queue.push_back(neighbor);
            }
        }
    }

    let mut faces = 0;
    for point in points {
        for neighbor in point.neighbors() {
            if fill.contains(&neighbor) {
                faces += 1;
            }
        }
    }

    faces
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"2,2,2
1,2,2
3,2,2
2,1,2
2,3,2
2,2,1
2,2,3
2,2,4
2,2,6
1,2,5
3,2,5
2,1,5
2,3,5";

    #[test]
    fn test18_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test1801() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 64);
    }

    #[test]
    fn test1802() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, 58);
    }
}
