/** Correct results:
 * - 70369
 * - 203002
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use itertools::Itertools;
use nom::{bytes::complete::tag, multi::separated_list0};

#[aoc_generator(day1)]
fn generator(input: &str) -> Result<Vec<Vec<u32>>, AocError> {
    input_parser(separated_list0(tag("\n\n"), by_lines(parse_int)))(input)
}

#[aoc(day1, part1)]
fn part01(input: &[Vec<u32>]) -> Result<u32, AocError> {
    input
        .iter()
        .map(|e| e.iter().sum())
        .max()
        .ok_or(AocError::Input)
}

#[aoc(day1, part2)]
fn part02(input: &[Vec<u32>]) -> u32 {
    input
        .iter()
        .map(|e| e.iter().sum())
        .sorted_by(|a: &u32, b: &u32| b.cmp(a))
        .take(3)
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"1000
2000
3000

4000

5000
6000

7000
8000
9000

10000";

    #[test]
    fn test00_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test0101() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input).unwrap();

        assert_eq!(res, 24000);
    }

    #[test]
    fn test0002() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, 45000);
    }
}
