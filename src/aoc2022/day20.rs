/** Correct results:
 * - 3473
 * - 7496649006261
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_signed_int},
};
use itertools::Itertools;
use std::collections::VecDeque;

#[aoc_generator(day20)]
fn generator(input: &str) -> Result<Vec<i64>, AocError> {
    input_parser(by_lines(parse_signed_int))(input)
}

fn mix(input: &[i64], num_mixes: u32) -> Result<i64, AocError> {
    let mut indexes = (0..input.len()).collect::<VecDeque<_>>();

    for _ in 0..num_mixes {
        for (original_idx, x) in input.iter().enumerate() {
            let current_num_idx = indexes
                .iter()
                .position(|n| *n == original_idx)
                .ok_or(AocError::Input)?;

            indexes.remove(current_num_idx);
            let new_num_idx =
                (current_num_idx as i64 + *x).rem_euclid(indexes.len() as i64) as usize;
            indexes.insert(new_num_idx, original_idx);
        }
    }

    let original_zero_idx = input.iter().position(|&i| i == 0).unwrap();
    let current_zero_idx = indexes
        .iter()
        .position(|&i| i == original_zero_idx)
        .unwrap();

    Ok([1000, 2000, 3000]
        .into_iter()
        .map(|i| input[indexes[(current_zero_idx + i) % indexes.len()]])
        .sum())
}

#[aoc(day20, part1)]
fn part01(input: &[i64]) -> Result<i64, AocError> {
    mix(input, 1)
}

#[aoc(day20, part2)]
fn part02(input: &[i64]) -> Result<i64, AocError> {
    let input = input.iter().map(|n| n * 811_589_153).collect_vec();

    mix(&input, 10)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"1
2
-3
3
-2
0
4";

    #[test]
    fn test20_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test2001() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input).unwrap();

        assert_eq!(res, 3);
    }

    #[test]
    fn test2002() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input).unwrap();

        assert_eq!(res, 1623178306);
    }
}
