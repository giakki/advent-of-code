/** Correct results:
 * - 8053
 * - 2425
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser},
};
use itertools::Itertools;
use nom::{character::complete::alpha1, combinator::map};

const fn item_value(item: u8) -> u32 {
    if item.is_ascii_lowercase() {
        (item - b'a' + 1) as u32
    } else {
        (item - b'A' + 27) as u32
    }
}

#[aoc_generator(day3)]
fn generator(input: &str) -> Result<Vec<String>, AocError> {
    input_parser(by_lines(map(alpha1, |s: &str| s.to_owned())))(input)
}

#[aoc(day3, part1)]
fn part01(input: &[String]) -> Result<u32, AocError> {
    let mut sum = 0;

    for rucksack in input {
        let (a, b) = rucksack.split_at(rucksack.len() / 2);
        let shared = a
            .as_bytes()
            .iter()
            .find(|c| b.as_bytes().contains(*c))
            .copied()
            .ok_or(AocError::Input)?;

        sum += item_value(shared);
    }

    Ok(sum)
}

#[aoc(day3, part2)]
fn part02(input: &[String]) -> Result<u32, AocError> {
    let mut sum = 0;

    for (a, b, c) in input.iter().tuples() {
        let shared = a
            .as_bytes()
            .iter()
            .find(|item| b.as_bytes().contains(*item) && c.as_bytes().contains(*item))
            .copied()
            .ok_or(AocError::Input)?;

        sum += item_value(shared);
    }

    Ok(sum)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw";

    #[test]
    fn test03_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test0301() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input).unwrap();

        assert_eq!(res, 157);
    }

    #[test]
    fn test0302() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input).unwrap();

        assert_eq!(res, 70);
    }
}
