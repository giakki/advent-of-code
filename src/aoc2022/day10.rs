/** Correct results:
 * - 14920
 * - BUCACBUZ
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_signed_int},
};
use nom::{
    branch::alt, bytes::complete::tag, character::complete::char, combinator::map,
    sequence::separated_pair,
};

#[derive(Debug, Clone, Copy)]
enum Instruction {
    Noop,
    Addx(i32),
}

struct Vm {
    running_op: Option<Instruction>,
    instructions: Vec<Instruction>,
    register: i32,
    signal_strength: i32,
    clock: i32,
}

impl Vm {
    pub fn new(instructions: Vec<Instruction>) -> Self {
        Self {
            running_op: None,
            instructions,
            register: 1,
            signal_strength: 0,
            clock: 0,
        }
    }

    pub fn tick(&mut self) {
        self.clock += 1;
        if self.clock == 20 || (self.clock + 20) % 40 == 0 {
            self.signal_strength += self.register * self.clock;
        }
        if let Some(Instruction::Addx(n)) = self.running_op {
            self.register += n;
            self.running_op = None;
        } else if let Some(Instruction::Addx(n)) = self.instructions.pop() {
            self.running_op = Some(Instruction::Addx(n));
        }
    }
}

#[aoc_generator(day10)]
fn generator(input: &str) -> Result<Vec<Instruction>, AocError> {
    input_parser(by_lines(alt((
        map(tag("noop"), |_| Instruction::Noop),
        map(
            separated_pair(tag("addx"), char(' '), parse_signed_int),
            |(_, n)| Instruction::Addx(n),
        ),
    ))))(input)
}

#[aoc(day10, part1)]
fn part01(input: &[Instruction]) -> i32 {
    let mut vm = Vm::new(input.iter().copied().rev().collect());

    for _ in 0..=220 {
        vm.tick();
    }

    vm.signal_strength
}

#[aoc(day10, part2)]
fn part02(input: &[Instruction]) -> String {
    let mut vm = Vm::new(input.iter().copied().rev().collect());

    let mut str = String::from("\n");
    while !vm.instructions.is_empty() {
        let display_clock = &vm.clock % 40;

        if vm.register.abs_diff(display_clock) < 2 {
            str.push('#');
        } else {
            str.push('.');
        }
        if display_clock % 40 == 39 {
            str.push('\n');
        }

        vm.tick();
    }

    str
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop";

    #[test]
    fn test10_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test1001() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 13140);
    }

    #[test]
    fn test1002() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(
            res,
            r"
##..##..##..##..##..##..##..##..##..##..
###...###...###...###...###...###...###.
####....####....####....####....####....
#####.....#####.....#####.....#####.....
######......######......######......####
#######.......#######.......#######.....\n
"
        );
    }
}
