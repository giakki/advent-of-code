/** Correct results:
 * - 58248
 * - 1069
 */
use crate::common::{
    chr::FromChar,
    errors::AocError,
    point::{Point, PointOps},
};
use ndarray::Array2;
use std::collections::HashMap;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Tile {
    Elf,
    Empty,
}

impl FromChar for Tile {
    type Err = AocError;

    fn from_char(c: &char) -> Result<Self, Self::Err> {
        match c {
            '#' => Ok(Self::Elf),
            '.' => Ok(Self::Empty),
            _ => Err(AocError::ParseError(format!("Cannot parse tile '{c}'"))),
        }
    }
}

fn generator(input: &str) -> Result<Array2<Tile>, AocError> {
    let y = input.lines().count();
    let x = input.len() / y;

    Ok(Array2::from_shape_vec(
        (y, x),
        input
            .lines()
            .flat_map(|l| l.chars().map(|c| Tile::from_char(&c)))
            .collect::<Result<_, _>>()?,
    )?)
}

fn run_round(elves: &mut [Point<i32>], directions: &[[Point<i32>; 3]; 4]) -> bool {
    let mut moved = false;
    let mut proposed_positions = HashMap::<Point<i32>, Vec<usize>>::new();

    for (idx, elf) in elves.iter().enumerate() {
        if !elf.all_neighbors().iter().any(|n| elves.contains(n)) {
            continue;
        }

        if let Some([new_pos, ..]) = directions
            .iter()
            .find(|d| d.iter().all(|pos| !elves.contains(&elf.add(pos))))
        {
            proposed_positions
                .entry(elf.add(new_pos))
                .or_default()
                .push(idx);
        }
    }

    for (to, from_indices) in proposed_positions {
        if let [idx] = from_indices[..] {
            elves[idx] = to;
            moved = true;
        }
    }

    moved
}

fn num_free_tiles(elves: &[Point<i32>]) -> usize {
    let mut min_y = i32::MAX;
    let mut max_y = i32::MIN;
    let mut min_x = i32::MAX;
    let mut max_x = i32::MIN;
    for elf in elves {
        min_y = i32::min(min_y, elf.y());
        max_y = i32::max(max_y, elf.y());
        min_x = i32::min(min_x, elf.x());
        max_x = i32::max(max_x, elf.x());
    }

    let mut n = 0;
    for y in min_y..=max_y {
        for x in min_x..=max_x {
            if !elves.contains(&(x, y)) {
                n += 1;
            }
        }
    }

    n
}

#[aoc(day23, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    let mut elves = generator(input)?
        .indexed_iter()
        .filter_map(|(pos, tile)| {
            if *tile == Tile::Elf {
                Some((pos.y() as i32, pos.x() as i32))
            } else {
                None
            }
        })
        .collect::<Vec<_>>();

    let mut directions = [
        [(0, -1), (1, -1), (-1, -1)],
        [(0, 1), (1, 1), (-1, 1)],
        [(-1, 0), (-1, -1), (-1, 1)],
        [(1, 0), (1, -1), (1, 1)],
    ];

    for _ in 0..10 {
        run_round(&mut elves, &directions);
        directions.rotate_left(1);
    }

    Ok(num_free_tiles(&elves))
}

#[aoc(day23, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    let mut elves = generator(input)?
        .indexed_iter()
        .filter_map(|(pos, tile)| {
            if *tile == Tile::Elf {
                Some((pos.y() as i32, pos.x() as i32))
            } else {
                None
            }
        })
        .collect::<Vec<_>>();

    let mut directions = [
        [(0, -1), (1, -1), (-1, -1)],
        [(0, 1), (1, 1), (-1, 1)],
        [(-1, 0), (-1, -1), (-1, 1)],
        [(1, 0), (1, -1), (1, 1)],
    ];

    for round in 1..usize::MAX {
        if !run_round(&mut elves, &directions) {
            return Ok(round);
        }
        directions.rotate_left(1);
    }

    Err(AocError::NoSolution)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"....#..
..###.#
#...#.#
.#...##
#.###..
##.#.##
.#..#..";

    #[test]
    fn test23_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test2301() {
        let res = part01(BUILD_INPUT).unwrap();

        assert_eq!(res, 110);
    }

    #[test]
    fn test2302() {
        let res = part02(BUILD_INPUT).unwrap();

        assert_eq!(res, 20);
    }
}
