/** Correct results:
 * - 2-=12=2-2-2-=0012==2
 * - 0
 */
use itertools::Itertools;

const SNAFU_MAP: [(i64, char); 5] = [(0, '0'), (-1, '1'), (-2, '2'), (2, '='), (1, '-')];

fn to_snafu(digits: &str) -> i64 {
    digits
        .as_bytes()
        .iter()
        .rev()
        .enumerate()
        .map(|(pow, digit)| {
            5i64.pow(pow as u32)
                * match digit {
                    b'0' => 0,
                    b'1' => 1,
                    b'2' => 2,
                    b'-' => -1,
                    b'=' => -2,
                    _ => unreachable!(),
                }
        })
        .sum()
}

fn from_snafu(mut n: i64) -> String {
    let mut snafu_digits = Vec::new();

    while n != 0 {
        let remainder = (n % 5) as usize;
        let (offset, snafu_digit) = SNAFU_MAP[remainder];
        snafu_digits.push(snafu_digit);
        n = (n + offset) / 5;
    }

    snafu_digits.iter().rev().join("")
}

#[aoc(day25, part1)]
fn part01(input: &str) -> String {
    from_snafu(input.lines().map(to_snafu).sum())
}

#[aoc(day25, part2)]
const fn part02(_input: &str) -> usize {
    0
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"1=-0-2
12111
2=0=
21
2=01
111
20012
112
1=-1=
1-12
12
1=
122";

    #[test]
    fn test2501() {
        let res = part01(BUILD_INPUT);

        assert_eq!(res, "2=-1=0");
    }

    #[test]
    fn test2502() {
        let res = part02(BUILD_INPUT);

        assert_eq!(res, 0);
    }
}
