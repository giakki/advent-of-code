/** Correct results:
 * - 61503
 * - 14081365540
 */
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use itertools::Itertools;
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{char, digit1},
    combinator::{map, value},
    multi::separated_list1,
    sequence::{delimited, preceded, terminated, tuple},
    IResult,
};
use std::collections::VecDeque;

#[derive(Debug, Clone, Copy)]
enum Operator {
    Add,
    Mul,
}

#[derive(Debug, Clone, Copy)]
enum Operand {
    Num(u64),
    Old,
}

#[derive(Debug, Clone, Copy)]
struct Operation {
    lhs: Operand,
    op: Operator,
    rhs: Operand,
}

#[derive(Debug, Clone)]
struct Monkey {
    items: VecDeque<u64>,
    operation: Operation,
    test: u64,
    test_true: usize,
    test_false: usize,
    activity: u64,
}

fn parse_operator(s: &str) -> IResult<&str, Operator> {
    alt((
        value(Operator::Add, char('+')),
        value(Operator::Mul, char('*')),
    ))(s)
}

fn parse_operand(s: &str) -> IResult<&str, Operand> {
    alt((
        value(Operand::Old, tag("old")),
        map(parse_int, Operand::Num),
    ))(s)
}

fn parse_operation(s: &str) -> IResult<&str, Operation> {
    map(
        tuple((
            terminated(parse_operand, char(' ')),
            parse_operator,
            preceded(char(' '), parse_operand),
        )),
        |(lhs, op, rhs)| Operation { lhs, op, rhs },
    )(s)
}

fn parse_monkey(s: &str) -> IResult<&str, Monkey> {
    map(
        tuple((
            delimited(tag("Monkey "), digit1, tag(":\n")),
            delimited(
                tag("  Starting items: "),
                separated_list1(tag(", "), parse_int),
                char('\n'),
            ),
            delimited(tag("  Operation: new = "), parse_operation, char('\n')),
            delimited(tag("  Test: divisible by "), parse_int, char('\n')),
            delimited(tag("    If true: throw to monkey "), parse_int, char('\n')),
            preceded(tag("    If false: throw to monkey "), parse_int),
        )),
        |(_name, items, operation, test, test_true, test_false)| Monkey {
            items: VecDeque::from(items),
            operation,
            test,
            test_true,
            test_false,
            activity: 0,
        },
    )(s)
}

const fn apply_operation(operation: &Operation, old: u64) -> u64 {
    let lhs = match operation.lhs {
        Operand::Num(n) => n,
        Operand::Old => old,
    };
    let rhs = match operation.rhs {
        Operand::Num(n) => n,
        Operand::Old => old,
    };

    match operation.op {
        Operator::Add => lhs + rhs,
        Operator::Mul => lhs * rhs,
    }
}

fn play_round(monkeys: &mut [Monkey], part_one: bool) {
    let divisor: u64 = monkeys.iter().map(|m| m.test).product();

    for i in 0..monkeys.len() {
        while let Some(mut item) = monkeys[i].items.pop_front() {
            monkeys[i].activity += 1;
            item = apply_operation(&monkeys[i].operation, item);

            if part_one {
                item /= 3;
            } else {
                item %= divisor;
            }

            let target = if item % monkeys[i].test == 0 {
                &mut monkeys[monkeys[i].test_true]
            } else {
                &mut monkeys[monkeys[i].test_false]
            };

            target.items.push_back(item);
        }
    }
}

#[aoc_generator(day11)]
fn generator(input: &str) -> Result<Vec<Monkey>, AocError> {
    input_parser(separated_list1(tag("\n\n"), parse_monkey))(input)
}

#[aoc(day11, part1)]
fn part01(input: &[Monkey]) -> u64 {
    let mut monkeys = input.to_vec();

    for _ in 0..20 {
        play_round(&mut monkeys, true);
    }

    monkeys
        .iter()
        .map(|m| m.activity)
        .sorted()
        .rev()
        .take(2)
        .product()
}

#[aoc(day11, part2)]
fn part02(input: &[Monkey]) -> u64 {
    let mut monkeys = input.to_vec();

    for _ in 0..10000 {
        play_round(&mut monkeys, false);
    }

    monkeys
        .iter()
        .map(|m| m.activity)
        .sorted()
        .rev()
        .take(2)
        .product()
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1";

    #[test]
    fn test11_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test1101() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 10605);
    }

    #[test]
    fn test1102() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, 2713310158);
    }
}
