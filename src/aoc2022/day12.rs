/** Correct results:
 * - 447
 * - 446
 */
use crate::common::{
    errors::AocError,
    point::{Point, PointOps},
};
use itertools::Itertools;

#[aoc_generator(day12)]
fn generator(input: &str) -> (Vec<Vec<u8>>, Point<usize>, Point<usize>) {
    let mut start = (0, 0);
    let mut end = (0, 0);
    let mut map = Vec::new();

    for (y, row) in input.lines().enumerate() {
        let mut map_row = Vec::with_capacity(row.len());
        for (x, elevation) in row.as_bytes().iter().enumerate() {
            if *elevation == b'S' {
                start = (x, y);
                map_row.push(b'a');
            } else if *elevation == b'E' {
                end = (x, y);
                map_row.push(b'z');
            } else {
                map_row.push(*elevation);
            }
        }
        map.push(map_row);
    }

    (map, start, end)
}

fn get_successors(p: &Point<usize>, map: &[Vec<u8>]) -> Vec<Point<usize>> {
    p.cardinal_neighbors_checked(map[0].len() - 1, map.len() - 1)
        .into_iter()
        .filter(|n| {
            let dest = map[n.y()][n.x()];
            let orig = map[p.y()][p.x()];

            dest <= orig + 1
        })
        .collect_vec()
}

fn find_shortest_path(map: &[Vec<u8>], start: &Point<usize>, end: &Point<usize>) -> Option<usize> {
    pathfinding::directed::bfs::bfs(start, |p| get_successors(p, map), |p| p == end)
        .map(|path| path.len() - 1)
}

#[aoc(day12, part1)]
fn part01(
    (map, start, end): &(Vec<Vec<u8>>, Point<usize>, Point<usize>),
) -> Result<usize, AocError> {
    find_shortest_path(map, start, end).ok_or(AocError::NoSolution)
}

#[aoc(day12, part2)]
fn part02((map, _, end): &(Vec<Vec<u8>>, Point<usize>, Point<usize>)) -> Result<usize, AocError> {
    map.iter()
        .enumerate()
        .flat_map(|(y, row)| {
            row.iter()
                .enumerate()
                .filter_map(|(x, elevation)| {
                    let start = (x, y);
                    if *elevation == b'a' {
                        find_shortest_path(map, &start, end)
                    } else {
                        None
                    }
                })
                .collect_vec()
        })
        .min()
        .ok_or(AocError::NoSolution)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi";

    #[test]
    fn test12_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test1201() {
        let input = generator(BUILD_INPUT);
        let res = part01(&input).unwrap();

        assert_eq!(res, 31);
    }

    #[test]
    fn test1202() {
        let input = generator(BUILD_INPUT);
        let res = part02(&input).unwrap();

        assert_eq!(res, 29);
    }
}
