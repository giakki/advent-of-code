/** Correct results:
 * - 5350
 * - 19570
 */
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use itertools::Itertools;
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::char,
    combinator::map,
    multi::{separated_list0, separated_list1},
    sequence::{delimited, separated_pair},
    IResult,
};
use std::iter;

#[derive(Debug, Clone, PartialEq, Eq)]
enum Token {
    Literal(u32),
    List(Vec<Self>),
}

impl Token {
    fn cmp_lists(a: &[Self], b: &[Self]) -> std::cmp::Ordering {
        for (a, b) in iter::zip(a, b) {
            if a != b {
                return a.cmp(b);
            }
        }

        a.len().cmp(&b.len())
    }
}

impl PartialOrd for Token {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Token {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match self {
            Self::Literal(n) => match other {
                Self::Literal(m) => n.cmp(m),
                Self::List(ms) => Self::cmp_lists(&[Self::Literal(*n)], ms),
            },
            Self::List(ns) => match other {
                Self::Literal(m) => Self::cmp_lists(ns, &[Self::Literal(*m)]),
                Self::List(ms) => Self::cmp_lists(ns, ms),
            },
        }
    }
}

fn parse_tokens(s: &str) -> IResult<&str, Token> {
    alt((
        map(
            delimited(
                char('['),
                separated_list0(char(','), parse_tokens),
                char(']'),
            ),
            Token::List,
        ),
        map(parse_int, Token::Literal),
    ))(s)
}

#[aoc_generator(day13)]
fn generator(input: &str) -> Result<Vec<(Token, Token)>, AocError> {
    input_parser(separated_list1(
        tag("\n\n"),
        separated_pair(parse_tokens, char('\n'), parse_tokens),
    ))(input)
}

#[aoc(day13, part1)]
fn part01(input: &[(Token, Token)]) -> usize {
    input
        .iter()
        .enumerate()
        .filter_map(|(idx, (a, b))| {
            if a.cmp(b) == std::cmp::Ordering::Greater {
                None
            } else {
                Some(idx)
            }
        })
        .map(|idx| idx + 1)
        .sum()
}

#[aoc(day13, part2)]
fn part02(input: &[(Token, Token)]) -> Result<usize, AocError> {
    let sep1 = Token::List(vec![Token::List(vec![Token::Literal(2)])]);
    let sep2 = Token::List(vec![Token::List(vec![Token::Literal(6)])]);

    let sorted = input
        .iter()
        .flat_map(|(a, b)| vec![a, b])
        .chain(std::iter::once(&sep1))
        .chain(std::iter::once(&sep2))
        .sorted()
        .collect_vec();

    let pos_a = sorted
        .iter()
        .find_position(|t| ***t == sep1)
        .map(|(idx, _)| idx + 1);
    let pos_b = sorted
        .iter()
        .find_position(|t| ***t == sep2)
        .map(|(idx, _)| idx + 1);

    pos_a
        .zip(pos_b)
        .map(|(a, b)| a * b)
        .ok_or(AocError::NoSolution)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]";

    #[test]
    fn test13_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test1301() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 13);
    }

    #[test]
    fn test1302() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input).unwrap();

        assert_eq!(res, 140);
    }
}
