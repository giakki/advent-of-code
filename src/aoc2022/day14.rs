/** Correct results:
 * - 665
 * - 25434
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_point_comma},
    point::{Point, PointOps},
};
use itertools::Itertools;
use ndarray::Array2;
use nom::{bytes::complete::tag, multi::separated_list1};
use std::fmt::Debug;

#[derive(Clone, PartialEq, Eq)]
enum Tile {
    Air,
    Rock,
    Sand,
}

impl Debug for Tile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Air => write!(f, "."),
            Self::Rock => write!(f, "#"),
            Self::Sand => write!(f, "o"),
        }
    }
}

fn get_bounds(input: &[Vec<Point<usize>>]) -> (Point<usize>, Point<usize>) {
    let mut min_x = usize::MAX;
    let mut min_y = 0;
    let mut max_x = 500;
    let mut max_y = 0;

    for line in input {
        for segment in line {
            min_x = usize::min(min_x, segment.x());
            min_y = usize::min(min_y, segment.y());
            max_x = usize::max(max_x, segment.x());
            max_y = usize::max(max_y, segment.y());
        }
    }

    ((min_x, min_y), (max_x, max_y))
}

fn fill_grid(lines: Vec<Vec<Point<usize>>>, grid: &mut Array2<Tile>) {
    for line in lines {
        for (start, end) in line.iter().tuple_windows() {
            if start.y() == end.y() {
                let start_x = usize::min(start.x(), end.x());
                let end_x = usize::max(start.x(), end.x());
                for x in start_x..=end_x {
                    grid[(start.y(), x + 1)] = Tile::Rock;
                }
            } else {
                let start_y = usize::min(start.y(), end.y());
                let end_y = usize::max(start.y(), end.y());
                for y in start_y..=end_y {
                    grid[(y, start.x() + 1)] = Tile::Rock;
                }
            }
        }
    }
}

fn run_sand(
    grid: &mut Array2<Tile>,
    sand_generator_pos: Point<usize>,
    test: impl Fn(Point<usize>) -> bool,
) {
    loop {
        let mut sand_pos = sand_generator_pos;

        loop {
            let mut has_moved = false;

            if grid[(sand_pos.y() + 1, sand_pos.x())] == Tile::Air {
                sand_pos.1 += 1;
                has_moved = true;
            } else if grid[(sand_pos.y() + 1, sand_pos.x() - 1)] == Tile::Air {
                sand_pos.0 -= 1;
                sand_pos.1 += 1;
                has_moved = true;
            } else if grid[(sand_pos.y() + 1, sand_pos.x() + 1)] == Tile::Air {
                sand_pos.0 += 1;
                sand_pos.1 += 1;
                has_moved = true;
            }

            if test(sand_pos) {
                return;
            }

            if !has_moved {
                break;
            }
        }

        grid[(sand_pos.y(), sand_pos.x())] = Tile::Sand;
    }
}

#[aoc_generator(day14)]
fn generator(input: &str) -> Result<Vec<Vec<Point<usize>>>, AocError> {
    input_parser(by_lines(separated_list1(tag(" -> "), parse_point_comma)))(input)
}

#[aoc(day14, part1)]
fn part01(input: &[Vec<Point<usize>>]) -> usize {
    let (min, max) = get_bounds(input);
    let normalized_input = input
        .iter()
        .map(|line| {
            line.iter()
                .map(|segment| (segment.x() - min.x(), segment.y() - min.y()))
                .collect_vec()
        })
        .collect_vec();

    let normalized_max = (max.x() - min.x(), max.y() - min.y());

    let mut grid = Array2::from_elem((normalized_max.y() + 2, normalized_max.x() + 3), Tile::Air);
    fill_grid(normalized_input, &mut grid);

    let sand_generator_pos = (500 - min.x() + 1, 0);
    run_sand(&mut grid, sand_generator_pos, |p| {
        p.y() == normalized_max.y()
    });

    grid.into_iter().filter(|t| *t == Tile::Sand).count()
}

#[aoc(day14, part2)]
fn part02(input: &[Vec<Point<usize>>]) -> usize {
    let (min, max) = get_bounds(input);

    let height = max.y() - min.y();

    let min = (usize::min(500 - height - 20, min.x()), min.y());
    let max = (usize::max(500 + height + 20, max.x()), max.y());

    let normalized_input = input
        .iter()
        .map(|line| {
            line.iter()
                .map(|segment| (segment.x() - min.x(), segment.y() - min.y()))
                .collect_vec()
        })
        .collect_vec();

    let normalized_max = (max.x() - min.x(), max.y() - min.y());

    let mut grid = Array2::from_elem((normalized_max.y() + 3, normalized_max.x() + 3), Tile::Air);

    fill_grid(normalized_input, &mut grid);
    for x in 0..normalized_max.x() {
        grid[(max.y() + 2, x)] = Tile::Rock;
    }

    let sand_generator_pos = (500 - min.x() + 1, 0);
    run_sand(&mut grid, sand_generator_pos, |p| p == sand_generator_pos);

    grid.into_iter().filter(|t| *t == Tile::Sand).count() + 1
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9";

    #[test]
    fn test14_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test1401() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 24);
    }

    #[test]
    fn test1402() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, 93);
    }
}
