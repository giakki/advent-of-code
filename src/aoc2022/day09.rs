/** Correct results:
 * - 6311
 * - 2482
 */
use crate::common::{
    direction::Direction,
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
    point::{Point, PointOps},
};
use nom::{character::complete::char, sequence::separated_pair};
use std::collections::HashSet;

#[aoc_generator(day9)]
fn generator(input: &str) -> Result<Vec<(Direction, i32)>, AocError> {
    input_parser(by_lines(separated_pair(
        Direction::parse_from_initial,
        char(' '),
        parse_int,
    )))(input)
}

fn move_knot(head: Point<i32>, tail: &mut Point<i32>) {
    let dx = head.x() - tail.x();
    let dy = head.y() - tail.y();

    if dx.abs() > 1 || dy.abs() > 1 {
        tail.set_x(tail.x() + dx.signum());
        tail.set_y(tail.y() + dy.signum());
    }
}

fn move_rope(rope_length: usize, movements: &[(Direction, i32)]) -> usize {
    let mut rope = vec![(0, 0); rope_length];
    let mut visited = HashSet::new();
    visited.insert(rope[rope.len() - 1]);

    for (direction, amount) in movements {
        for _ in 0..*amount {
            rope[0].move_by(1, direction);
            for i in 1..rope.len() {
                move_knot(rope[i - 1], &mut rope[i]);
                visited.insert(rope[rope.len() - 1]);
            }
        }
    }

    visited.len()
}

#[aoc(day9, part1)]
fn part01(input: &[(Direction, i32)]) -> usize {
    move_rope(2, input)
}

#[aoc(day9, part2)]
fn part02(input: &[(Direction, i32)]) -> usize {
    move_rope(10, input)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT_1: &str = r"R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2";

    const BUILD_INPUT_2: &str = r"R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20";

    #[test]
    fn test09_gen() {
        let input = generator(BUILD_INPUT_1);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test0901() {
        let input = generator(BUILD_INPUT_1).unwrap();
        let res = part01(&input);

        assert_eq!(res, 13);
    }

    #[test]
    fn test0902() {
        let input = generator(BUILD_INPUT_2).unwrap();
        let res = part02(&input);

        assert_eq!(res, 36);
    }
}
