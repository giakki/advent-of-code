/** Correct results:
 * - 10595
 * - 9541
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser},
};
use nom::{
    character::complete::{anychar, char},
    sequence::{preceded, tuple},
};

enum Move {
    Rock,
    Paper,
    Scissors,
}

impl Move {
    const fn score(&self) -> u32 {
        match self {
            Self::Rock => 1,
            Self::Paper => 2,
            Self::Scissors => 3,
        }
    }

    const fn play(&self, other: &Self) -> Outcome {
        match (self, other) {
            (Self::Rock, Self::Rock)
            | (Self::Paper, Self::Paper)
            | (Self::Scissors, Self::Scissors) => Outcome::Draw,
            (Self::Rock, Self::Paper)
            | (Self::Paper, Self::Scissors)
            | (Self::Scissors, Self::Rock) => Outcome::Lose,
            (Self::Rock, Self::Scissors)
            | (Self::Paper, Self::Rock)
            | (Self::Scissors, Self::Paper) => Outcome::Win,
        }
    }

    const fn with_outcome(&self, outcome: &Outcome) -> Self {
        match (self, outcome) {
            (Self::Rock, Outcome::Draw)
            | (Self::Paper, Outcome::Lose)
            | (Self::Scissors, Outcome::Win) => Self::Rock,
            (Self::Rock, Outcome::Win)
            | (Self::Paper, Outcome::Draw)
            | (Self::Scissors, Outcome::Lose) => Self::Paper,
            (Self::Rock, Outcome::Lose)
            | (Self::Paper, Outcome::Win)
            | (Self::Scissors, Outcome::Draw) => Self::Scissors,
        }
    }
}

enum Outcome {
    Lose,
    Draw,
    Win,
}

impl Outcome {
    const fn score(&self) -> u32 {
        match self {
            Self::Lose => 0,
            Self::Draw => 3,
            Self::Win => 6,
        }
    }
}

#[aoc_generator(day2)]
fn generator(input: &str) -> Result<Vec<(char, char)>, AocError> {
    input_parser(by_lines(tuple((anychar, preceded(char(' '), anychar)))))(input)
}

#[aoc(day2, part1)]
fn part01(input: &[(char, char)]) -> Result<u32, AocError> {
    input
        .iter()
        .map(|(a, b)| {
            let elf = match a {
                'A' => Move::Rock,
                'B' => Move::Paper,
                'C' => Move::Scissors,
                _ => Err(AocError::Input)?,
            };
            let me = match b {
                'X' => Move::Rock,
                'Y' => Move::Paper,
                'Z' => Move::Scissors,
                _ => Err(AocError::Input)?,
            };

            Ok(me.play(&elf).score() + me.score())
        })
        .collect::<Result<Vec<_>, _>>()
        .map(|rounds| rounds.iter().sum())
}

#[aoc(day2, part2)]
fn part02(input: &[(char, char)]) -> Result<u32, AocError> {
    input
        .iter()
        .map(|(a, b)| {
            let elf = match a {
                'A' => Move::Rock,
                'B' => Move::Paper,
                'C' => Move::Scissors,
                _ => Err(AocError::Input)?,
            };
            let outcome = match b {
                'X' => Outcome::Lose,
                'Y' => Outcome::Draw,
                'Z' => Outcome::Win,
                _ => Err(AocError::Input)?,
            };

            Ok(elf.with_outcome(&outcome).score() + outcome.score())
        })
        .collect::<Result<Vec<_>, _>>()
        .map(|rounds| rounds.iter().sum())
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"A Y
B X
C Z";

    #[test]
    fn test00_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test0001() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input).unwrap();

        assert_eq!(res, 15);
    }

    #[test]
    fn test0002() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input).unwrap();

        assert_eq!(res, 12);
    }
}
