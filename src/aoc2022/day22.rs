/** Correct results:
 * - 58248
 * - 179091
 */
use crate::common::{
    chr::FromChar,
    direction::{Direction, Turn},
    errors::AocError,
    parsers::{input_parser, parse_int},
    point::{Point, PointOps},
};
use itertools::Itertools;
use ndarray::Array2;
use nom::{
    branch::alt,
    character::complete::char,
    combinator::{map, value},
    multi::many1,
};
use std::iter::repeat;

const FACE_SIZE: usize = 50;
const NUM_FACES_X: usize = 3;
const NUM_FACES_Y: usize = 4;

#[derive(Debug, Clone, Copy)]
enum Instruction {
    Turn(Turn),
    Forward(usize),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Tile {
    Free,
    Wall,
    Edge,
}
impl FromChar for Tile {
    type Err = AocError;

    fn from_char(c: &char) -> Result<Self, Self::Err> {
        match c {
            ' ' => Ok(Self::Edge),
            '#' => Ok(Self::Wall),
            '.' => Ok(Self::Free),
            _ => Err(AocError::ParseError(format!("Cannot parse tile {c}"))),
        }
    }
}

fn parse_map(s: &str) -> Result<Array2<Tile>, AocError> {
    let map = s
        .lines()
        .map(|line| {
            line.chars()
                .map(|c| Tile::from_char(&c))
                .collect::<Result<Vec<_>, _>>()
        })
        .collect::<Result<Vec<_>, _>>()?;

    let max_width = map.iter().map(Vec::len).max().ok_or(AocError::Input)?;
    let height = map.len();

    Ok(Array2::from_shape_vec(
        (height, max_width),
        map.into_iter()
            .flat_map(|row| {
                let row_len = row.len();
                row.into_iter()
                    .chain(std::iter::repeat(Tile::Edge).take(max_width - row_len))
            })
            .collect_vec(),
    )?)
}

fn parse_instructions(s: &str) -> Result<Vec<Instruction>, AocError> {
    input_parser(many1(alt((
        map(parse_int, Instruction::Forward),
        value(Instruction::Turn(Turn::Cw), char('R')),
        value(Instruction::Turn(Turn::Ccw), char('L')),
    ))))(s)
}

#[aoc_generator(day22)]
fn generator(input: &str) -> Result<(Array2<Tile>, Vec<Instruction>), AocError> {
    let (map_str, instructions_str) = input.split_once("\n\n").ok_or(AocError::Input)?;

    Ok((parse_map(map_str)?, parse_instructions(instructions_str)?))
}

fn try_move(
    position: &Point<usize>,
    direction: Direction,
    max: Point<usize>,
) -> Option<Point<usize>> {
    match direction {
        Direction::Up => {
            if position.y() > 0 {
                Some((position.x(), position.y() - 1))
            } else {
                None
            }
        }
        Direction::Down => {
            if position.y() < (max.y() - 1) {
                Some((position.x(), position.y() + 1))
            } else {
                None
            }
        }
        Direction::Left => {
            if position.x() > 0 {
                Some((position.x() - 1, position.y()))
            } else {
                None
            }
        }
        Direction::Right => {
            if position.x() < (max.x() - 1) {
                Some((position.x() + 1, position.y()))
            } else {
                None
            }
        }
    }
}

fn try_move_wrapping(
    position: &Point<usize>,
    direction: Direction,
    map: &Array2<Tile>,
) -> Result<Option<Point<usize>>, AocError> {
    let (height, width) = map.dim();

    let coords = match direction {
        Direction::Up => ((position.y()..height).rev().zip(repeat(position.x()))).collect_vec(),
        Direction::Down => (0..position.y()).zip(repeat(position.x())).collect_vec(),
        Direction::Right => (repeat(position.y()).zip(0..position.x())).collect_vec(),
        Direction::Left => repeat(position.y())
            .zip((position.x()..width).rev())
            .collect_vec(),
    };

    for pos in coords {
        match map[pos] {
            Tile::Edge => continue,
            Tile::Free => return Ok(Some(pos.flip())),
            Tile::Wall => return Ok(None),
        }
    }

    Err(AocError::Generic("Cannot wrap".to_owned()))
}

fn take_step_wrapping(
    position: &mut Point<usize>,
    direction: Direction,
    map: &Array2<Tile>,
) -> Result<(), AocError> {
    let (height, width) = map.dim();

    if let Some(new_position) = try_move(position, direction, (width, height)) {
        match map[new_position.flip()] {
            Tile::Free => {
                *position = new_position;
            }
            Tile::Wall => return Ok(()),
            Tile::Edge => {
                if let Some(new_pos) = try_move_wrapping(&new_position, direction, map)? {
                    *position = new_pos;
                } else {
                    return Ok(());
                }
            }
        }
    } else if let Some(new_pos) = try_move_wrapping(position, direction, map)? {
        *position = new_pos;
    } else {
        return Ok(());
    }

    Ok(())
}

fn calculate_password(position: Point<usize>, direction: Direction) -> usize {
    let dir_val = match direction {
        Direction::Right => 0,
        Direction::Down => 1,
        Direction::Left => 2,
        Direction::Up => 3,
    };

    (1000 * position.y()) + (4 * position.x()) + dir_val
}

fn take_step_rotating(
    face_index: &mut usize,
    position: &mut Point<usize>,
    direction: &mut Direction,
    map: &Array2<Tile>,
    starting_points: &[Point<usize>],
) {
    if let Some(new_position) = try_move(position, *direction, (FACE_SIZE, FACE_SIZE)) {
        match map[new_position.add(&starting_points[*face_index]).flip()] {
            Tile::Free => {
                *position = new_position;
            }
            Tile::Wall => (),
            Tile::Edge => unreachable!(),
        }
    } else {
        let (new_face, new_position, new_direction) = move_face(*face_index, position, *direction);
        match map[new_position.add(&starting_points[new_face]).flip()] {
            Tile::Free => {
                *face_index = new_face;
                *position = new_position;
                *direction = new_direction;
            }
            Tile::Wall => (),
            Tile::Edge => unreachable!(),
        }
    }
}
fn move_face(
    face_index: usize,
    pos: &Point<usize>,
    direction: Direction,
) -> (usize, Point<usize>, Direction) {
    match face_index {
        0 => match direction {
            Direction::Right => (1, (0, pos.y()), Direction::Right),
            Direction::Down => (2, (pos.x(), 0), Direction::Down),
            Direction::Left => (3, (0, FACE_SIZE - 1 - pos.y()), Direction::Right),
            Direction::Up => (5, (0, pos.x()), Direction::Right),
        },

        1 => match direction {
            Direction::Left => (0, (FACE_SIZE - 1, pos.y()), Direction::Left),
            Direction::Down => (2, (FACE_SIZE - 1, pos.x()), Direction::Left),
            Direction::Right => (4, (FACE_SIZE - 1, FACE_SIZE - 1 - pos.y()), Direction::Left),
            Direction::Up => (5, (pos.x(), FACE_SIZE - 1), Direction::Up),
        },

        2 => match direction {
            Direction::Up => (0, (pos.x(), FACE_SIZE - 1), Direction::Up),
            Direction::Right => (1, (pos.y(), FACE_SIZE - 1), Direction::Up),
            Direction::Down => (4, (pos.x(), 0), Direction::Down),
            Direction::Left => (3, (pos.y(), 0), Direction::Down),
        },

        3 => match direction {
            Direction::Right => (4, (0, pos.y()), Direction::Right),
            Direction::Down => (5, (pos.x(), 0), Direction::Down),
            Direction::Up => (2, (0, pos.x()), Direction::Right),
            Direction::Left => (0, (0, FACE_SIZE - 1 - pos.y()), Direction::Right),
        },

        4 => match direction {
            Direction::Up => (2, (pos.x(), FACE_SIZE - 1), Direction::Up),
            Direction::Left => (3, (FACE_SIZE - 1, pos.y()), Direction::Left),
            Direction::Down => (5, (FACE_SIZE - 1, pos.x()), Direction::Left),
            Direction::Right => (1, (FACE_SIZE - 1, FACE_SIZE - 1 - pos.y()), Direction::Left),
        },

        5 => match direction {
            Direction::Up => (3, (pos.x(), FACE_SIZE - 1), Direction::Up),
            Direction::Right => (4, (pos.y(), FACE_SIZE - 1), Direction::Up),
            Direction::Down => (1, (pos.x(), 0), Direction::Down),
            Direction::Left => (0, (pos.y(), 0), Direction::Down),
        },
        _ => unreachable!(),
    }
}

#[aoc(day22, part1)]
fn part01((map, instructions): &(Array2<Tile>, Vec<Instruction>)) -> Result<usize, AocError> {
    let mut position = map
        .indexed_iter()
        .find_map(|((y, x), tile)| {
            if *tile == Tile::Free {
                Some((x, y))
            } else {
                None
            }
        })
        .ok_or(AocError::Input)?;

    let mut direction = Direction::Right;

    for instruction in instructions {
        match instruction {
            Instruction::Turn(turn) => direction = direction.rotate(turn),
            Instruction::Forward(n) => {
                for _ in 0..*n {
                    take_step_wrapping(&mut position, direction, map)?;
                }
            }
        }
    }

    Ok(calculate_password(
        (position.x() + 1, position.y() + 1),
        direction,
    ))
}

#[aoc(day22, part2)]
fn part02((map, instructions): &(Array2<Tile>, Vec<Instruction>)) -> usize {
    let mut starting_points: Vec<(usize, usize)> = vec![];
    for y in (0..(FACE_SIZE * NUM_FACES_Y)).step_by(FACE_SIZE) {
        for x in (0..(FACE_SIZE * NUM_FACES_X)).step_by(FACE_SIZE) {
            if map[(y, x)] != Tile::Edge {
                starting_points.push((x, y));
            }
        }
    }

    let mut face_index = 0;
    let mut position = (0, 0);
    let mut direction = Direction::Right;

    for instruction in instructions {
        match instruction {
            Instruction::Turn(turn) => direction = direction.rotate(turn),
            Instruction::Forward(n) => {
                for _ in 0..*n {
                    take_step_rotating(
                        &mut face_index,
                        &mut position,
                        &mut direction,
                        map,
                        &starting_points,
                    );
                }
            }
        }
    }

    let final_position = position.add(&starting_points[face_index]).add(&(1, 1));

    calculate_password(final_position, direction)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"        ...#
        .#..
        #...
        ....
...#.......#
........#...
..#....#....
..........#.
        ...#....
        .....#..
        .#......
        ......#.

10R5L5R10L4R5L5";

    #[test]
    fn test21_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test2201() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input).unwrap();

        assert_eq!(res, 6032);
    }
}
