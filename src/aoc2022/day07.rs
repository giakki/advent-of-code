/** Correct results:
 * - 1118405
 * - 12545514
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{alpha1, char},
    combinator::{map, value},
    sequence::{preceded, separated_pair, tuple},
    IResult,
};
use std::collections::HashMap;

#[derive(Debug, Clone)]
enum Command {
    CdRoot,
    CdBack,
    Cd(String),
    Ls,
    DirEntry,
    FileEntry(u32),
}

impl Command {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        alt((
            value(Self::CdRoot, tag("$ cd /")),
            value(Self::CdBack, tag("$ cd ..")),
            map(preceded(tag("$ cd "), alpha1), |dir: &str| {
                Self::Cd(dir.to_owned())
            }),
            value(Self::Ls, tag("$ ls")),
            map(preceded(tag("dir "), alpha1), |_: &str| Self::DirEntry),
            map(
                tuple((parse_int, preceded(char(' '), parse_filename))),
                |(size, _)| Self::FileEntry(size),
            ),
        ))(s)
    }
}

fn parse_filename(input: &str) -> IResult<&str, String> {
    alt((
        map(
            separated_pair(alpha1, char('.'), alpha1),
            |(a, b): (&str, &str)| a.to_owned() + "." + b,
        ),
        map(alpha1, String::from),
    ))(input)
}

#[aoc_generator(day7)]
fn generator(input: &str) -> Result<Vec<Command>, AocError> {
    input_parser(by_lines(Command::parse))(input)
}

fn build_fs(commands: &[Command]) -> HashMap<String, u32> {
    let mut fs = HashMap::new();

    let mut cwd = vec![""];
    for command in commands {
        match command {
            Command::CdRoot => cwd = vec![""],
            Command::CdBack => {
                cwd.pop();
            }
            Command::Cd(subfolder) => cwd.push(subfolder),
            Command::FileEntry(size) => {
                let mut path_builder = String::new();
                for part in &cwd {
                    path_builder += part;
                    *fs.entry(path_builder.clone()).or_insert(0) += size;
                }
            }
            Command::Ls | Command::DirEntry => {}
        };
    }

    fs
}

#[aoc(day7, part1)]
fn part01(input: &[Command]) -> u32 {
    let fs = build_fs(input);

    fs.values().filter(|v| **v <= 100_000).sum()
}

#[aoc(day7, part2)]
fn part02(input: &[Command]) -> Option<u32> {
    let fs = build_fs(input);
    let used_space = fs[""];
    let unused_space = 70_000_000 - used_space;
    let space_needed = 30_000_000 - unused_space;

    fs.values().filter(|v| **v >= space_needed).min().copied()
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k";

    #[test]
    fn test07_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test0701() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 95437);
    }

    #[test]
    fn test0702() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, Some(24933642));
    }
}
