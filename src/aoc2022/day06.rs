/** Correct results:
 * - 1262
 * - 3444
 */
use itertools::Itertools;

#[aoc(day6, part1)]
fn part01(input: &str) -> Option<usize> {
    input
        .as_bytes()
        .windows(4)
        .find_position(|cs| (0..cs.len() - 1).all(|i| !cs[(i + 1)..].contains(&cs[i])))
        .map(|(i, _)| i + 4)
}

#[aoc(day6, part2)]
fn part02(input: &str) -> Option<usize> {
    input
        .as_bytes()
        .windows(14)
        .find_position(|cs| (0..cs.len()).all(|i| !cs[(i + 1)..].contains(&cs[i])))
        .map(|(i, _)| i + 14)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT_1: &str = r"mjqjpqmgbljsphdztnvjfqwrcgsmlb";
    const BUILD_INPUT_2: &str = r"bvwbjplbgvbhsrlpgdmjqwftvncz";
    const BUILD_INPUT_3: &str = r"nppdvjthqldpwncqszvftbrmjlhg";
    const BUILD_INPUT_4: &str = r"nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg";
    const BUILD_INPUT_5: &str = r"zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw";

    #[test]
    fn test0601() {
        assert_eq!(part01(BUILD_INPUT_1), Some(7));
        assert_eq!(part01(BUILD_INPUT_2), Some(5));
        assert_eq!(part01(BUILD_INPUT_3), Some(6));
        assert_eq!(part01(BUILD_INPUT_4), Some(10));
        assert_eq!(part01(BUILD_INPUT_5), Some(11));
    }

    #[test]
    fn test0602() {
        assert_eq!(part02(BUILD_INPUT_1), Some(19));
        assert_eq!(part02(BUILD_INPUT_2), Some(23));
        assert_eq!(part02(BUILD_INPUT_3), Some(23));
        assert_eq!(part02(BUILD_INPUT_4), Some(29));
        assert_eq!(part02(BUILD_INPUT_5), Some(26));
    }
}
