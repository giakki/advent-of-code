/** Correct results:
 * - 1647
 * - 392080
 */
use crate::common::errors::AocError;
use std::collections::HashSet;

#[aoc_generator(day8)]
fn generator(input: &str) -> Result<Vec<Vec<i32>>, AocError> {
    input
        .lines()
        .map(|line| {
            line.chars()
                .map(|c| c.to_digit(10).map(|c| c as i32).ok_or(AocError::Input))
                .collect::<Result<_, _>>()
        })
        .collect()
}

#[aoc(day8, part1)]
fn part01(input: &[Vec<i32>]) -> usize {
    let mut visible = HashSet::new();

    for (y, row) in input.iter().enumerate() {
        let mut max = -1;
        for (x, tree) in row.iter().copied().enumerate() {
            if tree > max {
                max = tree;
                visible.insert((x, y));
            }
        }
        let mut max = -1;
        for (x, tree) in row.iter().copied().enumerate().rev() {
            if tree > max {
                max = tree;
                visible.insert((x, y));
            }
        }
    }

    for x in 0..input[0].len() {
        let mut max = -1;
        for (y, col) in input.iter().enumerate() {
            let tree = col[x];
            if tree > max {
                max = tree;
                visible.insert((x, y));
            }
        }
        let mut max = -1;
        for (y, col) in input.iter().enumerate().rev() {
            let tree = col[x];
            if tree > max {
                max = tree;
                visible.insert((x, y));
            }
        }
    }

    visible.len()
}

#[aoc(day8, part2)]
fn part02(input: &[Vec<i32>]) -> usize {
    let mut max = 0;
    for (y, row) in input.iter().enumerate() {
        for (x, tree) in row.iter().copied().enumerate() {
            let mut top_view = 0;
            for vy in (0..y).rev() {
                top_view += 1;
                if input[vy][x] >= tree {
                    break;
                }
            }
            let mut bottom_view = 0;
            for row in input.iter().skip(y + 1) {
                bottom_view += 1;
                if row[x] >= tree {
                    break;
                }
            }
            let mut left_view = 0;
            for other_tree in row[..x].iter().rev() {
                left_view += 1;
                if *other_tree >= tree {
                    break;
                }
            }
            let mut right_view = 0;
            for other_tree in &row[x + 1..] {
                right_view += 1;
                if *other_tree >= tree {
                    break;
                }
            }

            let score = top_view * bottom_view * left_view * right_view;
            if score > max {
                max = score;
            }
        }
    }

    max
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"30373
25512
65332
33549
35390";

    #[test]
    fn test08_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test0801() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 21);
    }

    #[test]
    fn test0802() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, 8);
    }
}
