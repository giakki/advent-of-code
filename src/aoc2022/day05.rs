/** Correct results:
 * - NTWZZWHFV
 * - BRZGFVBTJ
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use itertools::Itertools;
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{anychar, char, digit1, space1},
    combinator::map,
    multi::{many1, separated_list1},
    sequence::{delimited, preceded, tuple},
    IResult,
};

type Move = (usize, usize, usize);

fn parse_stacks(s: &str) -> IResult<&str, Vec<Vec<char>>> {
    map(
        tuple((
            by_lines(separated_list1(
                char(' '),
                alt((
                    delimited(char(' '), char(' '), char(' ')),
                    delimited(char('['), anychar, char(']')),
                )),
            )),
            preceded(char('\n'), many1(alt((space1, digit1)))),
        )),
        |(levels, _)| {
            let mut stacks = vec![Vec::new(); levels[0].len()];
            for level in levels.iter().rev() {
                for (idx, crt) in level.iter().enumerate() {
                    if *crt != ' ' {
                        stacks[idx].push(*crt);
                    }
                }
            }

            stacks
        },
    )(s)
}

fn parse_moves(s: &str) -> IResult<&str, Vec<Move>> {
    by_lines(tuple((
        preceded(tag("move "), parse_int),
        preceded(tag(" from "), parse_int),
        preceded(tag(" to "), parse_int),
    )))(s)
}

#[aoc_generator(day5)]
fn generator(input: &str) -> Result<(Vec<Vec<char>>, Vec<Move>), AocError> {
    input_parser(tuple((parse_stacks, preceded(tag("\n\n"), parse_moves))))(input)
}

#[aoc(day5, part1)]
fn part01((stacks, moves): &(Vec<Vec<char>>, Vec<Move>)) -> String {
    let mut stacks = stacks.clone();
    for (n, from, to) in moves {
        let len = stacks[from - 1].len();
        let ext = stacks[from - 1].drain((len - n)..).rev().collect_vec();
        stacks[to - 1].extend(ext);
    }

    stacks
        .iter()
        .map(|s| s.last().copied().unwrap_or('?'))
        .join("")
}

#[aoc(day5, part2)]
fn part02((stacks, moves): &(Vec<Vec<char>>, Vec<Move>)) -> String {
    let mut stacks = stacks.clone();
    for (n, from, to) in moves {
        let len = stacks[from - 1].len();
        let ext = stacks[from - 1].drain((len - n)..).collect_vec();
        stacks[to - 1].extend(ext);
    }

    stacks
        .iter()
        .map(|s| s.last().copied().unwrap_or('?'))
        .join("")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[rustfmt::skip]
    const BUILD_INPUT: &str = r"        [D]
[N]     [C]
[Z] [M] [P]
 1   2   3

move 1 from 3 to 1
move 1 from 3 to 2
move 1 from 1 to 2
move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2";

    #[test]
    fn test05_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test0501() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, "CMZ");
    }

    #[test]
    fn test0502() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, "MCD");
    }
}
