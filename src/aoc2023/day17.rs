/** Correct results:
 * - 791
 * - 900
 */
use crate::common::{
    direction::Direction,
    errors::AocError,
    map::Map,
    point::{Point, PointOps},
};
use itertools::Itertools;
use ndarray::{Array3, Dimension, Ix3, NdIndex};
use std::{collections::BinaryHeap, ops::Index};

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Hash)]
struct DijkstraNode {
    pos: Point<usize>,
    direction: Option<Direction>,
}

impl DijkstraNode {
    const fn dir_ix(&self) -> usize {
        match self.direction {
            Some(Direction::Up) => 0,
            Some(Direction::Down) => 1,
            Some(Direction::Left) => 2,
            Some(Direction::Right) => 3,
            _ => 4,
        }
    }
}

impl Ord for DijkstraNode {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.pos.cmp(&other.pos)
    }
}
impl PartialOrd for DijkstraNode {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

// SAFETY: impl is safe
unsafe impl NdIndex<Ix3> for DijkstraNode {
    #[inline]
    fn index_checked(&self, dim: &Ix3, strides: &Ix3) -> Option<isize> {
        dim.stride_offset_checked(strides, &Ix3(self.pos.x(), self.pos.y(), self.dir_ix()))
    }
    #[inline]
    fn index_unchecked(&self, strides: &Ix3) -> isize {
        (self.pos.x() * (*strides.index(0))
            + (self.pos.y() * (*strides.index(1)))
            + (self.dir_ix() * (*strides.index(2)))) as isize
    }
}

#[aoc_generator(day17)]
fn generator(input: &str) -> Map<usize> {
    let tiles = input
        .lines()
        .map(|line| {
            line.as_bytes()
                .iter()
                .map(|byte| (*byte - b'0') as usize)
                .collect_vec()
        })
        .collect_vec();
    Map::from_vec2(&tiles)
}

fn dijkstra(map: &Map<usize>, min_steps: usize, max_steps: usize) -> Result<usize, AocError> {
    let destination = (map.width() - 1, map.height() - 1);
    let mut visited: Array3<Option<isize>> =
        Array3::from_elem((map.height(), map.width(), 5), None);
    let mut queue = BinaryHeap::new();
    queue.push((0, DijkstraNode::default()));

    while let Some((cost, node)) = queue.pop() {
        if node.pos == destination {
            return Ok(-cost as usize);
        }

        if visited[node].is_some_and(|old_cost| -cost > old_cost) {
            continue;
        }

        for next_dir in [
            Direction::Up,
            Direction::Down,
            Direction::Left,
            Direction::Right,
        ] {
            if node
                .direction
                .is_some_and(|dir| dir == next_dir || dir == next_dir.flip())
            {
                continue;
            }
            let (dx, dy) = next_dir.as_diff::<isize>();
            let mut next_cost = -cost;
            for distance in 1..=(max_steps as isize) {
                let nx_isize = node.pos.x() as isize + dx * distance;
                let ny_isize = node.pos.y() as isize + dy * distance;
                if nx_isize < 0 || ny_isize < 0 {
                    continue;
                }
                let nx = nx_isize as usize;
                let ny = ny_isize as usize;
                if nx > map.width() - 1 || ny > map.height() - 1 {
                    continue;
                }
                next_cost += *(map.at((nx, ny))) as isize;
                if distance < min_steps as isize {
                    continue;
                }
                let key = DijkstraNode {
                    pos: (nx, ny),
                    direction: Some(next_dir),
                };

                if next_cost < visited[key].unwrap_or(isize::MAX) {
                    visited[key] = Some(next_cost);
                    queue.push((-next_cost, key));
                }
            }
        }
    }

    Err(AocError::NoSolution)
}

#[aoc(day17, part1)]
fn part01(input: &Map<usize>) -> Result<usize, AocError> {
    dijkstra(input, 1, 3)
}

#[aoc(day17, part2)]
fn part02(input: &Map<usize>) -> Result<usize, AocError> {
    dijkstra(input, 4, 10)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT_1: &str = r"2413432311323
3215453535623
3255245654254
3446585845452
4546657867536
1438598798454
4457876987766
3637877979653
4654967986887
4564679986453
1224686865563
2546548887735
4322674655533";

    const BUILD_INPUT_2: &str = r"111111111111
999999999991
999999999991
999999999991
999999999991";

    #[test]
    fn test_gen() {
        let input = generator(BUILD_INPUT_1);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test_01() {
        let input = generator(BUILD_INPUT_1);
        let res = part01(&input).unwrap();

        assert_eq!(res, 102);
    }

    #[test]
    fn test_02() {
        let input = generator(BUILD_INPUT_1);
        let res = part02(&input).unwrap();
        assert_eq!(res, 94);

        let input = generator(BUILD_INPUT_2);
        let res = part02(&input).unwrap();
        assert_eq!(res, 71);
    }
}
