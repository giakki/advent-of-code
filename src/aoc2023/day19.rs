/** Correct results:
 * - 399284
 * - 121964982771486
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{alpha1, char},
    combinator::{map, value},
    multi::separated_list0,
    sequence::{delimited, pair, preceded, separated_pair, tuple},
    IResult,
};
use std::{
    collections::HashMap,
    ops::{Index, IndexMut},
};

#[derive(Debug, Clone, Copy)]
enum Field {
    X,
    M,
    A,
    S,
}

impl Field {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        alt((
            value(Self::X, char('x')),
            value(Self::M, char('m')),
            value(Self::A, char('a')),
            value(Self::S, char('s')),
        ))(s)
    }
}
impl<T> Index<Field> for [T] {
    type Output = T;

    fn index(&self, index: Field) -> &Self::Output {
        match index {
            Field::X => &self[0],
            Field::M => &self[1],
            Field::A => &self[2],
            Field::S => &self[3],
        }
    }
}
impl<T> IndexMut<Field> for [T] {
    fn index_mut(&mut self, index: Field) -> &mut Self::Output {
        match index {
            Field::X => &mut self[0],
            Field::M => &mut self[1],
            Field::A => &mut self[2],
            Field::S => &mut self[3],
        }
    }
}

#[derive(Debug, Clone)]
enum Operator {
    LT,
    GT,
}

impl Operator {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        alt((value(Self::LT, char('<')), value(Self::GT, char('>'))))(s)
    }
}

#[derive(Debug, Clone)]
struct Operation<'a> {
    lhs: Field,
    operator: Operator,
    rhs: usize,
    destination: &'a str,
}

impl<'a> Operation<'a> {
    pub fn parse(s: &'a str) -> IResult<&'a str, Self> {
        map(
            tuple((
                Field::parse,
                Operator::parse,
                parse_int,
                preceded(char(':'), alpha1),
            )),
            |(lhs, operator, rhs, destination)| Self {
                lhs,
                operator,
                rhs,
                destination,
            },
        )(s)
    }
}

#[derive(Debug, Clone)]
enum OperationKind<'a> {
    Filter(Operation<'a>),
    Send(&'a str),
}

impl<'a> OperationKind<'a> {
    pub fn parse(s: &'a str) -> IResult<&'a str, Self> {
        alt((map(Operation::parse, Self::Filter), map(alpha1, Self::Send)))(s)
    }
}

#[derive(Debug)]
struct Workflow<'a> {
    name: &'a str,
    operations: Vec<OperationKind<'a>>,
}

impl<'a> Workflow<'a> {
    pub fn parse(s: &'a str) -> IResult<&'a str, Self> {
        map(
            pair(
                alpha1,
                delimited(
                    char('{'),
                    separated_list0(char(','), OperationKind::parse),
                    char('}'),
                ),
            ),
            |(name, operations)| Self { name, operations },
        )(s)
    }
}

#[derive(Debug)]
struct Part([usize; 4]);

impl Part {
    pub fn parse(input: &str) -> IResult<&str, Self> {
        let (s, values) = delimited(
            char('{'),
            separated_list0(char(','), separated_pair(Field::parse, tag("="), parse_int)),
            char('}'),
        )(input)?;

        let mut fields = [0_usize; 4];
        for (field, count) in values {
            fields[field as usize] = count;
        }

        Ok((s, Self(fields)))
    }
}

fn generator(input: &str) -> Result<(HashMap<&str, Workflow>, Vec<Part>), AocError> {
    let (workflows, parts) = input_parser(separated_pair(
        by_lines(Workflow::parse),
        tag("\n\n"),
        by_lines(Part::parse),
    ))(input)?;

    let mut workflows_by_name = HashMap::new();
    for workflow in workflows {
        workflows_by_name.insert(workflow.name, workflow);
    }

    Ok((workflows_by_name, parts))
}

#[aoc(day19, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    let (workflows, parts) = generator(input)?;
    let mut result = 0;
    for part in parts {
        let mut current_operation = "in";
        while current_operation != "A" && current_operation != "R" {
            if let Some(workflow) = workflows.get(&current_operation) {
                for operation in &workflow.operations {
                    match operation {
                        OperationKind::Send(destination) => {
                            current_operation = destination;
                            break;
                        }
                        OperationKind::Filter(op) => {
                            let lhs = part.0[op.lhs];
                            let operator = match op.operator {
                                Operator::LT => usize::lt,
                                Operator::GT => usize::gt,
                            };
                            if operator(&lhs, &op.rhs) {
                                current_operation = op.destination;
                                break;
                            }
                        }
                    }
                }
            } else {
                return Err(AocError::Input);
            }
        }
        if current_operation == "A" {
            result += part.0.into_iter().sum::<usize>();
        }
    }
    Ok(result)
}

#[aoc(day19, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    let (workflows, _) = generator(input)?;

    let mut result = 0;
    let mut paths: Vec<Vec<Operation<'_>>> = Vec::new();
    let mut queue = vec![(OperationKind::Send("in"), Vec::new())];
    while let Some((operation, operations_up_to_here)) = queue.pop() {
        if matches!(operation, OperationKind::Send("A")) {
            paths.push(operations_up_to_here);
            continue;
        }
        if matches!(operation, OperationKind::Send("R")) {
            continue;
        }
        match operation {
            OperationKind::Send(destination) => {
                let mut next_operations = operations_up_to_here;
                let workflow_operations = &workflows
                    .get(&destination)
                    .ok_or(AocError::Unspecified)?
                    .operations;

                for o in workflow_operations {
                    match o {
                        OperationKind::Send(_) => {
                            queue.push((o.clone(), next_operations.clone()));
                        }
                        OperationKind::Filter(op) => {
                            queue.push((o.clone(), next_operations.clone()));

                            let mut additional_op = op.clone();
                            additional_op.operator = match additional_op.operator {
                                Operator::LT => {
                                    additional_op.rhs -= 1;
                                    Operator::GT
                                }
                                Operator::GT => {
                                    additional_op.rhs += 1;
                                    Operator::LT
                                }
                            };
                            next_operations.push(additional_op);
                        }
                    }
                }
            }
            OperationKind::Filter(o) => {
                let mut next_operations = operations_up_to_here.clone();
                next_operations.push(o.clone());

                queue.push((OperationKind::Send(o.destination), next_operations.clone()));
            }
        }
    }

    for path in paths {
        let mut ranges = [(1, 4000), (1, 4000), (1, 4000), (1, 4000)];

        for operation in path {
            let range = &mut ranges[operation.lhs];
            match operation.operator {
                Operator::LT => range.1 = usize::min(range.1, operation.rhs - 1),
                Operator::GT => range.0 = usize::max(range.0, operation.rhs + 1),
            };
        }
        if ranges.iter().any(|(min, max)| min > max) {
            continue;
        }

        result += ranges
            .into_iter()
            .map(|(min, max)| (max - min + 1))
            .product::<usize>();
    }
    Ok(result)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"px{a<2006:qkq,m>2090:A,rfg}
pv{a>1716:R,A}
lnx{m>1548:A,A}
rfg{s<537:gd,x>2440:R,A}
qs{s>3448:A,lnx}
qkq{x<1416:A,crn}
crn{x>2662:A,R}
in{s<1351:px,qqz}
qqz{s>2770:qs,m<1801:hdj,R}
gd{a>3333:R,R}
hdj{m>838:A,pv}

{x=787,m=2655,a=1222,s=2876}
{x=1679,m=44,a=2067,s=496}
{x=2036,m=264,a=79,s=2244}
{x=2461,m=1339,a=466,s=291}
{x=2127,m=1623,a=2188,s=1013}";

    #[test]
    fn test_gen() {
        let input = generator(BUILD_INPUT).unwrap();

        println!("-- {:?} --", input)
    }

    #[test]
    fn test_01() {
        let res = part01(BUILD_INPUT).unwrap();
        assert_eq!(res, 19114);
    }

    #[test]
    fn test_02() {
        let res = part02(BUILD_INPUT).unwrap();
        assert_eq!(res, 167409079868000);
    }
}
