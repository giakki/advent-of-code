/** Correct results:
 * - 3697
 * - 608152828731262
 */
use crate::common::{
    chr::FromChar,
    errors::AocError,
    map::Map,
    point::{Point, PointOps},
};
use std::{collections::HashSet, str::FromStr};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Tile {
    Start,
    Garden,
    Rock,
}

impl FromChar for Tile {
    type Err = AocError;

    fn from_char(c: &char) -> Result<Self, Self::Err> {
        match c {
            'S' => Ok(Self::Start),
            '.' => Ok(Self::Garden),
            '#' => Ok(Self::Rock),
            _ => Err(AocError::new_parse("Invalid tile")),
        }
    }
}

fn generator(input: &str) -> Result<(Map<Tile>, Point<usize>), AocError> {
    let mut map = Map::from_str(input)?;

    let mut start = None;
    for (pos, tile) in map.tiles.indexed_iter_mut() {
        if matches!(tile, Tile::Start) {
            start = Some(pos);
            *tile = Tile::Garden;
        }
    }

    start.map(|point| (map, point)).ok_or(AocError::Input)
}

fn walk(map: &Map<Tile>, start: Point<usize>, num_steps: usize) -> usize {
    let mut positions: HashSet<(usize, usize)> = HashSet::new();
    positions.insert(start);

    for _ in 0..num_steps {
        let mut new_positions: HashSet<(usize, usize)> = HashSet::new();
        for position in positions {
            for neighbor in position
                .cardinal_neighbors_checked(map.width() - 1, map.height() - 1)
                .into_iter()
                .filter(|pos| *map.at(*pos) == Tile::Garden)
            {
                new_positions.insert(neighbor);
            }
        }
        positions = new_positions;
    }
    positions.len()
}

#[aoc(day21, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    let (map, start) = generator(input)?;

    Ok(walk(&map, start, 64))
}

#[aoc(day21, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    let (map, start) = generator(input)?;

    // https://www.youtube.com/watch?v=9UOMZSL0JTg
    // TODO: Assert grid is square
    // TODO: Assert start is at the center of the square
    // TODO: Assert the number of steps is a multiple of size / 2
    let grid_size: usize = 26_501_365 / map.width() - 1;
    let num_odd_grids = (grid_size / 2 * 2 + 1).pow(2);
    let num_even_grids = ((grid_size + 1) / 2 * 2).pow(2);
    // TODO: Assert that there are no weird twists???
    let num_odd_positions = walk(&map, start, map.width() * 2 + 1);
    let num_even_positions = walk(&map, start, map.width() * 2);

    let num_corner_points = [
        (0, start.y()),
        (start.x(), 0),
        (map.width() - 1, start.y()),
        (start.x(), map.height() - 1),
    ]
    .into_iter()
    .map(|corner| walk(&map, corner, map.width() - 1))
    .sum::<usize>();

    let num_small_diagonal_points = [
        (0, 0),
        (map.width() - 1, 0),
        (map.width() - 1, map.height() - 1),
        (0, map.height() - 1),
    ]
    .into_iter()
    .map(|point| walk(&map, point, map.width() * 3 / 2 - 1))
    .sum::<usize>();

    let num_big_diagonal_positions = [
        (0, 0),
        (map.width() - 1, 0),
        (map.width() - 1, map.height() - 1),
        (0, map.height() - 1),
    ]
    .into_iter()
    .map(|point| walk(&map, point, map.width() / 2 - 1))
    .sum::<usize>();

    Ok(num_odd_positions * num_odd_grids
        + num_even_positions * num_even_grids
        + num_corner_points
        + num_big_diagonal_positions * (grid_size + 1)
        + num_small_diagonal_points * grid_size)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"...........
.....###.#.
.###.##..#.
..#.#...#..
....#.#....
.##..S####.
.##..#...#.
.......##..
.##.#.####.
.##..##.##.
...........";

    #[test]
    fn test_gen() {
        let input = generator(BUILD_INPUT).unwrap();
        println!("-- {:?} --", input);
    }

    #[test]
    fn test_02() {
        let res = part02(BUILD_INPUT).unwrap();
        assert_eq!(res, 470149860542205);
    }
}
