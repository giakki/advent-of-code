/** Correct results:
 * - 12643
 * - 13133452426987
 */
use crate::common::{errors::AocError, parsers::input_parser};
use itertools::Itertools;
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{alphanumeric1, char},
    combinator::{map, map_res},
    multi::{many0, separated_list0},
    sequence::{delimited, separated_pair},
    IResult,
};
use num::Integer;
use std::collections::HashMap;

#[derive(Debug)]
enum Direction {
    Left,
    Right,
}

impl Direction {
    fn parse(s: &str) -> IResult<&str, Self> {
        map_res(alt((char('R'), char('L'))), |chr| match chr {
            'L' => Ok(Self::Left),
            'R' => Ok(Self::Right),
            _ => Err(AocError::ParseError(format!(
                "Cannot parse Direction: '{chr}'",
            ))),
        })(s)
    }
}

#[derive(Debug)]
struct Map {
    directions: Vec<Direction>,
    network: HashMap<String, (String, String)>,
}

impl Map {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        map(
            separated_pair(
                many0(Direction::parse),
                tag("\n\n"),
                separated_list0(
                    char('\n'),
                    separated_pair(
                        alphanumeric1,
                        tag(" = "),
                        delimited(
                            char('('),
                            separated_pair(alphanumeric1, tag(", "), alphanumeric1),
                            char(')'),
                        ),
                    ),
                ),
            ),
            |(directions, nodes)| Self {
                directions,
                network: nodes
                    .into_iter()
                    .map(|(a, (b, c))| (a.to_owned(), (b.to_owned(), c.to_owned())))
                    .collect(),
            },
        )(s)
    }
}

#[aoc_generator(day8)]
fn generator(input: &str) -> Result<Map, AocError> {
    input_parser(Map::parse)(input)
}

#[aoc(day8, part1)]
fn part01(input: &Map) -> usize {
    let mut current_step = "AAA";
    for (idx, direction) in input.directions.iter().cycle().enumerate() {
        if current_step == "ZZZ" {
            return idx;
        }
        match direction {
            Direction::Left => current_step = &input.network[current_step].0,
            Direction::Right => current_step = &input.network[current_step].1,
        }
    }

    0
}

#[aoc(day8, part2)]
fn part02(input: &Map) -> usize {
    let current_steps = input
        .network
        .keys()
        .filter(|key| key.ends_with('A'))
        .collect_vec();
    let mut steps_needed = vec![0; current_steps.len()];

    for (step_idx, step) in current_steps.into_iter().enumerate() {
        let mut current_step = step;

        for (num_steps, direction) in input.directions.iter().cycle().enumerate() {
            if current_step.ends_with('Z') {
                steps_needed[step_idx] = num_steps;
                break;
            }
            match direction {
                Direction::Left => current_step = &input.network[current_step].0,
                Direction::Right => current_step = &input.network[current_step].1,
            }
        }
    }

    steps_needed.into_iter().fold(1, |lcm, n| n.lcm(&lcm))
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT_1: &str = r"RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)";

    const BUILD_INPUT_2: &str = r"LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)";

    const BUILD_INPUT_3: &str = r"LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)";

    #[test]
    fn test_gen() {
        let input = generator(BUILD_INPUT_1);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test_01() {
        let input = generator(BUILD_INPUT_1).unwrap();
        let res = part01(&input);

        assert_eq!(res, 2);

        let input = generator(BUILD_INPUT_2).unwrap();
        let res = part01(&input);

        assert_eq!(res, 6);
    }

    #[test]
    fn test_02() {
        let input = generator(BUILD_INPUT_3).unwrap();
        let res = part02(&input);

        assert_eq!(res, 6);
    }
}
