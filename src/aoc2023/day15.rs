/** Correct results:
 * - 518107
 * - 303404
 */
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use nom::{
    branch::alt,
    character::complete::{alpha1, char},
    combinator::map,
    multi::separated_list0,
    sequence::{separated_pair, terminated},
    IResult,
};

enum Instruction<'a> {
    Remove(&'a str),
    Assign(&'a str, u32),
}

impl<'a> Instruction<'a> {
    pub fn parse(s: &'a str) -> IResult<&'a str, Self> {
        alt((
            map(terminated(alpha1, char('-')), Self::Remove),
            map(
                separated_pair(alpha1, char('='), parse_int),
                |(label, focal_length)| Self::Assign(label, focal_length),
            ),
        ))(s)
    }
}

fn generator(input: &str) -> Result<Vec<Instruction>, AocError> {
    input_parser(separated_list0(char(','), Instruction::parse))(input)
}

fn hash(s: &str) -> u8 {
    s.as_bytes()
        .iter()
        .fold(0_u32, |acc, chr| ((acc + u32::from(*chr)) * 17) % 256) as u8
}

#[aoc(day15, part1)]
fn part01(input: &str) -> u32 {
    input.split(',').map(|s| u32::from(hash(s))).sum()
}

#[aoc(day15, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    let mut boxes: Vec<Vec<(&str, usize)>> = vec![vec![]; 256];

    for instruction in generator(input)? {
        match instruction {
            Instruction::Remove(label) => {
                let idx = hash(label);
                let bucket = &mut boxes[idx as usize];

                if let Some(bucket_idx) = bucket.iter().position(|b| b.0 == label) {
                    bucket.remove(bucket_idx);
                }
            }
            Instruction::Assign(label, value) => {
                let idx = hash(label);
                let bucket = &mut boxes[idx as usize];

                if let Some(bucket_idx) = bucket.iter().position(|b| b.0 == label) {
                    bucket[bucket_idx] = (label, value as usize);
                } else {
                    bucket.push((label, value as usize));
                }
            }
        }
    }

    Ok(boxes
        .into_iter()
        .enumerate()
        .flat_map(|(bucket_idx, bucket)| {
            bucket
                .into_iter()
                .enumerate()
                .map(move |(idx, (_, focal_length))| (bucket_idx + 1) * (idx + 1) * focal_length)
        })
        .sum())
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7";

    #[test]
    fn test_01() {
        let res = part01(BUILD_INPUT);

        assert_eq!(res, 1320);
    }

    #[test]
    fn test_02() {
        let res = part02(BUILD_INPUT).unwrap();

        assert_eq!(res, 145);
    }
}
