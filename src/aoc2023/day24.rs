/** Correct results:
 * - 15318
 * - 870379016024859
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_signed_int},
    point3d::{Point3D, Point3dOps},
};
use itertools::Itertools;
use nom::{
    character::complete::{char, space0},
    combinator::map,
    sequence::{delimited, pair, separated_pair, terminated, tuple},
    IResult,
};
use z3::{
    ast::{Ast, Int},
    Config, Context, SatResult, Solver,
};

#[derive(Debug)]
struct Hailstone {
    pos: Point3D<f64>,
    vel: Point3D<f64>,
}

impl Hailstone {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        map(
            separated_pair(
                tuple((
                    terminated(parse_signed_int, pair(char(','), space0)),
                    terminated(parse_signed_int, pair(char(','), space0)),
                    parse_signed_int,
                )),
                delimited(space0, char('@'), space0),
                tuple((
                    terminated(parse_signed_int, pair(char(','), space0)),
                    terminated(parse_signed_int, pair(char(','), space0)),
                    parse_signed_int,
                )),
            ),
            |(pos, vel)| Self { pos, vel },
        )(s)
    }
}

fn find_collision_point(a: &Hailstone, b: &Hailstone) -> Option<(f64, f64)> {
    let dva = a.vel.y() / a.vel.x();
    let dvb = b.vel.y() / b.vel.x();
    if (dva - dvb).abs() < f64::EPSILON {
        return None;
    }

    Some((
        (a.pos.x().mul_add(dva, -b.pos.x() * dvb) + b.pos.y() - a.pos.y()) / (dva - dvb),
        ((b.pos.x() - a.pos.x()) * dva).mul_add(dvb, a.pos.y().mul_add(dvb, -b.pos.y() * dva))
            / (dvb - dva),
    ))
}

fn count_xy_intersections(hailstones: &[Hailstone], min: f64, max: f64) -> usize {
    hailstones
        .iter()
        .tuple_combinations()
        .filter(|(a, b)| {
            if let Some((x, y)) = find_collision_point(a, b) {
                if a.vel.x().signum() != (x - a.pos.x()).signum()
                    || b.vel.x().signum() != (x - b.pos.x()).signum()
                {
                    return false;
                }
                x >= min && x <= max && y >= min && y <= max
            } else {
                false
            }
        })
        .count()
}

#[aoc_generator(day24)]
fn generator(input: &str) -> Result<Vec<Hailstone>, AocError> {
    input_parser(by_lines(Hailstone::parse))(input)
}

#[aoc(day24, part1)]
fn part01(input: &[Hailstone]) -> usize {
    count_xy_intersections(input, 200_000_000_000_000., 400_000_000_000_000.)
}

#[aoc(day24, part2)]
fn part02(input: &[Hailstone]) -> Result<i64, AocError> {
    let ctx = Context::new(&Config::new());
    let solver = Solver::new(&ctx);

    let zero = Int::from_i64(&ctx, 0);
    let f_pos_x = Int::new_const(&ctx, "f_pos_x");
    let f_pos_y = Int::new_const(&ctx, "f_pos_y");
    let f_pos_z = Int::new_const(&ctx, "f_pos_z");
    let f_vel_x = Int::new_const(&ctx, "f_vel_x");
    let f_vel_y = Int::new_const(&ctx, "f_vel_y");
    let f_vel_z = Int::new_const(&ctx, "f_vel_z");

    for (i, hailstone) in input.iter().enumerate() {
        let pos_x = Int::from_i64(&ctx, hailstone.pos.x() as i64);
        let pos_y = Int::from_i64(&ctx, hailstone.pos.y() as i64);
        let pos_z = Int::from_i64(&ctx, hailstone.pos.z() as i64);
        let vel_x = Int::from_i64(&ctx, hailstone.vel.x() as i64);
        let vel_y = Int::from_i64(&ctx, hailstone.vel.y() as i64);
        let vel_z = Int::from_i64(&ctx, hailstone.vel.z() as i64);

        let time = Int::new_const(&ctx, format!("t{i}"));
        solver.assert(&time.ge(&zero));
        solver.assert(&((&pos_x + &vel_x * &time)._eq(&(&f_pos_x + &f_vel_x * &time))));
        solver.assert(&((&pos_y + &vel_y * &time)._eq(&(&f_pos_y + &f_vel_y * &time))));
        solver.assert(&((&pos_z + &vel_z * &time)._eq(&(&f_pos_z + &f_vel_z * &time))));
    }

    if solver.check() == SatResult::Sat {
        let model = solver.get_model().ok_or(AocError::Unspecified)?;

        model
            .eval(&(&f_pos_x + &f_pos_y + &f_pos_z), true)
            .and_then(|res| res.as_i64())
            .ok_or(AocError::NoSolution)
    } else {
        Err(AocError::NoSolution)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"19, 13, 30 @ -2,  1, -2
18, 19, 22 @ -1, -1, -2
20, 25, 34 @ -2, -2, -4
12, 31, 28 @ -1, -2, -1
20, 19, 15 @  1, -5, -3";

    #[test]
    fn test_gen() {
        let input = generator(BUILD_INPUT).unwrap();
        println!("-- {:?} --", input);
    }

    #[test]
    fn test_01() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = count_xy_intersections(&input, 7., 27.);
        assert_eq!(res, 2);
    }

    #[test]
    fn test_02() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input).unwrap();
        assert_eq!(res, 47);
    }
}
