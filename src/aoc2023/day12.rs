/** Correct results:
 * - 7090
 * - 6792010726878
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use itertools::{intersperse, Itertools};
use nom::{
    branch::alt,
    character::complete::{char, space1},
    combinator::{map, value},
    multi::{many1, separated_list1},
    sequence::separated_pair,
    IResult,
};
use std::ops::Add;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Tile {
    Operational,
    Damaged,
    Unknown,
}

impl Tile {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        alt((
            value(Self::Operational, char('.')),
            value(Self::Damaged, char('#')),
            value(Self::Unknown, char('?')),
        ))(s)
    }
}

#[derive(Debug)]
struct GeneratorRow {
    tiles: Vec<Tile>,
    descriptions: Vec<usize>,
}

impl GeneratorRow {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        map(
            separated_pair(
                many1(Tile::parse),
                space1,
                separated_list1(char(','), parse_int),
            ),
            |(tiles, description)| Self {
                tiles,
                descriptions: description,
            },
        )(s)
    }

    pub fn unfold(&mut self) {
        self.tiles = intersperse(vec![self.tiles.clone(); 5], vec![Tile::Unknown])
            .flatten()
            .collect_vec();
        self.descriptions = self.descriptions.repeat(5);
    }
}

fn generator(input: &str) -> Result<Vec<GeneratorRow>, AocError> {
    input_parser(by_lines(GeneratorRow::parse))(input)
}

fn runner(row: GeneratorRow) -> Result<usize, AocError> {
    let tiles = {
        let mut t = vec![Tile::Operational];
        t.extend(row.tiles);
        t
    };

    let mut arrangements = vec![0; tiles.len() + 1];
    arrangements[0] = 1;

    for i in 0..tiles.len() {
        if tiles[i] == Tile::Damaged {
            break;
        }
        arrangements[i + 1] = 1;
    }

    row.descriptions
        .into_iter()
        .fold(arrangements, |arrs, desc| {
            tiles
                .iter()
                .enumerate()
                .fold(
                    (vec![0; tiles.len() + 1], 0_usize),
                    |(mut arrangement_map, mut group_size), (idx, tile)| {
                        if *tile == Tile::Operational {
                            group_size = 0;
                        } else {
                            group_size += 1;
                        }

                        if *tile != Tile::Damaged {
                            arrangement_map[idx + 1] += arrangement_map[idx];
                        }

                        if group_size >= desc && tiles[idx - desc] != Tile::Damaged {
                            arrangement_map[idx + 1] += arrs[idx - desc];
                        }

                        (arrangement_map, group_size)
                    },
                )
                .0
        })
        .last()
        .copied()
        .ok_or(AocError::Unspecified)
}

#[aoc(day12, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    generator(input)?
        .into_iter()
        .map(runner)
        .fold_ok(0, Add::add)
}

#[aoc(day12, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    generator(input)?
        .into_iter()
        .map(|mut row| {
            row.unfold();
            runner(row)
        })
        .fold_ok(0, Add::add)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1";

    #[test]
    fn test_gen() {
        let input = generator(BUILD_INPUT);

        println!("{:?}", input)
    }

    #[test]
    fn test_01() {
        let res = part01(BUILD_INPUT).unwrap();

        assert_eq!(res, 21);
    }

    #[test]
    fn test_02() {
        let res = part02(BUILD_INPUT).unwrap();

        assert_eq!(res, 525152);
    }
}
