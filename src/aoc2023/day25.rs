/** Correct results:
 * - 545528
 */
use std::collections::{HashMap, HashSet, VecDeque};

use itertools::Itertools;
use nom::{
    bytes::complete::tag,
    character::complete::{alpha1, char},
    combinator::map,
    multi::separated_list1,
    sequence::separated_pair,
    IResult,
};
use petgraph::{graph::NodeIndex, Graph, Undirected};

use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser},
};

#[derive(Debug)]
struct Component<'a> {
    name: &'a str,
    wires: Vec<&'a str>,
}

impl<'a> Component<'a> {
    pub const fn new(name: &'a str) -> Self {
        Self {
            name,
            wires: Vec::new(),
        }
    }

    pub fn parse(s: &'a str) -> IResult<&'a str, Self> {
        map(
            separated_pair(alpha1, tag(": "), separated_list1(char(' '), alpha1)),
            |(name, wires)| Self { name, wires },
        )(s)
    }
}

fn generator(input: &str) -> Result<Vec<Component>, AocError> {
    input_parser(by_lines(Component::parse))(input)
}

#[aoc(day25, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    let cs = generator(input)?;
    let components = to_indexed_edges(cs);
    let mut graph = Graph::from_edges(components);

    for _ in 0..3 {
        let (n1, n2) = find_most_used_wire(&graph)?;
        while let Some(edge) = graph.find_edge(n1, n2) {
            graph.remove_edge(edge);
        }
    }

    Ok(visit_partitions(&graph))
}

fn to_indexed_edges(mut components: Vec<Component<'_>>) -> Vec<(usize, usize)> {
    let mut missing_components = Vec::new();
    for component in &components {
        for wire in &component.wires {
            if !components.iter().any(|c| c.name == *wire) {
                missing_components.push(Component::new(wire));
            }
        }
    }
    components.extend(missing_components);

    #[allow(clippy::unwrap_used)]
    components
        .iter()
        .enumerate()
        .flat_map(|(idx, component)| {
            component
                .wires
                .iter()
                .map(move |wire| (idx, wire))
                .map(|(i, wire)| (i, components.iter().position(|c| c.name == *wire).unwrap()))
        })
        .collect_vec()
}

fn find_most_used_wire(
    graph: &Graph<(), (), Undirected, usize>,
) -> Result<(NodeIndex<usize>, NodeIndex<usize>), AocError> {
    let mut wire_weights: HashMap<(NodeIndex<usize>, NodeIndex<usize>), usize> = HashMap::new();

    for node in graph.node_indices() {
        let mut visited = HashSet::from([node]);
        let mut queue = VecDeque::from([node]);
        while let Some(n) = queue.pop_front() {
            for neighbor in graph.neighbors_undirected(n) {
                if !visited.insert(neighbor) {
                    continue;
                }

                let wire = (std::cmp::min(n, neighbor), std::cmp::max(n, neighbor));

                *wire_weights.entry(wire).or_default() += 1;
                queue.push_back(neighbor);
            }
        }
    }

    wire_weights
        .into_iter()
        .max_by_key(|(_, weight)| *weight)
        .map(|(nodes, _)| nodes)
        .ok_or(AocError::Unspecified)
}

fn visit_partitions(graph: &Graph<(), (), Undirected, usize>) -> usize {
    let mut res = 1;
    let mut node_indices = graph.node_indices().collect::<HashSet<_>>();

    while let Some(start) = node_indices.iter().next() {
        let mut visited = HashSet::new();
        let mut queue = VecDeque::from([*start]);
        while let Some(curr) = queue.pop_front() {
            if visited.insert(curr) {
                queue.extend(graph.neighbors_undirected(curr));
            }
        }

        node_indices = node_indices.difference(&visited).copied().collect();
        res *= visited.len();
    }

    res
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"jqt: rhn xhk nvd
rsh: frs pzl lsr
xhk: hfx
cmg: qnr nvd lhk bvb
rhn: xhk bvb hfx
bvb: xhk hfx
pzl: lsr hfx nvd
qnr: nvd
ntq: jqt hfx bvb xhk
nvd: lhk
lsr: lhk
rzs: qnr cmg lsr rsh
frs: qnr lhk lsr";

    #[test]
    fn test_gen() {
        let input = generator(BUILD_INPUT).unwrap();
        println!("-- {:?} --", input);
    }

    #[test]
    fn test_01() {
        let res = part01(&BUILD_INPUT).unwrap();
        assert_eq!(res, 54);
    }
}
