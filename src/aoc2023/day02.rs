/** Correct results:
 * - 2913
 * - 55593
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::char,
    combinator::{map, value},
    multi::separated_list0,
    sequence::{delimited, pair, terminated, tuple},
    IResult,
};

#[derive(Debug, Clone)]
enum Color {
    Red,
    Green,
    Blue,
}

#[derive(Debug, Default, PartialEq)]
struct Revealed {
    r: usize,
    g: usize,
    b: usize,
}

impl Revealed {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        let (remaining, cubes) = separated_list0(
            tag(", "),
            pair(
                terminated(parse_int, char(' ')),
                alt((
                    value(Color::Red, tag("red")),
                    value(Color::Green, tag("green")),
                    value(Color::Blue, tag("blue")),
                )),
            ),
        )(s)?;

        Ok((remaining, Self::from(cubes)))
    }

    pub const fn is_valid(&self) -> bool {
        self.r <= 12 && self.g <= 13 && self.b <= 14
    }

    pub const fn power(&self) -> usize {
        self.r * self.g * self.b
    }
}

impl From<Vec<(usize, Color)>> for Revealed {
    fn from(value: Vec<(usize, Color)>) -> Self {
        value
            .iter()
            .fold(Self::default(), |mut revealed, (amount, idx)| {
                match idx {
                    Color::Red => revealed.r += amount,
                    Color::Green => revealed.g += amount,
                    Color::Blue => revealed.b += amount,
                }
                revealed
            })
    }
}

#[derive(Debug, PartialEq)]
struct Game {
    pub id: usize,
    pub revealed: Vec<Revealed>,
}

impl Game {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        map(
            tuple((
                delimited(tag("Game "), parse_int, tag(": ")),
                separated_list0(tag("; "), Revealed::parse),
            )),
            |(id, revealed)| Self { id, revealed },
        )(s)
    }
}

#[aoc_generator(day2)]
fn generator(input: &str) -> Result<Vec<Game>, AocError> {
    input_parser(by_lines(Game::parse))(input)
}

#[aoc(day2, part1)]
fn part01(input: &[Game]) -> usize {
    input
        .iter()
        .filter_map(|game| {
            game.revealed
                .iter()
                .all(Revealed::is_valid)
                .then_some(game.id)
        })
        .sum()
}

#[aoc(day2, part2)]
fn part02(input: &[Game]) -> usize {
    input
        .iter()
        .map(|game| {
            game.revealed
                .iter()
                .fold(Revealed::default(), |mut max, value| {
                    max.r = max.r.max(value.r);
                    max.g = max.g.max(value.g);
                    max.b = max.b.max(value.b);
                    max
                })
                .power()
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green";

    #[test]
    fn test_01() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 8);
    }

    #[test]
    fn test_02() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, 2286);
    }
}
