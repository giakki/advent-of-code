/** Correct results:
 * - 27202
 * - 41566
 */
use crate::common::errors::AocError;
use itertools::Itertools;
use ndarray::Array2;
use std::ops::Add;

fn generator(input: &str) -> Result<Vec<Array2<u8>>, AocError> {
    input
        .split("\n\n")
        .map(|pattern| {
            let y = pattern.lines().count();
            let x = pattern.len() / y;
            Array2::from_shape_vec(
                (y, x),
                pattern
                    .lines()
                    .flat_map(|line| line.as_bytes().iter().copied())
                    .collect_vec(),
            )
            .map_err(AocError::from)
        })
        .collect::<Result<Vec<Array2<u8>>, AocError>>()
}

fn find_reflections(pattern: &Array2<u8>) -> Vec<usize> {
    let mut reflections = Vec::new();
    let (height, _) = pattern.dim();
    let mut start = 0;
    let mut end = 1;
    while end < height {
        let mut is_reflection = false;
        let mut i0 = start;
        let mut i1 = end;

        loop {
            if pattern.row(i0) != pattern.row(i1) {
                break;
            }
            if i0 == 0 || i1 == height - 1 {
                is_reflection = true;
                break;
            }
            i0 -= 1;
            i1 += 1;
        }

        if is_reflection {
            reflections.push(start + 1);
        }

        start += 1;
        end += 1;
    }
    reflections
}

fn reflect(pattern: &Array2<u8>) -> impl Iterator<Item = usize> {
    find_reflections(pattern)
        .into_iter()
        .map(|n| n * 100)
        .chain(find_reflections(&pattern.clone().reversed_axes()))
}

fn smudge(mut pattern: Array2<u8>) -> Option<usize> {
    let base = reflect(&pattern).collect_vec();
    for y in 0..pattern.dim().0 {
        for x in 0..pattern.dim().1 {
            let init = pattern[(y, x)];
            if pattern[(y, x)] == b'.' {
                pattern[(y, x)] = b'#';
            } else {
                pattern[(y, x)] = b'.';
            }
            let new_reflections = reflect(&pattern)
                .filter(|n| !base.contains(n))
                .collect_vec();
            if new_reflections.is_empty() {
                pattern[(y, x)] = init;
            } else {
                return Some(new_reflections.into_iter().sum());
            }
        }
    }
    None
}

#[aoc(day13, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    Ok(generator(input)?
        .iter()
        .map(|pattern| reflect(pattern).sum::<usize>())
        .sum())
}

#[aoc(day13, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    generator(input)?
        .into_iter()
        .map(smudge)
        .fold_options(0, Add::add)
        .ok_or(AocError::NoSolution)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.

#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#";

    #[test]
    fn test_gen() {
        let input = generator(BUILD_INPUT);

        println!("{:?}", input)
    }

    #[test]
    fn test_01() {
        let res = part01(BUILD_INPUT).unwrap();

        assert_eq!(res, 405);
    }

    #[test]
    fn test_02() {
        let res = part02(BUILD_INPUT).unwrap();

        assert_eq!(res, 400);
    }
}
