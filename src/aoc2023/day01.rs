/** Correct results:
 * - 55712
 * - 55413
 */
use crate::common::chr::FromAsciiDigit;

#[aoc(day1, part1)]
fn part01(input: &str) -> u32 {
    input
        .lines()
        .filter_map(|line| {
            let mut digits = line
                .as_bytes()
                .iter()
                .filter(|&chr| chr.is_ascii_digit())
                .map(|chr| u32::from_ascii_digit(*chr));

            digits
                .next()
                .map(|first| 10 * first + digits.last().unwrap_or(first))
        })
        .sum()
}

#[aoc(day1, part2)]
fn part02(input: &str) -> usize {
    const MAPPING: [&str; 9] = [
        "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
    ];

    #[allow(clippy::string_slice)]
    input
        .lines()
        .filter_map(|line| {
            let mut digits = line.as_bytes().iter().enumerate().filter_map(|(idx, chr)| {
                if chr.is_ascii_digit() {
                    Some(usize::from_ascii_digit(*chr))
                } else {
                    for (n, lit) in MAPPING.iter().enumerate() {
                        if line[idx..].starts_with(lit) {
                            return Some(n + 1);
                        }
                    }
                    None
                }
            });

            digits
                .next()
                .map(|first| 10 * first + digits.last().unwrap_or(first))
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT_1: &str = r"1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet";

    const BUILD_INPUT_2: &str = r"two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen";

    #[test]
    fn test_01() {
        let res = part01(BUILD_INPUT_1);

        assert_eq!(res, 142);
    }

    #[test]
    fn test_02() {
        let res = part02(BUILD_INPUT_2);

        assert_eq!(res, 281);
    }
}
