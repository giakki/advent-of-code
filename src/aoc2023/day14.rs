/** Correct results:
 * - 105461
 * - 102829
 */
use crate::common::{
    chr::FromChar, direction::Direction, errors::AocError, map::Map, point::PointOps,
};
use itertools::Itertools;
use std::{collections::HashMap, fmt::Display, str::FromStr};

#[derive(Copy, Clone, Debug)]
enum Tile {
    RoundStone,
    CubicStone,
    Empty,
}

impl FromChar for Tile {
    type Err = AocError;

    fn from_char(c: &char) -> Result<Self, Self::Err> {
        match c {
            'O' => Ok(Self::RoundStone),
            '#' => Ok(Self::CubicStone),
            '.' => Ok(Self::Empty),
            _ => Err(AocError::ParseError(format!("Cannot parse tile '{c}'"))),
        }
    }
}

impl Display for Tile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::RoundStone => f.write_str("O"),
            Self::CubicStone => f.write_str("#"),
            Self::Empty => f.write_str("."),
        }
    }
}

fn map_weight(map: &Map<Tile>) -> usize {
    map.tiles
        .indexed_iter()
        .filter(|(_, tile)| matches!(tile, Tile::RoundStone))
        .map(|(pos, _)| map.height() - pos.y())
        .sum()
}

fn tilt(map: &mut Map<Tile>, direction: Direction) {
    let axis_iter = match direction {
        Direction::Up | Direction::Down => 0..map.width(),
        Direction::Left | Direction::Right => 0..map.height(),
    };

    for axis_index in axis_iter {
        tilt_axis(map, axis_index, direction);
    }
}

fn tilt_axis(map: &mut Map<Tile>, axis_index: usize, direction: Direction) {
    match direction {
        Direction::Up => tilt_up(map, axis_index, direction),
        Direction::Down => tilt_down(map, axis_index, direction),
        Direction::Left => tilt_left(map, axis_index, direction),
        Direction::Right => tilt_right(map, axis_index, direction),
    }
}

fn tilt_up(map: &mut Map<Tile>, axis_index: usize, direction: Direction) {
    for iter_y in 0..map.height() {
        let mut curr_pos = (axis_index, iter_y);
        if matches!(map.at(curr_pos), Tile::RoundStone) {
            while curr_pos.y() > 0 {
                let next_pos = curr_pos.moved(1, &direction);
                let next = map.at_mut(next_pos);
                if matches!(next, Tile::Empty) {
                    *next = Tile::RoundStone;
                    *map.at_mut(curr_pos) = Tile::Empty;
                } else {
                    break;
                }
                curr_pos = next_pos;
            }
        }
    }
}

fn tilt_down(map: &mut Map<Tile>, axis_index: usize, direction: Direction) {
    for iter_y in (0..map.height()).rev() {
        let mut curr_pos = (axis_index, iter_y);
        if matches!(map.at(curr_pos), Tile::RoundStone) {
            while curr_pos.y() < map.height() - 1 {
                let next_pos = curr_pos.moved(1, &direction);
                let next = map.at_mut(next_pos);
                if matches!(next, Tile::Empty) {
                    *next = Tile::RoundStone;
                    *map.at_mut(curr_pos) = Tile::Empty;
                } else {
                    break;
                }
                curr_pos = next_pos;
            }
        }
    }
}

fn tilt_left(map: &mut Map<Tile>, axis_index: usize, direction: Direction) {
    for iter_x in 0..map.width() {
        let mut curr_pos = (iter_x, axis_index);
        if matches!(map.at(curr_pos), Tile::RoundStone) {
            while curr_pos.x() > 0 {
                let next_pos = curr_pos.moved(1, &direction);
                let next = map.at_mut(next_pos);
                if matches!(next, Tile::Empty) {
                    *next = Tile::RoundStone;
                    *map.at_mut(curr_pos) = Tile::Empty;
                } else {
                    break;
                }
                curr_pos = next_pos;
            }
        }
    }
}

fn tilt_right(map: &mut Map<Tile>, axis_index: usize, direction: Direction) {
    for iter_x in (0..map.width()).rev() {
        let mut curr_pos = (iter_x, axis_index);
        if matches!(map.at(curr_pos), Tile::RoundStone) {
            while curr_pos.x() < map.width() - 1 {
                let next_pos = curr_pos.moved(1, &direction);
                let next = map.at_mut(next_pos);
                if matches!(next, Tile::Empty) {
                    *next = Tile::RoundStone;
                    *map.at_mut(curr_pos) = Tile::Empty;
                } else {
                    break;
                }
                curr_pos = next_pos;
            }
        }
    }
}

#[aoc(day14, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    let mut map = Map::from_str(input)?;

    tilt(&mut map, Direction::Up);

    Ok(map_weight(&map))
}

#[aoc(day14, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    let mut map = Map::from_str(input)?;

    let mut cache_by_stones = HashMap::new();
    let mut cache_by_index = Vec::new();

    for i in 0_usize..1_000_000_000 {
        let round_stones = map
            .tiles
            .indexed_iter()
            .filter_map(|(pos, tile)| matches!(tile, Tile::RoundStone).then_some(pos))
            .collect_vec();
        if let Some(start) = cache_by_stones.get(&round_stones) {
            let repetition_length = i - start;
            let cycles_missing = 1_000_000_000 - start;
            let cycle_index = cycles_missing % repetition_length;

            return Ok(cache_by_index[start + cycle_index - 1]);
        }

        tilt(&mut map, Direction::Up);
        tilt(&mut map, Direction::Left);
        tilt(&mut map, Direction::Down);
        tilt(&mut map, Direction::Right);

        cache_by_stones.insert(round_stones, i);
        cache_by_index.push(map_weight(&map));
    }

    Ok(map_weight(&map))
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"O....#....
O.OO#....#
.....##...
OO.#O....O
.O.....O#.
O.#..O.#.#
..O..#O..O
.......O..
#....###..
#OO..#....";

    #[test]
    fn test_gen() {
        let input = Map::<Tile>::from_str(BUILD_INPUT);

        println!("{:?}", input);
    }

    #[test]
    fn test_01() {
        let res = part01(BUILD_INPUT).unwrap();

        assert_eq!(res, 136);
    }

    #[test]
    fn test_02() {
        let res = part02(BUILD_INPUT).unwrap();

        assert_eq!(res, 64);
    }
}
