/** Correct results:
 * - 912199500
 * - 237878264003759
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser},
};
use itertools::Itertools;
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{alpha1, char},
    combinator::map,
    multi::separated_list1,
    sequence::{preceded, separated_pair},
    IResult,
};
use num::Integer;
use std::collections::{HashMap, VecDeque};

#[derive(Debug, Clone)]
enum ModuleRaw<'a> {
    FlipFlop(Vec<&'a str>, bool),
    Conjunction(Vec<&'a str>, HashMap<&'a str, bool>),
    Broadcast(Vec<&'a str>),
}

impl<'a> ModuleRaw<'a> {
    pub fn parse(s: &'a str) -> IResult<&'a str, (&'a str, Self)> {
        alt((
            map(
                preceded(tag("broadcaster -> "), separated_list1(tag(", "), alpha1)),
                |destinations| ("", Self::Broadcast(destinations)),
            ),
            map(
                separated_pair(
                    preceded(char('%'), alpha1),
                    tag(" -> "),
                    separated_list1(tag(", "), alpha1),
                ),
                |(name, destinations)| (name, Self::FlipFlop(destinations, false)),
            ),
            map(
                separated_pair(
                    preceded(char('&'), alpha1),
                    tag(" -> "),
                    separated_list1(tag(", "), alpha1),
                ),
                |(name, destinations)| (name, Self::Conjunction(destinations, HashMap::new())),
            ),
        ))(s)
    }

    pub const fn outputs(&self) -> &Vec<&'a str> {
        match self {
            Self::FlipFlop(destinations, _)
            | Self::Conjunction(destinations, _)
            | Self::Broadcast(destinations) => destinations,
        }
    }
}

#[derive(Debug, Clone)]
enum Module {
    FlipFlop(Vec<usize>, bool),
    Conjunction(Vec<usize>, HashMap<usize, bool>),
    Broadcast(Vec<usize>),
}

impl Module {
    pub fn from_raw(raw: ModuleRaw, index_map: &HashMap<&str, usize>) -> Self {
        match raw {
            ModuleRaw::FlipFlop(destinations, state) => Self::FlipFlop(
                destinations
                    .into_iter()
                    .filter_map(|destination| index_map.get(destination).copied())
                    .collect_vec(),
                state,
            ),
            ModuleRaw::Conjunction(destinations, memory) => Self::Conjunction(
                destinations
                    .into_iter()
                    .filter_map(|destination| index_map.get(destination).copied())
                    .collect_vec(),
                memory
                    .into_iter()
                    .filter_map(|(origin, state)| index_map.get(origin).map(|d| (*d, state)))
                    .collect(),
            ),
            ModuleRaw::Broadcast(destinations) => Self::Broadcast(
                destinations
                    .into_iter()
                    .filter_map(|destination| index_map.get(destination).copied())
                    .collect_vec(),
            ),
        }
    }

    pub fn signal(&mut self, from: usize, signal: bool) -> Option<(&Vec<usize>, bool)> {
        match self {
            Self::Conjunction(outputs, memory) => {
                memory.entry(from).and_modify(|e| *e = signal);
                let is_high = memory.values().all(|v| *v);
                Some((outputs, !is_high))
            }
            Self::FlipFlop(outputs, state) => {
                if signal {
                    None
                } else {
                    *state = !*state;
                    Some((outputs, *state))
                }
            }
            Self::Broadcast(outputs) => Some((outputs, signal)),
        }
    }

    pub const fn outputs(&self) -> &Vec<usize> {
        match self {
            Self::FlipFlop(destinations, _)
            | Self::Conjunction(destinations, _)
            | Self::Broadcast(destinations) => destinations,
        }
    }
}

#[derive(Debug)]
enum Cycle {
    None,
    Start(usize),
    End(usize),
}

fn generator(input: &str) -> Result<(HashMap<&str, usize>, Vec<Module>), AocError> {
    let mut modules = input_parser(by_lines(ModuleRaw::parse))(input)?;

    for (module_name, module) in modules.clone() {
        for output in module.outputs() {
            match modules.iter_mut().find(|(name, _)| name == output) {
                Some((_, ModuleRaw::Conjunction(_, inputs))) => {
                    inputs.entry(module_name).or_insert(false);
                }
                None => {
                    modules.push((output, ModuleRaw::Broadcast(Vec::new())));
                }
                _ => {}
            }
        }
    }

    let mut modules_by_index = Vec::with_capacity(modules.len());
    let mut module_indexes_by_name = HashMap::new();
    for (idx, (name, _)) in modules.iter().enumerate() {
        module_indexes_by_name.entry(*name).or_insert(idx);
    }
    for (_, module) in modules {
        modules_by_index.push(Module::from_raw(module, &module_indexes_by_name));
    }

    Ok((module_indexes_by_name, modules_by_index))
}

#[aoc(day20, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    let (modules_by_name, mut modules) = generator(input)?;

    let mut lows = 0;
    let mut highs = 0;

    let broadcast_addr = modules_by_name[""];
    let mut signals: VecDeque<(usize, usize, bool)> = VecDeque::new();
    for _ in 0..1000 {
        signals.push_back((usize::MAX, broadcast_addr, false));

        while let Some((from, module_idx, signal)) = signals.pop_front() {
            if signal {
                highs += 1;
            } else {
                lows += 1;
            }
            if let Some((outputs, s)) = modules[module_idx].signal(from, signal) {
                signals.extend(outputs.iter().map(|to| (module_idx, *to, s)));
            }
        }
    }

    Ok(lows * highs)
}

#[aoc(day20, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    let (modules_by_name, mut modules) = generator(input)?;

    // Hand rolled solution.
    let rx_addr = modules_by_name["rx"];
    let rx_sources = modules
        .iter()
        .enumerate()
        .filter_map(|(idx, module)| module.outputs().contains(&rx_addr).then_some(idx))
        .collect_vec();
    let rx_sources_sources = rx_sources
        .into_iter()
        .flat_map(|idx| {
            modules
                .iter()
                .enumerate()
                .filter_map(|(source_idx, module)| {
                    module.outputs().contains(&idx).then_some(source_idx)
                })
                .collect_vec()
        })
        .collect_vec();

    let mut cycles: HashMap<usize, Cycle> = rx_sources_sources
        .into_iter()
        .map(|idx| (idx, Cycle::None))
        .collect();

    let broadcast_addr = modules_by_name[""];
    let mut signals: VecDeque<(usize, usize, bool)> = VecDeque::new();
    for i in 0.. {
        if cycles.values().all(|v| matches!(v, Cycle::End(_))) {
            return Ok(cycles
                .values()
                .filter_map(|c| if let Cycle::End(v) = c { Some(v) } else { None })
                .fold(1, |x, y| x.lcm(y)));
        }

        signals.push_back((usize::MAX, broadcast_addr, false));
        while let Some((from, module_idx, signal)) = signals.pop_front() {
            if module_idx == rx_addr && !signal {
                return Ok(i + 1);
            }
            if !signal {
                if let Some(cycle) = cycles.get_mut(&module_idx) {
                    match cycle {
                        Cycle::None => *cycle = Cycle::Start(i + 1),
                        Cycle::Start(start) => *cycle = Cycle::End(i + 1 - *start),
                        Cycle::End(_) => {}
                    }
                }
            }
            if let Some((outputs, s)) = modules[module_idx].signal(from, signal) {
                signals.extend(outputs.iter().map(|to| (module_idx, *to, s)));
            }
        }
    }

    Err(AocError::NoSolution)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT_1: &str = r"broadcaster -> a, b, c
%a -> b
%b -> c
%c -> inv
&inv -> a";

    const BUILD_INPUT_2: &str = r"broadcaster -> a
%a -> inv, con
&inv -> b
%b -> con
&con -> output";

    #[test]
    fn test_gen() {
        let input = generator(BUILD_INPUT_1).unwrap();
        println!("-- {:?} --", input);

        let input = generator(BUILD_INPUT_2).unwrap();
        println!("-- {:?} --", input);
    }

    #[test]
    fn test_01() {
        let res = part01(BUILD_INPUT_1).unwrap();
        assert_eq!(res, 32000000);
        let res = part01(BUILD_INPUT_2).unwrap();
        assert_eq!(res, 11687500);
    }

    #[test]
    fn test_02() {
        let res = part02(BUILD_INPUT_1).unwrap();
        assert_eq!(res, 952408144115);
    }
}
