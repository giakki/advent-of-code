/** Correct results:
 * - 475
 * - 79144
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_point3d_comma},
    point3d::{Point3D, Point3dOps},
};
use ndarray::{s, Array2, Dim, SliceInfo, SliceInfoElem};
use nom::{character::complete::char, combinator::map, sequence::separated_pair, IResult};

use std::{cmp::Ordering, collections::HashSet};

#[derive(Debug)]
struct Brick {
    a: Point3D<usize>,
    b: Point3D<usize>,
}

impl Brick {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        map(
            separated_pair(parse_point3d_comma, char('~'), parse_point3d_comma),
            |((x1, y1, z1), (x2, y2, z2)): (Point3D<usize>, Point3D<usize>)| Self {
                a: (x1, y1, z1),
                b: (x2 + 1, y2 + 1, z2 + 1),
            },
        )(s)
    }

    pub fn as_xy_slice(&self) -> SliceInfo<[SliceInfoElem; 2], Dim<[usize; 2]>, Dim<[usize; 2]>> {
        s![self.a.x()..self.b.x(), self.a.y()..self.b.y()]
    }

    pub fn ord_key_z(&self) -> Point3D<usize> {
        (self.a.z(), self.a.x(), self.a.y())
    }

    pub fn height(&self) -> usize {
        self.b.z() - self.a.z()
    }
}

const NO_SUPPORT: usize = usize::MAX;

fn generator(input: &str) -> Result<Vec<Brick>, AocError> {
    input_parser(by_lines(Brick::parse))(input)
}

#[aoc(day22, part1)]
pub fn part01(input: &str) -> Result<usize, AocError> {
    let mut bricks = generator(input)?;
    bricks.sort_unstable_by_key(Brick::ord_key_z);
    let (max_x, max_y) = bricks.iter().fold((0, 0), |(max_x, max_y), brick| {
        (
            usize::max(max_x, brick.b.x()),
            usize::max(max_y, brick.b.y()),
        )
    });

    let mut supports = vec![false; bricks.len()];
    let mut top_view = Array2::from_elem((max_x, max_y), (0_usize, NO_SUPPORT));

    for (idx, brick) in bricks.into_iter().enumerate() {
        let max_z = top_view
            .slice(brick.as_xy_slice())
            .iter()
            .max_by_key(|(z, _)| z)
            .map_or(0, |(z, _)| *z);

        let mut topmost_support = None;
        let mut num_supports = 0;
        for (z, support_idx) in &mut top_view.slice_mut(brick.as_xy_slice()) {
            if *z == max_z && *support_idx != NO_SUPPORT && topmost_support != Some(*support_idx) {
                num_supports += 1;
                topmost_support = Some(*support_idx);
            }

            *support_idx = idx;
            *z = max_z + brick.height();
        }

        if num_supports == 1 {
            if let Some(ts) = topmost_support {
                supports[ts] = true;
            }
        }
    }

    Ok(supports.iter().filter(|&&sup| !sup).count())
}

#[aoc(day22, part2)]
pub fn part02(input: &str) -> Result<usize, AocError> {
    let mut bricks = generator(input)?;
    let num_bricks = bricks.len();
    bricks.sort_unstable_by_key(Brick::ord_key_z);

    let (max_x, max_y) = bricks.iter().fold((0, 0), |(max_x, max_y), brick| {
        (
            usize::max(max_x, brick.b.x()),
            usize::max(max_y, brick.b.y()),
        )
    });

    let mut top_view = Array2::from_elem((max_x, max_y), (0_usize, NO_SUPPORT));

    let mut dominators = vec![(NO_SUPPORT, 0); bricks.len() + 1];
    dominators[0].0 = 0;

    let mut result = 0;

    for (idx, brick) in bricks.into_iter().enumerate() {
        let mut supported_by_this = HashSet::new();
        let max_z = top_view
            .slice(brick.as_xy_slice())
            .iter()
            .max_by_key(|(z, _)| z)
            .map_or(0, |(z, _)| *z);

        for (z, support_idx) in &mut top_view.slice_mut(brick.as_xy_slice()) {
            if *z == max_z && *support_idx != NO_SUPPORT {
                supported_by_this.insert(*support_idx);
            }
            *support_idx = idx;
            *z = max_z + brick.height();
        }

        dominators[idx + 1].0 = supported_by_this
            .iter()
            .map(|&n| n + 1)
            .reduce(|mut f1, mut f2| loop {
                match f1.cmp(&f2) {
                    Ordering::Less => f2 = dominators[f2].0,
                    Ordering::Greater => f1 = dominators[f1].0,
                    Ordering::Equal => return f1,
                }
            })
            .unwrap_or(0);
        dominators[idx + 1].1 = dominators[dominators[idx + 1].0].1 + 1;
        result += dominators[idx + 1].1;
    }

    Ok(result - num_bricks)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"1,0,1~1,2,1
0,0,2~2,0,2
0,2,3~2,2,3
0,0,4~0,2,4
2,0,5~2,2,5
0,1,6~2,1,6
1,1,8~1,1,9";

    #[test]
    fn test_gen() {
        let input = generator(BUILD_INPUT).unwrap();
        println!("-- {:?} --", input);
    }

    #[test]
    fn test_01() {
        let res = part01(BUILD_INPUT).unwrap();
        assert_eq!(res, 5);
    }

    #[test]
    fn test_02() {
        let res = part02(BUILD_INPUT).unwrap();
        assert_eq!(res, 7);
    }
}
