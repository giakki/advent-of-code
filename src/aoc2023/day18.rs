/** Correct results:
 * - 33491
 * - 87716969654406
 */
use crate::common::{
    direction::Direction,
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
    point::PointOps,
};
use nom::{
    bytes::complete::{tag, take},
    character::complete::{char, one_of, space1},
    combinator::map,
    sequence::{delimited, pair, preceded, tuple},
    IResult,
};

#[derive(Debug)]
struct Color {
    amount: isize,
    direction: Direction,
}

impl Color {
    pub fn parse(input: &str) -> IResult<&str, Self> {
        let (s, (a, d)) =
            delimited(tag("(#"), pair(take(5_usize), one_of("0123")), char(')'))(input)?;

        let amount = isize::from_str_radix(a, 16).map_err(|_| {
            nom::Err::Failure(nom::error::make_error(s, nom::error::ErrorKind::Verify))
        })?;

        let direction = match d {
            '0' => Direction::Right,
            '1' => Direction::Down,
            '2' => Direction::Left,
            '3' => Direction::Up,
            _ => {
                return Err(nom::Err::Failure(nom::error::make_error(
                    s,
                    nom::error::ErrorKind::Verify,
                )));
            }
        };

        Ok((s, Self { amount, direction }))
    }
}

#[derive(Debug)]
struct Instruction {
    direction: Direction,
    amount: isize,
    color: Color,
}

impl Instruction {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        map(
            tuple((
                Direction::parse_from_initial,
                preceded(space1, parse_int),
                preceded(space1, Color::parse),
            )),
            |(direction, amount, color)| Self {
                direction,
                amount,
                color,
            },
        )(s)
    }
}

#[aoc_generator(day18)]
fn generator(input: &str) -> Result<Vec<Instruction>, AocError> {
    input_parser(by_lines(Instruction::parse))(input)
}

fn solve(instructions: impl Iterator<Item = (isize, Direction)>) -> usize {
    let (_, area, perimeter) = instructions.fold(
        ((0, 0), 0, 0),
        |((x1, y1), area, perimeter), (amount, direction)| {
            let (x2, y2) = (x1, y1).moved(amount, &direction);

            ((x2, y2), area + (y1 + y2) * (x2 - x1), perimeter + amount)
        },
    );

    (perimeter / 2 + area.abs() / 2 + 1) as usize
}

#[aoc(day18, part1)]
fn part01(input: &[Instruction]) -> usize {
    solve(
        input
            .iter()
            .map(|instruction| (instruction.amount, instruction.direction)),
    )
}

#[aoc(day18, part2)]
fn part02(input: &[Instruction]) -> usize {
    solve(
        input
            .iter()
            .map(|instruction| (instruction.color.amount, instruction.color.direction)),
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"R 6 (#70c710)
D 5 (#0dc571)
L 2 (#5713f0)
D 2 (#d2c081)
R 2 (#59c680)
D 2 (#411b91)
L 5 (#8ceee2)
U 2 (#caa173)
L 1 (#1b58a2)
U 2 (#caa171)
R 2 (#7807d2)
U 3 (#a77fa3)
L 2 (#015232)
U 2 (#7a21e3)";

    #[test]
    fn test_gen() {
        let input = generator(BUILD_INPUT).unwrap();

        println!("-- {:?} --", input)
    }

    #[test]
    fn test_01() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 62);
    }

    #[test]
    fn test_02() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);
        assert_eq!(res, 952408144115);
    }
}
