/** Correct results:
 * - 174137457
 * - 1493866
 */
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;
use nom::{
    bytes::complete::tag,
    character::complete::{alpha1, char},
    combinator::map,
    multi::separated_list0,
    sequence::{preceded, separated_pair, terminated, tuple},
    IResult,
};

#[derive(Debug)]
struct Range {
    destination_range: std::ops::Range<usize>,
    source_range: std::ops::Range<usize>,
}

impl Range {
    pub fn parse(input: &str) -> IResult<&str, Self> {
        map(
            tuple((
                terminated(parse_int, char(' ')),
                terminated(parse_int, char(' ')),
                parse_int,
            )),
            |(d, s, l): (usize, usize, usize)| Self {
                destination_range: d..d + l,
                source_range: s..s + l,
            },
        )(input)
    }

    pub fn map(&self, value: usize) -> Option<usize> {
        self.destination_range.contains(&value).then(|| {
            let diff = value - self.source_range.start;
            self.destination_range.start + diff
        })
    }

    pub fn unmap(&self, value: usize) -> Option<usize> {
        self.destination_range.contains(&value).then(|| {
            let diff = value - self.destination_range.start;
            self.source_range.start + diff
        })
    }
}

#[derive(Debug)]
struct Map {
    source: String,
    destination: String,
    ranges: Vec<Range>,
}

impl Map {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        map(
            tuple((
                terminated(separated_pair(alpha1, tag("-to-"), alpha1), tag(" map:\n")),
                separated_list0(tag("\n"), Range::parse),
            )),
            |((source, destination), ranges)| Self {
                source: source.to_owned(),
                destination: destination.to_owned(),
                ranges,
            },
        )(s)
    }

    pub fn locate(&self, value: usize) -> usize {
        self.ranges
            .iter()
            .filter_map(|range| range.map(value))
            .min()
            .unwrap_or(value)
    }

    pub fn unlocate(&self, value: usize) -> usize {
        self.ranges
            .iter()
            .filter_map(|range| range.unmap(value))
            .min()
            .unwrap_or(value)
    }
}

#[derive(Debug)]
struct Almanac {
    seeds: Vec<usize>,
    maps: Vec<Map>,
}

impl Almanac {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        let (remaining, almanac) = map(
            separated_pair(
                preceded(tag("seeds: "), separated_list0(char(' '), parse_int)),
                tag("\n\n"),
                separated_list0(tag("\n\n"), Map::parse),
            ),
            |(seeds, maps)| Self { seeds, maps },
        )(s)?;

        // Assert input is well-formed
        for maps in almanac.maps.windows(2) {
            if maps[0].destination != maps[1].source {
                return Err(nom::Err::Failure(nom::error::make_error(
                    s,
                    nom::error::ErrorKind::Verify,
                )));
            }
        }

        Ok((remaining, almanac))
    }

    pub fn find_seed_location(&self, seed: usize) -> usize {
        self.maps
            .iter()
            .fold(seed, |location, map| map.locate(location))
    }

    pub fn find_location_seed(&self, location: usize) -> usize {
        self.maps
            .iter()
            .rev()
            .fold(location, |l, map| map.unlocate(l))
    }
}

#[aoc_generator(day5)]
fn generator(input: &str) -> Result<Almanac, AocError> {
    input_parser(Almanac::parse)(input)
}

#[aoc(day5, part1)]
fn part01(input: &Almanac) -> usize {
    let mut min_location = usize::MAX;
    for seed in &input.seeds {
        min_location = min_location.min(input.find_seed_location(*seed));
    }

    min_location
}

#[aoc(day5, part2)]
fn part02(input: &Almanac) -> Result<usize, AocError> {
    let ranges = input
        .seeds
        .chunks(2)
        .map(|chunk| {
            let start = chunk[0];
            let end = start + chunk[1];
            start..end
        })
        .sorted_by_key(|range| range.start)
        .collect_vec();

    for location in 0..usize::MAX {
        let seed = input.find_location_seed(location);
        if ranges.iter().any(|range| range.contains(&seed)) {
            return Ok(location);
        }
    }

    Err(AocError::NoSolution)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4";

    #[test]
    fn test_gen() {
        let res = generator(BUILD_INPUT);

        println!("{:?}", res);
    }

    #[test]
    fn test_01() {
        let input = generator(BUILD_INPUT).unwrap();

        let res = part01(&input);

        assert_eq!(res, 35);
    }

    #[test]
    fn test_02() {
        let input = generator(BUILD_INPUT).unwrap();

        let res = part02(&input).unwrap();

        assert_eq!(res, 46);
    }
}
