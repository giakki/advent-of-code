/** Correct results:
 * - 248105065
 * - 249515436
 */
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;
use nom::{
    character::complete::{char, one_of, space1},
    combinator::map_res,
    multi::{many_m_n, separated_list0},
    sequence::separated_pair,
    IResult,
};
use num_derive::{FromPrimitive, ToPrimitive};
use std::cmp::Ordering;

#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd, FromPrimitive, ToPrimitive)]
enum Card {
    Ace,
    King,
    Queen,
    Jack,
    Ten,
    Nine,
    Eight,
    Seven,
    Six,
    Five,
    Four,
    Three,
    Two,
    Joker,
}

impl TryFrom<char> for Card {
    type Error = AocError;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            'A' => Ok(Self::Ace),
            'K' => Ok(Self::King),
            'Q' => Ok(Self::Queen),
            'J' => Ok(Self::Jack),
            'T' => Ok(Self::Ten),
            '9' => Ok(Self::Nine),
            '8' => Ok(Self::Eight),
            '7' => Ok(Self::Seven),
            '6' => Ok(Self::Six),
            '5' => Ok(Self::Five),
            '4' => Ok(Self::Four),
            '3' => Ok(Self::Three),
            '2' => Ok(Self::Two),
            _ => Err(AocError::Input),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
enum HandCombination {
    FiveOfAKind,
    FourOfAKind,
    FullHouse,
    ThreeOfAKind,
    TwoPair,
    OnePair,
    HighCard,
}

#[derive(Debug, PartialEq, Eq)]
struct Hand {
    combination: HandCombination,
    cards: [Card; 5],
}

impl Hand {
    pub fn with_jokers(&self) -> Self {
        let mut cards = self.cards;
        for card in &mut cards {
            if *card == Card::Jack {
                *card = Card::Joker;
            }
        }
        Self::from(cards)
    }
}

impl Ord for Hand {
    fn cmp(&self, other: &Self) -> Ordering {
        match self.combination.cmp(&other.combination) {
            Ordering::Equal => {
                for (a, b) in self.cards.iter().zip(&other.cards) {
                    if a > b {
                        return Ordering::Greater;
                    }
                    if a < b {
                        return Ordering::Less;
                    }
                }
                Ordering::Equal
            }
            ord => ord,
        }
    }
}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl From<[Card; 5]> for Hand {
    fn from(cards: [Card; 5]) -> Self {
        let mut bins = [0; Card::Joker as usize + 1];
        for card in &cards {
            bins[*card as usize] += 1;
        }

        let mut combination = HandCombination::HighCard;
        for bin in bins.into_iter().take(Card::Two as usize + 1) {
            if bin == 5 {
                return Self {
                    combination: HandCombination::FiveOfAKind,
                    cards,
                };
            }
            if bin == 4 {
                combination = HandCombination::FourOfAKind;
            } else if bin == 3 {
                if combination == HandCombination::OnePair {
                    combination = HandCombination::FullHouse;
                } else {
                    combination = HandCombination::ThreeOfAKind;
                }
            } else if bin == 2 {
                combination = match combination {
                    HandCombination::ThreeOfAKind => HandCombination::FullHouse,
                    HandCombination::OnePair => HandCombination::TwoPair,
                    HandCombination::HighCard => HandCombination::OnePair,
                    _ => combination,
                }
            } else {
                // noop
            }
        }

        #[allow(clippy::expect_used)]
        match bins.last().expect("Logic error") {
            5 | 4 => Self {
                combination: HandCombination::FiveOfAKind,
                cards,
            },
            3 => Self {
                combination: if combination == HandCombination::OnePair {
                    HandCombination::FiveOfAKind
                } else {
                    HandCombination::FourOfAKind
                },
                cards,
            },
            2 => Self {
                combination: if combination == HandCombination::ThreeOfAKind {
                    HandCombination::FiveOfAKind
                } else if combination == HandCombination::OnePair {
                    HandCombination::FourOfAKind
                } else {
                    HandCombination::ThreeOfAKind
                },
                cards,
            },
            1 => Self {
                combination: if combination == HandCombination::FourOfAKind {
                    HandCombination::FiveOfAKind
                } else if combination == HandCombination::ThreeOfAKind {
                    HandCombination::FourOfAKind
                } else if combination == HandCombination::TwoPair {
                    HandCombination::FullHouse
                } else if combination == HandCombination::OnePair {
                    HandCombination::ThreeOfAKind
                } else {
                    HandCombination::OnePair
                },
                cards,
            },
            _ => Self { combination, cards },
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
struct Game {
    bid: usize,
    hand: Hand,
}

impl Game {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        map_res(
            separated_pair(
                many_m_n(5, 5, map_res(one_of("AKQJT98765432"), Card::try_from)),
                space1,
                parse_int,
            ),
            |(cards, bid)| {
                let card_array: [Card; 5] = cards.try_into().map_err(|_| AocError::Unspecified)?;
                Ok::<Self, AocError>(Self {
                    hand: Hand::from(card_array),
                    bid,
                })
            },
        )(s)
    }

    pub fn with_jokers(&self) -> Self {
        Self {
            bid: self.bid,
            hand: self.hand.with_jokers(),
        }
    }
}

impl Ord for Game {
    fn cmp(&self, other: &Self) -> Ordering {
        self.hand.cmp(&other.hand)
    }
}

impl PartialOrd for Game {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[aoc_generator(day7)]
fn generator(input: &str) -> Result<Vec<Game>, AocError> {
    input_parser(separated_list0(char('\n'), Game::parse))(input)
}

#[aoc(day7, part1)]
fn part01(input: &[Game]) -> usize {
    input
        .iter()
        .sorted_unstable()
        .rev()
        .enumerate()
        .fold(0, |result, (rank, game)| result + (game.bid * (rank + 1)))
}

#[aoc(day7, part2)]
fn part02(input: &[Game]) -> usize {
    input
        .iter()
        .map(Game::with_jokers)
        .sorted_unstable()
        .rev()
        .enumerate()
        .fold(0, |result, (rank, game)| result + (game.bid * (rank + 1)))
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483";

    #[test]
    fn test_gen() {
        let res = generator(BUILD_INPUT);

        println!("{:?}", res);
    }

    #[test]
    fn test_01() {
        let input = generator(BUILD_INPUT).unwrap();

        let res = part01(&input);

        assert_eq!(res, 6592);
    }

    #[test]
    fn test_02() {
        let input = generator(BUILD_INPUT).unwrap();

        let res = part02(&input);

        assert_eq!(res, 6839);
    }
}
