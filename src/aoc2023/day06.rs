/** Correct results:
 * - 1083852
 * - 23501589
 */
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use aoc_runner_derive::{aoc, aoc_generator};
use nom::{
    bytes::complete::tag,
    character::complete::space1,
    multi::separated_list0,
    sequence::{pair, preceded, separated_pair},
};
use num::integer::Roots;

#[aoc_generator(day6)]
fn generator(input: &str) -> Result<(Vec<usize>, Vec<usize>), AocError> {
    input_parser(separated_pair(
        preceded(
            pair(tag("Time:"), space1),
            separated_list0(space1, parse_int),
        ),
        tag("\n"),
        preceded(
            pair(tag("Distance:"), space1),
            separated_list0(space1, parse_int),
        ),
    ))(input)
}

// If time = n, distance = -x^2 + nx
// slope = 2x - n
// top of the curve = n/2
// If record = m, intersect distance curve with y = m
// Intersections = solve -x^2 + nx = m -> -x^2 + nx - m
// Intersections: 1/2 * (m +- sqrt(m^2  - 4n))
fn run_race(time: usize, distance: usize) -> Option<usize> {
    let m = time;
    let n = distance + 1;

    let a = m * m;
    let b = 4 * n;

    if b > a {
        return None;
    }

    let det = (a - b).sqrt();

    let i1 = (m - det).div_ceil(2);
    let i2 = (m + det) / 2;
    let range = i2 - i1 + 1;

    Some(range)
}

fn kern(input: &[usize]) -> usize {
    input
        .iter()
        .fold(0, |acc, n| acc * 10_usize.pow(n.ilog10() + 1) + n)
}

#[aoc(day6, part1)]
fn part01((times, distances): &(Vec<usize>, Vec<usize>)) -> usize {
    times
        .iter()
        .zip(distances)
        .filter_map(|(t, d)| run_race(*t, *d))
        .product()
}

#[aoc(day6, part2)]
fn part02((times, distances): &(Vec<usize>, Vec<usize>)) -> Result<usize, AocError> {
    let time = kern(times);
    let distance = kern(distances);

    run_race(time, distance).ok_or(AocError::NoSolution)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"Time:      7  15   30
Distance:  9  40  200";

    #[test]
    fn test_gen() {
        let res = generator(BUILD_INPUT);

        println!("{:?}", res);
    }

    #[test]
    fn test_01() {
        let input = generator(BUILD_INPUT).unwrap();

        let res = part01(&input);

        assert_eq!(res, 288);
    }

    #[test]
    fn test_02() {
        let input = generator(BUILD_INPUT).unwrap();

        let res = part02(&input).unwrap();

        assert_eq!(res, 71503);
    }
}
