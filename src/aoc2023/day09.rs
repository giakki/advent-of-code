/** Correct results:
 * - 1901217887
 * - 905
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_signed_int},
};
use itertools::Itertools;
use nom::{character::complete::space1, multi::separated_list0};
use std::collections::VecDeque;

#[aoc_generator(day9)]
fn generator(input: &str) -> Result<Vec<Vec<i32>>, AocError> {
    input_parser(by_lines(separated_list0(space1, parse_signed_int)))(input)
}

fn derivate(history: &[i32]) -> VecDeque<VecDeque<i32>> {
    let mut derivates: VecDeque<VecDeque<i32>> = VecDeque::with_capacity(history.len());
    derivates.push_back(history.iter().copied().collect());

    loop {
        let prev = &derivates[derivates.len() - 1];
        if prev.is_empty() {
            break;
        }
        let mut derivate = VecDeque::with_capacity(prev.len());
        for (a, b) in prev.iter().tuple_windows() {
            derivate.push_back(b - a);
        }

        if derivate.iter().all(|value| *value == 0) {
            derivates.push_back(derivate);

            break;
        }
        derivates.push_back(derivate);
    }

    derivates
}

#[aoc(day9, part1)]
fn part01(input: &[Vec<i32>]) -> i32 {
    let mut results = Vec::with_capacity(input.len());
    for history in input {
        let mut derivates = derivate(history);

        for (next_idx, prev_idx) in (0..derivates.len()).rev().tuple_windows() {
            let next = derivates[next_idx][derivates[next_idx].len() - 1];
            let prev = derivates[prev_idx][derivates[prev_idx].len() - 1];

            derivates[prev_idx].push_back(next + prev);
        }
        results.push(derivates[0][derivates[0].len() - 1]);
    }

    results.into_iter().sum()
}

#[aoc(day9, part2)]
fn part02(input: &[Vec<i32>]) -> i32 {
    let mut results = Vec::with_capacity(input.len());
    for history in input {
        let mut derivates = derivate(history);

        for (next_idx, prev_idx) in (0..derivates.len()).rev().tuple_windows() {
            let next = derivates[next_idx][0];
            let prev = derivates[prev_idx][0];

            derivates[prev_idx].push_front(prev - next);
        }
        results.push(derivates[0][0]);
    }

    results.into_iter().sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45";

    #[test]
    fn test_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test_01() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 114);
    }

    #[test]
    fn test_02() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, 2);
    }
}
