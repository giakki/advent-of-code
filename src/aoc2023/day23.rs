/** Correct results:
 * - 2214
 * - 6594
 */
use crate::common::{
    chr::FromChar,
    direction::Direction,
    errors::AocError,
    map::Map,
    point::{Point, PointOps},
};
use itertools::Itertools;
use ndarray::s;
use std::{
    collections::{HashMap, HashSet, VecDeque},
    str::FromStr,
};

#[derive(Debug, Copy, Clone)]
enum Tile {
    Path,
    Forest,
    Slope(Direction),
}

impl FromChar for Tile {
    type Err = AocError;

    fn from_char(c: &char) -> Result<Self, Self::Err> {
        match c {
            '.' => Ok(Self::Path),
            '#' => Ok(Self::Forest),
            '^' => Ok(Self::Slope(Direction::Up)),
            '>' => Ok(Self::Slope(Direction::Right)),
            'v' => Ok(Self::Slope(Direction::Down)),
            '<' => Ok(Self::Slope(Direction::Left)),
            _ => Err(AocError::ParseError(format!("Invalid Tile: {c}"))),
        }
    }
}

fn get_visitable_neighbors(
    map: &Map<Tile>,
    from: Point<usize>,
) -> impl Iterator<Item = (Point<usize>, Direction)> + '_ {
    [
        Direction::Up,
        Direction::Right,
        Direction::Down,
        Direction::Left,
    ]
    .into_iter()
    .filter_map(move |direction| {
        let (dx, dy) = direction.as_diff::<isize>();
        if (from.x() == 0 && dx < 0) || (from.y() == 0 && dy < 0) {
            return None;
        }
        let neighbor = from.moved(1, &direction);
        if neighbor.x() >= map.width() || neighbor.y() >= map.height() {
            return None;
        }

        if matches!(map.at(neighbor), Tile::Forest) {
            None
        } else {
            Some((neighbor, direction))
        }
    })
}

fn get_intersections(map: &Map<Tile>) -> impl Iterator<Item = Point<usize>> + '_ {
    [(1, 0), (map.width() - 2, map.height() - 1)]
        .into_iter()
        .chain(map.tiles.indexed_iter().filter_map(|(pos, tile)| {
            if matches!(tile, Tile::Forest) {
                return None;
            }

            (get_visitable_neighbors(map, pos).count() > 2).then_some(pos)
        }))
}

fn bfs_full(
    map: &Map<Tile>,
    skip_slopes: bool,
) -> HashMap<Point<usize>, Vec<(Point<usize>, usize)>> {
    let intersections = get_intersections(map).collect_vec();
    let mut queue = VecDeque::new();
    let mut visited = HashSet::new();
    let mut paths: HashMap<Point<usize>, Vec<(Point<usize>, usize)>> = HashMap::new();

    for intersection in &intersections {
        queue.clear();
        queue.push_back((0, *intersection));
        visited.clear();
        visited.insert(*intersection);

        let paths_from_here = paths.entry(*intersection).or_default();
        while let Some((distance, pos)) = queue.pop_front() {
            if pos != *intersection && intersections.contains(&pos) {
                paths_from_here.push((pos, distance));
                continue;
            }
            for (neighbor, direction) in get_visitable_neighbors(map, pos) {
                if visited.contains(&neighbor) {
                    continue;
                }
                match map.at(neighbor) {
                    Tile::Path => {
                        queue.push_back((distance + 1, neighbor));
                        visited.insert(neighbor);
                    }
                    Tile::Slope(dir) => {
                        if skip_slopes || *dir == direction {
                            queue.push_back((distance + 1, neighbor));
                            visited.insert(neighbor);
                        }
                    }
                    Tile::Forest => {}
                }
            }
        }
    }

    paths
}

fn longest_path_from(
    from: Point<usize>,
    to: Point<usize>,
    paths: &HashMap<Point<usize>, Vec<(Point<usize>, usize)>>,
) -> usize {
    longest_path_from_rec(from, to, paths, 0, &mut HashMap::new())
}

fn longest_path_from_rec(
    from: Point<usize>,
    to: Point<usize>,
    paths: &HashMap<Point<usize>, Vec<(Point<usize>, usize)>>,
    distance: usize,
    visited: &mut HashMap<Point<usize>, bool>,
) -> usize {
    let mut result = 0;

    for (pos, pos_distance) in paths.get(&from).unwrap_or(&Vec::new()) {
        let this_visited_entry = visited.entry(*pos).or_default();
        if *this_visited_entry {
            continue;
        }
        if *pos == to {
            return usize::max(result, distance + pos_distance);
        }
        *this_visited_entry = true;
        result = usize::max(
            result,
            longest_path_from_rec(*pos, to, paths, distance + pos_distance, visited),
        );
        *visited.entry(*pos).or_default() = false;
    }

    result
}

fn find_from_to(map: &Map<Tile>) -> Result<(Point<usize>, Point<usize>), AocError> {
    Ok((
        map.tiles
            .slice(s![.., 0..1])
            .indexed_iter()
            .find_map(|(pos, tile)| matches!(tile, Tile::Path).then(|| (pos.x(), 0)))
            .ok_or(AocError::Input)?,
        map.tiles
            .slice(s![.., map.height() - 1..map.height()])
            .indexed_iter()
            .find_map(|(pos, tile)| matches!(tile, Tile::Path).then(|| (pos.x(), map.height() - 1)))
            .ok_or(AocError::Input)?,
    ))
}

fn generator(input: &str) -> Result<Map<Tile>, AocError> {
    Map::from_str(input)
}

#[aoc(day23, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    let map = generator(input)?;
    let (from, to) = find_from_to(&map)?;
    let paths = bfs_full(&map, false);

    Ok(longest_path_from(from, to, &paths))
}

#[aoc(day23, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    let map = generator(input)?;
    let (from, to) = find_from_to(&map)?;
    let paths = bfs_full(&map, true);

    Ok(longest_path_from(from, to, &paths))
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"#.#####################
#.......#########...###
#######.#########.#.###
###.....#.>.>.###.#.###
###v#####.#v#.###.#.###
###.>...#.#.#.....#...#
###v###.#.#.#########.#
###...#.#.#.......#...#
#####.#.#.#######.#.###
#.....#.#.#.......#...#
#.#####.#.#.#########v#
#.#...#...#...###...>.#
#.#.#v#######v###.###v#
#...#.>.#...>.>.#.###.#
#####v#.#.###v#.#.###.#
#.....#...#...#.#.#...#
#.#########.###.#.#.###
#...###...#...#...#.###
###.###.#.###v#####v###
#...#...#.#.>.>.#.>.###
#.###.###.#.###.#.#v###
#.....###...###...#...#
#####################.#";

    #[test]
    fn test_gen() {
        let input = generator(BUILD_INPUT).unwrap();
        println!("-- {:?} --", input);
    }

    #[test]
    fn test_01() {
        let res = part01(BUILD_INPUT).unwrap();
        assert_eq!(res, 94);
    }

    #[test]
    fn test_02() {
        let res = part02(BUILD_INPUT).unwrap();
        assert_eq!(res, 154);
    }
}
