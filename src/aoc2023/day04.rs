/** Correct results:
 * - 17782
 * - 8477787
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use nom::{
    bytes::complete::tag,
    character::complete::{char, digit1, space1},
    combinator::map,
    multi::separated_list0,
    sequence::{pair, preceded, tuple},
    IResult,
};
use std::collections::HashSet;

#[derive(Debug)]
struct Ticket {
    checked: HashSet<usize>,
    winning: HashSet<usize>,
}

impl Ticket {
    fn num_winning(&self) -> usize {
        self.checked.intersection(&self.winning).count()
    }
}

fn parse_line(s: &str) -> IResult<&str, Ticket> {
    map(
        preceded(
            tuple((tag("Card"), space1, digit1, char(':'), space1)),
            pair(
                separated_list0(space1, parse_int),
                preceded(
                    tuple((space1, char('|'), space1)),
                    separated_list0(space1, parse_int),
                ),
            ),
        ),
        |(winning, checked)| Ticket {
            checked: HashSet::from_iter(checked),
            winning: HashSet::from_iter(winning),
        },
    )(s)
}

#[aoc_generator(day4)]
fn generator(input: &str) -> Result<Vec<Ticket>, AocError> {
    input_parser(by_lines(parse_line))(input)
}

#[aoc(day4, part1)]
fn part01(input: &[Ticket]) -> usize {
    input
        .iter()
        .map(|ticket| {
            let winning_numbers = ticket.num_winning() as u32;
            if winning_numbers == 0 {
                0
            } else {
                2_usize.pow(winning_numbers - 1)
            }
        })
        .sum()
}

#[aoc(day4, part2)]
fn part02(input: &[Ticket]) -> usize {
    let mut buckets = vec![1; input.len()];

    for (idx, ticket) in input.iter().enumerate() {
        let num_winning = ticket.num_winning();
        let start = idx + 1;
        let end = start + num_winning;

        for i in start..end {
            buckets[i] += buckets[idx];
        }
    }

    buckets.into_iter().sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11";

    #[test]
    fn test_01() {
        let input = generator(BUILD_INPUT).unwrap();

        let res = part01(&input);

        assert_eq!(res, 13);
    }

    #[test]
    fn test_02() {
        let input = generator(BUILD_INPUT).unwrap();

        let res = part02(&input);

        assert_eq!(res, 30);
    }
}
