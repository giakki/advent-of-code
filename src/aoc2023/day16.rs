/** Correct results:
 * - 6816
 * - 8163
 */
use crate::common::{
    chr::FromChar,
    direction::Direction,
    errors::AocError,
    map::Map,
    point::{Point, PointOps},
};
use ndarray::Array2;
use std::{fmt::Display, str::FromStr};

#[derive(Copy, Clone, Debug)]
enum Tile {
    VerticalSplitter,
    HorizontalSplitter,
    NWMirror,
    SEMirror,
    Empty,
}

impl FromChar for Tile {
    type Err = AocError;

    fn from_char(c: &char) -> Result<Self, Self::Err> {
        match c {
            '|' => Ok(Self::VerticalSplitter),
            '-' => Ok(Self::HorizontalSplitter),
            '/' => Ok(Self::NWMirror),
            '\\' => Ok(Self::SEMirror),
            '.' => Ok(Self::Empty),
            _ => Err(AocError::ParseError(format!("Cannot parse tile '{c}'"))),
        }
    }
}

impl Display for Tile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::VerticalSplitter => f.write_str("|"),
            Self::HorizontalSplitter => f.write_str("-"),
            Self::NWMirror => f.write_str("/"),
            Self::SEMirror => f.write_str("\\"),
            Self::Empty => f.write_str("."),
        }
    }
}

fn light_up(map: &Map<Tile>, start: Point<usize>, initial_direction: Direction) -> usize {
    let mut visited_map: Map<Option<Vec<Direction>>> =
        Map::from_ndarray(Array2::from_elem((map.height(), map.width()), None));

    let mut beams = vec![(start, initial_direction)];
    while let Some((pos, direction)) = beams.pop() {
        if let Some(dirs) = visited_map.at_mut(pos) {
            if dirs.contains(&direction) {
                continue;
            }
            dirs.push(direction);
        } else {
            *visited_map.at_mut(pos) = Some(vec![direction]);
        }

        let dirs: Vec<Direction>;
        match map.at(pos) {
            Tile::HorizontalSplitter => {
                if matches!(direction, Direction::Up | Direction::Down) {
                    dirs = vec![Direction::Left, Direction::Right];
                } else {
                    dirs = vec![direction];
                }
            }
            Tile::VerticalSplitter => {
                if matches!(direction, Direction::Left | Direction::Right) {
                    dirs = vec![Direction::Up, Direction::Down];
                } else {
                    dirs = vec![direction];
                }
            }
            Tile::NWMirror => match direction {
                Direction::Up | Direction::Down => dirs = vec![direction.rotate_cw()],
                Direction::Right | Direction::Left => dirs = vec![direction.rotate_ccw()],
            },
            Tile::SEMirror => match direction {
                Direction::Up | Direction::Down => dirs = vec![direction.rotate_ccw()],
                Direction::Right | Direction::Left => dirs = vec![direction.rotate_cw()],
            },
            Tile::Empty => dirs = vec![direction],
        }
        for dir in dirs {
            match dir {
                Direction::Up => {
                    if pos.y() > 0 {
                        beams.push((pos.moved(1, &dir), dir));
                    }
                }
                Direction::Down => {
                    if pos.y() < map.height() - 1 {
                        beams.push((pos.moved(1, &dir), dir));
                    }
                }
                Direction::Left => {
                    if pos.x() > 0 {
                        beams.push((pos.moved(1, &dir), dir));
                    }
                }
                Direction::Right => {
                    if pos.x() < map.width() - 1 {
                        beams.push((pos.moved(1, &dir), dir));
                    }
                }
            }
        }
    }

    visited_map
        .tiles
        .iter()
        .filter(|iter| iter.is_some())
        .count()
}

#[aoc(day16, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    let map = Map::from_str(input)?;

    Ok(light_up(&map, (0, 0), Direction::Right))
}

#[aoc(day16, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    let map = Map::from_str(input)?;

    let mut max = 0;
    for x in 0..map.width() {
        max = usize::max(max, light_up(&map, (x, 0), Direction::Down));
        max = usize::max(max, light_up(&map, (x, map.height() - 1), Direction::Up));
    }
    for y in 0..map.height() {
        max = usize::max(max, light_up(&map, (0, y), Direction::Right));
        max = usize::max(max, light_up(&map, (map.width() - 1, y), Direction::Left));
    }

    Ok(max)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r".|...\....
|.-.\.....
.....|-...
........|.
..........
.........\
..../.\\..
.-.-/..|..
.|....-|.\
..//.|....";

    #[test]
    fn test_gen() {
        let input = Map::<Tile>::from_str(BUILD_INPUT);

        println!("{:?}", input);
    }

    #[test]
    fn test_01() {
        let res = part01(BUILD_INPUT).unwrap();

        assert_eq!(res, 46);
    }

    #[test]
    fn test_02() {
        let res = part02(BUILD_INPUT).unwrap();

        assert_eq!(res, 51);
    }
}
