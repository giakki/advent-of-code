/** Correct results:
 * - 527144
 * - 81463996
 */
use crate::common::{chr::FromAsciiDigit, errors::AocError, point::PointOps};
use itertools::Itertools;
use ndarray::Array2;
use std::iter;

#[aoc_generator(day3)]
fn generator(input: &str) -> Result<Array2<u8>, AocError> {
    let lines = input.lines().collect_vec();
    let height = lines.len();
    let width = lines[0].as_bytes().len();

    let empty_line = vec![b'.'; width];

    let tiles = iter::once(empty_line.as_ref())
        .chain(lines.into_iter().map(str::as_bytes))
        .chain(iter::once(empty_line.as_ref()))
        .flat_map(|line| {
            iter::once(b'.')
                .chain(line.iter().copied())
                .chain(iter::once(b'.'))
        })
        .collect::<Vec<_>>();

    Ok(Array2::from_shape_vec((height + 2, width + 2), tiles)?.reversed_axes())
}

#[aoc(day3, part1)]
fn part01(input: &Array2<u8>) -> usize {
    let mut result = 0;

    let (width, height) = input.dim();
    let mut y = 1;
    let mut x = 1;
    while y < height - 1 {
        while x < width - 1 {
            let tile = input[(x, y)];
            if !tile.is_ascii_digit() {
                x += 1;
                continue;
            }

            let mut part_num = 0;
            let mut is_part_number = false;
            while x < width - 1 {
                is_part_number |= (x, y).all_neighbors().into_iter().any(|(nx, ny)| {
                    let neighbor = input[(nx, ny)];
                    !neighbor.is_ascii_digit() && neighbor != b'.'
                });
                part_num = part_num * 10 + usize::from_ascii_digit(input[(x, y)]);
                x += 1;
                if !input[(x, y)].is_ascii_digit() {
                    break;
                }
            }
            if is_part_number {
                result += part_num;
            }

            x += 1;
        }
        x = 0;
        y += 1;
    }

    result
}

#[aoc(day3, part2)]
fn part02(input: &Array2<u8>) -> usize {
    let mut result = 0;

    let (width, height) = input.dim();
    let mut y = 1;
    let mut x = 1;
    while y < height - 1 {
        while x < width - 1 {
            if input[(x, y)] != b'*' {
                x += 1;
                continue;
            }

            let mut neighboring_parts = Vec::new();

            // Top & Bottom
            // Only two cases:
            //  .N. N.N
            //  .*. .*.
            if input[(x, y - 1)].is_ascii_digit() {
                neighboring_parts.push((x, y - 1));
            } else {
                if input[(x - 1, y - 1)].is_ascii_digit() {
                    neighboring_parts.push((x - 1, y - 1));
                }
                if input[(x + 1, y - 1)].is_ascii_digit() {
                    neighboring_parts.push((x + 1, y - 1));
                }
            }

            if input[(x, y + 1)].is_ascii_digit() {
                neighboring_parts.push((x, y + 1));
            } else {
                if input[(x - 1, y + 1)].is_ascii_digit() {
                    neighboring_parts.push((x - 1, y + 1));
                }
                if input[(x + 1, y + 1)].is_ascii_digit() {
                    neighboring_parts.push((x + 1, y + 1));
                }
            }

            // Left and Right
            if input[(x - 1, y)].is_ascii_digit() {
                neighboring_parts.push((x - 1, y));
            }
            if input[(x + 1, y)].is_ascii_digit() {
                neighboring_parts.push((x + 1, y));
            }

            if neighboring_parts.len() == 2 {
                result += neighboring_parts
                    .into_iter()
                    .map(|(mut nx, ny)| {
                        while input[(nx - 1, ny)].is_ascii_digit() {
                            nx -= 1;
                        }

                        let mut part_num = 0;
                        while input[(nx, ny)].is_ascii_digit() {
                            part_num = part_num * 10 + usize::from_ascii_digit(input[(nx, ny)]);
                            nx += 1;
                        }
                        part_num
                    })
                    .product::<usize>();
            }

            x += 1;
        }
        x = 0;
        y += 1;
    }

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..";

    #[test]
    fn test_01() {
        let input = generator(BUILD_INPUT).unwrap();

        let res = part01(&input);

        assert_eq!(res, 4361);
    }

    #[test]
    fn test_02() {
        let input = generator(BUILD_INPUT).unwrap();

        let res = part02(&input);

        assert_eq!(res, 467835);
    }
}
