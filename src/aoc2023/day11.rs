/** Correct results:
 * - 9556712
 * - 678626199476
 */
use crate::common::point::{Point, PointOps};
use itertools::Itertools;

fn generator(input: &str) -> Vec<Point<usize>> {
    input
        .lines()
        .enumerate()
        .flat_map(move |(y, line)| {
            line.as_bytes()
                .iter()
                .enumerate()
                .filter_map(move |(x, chr)| (*chr == b'#').then_some((x, y)))
        })
        .collect_vec()
}

fn runner(universe: &str, expansion: usize) -> usize {
    let mut u = generator(universe);

    u.sort_by_key(PointOps::x);
    let x_holes = u
        .iter()
        .enumerate()
        .tuple_windows()
        .filter_map(|((_, g1), (i2, g2))| {
            let diff = g2.x() - g1.x();
            (diff > 1).then(|| (diff - 1, i2))
        })
        .collect_vec();

    for (diff, idx) in x_holes {
        for galaxy in &mut u[idx..] {
            galaxy.add_x(diff * expansion);
        }
    }

    u.sort_by_key(PointOps::y);
    let y_holes = u
        .iter()
        .enumerate()
        .tuple_windows()
        .filter_map(|((_, g1), (i2, g2))| {
            let diff = g2.y() - g1.y();
            (diff > 1).then(|| (diff - 1, i2))
        })
        .collect_vec();

    for (diff, idx) in y_holes {
        for galaxy in &mut u[idx..] {
            galaxy.add_y(diff * expansion);
        }
    }

    u.into_iter()
        .tuple_combinations()
        .fold(0, |result, (g1, g2)| {
            result + g1.x().abs_diff(g2.x()) + g1.y().abs_diff(g2.y())
        })
}

#[aoc(day11, part1)]
fn part01(input: &str) -> usize {
    runner(input, 1)
}

#[aoc(day11, part2)]
fn part02(input: &str) -> usize {
    runner(input, 999_999)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....";

    #[test]
    fn test_gen() {
        let input = generator(BUILD_INPUT);

        println!("{:?}", input)
    }

    #[test]
    fn test_runner() {
        assert_eq!(runner(BUILD_INPUT, 1), 374);
        assert_eq!(runner(BUILD_INPUT, 9), 1030);
        assert_eq!(runner(BUILD_INPUT, 99), 8410);
    }
}
