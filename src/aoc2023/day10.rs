/** Correct results:
 * - 7093
 * - 407
 */
use crate::common::{
    chr::FromChar, direction::Direction, errors::AocError, map::Map, point::PointOps,
};
use itertools::Itertools;
use std::fmt::Display;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Tile {
    Vertical,
    Horizontal,
    NEBend,
    NWBend,
    SWBend,
    SEBend,
    Ground,
    Start,
}

impl Display for Tile {
    #[allow(clippy::non_ascii_literal)]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Ground => f.write_str(" "),
            Self::Horizontal => f.write_str("─"),
            Self::NEBend => f.write_str("└"),
            Self::NWBend => f.write_str("┘"),
            Self::SEBend => f.write_str("┌"),
            Self::SWBend => f.write_str("┐"),
            Self::Start => f.write_str("S"),
            Self::Vertical => f.write_str("│"),
        }
    }
}

impl FromChar for Tile {
    type Err = AocError;

    fn from_char(c: &char) -> Result<Self, Self::Err> {
        match c {
            '|' => Ok(Self::Vertical),
            '-' => Ok(Self::Horizontal),
            'L' => Ok(Self::NEBend),
            'J' => Ok(Self::NWBend),
            '7' => Ok(Self::SWBend),
            'F' => Ok(Self::SEBend),
            '.' => Ok(Self::Ground),
            'S' => Ok(Self::Start),
            _ => Err(AocError::ParseError(format!("Unknown char: {c}"))),
        }
    }
}

fn walk_map(mut map: Map<Tile>) -> Result<(usize, usize), AocError> {
    let start = map
        .tiles
        .indexed_iter()
        .find_map(|(pos, tile)| (*tile == Tile::Start).then_some(pos))
        .ok_or_else(|| AocError::Generic("Cannot find start point".to_owned()))?;

    let mut direction = Direction::Up;
    let mut iter = start;
    let mut path = Vec::new();
    while iter != start || path.is_empty() {
        match map.at(iter) {
            Tile::NEBend => {
                // L
                if direction == Direction::Down {
                    direction = direction.rotate_ccw();
                } else {
                    direction = direction.rotate_cw();
                }
            }
            Tile::NWBend => {
                // J
                if direction == Direction::Down {
                    direction = direction.rotate_cw();
                } else {
                    direction = direction.rotate_ccw();
                }
            }
            Tile::SEBend => {
                // F
                if direction == Direction::Up {
                    direction = direction.rotate_cw();
                } else {
                    direction = direction.rotate_ccw();
                }
            }
            Tile::SWBend => {
                // 7
                if direction == Direction::Up {
                    direction = direction.rotate_ccw();
                } else {
                    direction = direction.rotate_cw();
                }
            }
            Tile::Start => on_start_tile(&mut map, start, &mut direction),
            _ => {}
        }
        path.push(iter);
        iter.move_by(1, &direction);
    }

    let perimeter = path.len();

    // https://en.wikipedia.org/wiki/Pick%27s_theorem
    let area = path
        .into_iter()
        .tuple_windows()
        .map(|((x1, y1), (x2, y2))| (y1 + y2) as isize * (x2 as isize - x1 as isize))
        .sum::<isize>()
        .unsigned_abs()
        / 2;

    Ok((perimeter / 2, (area).abs_diff(perimeter / 2) + 1))
}

fn on_start_tile(map: &mut Map<Tile>, start: (usize, usize), direction: &mut Direction) {
    let mut res = None;
    for pos in start.cardinal_neighbors_checked(map.width(), map.height()) {
        let tile = *map.at(pos);
        if pos.x() < start.x()
            && (tile == Tile::Horizontal || tile == Tile::SEBend || tile == Tile::NEBend)
        {
            res = Some(Direction::Left);
        } else if pos.x() > start.x()
            && (tile == Tile::Horizontal || tile == Tile::SWBend || tile == Tile::NWBend)
        {
            if let Some(dir) = res {
                *map.at_mut(start) = Tile::Horizontal;
                *direction = dir;
                break;
            }
            res = Some(Direction::Right);
        } else if pos.y() < start.y()
            && (tile == Tile::Vertical || tile == Tile::SEBend || tile == Tile::SWBend)
        {
            if let Some(dir) = res {
                if dir == Direction::Left {
                    *map.at_mut(start) = Tile::NWBend;
                } else {
                    *map.at_mut(start) = Tile::NEBend;
                }
                *direction = dir;
                break;
            }
            res = Some(Direction::Up);
        } else if pos.y() > start.y()
            && (tile == Tile::Vertical || tile == Tile::NEBend || tile == Tile::NWBend)
        {
            if let Some(dir) = res {
                if dir == Direction::Left {
                    *map.at_mut(start) = Tile::SWBend;
                } else if dir == Direction::Right {
                    *map.at_mut(start) = Tile::SEBend;
                } else {
                    *map.at_mut(start) = Tile::Vertical;
                }
                *direction = dir;
                break;
            }
            *direction = Direction::Down;
        } else {
            // noop
        }
    }
}

fn generator(input: &str) -> Result<Map<Tile>, AocError> {
    input
        .lines()
        .map(|line| {
            line.chars()
                .map(|c| Tile::from_char(&c))
                .collect::<Result<Vec<_>, AocError>>()
        })
        .collect::<Result<Vec<_>, AocError>>()
        .map(|tiles| Map::from_vec2(&tiles))
}

#[aoc(day10, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    let map = generator(input)?;
    walk_map(map).map(|res| res.0)
}

#[aoc(day10, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    let map = generator(input)?;
    let (_, area) = walk_map(map)?;

    Ok(area)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT_1: &str = r".....
.S-7.
.|.|.
.L-J.
.....";

    const BUILD_INPUT_2: &str = r"..F7.
.FJ|.
SJ.L7
|F--J
LJ...";

    const BUILD_INPUT_3: &str = r"...........
.S-------7.
.|F-----7|.
.||.....||.
.||.....||.
.|L-7.F-J|.
.|..|.|..|.
.L--J.L--J.
...........";

    const BUILD_INPUT_4: &str = r".F----7F7F7F7F-7....
.|F--7||||||||FJ....
.||.FJ||||||||L7....
FJL7L7LJLJ||LJ.L-7..
L--J.L7...LJS7F-7L7.
....F-J..F7FJ|L7L7L7
....L7.F7||L7|.L7L7|
.....|FJLJ|FJ|F7|.LJ
....FJL-7.||.||||...
....L---J.LJ.LJLJ...";

    #[test]
    fn test_gen() {
        let input = generator(BUILD_INPUT_1).unwrap();

        println!("{:?}", input)
    }

    #[test]
    fn test_01() {
        let res = part01(BUILD_INPUT_1).unwrap();
        assert_eq!(res, 4);

        let res = part01(BUILD_INPUT_2).unwrap();
        assert_eq!(res, 8);
    }

    #[test]
    fn test_02() {
        let res = part02(BUILD_INPUT_3).unwrap();
        assert_eq!(res, 4);

        let res = part02(BUILD_INPUT_4).unwrap();
        assert_eq!(res, 8);
    }
}
