/** Correct results:
 * - 6530
 * - 3323
 */

#[aoc_generator(day06)]
fn generator(input: &str) -> Vec<Vec<Vec<char>>> {
    input
        .split("\n\n")
        .map(|group| {
            group
                .lines()
                .map(|person| person.chars().collect())
                .collect()
        })
        .collect()
}

#[aoc(day06, part1)]
fn part01(input: &[Vec<Vec<char>>]) -> usize {
    input
        .iter()
        .map(|group| {
            let mut responses = [false; 26];

            for person in group {
                for answer in person {
                    responses[*answer as usize - ('a' as usize)] = true;
                }
            }

            responses.iter().filter(|x| **x).count()
        })
        .sum()
}

#[aoc(day06, part2)]
fn part02(input: &[Vec<Vec<char>>]) -> usize {
    input
        .iter()
        .map(|group| {
            let mut responses = [0; 26];

            for person in group {
                for answer in person {
                    responses[*answer as usize - ('a' as usize)] += 1;
                }
            }

            responses.iter().filter(|x| **x == group.len()).count()
        })
        .sum()
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r"abc

a
b
c

ab
ac

a
a
a
a

b";

    #[test]
    fn test0601() {
        let input = super::generator(BUILD_INPUT);
        let res = super::part01(&input);

        println!("-- {} --", res)
    }

    #[test]
    fn test0602() {
        let input = super::generator(BUILD_INPUT);
        let res = super::part02(&input);

        println!("-- {} --", res)
    }
}
