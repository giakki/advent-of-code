/**
 * Correct results:
 * - 197451
 * - 138233720
 */
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use nom::{character::complete::char, multi::separated_list0};

fn generator(input: &str) -> Result<Vec<usize>, AocError> {
    input_parser(separated_list0(char('\n'), parse_int))(input)
}

#[aoc(day1, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    let lines = generator(input)?;
    for x in 0..lines.len() {
        for y in x..lines.len() {
            if lines[x] + lines[y] == 2020 {
                return Ok(lines[x] * lines[y]);
            }
        }
    }

    Err(AocError::NoSolution)
}

#[aoc(day1, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    let lines = generator(input)?;

    for x in 0..lines.len() {
        for y in x..lines.len() {
            for z in y..lines.len() {
                if lines[x] + lines[y] + lines[z] == 2020 {
                    return Ok(lines[x] * lines[y] * lines[z]);
                }
            }
        }
    }

    Err(AocError::NoSolution)
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r"1721
979
366
299
675
1456";

    #[test]
    fn test0101() {
        let res = super::part01(BUILD_INPUT);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test0102() {
        let res = super::part02(BUILD_INPUT);

        println!("-- {:?} --", res)
    }
}
