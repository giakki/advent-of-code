/** Correct results:
* - 2203
* - fqfm,kxjttzg,ldm,mnzbc,zjmdst,ndvrq,fkjmz,kjkrm
*/
use crate::common::{errors::AocError, parsers::input_parser};
use nom::{
    bytes::complete::tag,
    character::complete::{alpha1, char},
    combinator::map,
    multi::{separated_list0, separated_list1},
    sequence::{delimited, preceded, separated_pair},
    IResult,
};
use std::collections::{HashMap, HashSet};
use std::iter::FromIterator;

type Food<'a> = (HashSet<&'a str>, HashSet<&'a str>);

fn parse_ingredients(s: &str) -> IResult<&str, HashSet<&str>> {
    map(separated_list1(char(' '), alpha1), |v| {
        HashSet::from_iter(v)
    })(s)
}

fn parse_allergens(s: &str) -> IResult<&str, HashSet<&str>> {
    delimited(
        char('('),
        preceded(
            tag("contains "),
            map(separated_list1(tag(", "), alpha1), |v| {
                HashSet::from_iter(v)
            }),
        ),
        char(')'),
    )(s)
}

fn parse_food(s: &str) -> IResult<&str, Food> {
    separated_pair(parse_ingredients, char(' '), parse_allergens)(s)
}

fn generator(input: &str) -> Result<Vec<Food>, AocError> {
    input_parser(separated_list0(char('\n'), parse_food))(input)
}

fn calculate_ingredients<'a>(
    foods: &[(HashSet<&'a str>, HashSet<&'a str>)],
) -> Result<HashMap<&'a str, &'a str>, AocError> {
    let all_allergens = foods
        .iter()
        .flat_map(|f| &f.1)
        .copied()
        .collect::<HashSet<&str>>();

    let mut food_to_allergen_choices = HashMap::new();
    for (ingredients, allergens) in foods {
        for ingredient in ingredients {
            for allergen in allergens {
                let possibilities = food_to_allergen_choices
                    .entry(ingredient)
                    .or_insert_with(|| HashSet::with_capacity(all_allergens.len()));
                possibilities.insert(*allergen);
            }
        }
    }

    let mut food_to_allergen = HashMap::new();
    for _ in 0..all_allergens.len() {
        for allergen in &all_allergens {
            let valid_ingredients: Vec<&str> = foods
                .iter()
                .filter_map(|(ing, al)| {
                    if al.contains(allergen) {
                        Some(ing)
                    } else {
                        None
                    }
                })
                .cloned()
                .reduce(|x, y| x.intersection(&y).copied().collect())
                .ok_or(AocError::NoSolution)?
                .into_iter()
                .filter(|v| !food_to_allergen.contains_key(v))
                .collect();

            if let [ing] = valid_ingredients.as_slice() {
                food_to_allergen.insert(*ing, *allergen);
            }
        }
    }

    Ok(food_to_allergen)
}

#[aoc(day21, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    let foods = generator(input)?;

    let food_to_allergen = calculate_ingredients(&foods)?;

    Ok(foods
        .iter()
        .flat_map(|f| {
            f.0.iter()
                .filter(|ing| !food_to_allergen.contains_key(*ing))
        })
        .count())
}

#[aoc(day21, part2)]
fn part02(input: &str) -> Result<String, AocError> {
    let foods = generator(input)?;

    let mut food_to_allergen_pairs = calculate_ingredients(&foods)?
        .into_iter()
        .collect::<Vec<_>>();

    food_to_allergen_pairs.sort_by(|(_, a), (_, b)| a.cmp(b));

    Ok(food_to_allergen_pairs
        .into_iter()
        .map(|(a, _)| a)
        .collect::<Vec<_>>()
        .join(","))
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r"mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)";

    #[test]
    fn test21_gen() {
        let input = super::generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test2101() {
        let res = super::part01(BUILD_INPUT);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test2102() {
        let res = super::part02(BUILD_INPUT);

        println!("-- {:?} --", res)
    }
}
