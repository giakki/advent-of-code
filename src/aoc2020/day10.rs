/** Correct results:
 * - 1755
 * - 4049565169664
 */
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use itertools::Itertools;
use nom::{character::complete::char, multi::separated_list0};

const TRIBONACCI_CACHE: [usize; 6] = [0, 1, 1, 2, 4, 7];

const fn tribonacci(n: usize) -> usize {
    if n < 6 {
        return TRIBONACCI_CACHE[n];
    }

    tribonacci(n - 1) + tribonacci(n - 2) + tribonacci(n - 3)
}

#[aoc_generator(day10)]
fn generator(input: &str) -> Result<Vec<usize>, AocError> {
    input_parser(separated_list0(char('\n'), parse_int))(input)
}

#[aoc(day10, part1)]
fn part01(original_input: &[usize]) -> usize {
    let mut input = original_input.iter().map(usize::clone).collect::<Vec<_>>();
    input.push(0);
    input.sort_unstable();
    input.push(input[input.len() - 1] + 3);

    let mut diffs_1 = 0;
    let mut diffs_3 = 0;
    for (a, b) in input.iter().tuple_windows() {
        let diff = b - a;
        if diff == 1 {
            diffs_1 += 1;
        } else if diff == 3 {
            diffs_3 += 1;
        }
    }

    diffs_1 * diffs_3
}

#[aoc(day10, part2)]
fn part02(original_input: &[usize]) -> usize {
    let mut input = original_input.iter().map(usize::clone).collect::<Vec<_>>();
    input.push(0);
    input.sort_unstable();
    input.push(input[input.len() - 1] + 3);

    let mut slices = Vec::new();
    let mut accum = 1usize;
    for (a, b) in input.iter().tuple_windows() {
        if b - a == 3 {
            slices.push(accum);
            accum = 0;
        }
        accum += 1;
    }

    slices.into_iter().map(tribonacci).product()
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT_1: &str = r"16
10
15
5
1
11
7
19
6
12
4";

    const BUILD_INPUT_2: &str = r"28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3";

    #[test]
    fn test1001() {
        let input = super::generator(BUILD_INPUT_1).unwrap();
        let res = super::part01(&input);

        println!("-- {} --", res)
    }

    #[test]
    fn test1002() {
        let input = super::generator(BUILD_INPUT_2).unwrap();
        let res = super::part02(&input);

        println!("-- {} --", res)
    }
}
