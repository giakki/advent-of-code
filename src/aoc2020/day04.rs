/**
 * Correct results:
 * - 245
 * - 133
 */
use lazy_static::lazy_static;
use regex::Regex;
use std::str::FromStr;

lazy_static! {
    static ref BYR_REGEX: Regex = Regex::new(r"byr:(.+)").unwrap();
    static ref IYR_REGEX: Regex = Regex::new(r"iyr:(.+)").unwrap();
    static ref EYR_REGEX: Regex = Regex::new(r"eyr:(.+)").unwrap();
    static ref HGT_REGEX: Regex = Regex::new(r"hgt:(.+)").unwrap();
    static ref HCL_REGEX: Regex = Regex::new(r"hcl:(.+)").unwrap();
    static ref ECL_REGEX: Regex = Regex::new(r"ecl:(.+)").unwrap();
    static ref PID_REGEX: Regex = Regex::new(r"pid:(.+)").unwrap();
    static ref CID_REGEX: Regex = Regex::new(r"cid:(.+)").unwrap();
    static ref HGT_VALIDATOR_REGEX: Regex = Regex::new(r"^(\d+)(in|cm)$").unwrap();
    static ref HCL_VALIDATOR_REGEX: Regex = Regex::new(r"^#[a-z0-9]{6}$").unwrap();
    static ref ECL_VALIDATOR_REGEX: Regex = Regex::new(r"^(amb|blu|brn|gry|grn|hzl|oth)$").unwrap();
    static ref PID_VALIDATOR_REGEX: Regex = Regex::new(r"^\d{9}$").unwrap();
}

#[derive(Debug, PartialEq)]
struct Passport {
    pub byr: String,
    pub iyr: String,
    pub eyr: String,
    pub hgt: String,
    pub hcl: String,
    pub ecl: String,
    pub pid: String,
    pub cid: Option<String>,
}

impl FromStr for Passport {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self {
            byr: s
                .split(' ')
                .filter_map(|p| BYR_REGEX.captures(p))
                .map(|c| c[1].to_string())
                .next()
                .ok_or("Missing byr")?,
            iyr: s
                .split(' ')
                .filter_map(|p| IYR_REGEX.captures(p))
                .map(|c| c[1].to_string())
                .next()
                .ok_or("Missing iyr")?,
            eyr: s
                .split(' ')
                .filter_map(|p| EYR_REGEX.captures(p))
                .map(|c| c[1].to_string())
                .next()
                .ok_or("Missing eyr")?,
            hgt: s
                .split(' ')
                .filter_map(|p| HGT_REGEX.captures(p))
                .map(|c| c[1].to_string())
                .next()
                .ok_or("Missing hgt")?,
            hcl: s
                .split(' ')
                .filter_map(|p| HCL_REGEX.captures(p))
                .map(|c| c[1].to_string())
                .next()
                .ok_or("Missing hcl")?,
            ecl: s
                .split(' ')
                .filter_map(|p| ECL_REGEX.captures(p))
                .map(|c| c[1].to_string())
                .next()
                .ok_or("Missing ecl")?,
            pid: s
                .split(' ')
                .filter_map(|p| PID_REGEX.captures(p))
                .map(|c| c[1].to_string())
                .next()
                .ok_or("Missing pid")?,
            cid: s
                .split(' ')
                .filter_map(|p| CID_REGEX.captures(p))
                .map(|c| c[1].to_string())
                .next(),
        })
    }
}

fn is_valid_byr(val: &str) -> bool {
    if let Ok(byr) = val.parse::<usize>() {
        if (1920..=2002).contains(&byr) {
            return true;
        }
    }
    false
}

fn is_valid_iyr(val: &str) -> bool {
    if let Ok(iyr) = val.parse::<usize>() {
        if (2010..=2020).contains(&iyr) {
            return true;
        }
    }
    false
}

fn is_valid_eyr(val: &str) -> bool {
    if let Ok(eyr) = val.parse::<usize>() {
        if (2020..=2030).contains(&eyr) {
            return true;
        }
    }
    false
}

fn is_valid_hgt(val: &str) -> bool {
    if let Some(matches) = HGT_VALIDATOR_REGEX.captures(val) {
        if let Ok(hgt) = matches[1].parse::<usize>() {
            if &matches[2] == "cm" && (150..=193).contains(&hgt) {
                return true;
            }
            if &matches[2] == "in" && (59..=76).contains(&hgt) {
                return true;
            }
        }
    }

    false
}

fn is_valid_hcl(val: &str) -> bool {
    HCL_VALIDATOR_REGEX.is_match(val)
}

fn is_valid_ecl(val: &str) -> bool {
    ECL_VALIDATOR_REGEX.is_match(val)
}

fn is_valid_pid(val: &str) -> bool {
    PID_VALIDATOR_REGEX.is_match(val)
}

impl Passport {
    pub fn is_valid(&self) -> bool {
        is_valid_byr(&self.byr)
            && is_valid_iyr(&self.iyr)
            && is_valid_eyr(&self.eyr)
            && is_valid_hgt(&self.hgt)
            && is_valid_hcl(&self.hcl)
            && is_valid_ecl(&self.ecl)
            && is_valid_pid(&self.pid)
    }
}

#[aoc_generator(day4)]
fn generator(input: &str) -> Vec<Result<Passport, Box<dyn std::error::Error>>> {
    let mut results = Vec::new();

    let mut line_accum = Vec::new();
    for line in input.lines() {
        if line.is_empty() {
            results.push(Passport::from_str(&line_accum.join(" ")));
            line_accum.clear();
        } else {
            line_accum.push(line);
        }
    }

    results.push(Passport::from_str(&line_accum.join(" ")));

    results
}

#[aoc(day4, part1)]
fn part01(input: &[Result<Passport, Box<dyn std::error::Error>>]) -> usize {
    input.iter().filter(|p| p.is_ok()).count()
}

#[aoc(day4, part2)]
fn part02(input: &[Result<Passport, Box<dyn std::error::Error>>]) -> usize {
    input
        .iter()
        .filter(|p| p.as_ref().map_or(false, Passport::is_valid))
        .count()
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT_1: &str = r"ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in";

    const BUILD_INPUT_2_INVALID: &str = r"eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007";

    const BUILD_INPUT_2_VALID: &str = r"pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719";

    #[test]
    fn test0401() {
        let input = super::generator(BUILD_INPUT_1);
        let res = super::part01(&input);

        println!("-- {} --", res);
    }

    #[test]
    fn test0402_invalid() {
        let input = super::generator(BUILD_INPUT_2_INVALID);
        let res = super::part02(&input);

        println!("-- {} --", res);
    }

    #[test]
    fn test0402_valid() {
        let input = super::generator(BUILD_INPUT_2_VALID);
        let res = super::part02(&input);

        println!("-- {} --", res);
    }
}
