/** Correct results:
 * - 562
 * - 101860
 */
use nom::{
    character::complete::{anychar, char},
    combinator::{map, map_res},
    multi::separated_list0,
    sequence::tuple,
    IResult,
};

use crate::common::{
    direction::{Direction, Turn},
    errors::AocError,
    parsers::{input_parser, parse_int},
    point::PointOps,
};

#[derive(Debug)]
enum Movement {
    Direction(Direction),
    Forward,
    Rotation(Turn),
}

#[derive(Debug)]
struct Command {
    mov: Movement,
    amount: i32,
}

fn parse_movement(s: &str) -> IResult<&str, Movement> {
    map_res(anychar, |c| match c {
        'N' => Ok(Movement::Direction(Direction::Up)),
        'S' => Ok(Movement::Direction(Direction::Down)),
        'E' => Ok(Movement::Direction(Direction::Right)),
        'W' => Ok(Movement::Direction(Direction::Left)),
        'L' => Ok(Movement::Rotation(Turn::Ccw)),
        'R' => Ok(Movement::Rotation(Turn::Cw)),
        'F' => Ok(Movement::Forward),
        _ => Err(AocError::new_parse("Invalid movement")),
    })(s)
}

fn parse_line(s: &str) -> IResult<&str, Command> {
    map(tuple((parse_movement, parse_int)), |(mov, amount)| {
        Command { mov, amount }
    })(s)
}

#[aoc_generator(day12)]
fn generator(input: &str) -> Result<Vec<Command>, AocError> {
    input_parser(separated_list0(char('\n'), parse_line))(input)
}

#[aoc(day12, part1)]
fn part01(input: &[Command]) -> i32 {
    let mut position = (0i32, 0);
    let mut current_direction = Direction::Right;

    for command in input {
        match command.mov {
            Movement::Direction(d) => {
                position.move_by(command.amount, &d);
            }
            Movement::Forward => {
                position.move_by(command.amount, &current_direction);
            }
            Movement::Rotation(t) => {
                let times = command.amount / 90;
                for _ in 0..times {
                    current_direction = current_direction.rotate(&t);
                }
            }
        }
    }

    position.x().abs() + position.y().abs()
}

#[aoc(day12, part2)]
fn part02(input: &[Command]) -> i32 {
    let mut ship_position = (0i32, 0);
    let mut waypoint_position = (10i32, -1);

    for command in input {
        match command.mov {
            Movement::Direction(d) => {
                waypoint_position.move_by(command.amount, &d);
            }
            Movement::Forward => {
                let amount_x = waypoint_position.x() * command.amount;
                let amount_y = waypoint_position.y() * command.amount;
                ship_position.set_x(ship_position.x() + amount_x);
                ship_position.set_y(ship_position.y() + amount_y);
            }
            Movement::Rotation(t) => {
                let times = command.amount / 90;
                for _ in 0..times {
                    waypoint_position.rotate(&t.invert());
                }
            }
        }
    }

    ship_position.x().abs() + ship_position.y().abs()
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r"F10
N3
F7
R90
F11";

    #[test]
    fn test_gen() {
        println!("{:?}", super::generator(BUILD_INPUT))
    }

    #[test]
    fn test1201() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::part01(&input);

        println!("-- {} --", res)
    }

    #[test]
    fn test1202() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::part02(&input);

        println!("-- {} --", res)
    }
}
