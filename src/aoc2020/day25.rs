/** Correct results:
* - 296776
*/
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use nom::{character::complete::char, sequence::separated_pair};

#[aoc_generator(day25)]
fn generator(input: &str) -> Result<(usize, usize), AocError> {
    input_parser(separated_pair(parse_int, char('\n'), parse_int))(input)
}

const fn step(subject: usize, value: usize) -> usize {
    (value * subject) % 20_201_227
}

fn find_loop_size(target: usize) -> Result<usize, AocError> {
    let mut value = 1;
    for i in 1.. {
        value = step(7, value);
        if value == target {
            return Ok(i);
        }
    }

    Err(AocError::NoSolution)
}

#[aoc(day25, part1)]
fn part01((pub1, pub2): &(usize, usize)) -> Result<usize, AocError> {
    let ls1 = find_loop_size(*pub2)?;

    let mut priv2 = 1;
    for _ in 0..ls1 {
        priv2 = step(*pub1, priv2);
    }

    Ok(priv2)
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r"5764801
17807724";

    #[test]
    fn test25_gen() {
        let input = super::generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test25_loop_size() {
        println!("-- {:?} --", super::find_loop_size(5764801));
        println!("-- {:?} --", super::find_loop_size(17807724));
    }

    #[test]
    fn test2501() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::part01(&input);

        println!("-- {:?} --", res)
    }
}
