/** Correct results:
* - 32489
* - 35676
*/
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use nom::{
    bytes::complete::tag,
    character::complete::{char, digit1},
    combinator::map,
    multi::separated_list0,
    sequence::{preceded, separated_pair, tuple},
    IResult,
};
use std::{
    cmp::Ordering,
    collections::{hash_map::DefaultHasher, HashSet, VecDeque},
    hash::{Hash, Hasher},
    iter::FromIterator,
};

#[derive(Clone, Copy)]
enum Winner {
    P1,
    P2,
}

fn parse_deck(s: &str) -> IResult<&str, VecDeque<usize>> {
    preceded(
        tuple((tag("Player "), digit1, tag(":\n"))),
        map(separated_list0(char('\n'), parse_int), |cards| {
            VecDeque::from_iter(cards)
        }),
    )(s)
}

fn generator(input: &str) -> Result<(VecDeque<usize>, VecDeque<usize>), AocError> {
    input_parser(separated_pair(parse_deck, tag("\n\n"), parse_deck))(input)
}

fn play_game_1(mut p1: VecDeque<usize>, mut p2: VecDeque<usize>) -> VecDeque<usize> {
    loop {
        if let Some(a) = p1.pop_front() {
            if let Some(b) = p2.pop_front() {
                match a.cmp(&b) {
                    Ordering::Greater => {
                        p1.push_back(a);
                        p1.push_back(b);
                    }
                    Ordering::Less => {
                        p2.push_back(b);
                        p2.push_back(a);
                    }
                    Ordering::Equal => {}
                }
            } else {
                p1.push_front(a);
                return p1;
            }
        } else {
            return p2;
        }
    }
}

fn hash_state(deck1: &VecDeque<usize>, deck2: &VecDeque<usize>) -> u64 {
    let mut hasher = DefaultHasher::new();
    deck1.hash(&mut hasher);
    deck2.hash(&mut hasher);
    hasher.finish()
}

fn play_game_2(mut p1: VecDeque<usize>, mut p2: VecDeque<usize>) -> (Winner, VecDeque<usize>) {
    let mut prev_turns = HashSet::new();

    loop {
        let state = hash_state(&p1, &p2);
        if !prev_turns.insert(state) {
            return (Winner::P1, p1);
        }

        match (p1.pop_front(), p2.pop_front()) {
            (None, None) => unreachable!(),
            (Some(a), None) => {
                return (Winner::P1, {
                    p1.push_front(a);
                    p1
                })
            }
            (None, Some(b)) => {
                return (Winner::P2, {
                    p2.push_front(b);
                    p2
                })
            }
            (Some(a), Some(b)) => {
                let winner = if p1.len() >= a && p2.len() >= b {
                    play_game_2(
                        p1.iter().take(a).copied().collect(),
                        p2.iter().take(b).copied().collect(),
                    )
                    .0
                } else if a > b {
                    Winner::P1
                } else {
                    Winner::P2
                };

                match winner {
                    Winner::P1 => {
                        p1.push_back(a);
                        p1.push_back(b);
                    }
                    Winner::P2 => {
                        p2.push_back(b);
                        p2.push_back(a);
                    }
                }
            }
        }
    }
}

#[aoc(day22, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    let (a, b) = generator(input)?;

    let winning_deck = play_game_1(a, b);

    Ok(winning_deck
        .into_iter()
        .rev()
        .enumerate()
        .map(|(i, card)| card * (i + 1))
        .sum())
}

#[aoc(day22, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    let (a, b) = generator(input)?;

    let (_, winning_deck) = play_game_2(a, b);

    Ok(winning_deck
        .into_iter()
        .rev()
        .enumerate()
        .map(|(i, card)| card * (i + 1))
        .sum())
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT_1: &str = r"Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10";

    const BUILD_INPUT_2: &str = r"Player 1:
43
19

Player 2:
2
29
14";

    #[test]
    fn test22_gen() {
        let input = super::generator(BUILD_INPUT_1);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test2201() {
        let res = super::part01(BUILD_INPUT_1);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test2202() {
        let res = super::part02(BUILD_INPUT_1);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test2202_2() {
        let res = super::part02(BUILD_INPUT_2);

        println!("-- {:?} --", res)
    }
}
