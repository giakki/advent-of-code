/**
 * Correct results:
 * - 445
 * - 491
 */
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use nom::{
    character::complete::{alpha1, anychar, char},
    combinator::map,
    multi::separated_list0,
    sequence::{preceded, terminated, tuple},
    IResult,
};

#[derive(Debug, PartialEq)]
struct PwdAndPolicy<'a> {
    pub min: usize,
    pub max: usize,
    pub chr: char,
    pub password: &'a str,
}

impl<'a> PwdAndPolicy<'a> {
    fn satisfies_policy_1(&self) -> bool {
        let count = self.password.chars().filter(|c| c == &self.chr).count();

        count >= self.min && count <= self.max
    }

    fn satisfies_policy_2(&self) -> bool {
        if let Some(x) = self
            .password
            .chars()
            .nth(self.min - 1)
            .map(|c| c == self.chr)
        {
            if let Some(y) = self
                .password
                .chars()
                .nth(self.max - 1)
                .map(|c| c == self.chr)
            {
                if x {
                    return !y;
                }
                return y;
            }
        }

        false
    }
}

fn parse_pwd_and_policy(s: &str) -> IResult<&str, PwdAndPolicy> {
    map(
        tuple((
            terminated(parse_int, char('-')),
            terminated(parse_int, char(' ')),
            terminated(anychar, char(':')),
            preceded(char(' '), alpha1),
        )),
        |(min, max, chr, password)| PwdAndPolicy {
            min,
            max,
            chr,
            password,
        },
    )(s)
}

fn generator(input: &str) -> Result<Vec<PwdAndPolicy>, AocError> {
    input_parser(separated_list0(char('\n'), parse_pwd_and_policy))(input)
}

#[aoc(day2, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    let lines = generator(input)?;
    Ok(lines.iter().filter(|p| p.satisfies_policy_1()).count())
}

#[aoc(day2, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    let lines = generator(input)?;

    Ok(lines.iter().filter(|p| p.satisfies_policy_2()).count())
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r"1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc";

    #[test]
    fn test02_gen() {
        let res = super::generator(BUILD_INPUT);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test0201() {
        let res = super::part01(BUILD_INPUT);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test0202() {
        let res = super::part02(BUILD_INPUT);

        println!("-- {:?} --", res)
    }
}
