/** Correct results:
* - 614
* - 1065
*/
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use nom::{character::complete::char, multi::separated_list1};

#[derive(Clone, Debug)]
enum Track {
    None,
    Once(usize),
    Twice(usize, usize),
}

impl Track {
    fn mark_as_seen_at(&mut self, val: usize) {
        *self = match self {
            Self::None => Self::Once(val),
            Self::Once(a) | Self::Twice(_, a) => Self::Twice(*a, val),
        }
    }
}

#[aoc_generator(day15)]
fn generator(input: &str) -> Result<Vec<usize>, AocError> {
    input_parser(separated_list1(char(','), parse_int))(input)
}

fn prepare_tracker(input: &[usize], reserve: usize) -> Vec<Track> {
    let mut tracker: Vec<Track> = vec![Track::None; reserve];

    for (i, n) in input.iter().enumerate() {
        tracker[*n].mark_as_seen_at(i + 1);
    }

    tracker
}

fn runner(mut tracker: Vec<Track>, first_turn: usize) -> Result<usize, AocError> {
    let mut last_spoken = 0;

    for i in first_turn..tracker.len() {
        let next = match tracker[last_spoken] {
            Track::None => return Err(AocError::Unspecified),
            Track::Once(_) => 0,
            Track::Twice(a, b) => b - a,
        };

        tracker[next].mark_as_seen_at(i + 1);
        last_spoken = next;
    }

    Ok(last_spoken)
}

#[aoc(day15, part1)]
fn part01(input: &[usize]) -> Result<usize, AocError> {
    let tracker = prepare_tracker(input, 2020);

    runner(tracker, input.len())
}

#[aoc(day15, part2)]
fn part02(input: &[usize]) -> Result<usize, AocError> {
    let tracker = prepare_tracker(input, 30_000_000);

    runner(tracker, input.len())
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r"0,3,6";

    #[test]
    fn test1501() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::part01(&input);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test1502() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::part02(&input);

        println!("-- {:?} --", res)
    }
}
