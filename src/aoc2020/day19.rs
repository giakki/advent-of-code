/** Correct results:
* - 222
* - 339
*/
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{alpha1, char},
    combinator::map,
    multi::separated_list1,
    sequence::{delimited, separated_pair},
    IResult,
};
use std::collections::HashMap;

#[derive(Clone, Debug)]
enum Rule<'a> {
    Lit(&'a str),
    Ref(usize),
    Seq(Vec<Rule<'a>>),
    Alt(Vec<Rule<'a>>),
}

impl<'a> Rule<'a> {
    fn matches<'b>(&self, rules: &'b HashMap<usize, Rule>, remaining: &'b str) -> Vec<&'b str> {
        if remaining.is_empty() {
            return vec![];
        }
        match self {
            Rule::Ref(i) => rules.get(i).unwrap().matches(rules, remaining),
            Rule::Alt(alt) => alt
                .iter()
                .flat_map(|r| r.matches(rules, remaining).into_iter())
                .collect(),
            Rule::Lit(c1) => {
                if let Some(zaz) = remaining.strip_prefix(c1) {
                    return vec![zaz];
                }
                vec![]
            }
            Rule::Seq(seq) => Self::match_seq(seq, rules, remaining),
        }
    }

    fn match_seq<'b>(
        seqs: &[Rule],
        rules: &'b HashMap<usize, Rule>,
        unparsed: &'b str,
    ) -> Vec<&'b str> {
        match seqs {
            [rule] => rule.matches(rules, unparsed),
            [head, tail @ ..] => {
                let mut r = Vec::new();
                for m in head.matches(rules, unparsed) {
                    for n in Self::match_seq(tail, rules, m) {
                        r.push(n);
                    }
                }
                r
            }
            [] => vec![],
        }
    }
}

fn parse_lit_rule(s: &str) -> IResult<&str, Rule> {
    map(delimited(char('"'), alpha1, char('"')), |lit| {
        Rule::Lit(lit)
    })(s)
}

fn parse_ref_rule(s: &str) -> IResult<&str, Rule> {
    map(parse_int, Rule::Ref)(s)
}

fn parse_seq_rule(s: &str) -> IResult<&str, Rule> {
    map(separated_list1(char(' '), parse_ref_rule), |seq| {
        Rule::Seq(seq)
    })(s)
}

fn parse_alt_rule(s: &str) -> IResult<&str, Rule> {
    map(separated_list1(tag(" | "), parse_seq_rule), |alts| {
        Rule::Alt(alts)
    })(s)
}

fn parse_rule(s: &str) -> IResult<&str, (usize, Rule)> {
    separated_pair(
        parse_int,
        tag(": "),
        alt((
            parse_alt_rule,
            parse_seq_rule,
            parse_ref_rule,
            parse_lit_rule,
        )),
    )(s)
}

fn generator(input: &str) -> Result<(HashMap<usize, Rule>, Vec<&str>), AocError> {
    let (rules, messages) = input_parser(separated_pair(
        separated_list1(char('\n'), parse_rule),
        tag("\n\n"),
        separated_list1(char('\n'), alpha1),
    ))(input)?;

    Ok((
        rules
            .into_iter()
            .fold(HashMap::new(), |mut acc, (id, rule)| {
                acc.entry(id).or_insert(rule);
                acc
            }),
        messages,
    ))
}

#[aoc(day19, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    let (rules, messages) = generator(input)?;

    let start = &rules[&0];
    let mut c = 0;
    for msg in messages {
        for m in start.matches(&rules, msg) {
            if m.is_empty() {
                c += 1;
                break;
            }
        }
    }

    Ok(c)
}

#[aoc(day19, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    let (mut rules, messages) = generator(input)?;

    rules.entry(8).and_modify(|r| {
        *r = Rule::Alt(vec![
            Rule::Ref(42),
            Rule::Seq(vec![Rule::Ref(42), Rule::Ref(8)]),
        ]);
    });
    rules.entry(11).and_modify(|r| {
        *r = Rule::Alt(vec![
            Rule::Seq(vec![Rule::Ref(42), Rule::Ref(31)]),
            Rule::Seq(vec![Rule::Ref(42), Rule::Ref(11), Rule::Ref(31)]),
        ]);
    });

    let start = &rules[&0];
    let mut c = 0;
    for msg in messages {
        for m in start.matches(&rules, msg) {
            if m.is_empty() {
                c += 1;
                break;
            }
        }
    }

    Ok(c)
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r#"0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: "a"
5: "b"

ababbb
bababa
abbbab
aaabbb
aaaabbb"#;

    #[test]
    fn test19_gen() {
        let input = super::generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test1901() {
        let res = super::part01(BUILD_INPUT);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test1902() {
        let res = super::part02(BUILD_INPUT);

        println!("-- {:?} --", res)
    }
}
