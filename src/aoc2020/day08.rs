/** Correct results:
* - 1671
* - 892
*/
use crate::common::parsers::parse_signed_int;
use crate::common::{errors::AocError, parsers::input_parser};
use nom::{
    branch::alt, bytes::complete::tag, character::complete::char, combinator::map,
    multi::separated_list0, sequence::preceded, IResult,
};

#[derive(Debug)]
enum InstructionType {
    Acc(i32),
    Jmp(i32),
    Nop,
}

#[derive(Debug)]
struct Instruction {
    pub itype: InstructionType,
    pub is_run: bool,
}

impl Instruction {
    pub const fn new(itype: InstructionType) -> Self {
        Self {
            itype,
            is_run: false,
        }
    }
}

fn parse_acc(s: &str) -> IResult<&str, Instruction> {
    map(preceded(tag("acc "), parse_signed_int), |n| {
        Instruction::new(InstructionType::Acc(n))
    })(s)
}

fn parse_jmp(s: &str) -> IResult<&str, Instruction> {
    map(preceded(tag("jmp "), parse_signed_int), |n| {
        Instruction::new(InstructionType::Jmp(n))
    })(s)
}

fn parse_nop(s: &str) -> IResult<&str, Instruction> {
    map(preceded(tag("nop "), parse_signed_int), |_: i32| {
        Instruction::new(InstructionType::Nop)
    })(s)
}

fn parse_instruction(s: &str) -> IResult<&str, Instruction> {
    alt((parse_acc, parse_jmp, parse_nop))(s)
}

fn generator(input: &str) -> Result<Vec<Instruction>, AocError> {
    input_parser(separated_list0(char('\n'), parse_instruction))(input)
}

fn run_till_loop(instructions: &mut [Instruction]) -> (bool, i32) {
    let mut acc = 0;
    let mut isp = 0;
    loop {
        if isp >= instructions.len() {
            return (true, acc);
        }
        let next_instruction = &mut instructions[isp];
        if next_instruction.is_run {
            return (false, acc);
        }

        next_instruction.is_run = true;

        match next_instruction.itype {
            InstructionType::Acc(n) => {
                acc += n;
                isp += 1;
            }
            InstructionType::Jmp(n) => isp = (isp as i32 + n) as usize,
            InstructionType::Nop => isp += 1,
        }
    }
}

#[aoc(day8, part1)]
fn part01(input: &str) -> Result<i32, AocError> {
    let mut instructions = generator(input)?;

    if let (false, acc) = run_till_loop(&mut instructions) {
        Ok(acc)
    } else {
        Err(AocError::NoSolution)
    }
}

#[aoc(day8, part2)]
fn part02(input: &str) -> Result<i32, AocError> {
    let mut instructions = generator(input)?;

    let jmps = instructions
        .iter()
        .enumerate()
        .filter_map(|(i, ins)| match ins.itype {
            InstructionType::Jmp(n) => Some((i, n)),
            _ => None,
        })
        .collect::<Vec<_>>();

    for (index, original) in jmps {
        instructions[index] = Instruction::new(InstructionType::Nop);
        if let (true, acc) = run_till_loop(&mut instructions) {
            return Ok(acc);
        }
        instructions[index] = Instruction::new(InstructionType::Jmp(original));
        for instruction in &mut instructions {
            instruction.is_run = false;
        }
    }

    Err(AocError::NoSolution)
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r"nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6";

    #[test]
    fn test0801() {
        let res = super::part01(BUILD_INPUT);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test0802() {
        let res = super::part02(BUILD_INPUT);

        println!("-- {:?} --", res)
    }
}
