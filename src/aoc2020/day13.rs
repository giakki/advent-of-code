/** Correct results:
 * - 136
 * - 305068317272992
 */
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{char, digit1},
    combinator::map,
    multi::separated_list0,
    sequence::{terminated, tuple},
    IResult,
};

fn parse_bus(s: &str) -> IResult<&str, Option<i32>> {
    map(alt((digit1, tag("x"))), |n: &str| n.parse::<i32>().ok())(s)
}

fn inv_mod(x: i64, p: i64) -> i64 {
    let mut res = 1;

    for _ in 0..(p - 2) {
        res = (res * x) % p;
    }

    res
}

#[aoc_generator(day13)]
fn generator(input: &str) -> Result<(i32, Vec<Option<i32>>), AocError> {
    input_parser(tuple((
        terminated(parse_int, char('\n')),
        separated_list0(char(','), parse_bus),
    )))(input)
}

#[aoc(day13, part1)]
fn part01((timestamp, buses): &(i32, Vec<Option<i32>>)) -> Result<i32, AocError> {
    let first = buses
        .iter()
        .filter_map(|bus| bus.map(|n| (n, (n - timestamp % n) % n)))
        .min_by(|x, y| x.1.cmp(&y.1))
        .ok_or(AocError::NoSolution)?;

    Ok(first.0 * first.1)
}

#[aoc(day13, part2)]
fn part02((_, buses): &(i32, Vec<Option<i32>>)) -> i64 {
    let requirements = buses
        .iter()
        .enumerate()
        .filter_map(|r| r.1.map_or(None, |n| Some((r.0 as i64, i64::from(n)))))
        .collect::<Vec<_>>();

    let prod: i64 = requirements.iter().map(|(_, b)| b).product();

    requirements
        .iter()
        .map(|&(a, b)| -a * (prod / b) * inv_mod(prod / b, b))
        .sum::<i64>()
        .rem_euclid(prod)
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT_1: &str = r"939
7,13,x,x,59,x,31,19";

    const BUILD_INPUT_2: &str = r"939
17,x,13,19";

    #[test]
    fn test_gen() {
        let input = super::generator(BUILD_INPUT_1);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test1301() {
        let input = super::generator(BUILD_INPUT_1).unwrap();
        let res = super::part01(&input);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test1302() {
        let input = super::generator(BUILD_INPUT_2).unwrap();
        let res = super::part02(&input);

        println!("-- {:?} --", res)
    }
}
