/** Correct results:
* - 11004703763391
* - 290726428573651
*/
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use nom::{
    branch::alt,
    character::complete::{char, space0},
    combinator::{map, map_res},
    multi::{many0, separated_list0},
    sequence::delimited,
    IResult,
};
use std::{cmp::Ordering, fmt::Debug, unreachable};

#[derive(Clone, Copy, Debug)]
enum Operation {
    Plus,
    Times,
}

type OperationPrecedenceComparer = fn(Operation, Operation) -> Ordering;

#[derive(Debug)]
enum Token {
    Literal(i64),
    Operation(Operation),
    Parens(Box<Expr>),
}

type Expr = Vec<Token>;

fn parse_literal_token(s: &str) -> IResult<&str, Token> {
    map(parse_int, Token::Literal)(s)
}

fn parse_operation_token(s: &str) -> IResult<&str, Token> {
    map_res(alt((char('+'), char('*'))), |c| match c {
        '+' => Ok(Token::Operation(Operation::Plus)),
        '*' => Ok(Token::Operation(Operation::Times)),
        _ => Err(AocError::Unspecified),
    })(s)
}

fn parse_parens_token(s: &str) -> IResult<&str, Token> {
    map(delimited(char('('), parse_expr, char(')')), |tokens| {
        Token::Parens(Box::new(tokens))
    })(s)
}

fn parse_token(s: &str) -> IResult<&str, Token> {
    delimited(
        space0,
        alt((
            parse_literal_token,
            parse_operation_token,
            parse_parens_token,
        )),
        space0,
    )(s)
}

fn parse_expr(s: &str) -> IResult<&str, Expr> {
    many0(parse_token)(s)
}

fn generator(input: &str) -> Result<Vec<Expr>, AocError> {
    input_parser(separated_list0(char('\n'), parse_expr))(input)
}

fn to_postfix(tokens: Expr, op_comparer: OperationPrecedenceComparer) -> Expr {
    let mut res = Vec::with_capacity(tokens.len());
    let mut op_stack = Vec::with_capacity(16);

    for token in tokens {
        match token {
            Token::Parens(nested) => res.extend(to_postfix(*nested, op_comparer)),
            Token::Literal(_) => res.push(token),
            Token::Operation(this_op) => {
                while op_stack.last().is_some() {
                    let tok = op_stack.pop().unwrap();
                    if let Token::Operation(last_op) = tok {
                        match op_comparer(last_op, this_op) {
                            Ordering::Equal => {
                                res.push(tok);
                                break;
                            }
                            Ordering::Less => {
                                op_stack.push(tok);
                                break;
                            }
                            Ordering::Greater => {
                                res.push(tok);
                            }
                        }
                    } else {
                        op_stack.push(tok);
                    }
                }
                op_stack.push(token);
            }
        }
    }

    while let Some(tok) = op_stack.pop() {
        res.push(tok);
    }

    res
}

fn eval(expr: &Expr) -> Result<i64, AocError> {
    let mut stack = Vec::new();
    for token in expr {
        match token {
            Token::Literal(_) | Token::Parens(_) => {
                let a = match token {
                    Token::Literal(n) => *n,
                    Token::Parens(nested) => eval(nested)?,
                    Token::Operation(_) => unreachable!(),
                };
                stack.push(a);
            }
            Token::Operation(op) => {
                if let Some(a) = stack.pop() {
                    if let Some(b) = stack.pop() {
                        match op {
                            Operation::Plus => stack.push(a + b),
                            Operation::Times => stack.push(a * b),
                        }
                    } else {
                        return Err(AocError::Unspecified);
                    }
                } else {
                    return Err(AocError::Unspecified);
                }
            }
        }
    }

    stack.pop().ok_or(AocError::Unspecified)
}

const fn part_1_comparer(_: Operation, _: Operation) -> Ordering {
    Ordering::Equal
}

const fn part_2_comparer(lhs: Operation, rhs: Operation) -> Ordering {
    match lhs {
        Operation::Plus => match rhs {
            Operation::Plus => Ordering::Equal,
            Operation::Times => Ordering::Greater,
        },
        Operation::Times => match rhs {
            Operation::Plus => Ordering::Less,
            Operation::Times => Ordering::Equal,
        },
    }
}

#[aoc(day18, part1)]
fn part01(input: &str) -> Result<i64, AocError> {
    let expressions = generator(input)?;

    Ok(expressions
        .into_iter()
        .map(|expr| eval(&to_postfix(expr, part_1_comparer)))
        .collect::<Result<Vec<_>, _>>()?
        .iter()
        .sum())
}

#[aoc(day18, part2)]
fn part02(input: &str) -> Result<i64, AocError> {
    let expressions = generator(input)?;

    Ok(expressions
        .into_iter()
        .map(|expr| eval(&to_postfix(expr, part_2_comparer)))
        .collect::<Result<Vec<_>, _>>()?
        .iter()
        .sum())
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT_1: &str = r"1 + (2 * 3) + (4 * (5 + 6))
2 * 3 + (4 * 5)
5 + (8 * 3 + 9 + 3 * 4 * 3)
5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))
((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2";

    #[test]
    fn test18_gen() {
        let input = super::generator(BUILD_INPUT_1).unwrap();

        for line in input {
            println!("{:?}", line);
        }
    }

    #[test]
    fn test18_to_postfix_1() {
        let input = super::generator(BUILD_INPUT_1).unwrap();

        for line in input {
            println!("{:?}", super::to_postfix(line, super::part_1_comparer));
        }
    }

    #[test]
    fn test18_to_postfix_2() {
        let input = super::generator(BUILD_INPUT_1).unwrap();

        for line in input.into_iter().skip(1).take(1) {
            println!("{:?}", super::to_postfix(line, super::part_2_comparer));
        }
    }

    #[test]
    fn test18_eval_1() {
        let input = super::generator(BUILD_INPUT_1).unwrap();

        for line in input {
            println!(
                "{:?}",
                super::eval(&super::to_postfix(line, super::part_1_comparer))
            );
        }
    }

    #[test]
    fn test18_eval_2() {
        let input = super::generator(BUILD_INPUT_1).unwrap();

        for line in input {
            println!(
                "{:?}",
                super::eval(&super::to_postfix(line, super::part_2_comparer))
            );
        }
    }
}
