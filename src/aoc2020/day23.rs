/** Correct results:
 * - 82934675
 * - 474600314018
 */
use crate::common::errors::AocError;
use itertools::Itertools;

#[aoc_generator(day23)]
fn generator(input: &str) -> Result<Vec<usize>, AocError> {
    input
        .chars()
        .map(|c| c.to_digit(10).map(|n| n as usize))
        .collect::<Option<Vec<usize>>>()
        .ok_or_else(|| AocError::ParseError("Invalid input".to_owned()))
}

fn round(cups: &mut [usize], current: &mut usize) {
    let mut grabbed_cups = [0, 0, 0];
    grabbed_cups[0] = cups[*current];
    grabbed_cups[1] = cups[grabbed_cups[0]];
    grabbed_cups[2] = cups[grabbed_cups[1]];

    let mut dest_cup = *current - 1;
    loop {
        if dest_cup == 0 {
            dest_cup = cups.len() - 1;
        } else if grabbed_cups.contains(&dest_cup) {
            dest_cup -= 1;
        } else {
            break;
        }
    }

    cups[*current] = cups[grabbed_cups[2]];

    let destination = cups[dest_cup];

    cups[dest_cup] = grabbed_cups[0];

    cups[grabbed_cups[0]] = grabbed_cups[1];
    cups[grabbed_cups[1]] = grabbed_cups[2];
    cups[grabbed_cups[2]] = destination;

    *current = cups[*current];
}

fn to_fake_linked_list(arr: &[usize]) -> Vec<usize> {
    let mut list = vec![usize::default(); arr.len() + 1];
    list[arr[arr.len() - 1]] = arr[0];

    for (cur, next) in arr.iter().tuple_windows() {
        list[*cur] = *next;
    }

    list
}

#[aoc(day23, part1)]
fn part01(input: &[usize]) -> String {
    let mut cursor = input[0];
    let mut labeling = to_fake_linked_list(input);

    for _ in 0..100 {
        round(&mut labeling, &mut cursor);
    }

    let mut result = String::new();
    cursor = 1;
    loop {
        cursor = labeling[cursor];
        if cursor == 1 {
            break;
        }
        result.push_str(&cursor.to_string());
    }

    result
}

#[aoc(day23, part2)]
fn part02(input: &[usize]) -> usize {
    let mut cursor = input[0];
    let mut labeling = input
        .iter()
        .copied()
        .chain(input.iter().max().unwrap() + 1..=1_000_000)
        .collect::<Vec<_>>();

    labeling = to_fake_linked_list(&labeling);

    for _ in 0..10_000_000 {
        round(&mut labeling, &mut cursor);
    }

    let l1 = labeling[1];
    let l2 = labeling[l1];

    l1 * l2
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r"389125467";

    #[test]
    fn test23_gen() {
        let input = super::generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test2301() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::part01(&input);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test2302() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::part02(&input);

        println!("-- {:?} --", res)
    }
}
