/** Correct results:
* - 18142
* - 1069784384303
*/
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use itertools::Itertools;
use nom::{
    bytes::complete::{tag, take_while1},
    character::complete::char,
    combinator::map,
    multi::separated_list0,
    sequence::{preceded, separated_pair, tuple},
    IResult,
};

type Input<'a> = (Vec<Rule<'a>>, Ticket, Vec<Ticket>);

type Rule<'a> = (&'a str, Vec<Range>);

type Ticket = Vec<usize>;

#[derive(Debug)]
struct Range {
    from: usize,
    to: usize,
}

impl Range {
    const fn includes(&self, num: usize) -> bool {
        num >= self.from && num <= self.to
    }
}

fn parse_range(s: &str) -> IResult<&str, Range> {
    map(
        separated_pair(parse_int, char('-'), parse_int),
        |(from, to)| Range { from, to },
    )(s)
}

fn parse_rule(s: &str) -> IResult<&str, Rule> {
    separated_pair(
        take_while1(|c| c != ':'),
        tag(": "),
        separated_list0(tag(" or "), parse_range),
    )(s)
}

fn parse_rules(s: &str) -> IResult<&str, Vec<Rule>> {
    separated_list0(char('\n'), parse_rule)(s)
}

fn parse_ticket(s: &str) -> IResult<&str, Ticket> {
    separated_list0(char(','), parse_int)(s)
}

fn parse_your_ticket(s: &str) -> IResult<&str, Ticket> {
    preceded(tag("your ticket:\n"), parse_ticket)(s)
}

fn parse_nearby_tickets(s: &str) -> IResult<&str, Vec<Ticket>> {
    preceded(
        tag("nearby tickets:\n"),
        separated_list0(char('\n'), parse_ticket),
    )(s)
}

fn generator(input: &str) -> Result<Input, AocError> {
    input_parser(tuple((
        parse_rules,
        preceded(tag("\n\n"), parse_your_ticket),
        preceded(tag("\n\n"), parse_nearby_tickets),
    )))(input)
}

#[aoc(day16, part1)]
fn part01(s: &str) -> Result<usize, AocError> {
    let (rules, _, tickets) = generator(s)?;

    let mut sum = 0;
    for ticket in tickets {
        for value in ticket {
            if !rules
                .iter()
                .any(|(_, ranges)| ranges.iter().any(|r| r.includes(value)))
            {
                sum += value;
            }
        }
    }

    Ok(sum)
}

#[aoc(day16, part2)]
fn part02(s: &str) -> Result<usize, AocError> {
    let (rules, your_ticket, tickets) = generator(s)?;

    let good_tickets = tickets
        .into_iter()
        .filter(|ticket| {
            ticket.iter().all(|value| {
                rules
                    .iter()
                    .any(|(_, ranges)| ranges.iter().any(|r| r.includes(*value)))
            })
        })
        .collect::<Vec<_>>();

    let num_fields = your_ticket.len();
    let mut valid_matrix = rules
        .iter()
        .map(|(_, rules)| {
            (0..num_fields)
                .map(|i| {
                    good_tickets
                        .iter()
                        .all(|ticket| rules.iter().any(|rule| rule.includes(ticket[i])))
                })
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>();

    let mut valid_configuration = vec![None; rules.len()];
    while valid_configuration.iter().any(Option::is_none) {
        if let Some((row, cols)) = valid_matrix
            .iter()
            .find_position(|cols| cols.iter().filter(|c| **c).count() == 1)
        {
            if let Some(col) = cols.iter().position(|c| *c) {
                valid_configuration[col] = Some(rules[row].0);

                for row in &mut valid_matrix {
                    row[col] = false;
                }
            } else {
                return Err(AocError::Unspecified);
            }
        } else {
            return Err(AocError::NoSolution);
        }
    }

    let solution = valid_configuration
        .into_iter()
        .enumerate()
        .map(|(i, opt_name)| {
            opt_name.map_or(Err(AocError::NoSolution), |name| {
                if name.starts_with("departure") {
                    Ok(your_ticket[i])
                } else {
                    Ok(1)
                }
            })
        })
        .collect::<Result<Vec<_>, _>>()?;

    Ok(solution.iter().product())
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT_1: &str = r"class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12";

    const BUILD_INPUT_2: &str = r"class: 0-1 or 4-19
row: 0-5 or 8-19
seat: 0-13 or 16-19

your ticket:
11,12,13

nearby tickets:
3,9,18
15,1,5
5,14,9";

    #[test]
    fn test16_gen() {
        let res = super::generator(BUILD_INPUT_1);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test1601() {
        let res = super::part01(BUILD_INPUT_1);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test1602() {
        let res = super::part02(BUILD_INPUT_2);

        println!("-- {:?} --", res)
    }
}
