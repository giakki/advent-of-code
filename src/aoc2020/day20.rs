/** Correct results:
* - 79412832860579
* - 2155
*/
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use nom::{
    bytes::complete::{tag, take_while},
    character::complete::char,
    multi::separated_list1,
    sequence::{delimited, separated_pair},
    IResult,
};

fn parse_id(s: &str) -> IResult<&str, usize> {
    delimited(tag("Tile "), parse_int, char(':'))(s)
}

fn parse_tiles(s: &str) -> IResult<&str, Vec<&str>> {
    separated_list1(char('\n'), take_while(|c| c == '#' || c == '.'))(s)
}

fn generator(input: &str) -> Result<Vec<(usize, Vec<&str>)>, AocError> {
    input
        .split("\n\n")
        .map(|s| input_parser(separated_pair(parse_id, char('\n'), parse_tiles))(s))
        .collect()
}

#[aoc(day20, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    let _images = generator(input)?;

    Ok(0)
}

#[aoc(day20, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    let _images = generator(input)?;

    Ok(0)
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r"Tile 2311:
..##.#..#.
##..#.....
#...##..#.
####.#...#
##.##.###.
##...#.###
.#.#.#..##
..#....#..
###...#.#.
..###..###

Tile 1951:
#.##...##.
#.####...#
.....#..##
#...######
.##.#....#
.###.#####
###.##.##.
.###....#.
..#.#..#.#
#...##.#..

Tile 1171:
####...##.
#..##.#..#
##.#..#.#.
.###.####.
..###.####
.##....##.
.#...####.
#.##.####.
####..#...
.....##...

Tile 1427:
###.##.#..
.#..#.##..
.#.##.#..#
#.#.#.##.#
....#...##
...##..##.
...#.#####
.#.####.#.
..#..###.#
..##.#..#.

Tile 1489:
##.#.#....
..##...#..
.##..##...
..#...#...
#####...#.
#..#.#.#.#
...#.#.#..
##.#...##.
..##.##.##
###.##.#..

Tile 2473:
#....####.
#..#.##...
#.##..#...
######.#.#
.#...#.#.#
.#########
.###.#..#.
########.#
##...##.#.
..###.#.#.

Tile 2971:
..#.#....#
#...###...
#.#.###...
##.##..#..
.#####..##
.#..####.#
#..#.#..#.
..####.###
..#.#.###.
...#.#.#.#

Tile 2729:
...#.#.#.#
####.#....
..#.#.....
....#..#.#
.##..##.#.
.#.####...
####.#.#..
##.####...
##..#.##..
#.##...##.

Tile 3079:
#.#.#####.
.#..######
..#.......
######....
####.#..#.
.#...#.##.
#.#####.##
..#.###...
..#.......
..#.###...";

    #[test]
    fn test20_gen() {
        let input = super::generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test2001() {
        let res = super::part01(BUILD_INPUT);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test2002() {
        let res = super::part02(BUILD_INPUT);

        println!("-- {:?} --", res)
    }
}
