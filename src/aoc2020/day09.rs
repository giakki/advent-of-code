/** Correct results:
 * - 22477624
 * - 2980044
 */
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use itertools::Itertools;
use itertools::MinMaxResult::MinMax;
use nom::{character::complete::char, multi::separated_list0};

const PREAMBLE: usize = 25;

fn check_sums(arr: &[usize], target: usize) -> bool {
    arr.iter()
        .enumerate()
        .any(|(index, x)| arr.iter().skip(index + 1).any(|y| x + y == target))
}

#[aoc_generator(day9)]
fn generator(input: &str) -> Result<Vec<usize>, AocError> {
    input_parser(separated_list0(char('\n'), parse_int))(input)
}

#[aoc(day9, part1)]
fn part01(input: &[usize]) -> Result<usize, AocError> {
    for w in input.windows(PREAMBLE + 1) {
        let slice = &w[0..(PREAMBLE)];
        let target = w[PREAMBLE];
        if !check_sums(slice, target) {
            return Ok(w[PREAMBLE]);
        }
    }

    Err(AocError::NoSolution)
}

#[aoc(day9, part2)]
fn part02(input: &[usize]) -> Result<usize, AocError> {
    let p1 = part01(input)?;

    for i in 0..input.len() {
        let mut acc = 0;
        let mut solution = Vec::new();

        for k in input.iter().skip(i + 1) {
            acc += k;
            solution.push(k);
            if acc == p1 {
                if let MinMax(min, max) = solution.into_iter().minmax() {
                    return Ok(*min + *max);
                }
                return Err(AocError::NoSolution);
            }
            if acc > p1 {
                break;
            }
        }
    }

    Err(AocError::NoSolution)
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r"35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576";

    // Set PREAMBLE to 5 before running tests.
    #[test]
    fn test0901() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::part01(&input);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test0902() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::part02(&input);

        println!("-- {:?} --", res)
    }
}
