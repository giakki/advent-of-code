/** Correct results:
* - 2481
* - 2227
*/
use ndarray::{Array2, ShapeError};

use crate::common::chr::FromChar;
use crate::common::errors::AocError;
use crate::common::map::Map;
use std::fmt;
use std::str::FromStr;

#[derive(Clone, Copy, Debug, PartialEq)]
enum Tile {
    Floor,
    Empty,
    Occupied,
}

impl fmt::Display for Tile {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::Floor => '.',
                Self::Empty => 'L',
                Self::Occupied => '#',
            }
        )
    }
}

impl FromChar for Tile {
    type Err = AocError;

    fn from_char(c: &char) -> Result<Self, AocError> {
        match c {
            '.' => Ok(Self::Floor),
            'L' => Ok(Self::Empty),
            '#' => Ok(Self::Occupied),
            _ => Err(AocError::new_parse("Invalid tile")),
        }
    }
}

fn fix<E, T: PartialEq, F: Fn(&T) -> Result<T, E>>(function: F, initial_value: T) -> Result<T, E> {
    let mut next = initial_value;
    loop {
        let tmp = function(&next)?;
        if tmp == next {
            return Ok(tmp);
        }

        next = tmp;
    }
}

fn step1(original: &Map<Tile>) -> Result<Map<Tile>, ShapeError> {
    let tiles = original
        .iter_neighbors_fn(|tile, neighbors| {
            if tile == Tile::Floor {
                return tile;
            }

            let num_occupied = neighbors
                .into_iter()
                .filter(|t| *t == Tile::Occupied)
                .count();

            if num_occupied == 0 {
                Tile::Occupied
            } else if num_occupied > 3 {
                Tile::Empty
            } else {
                tile
            }
        })
        .collect::<Vec<_>>();

    Ok(Map::from_ndarray(
        Array2::from_shape_vec((original.height(), original.width()), tiles)?.reversed_axes(),
    ))
}

fn move_ray(ray: &mut (i32, i32), dir: (i32, i32)) {
    ray.0 += dir.0;
    ray.1 += dir.1;
}

fn get_num_occupied_2(map: &Map<Tile>, pos: (i32, i32)) -> usize {
    let max = map.tiles.dim();
    let mut num_occupied = 0;

    let dirs = [
        (-1, -1),
        (0, -1),
        (1, -1),
        (-1, 0),
        (1, 0),
        (-1, 1),
        (0, 1),
        (1, 1),
    ];
    for dir in &dirs {
        let mut ray_pos = (pos.0, pos.1);
        move_ray(&mut ray_pos, *dir);
        while ray_pos.0 >= 0
            && ray_pos.0 < max.0 as i32
            && ray_pos.1 >= 0
            && ray_pos.1 < max.1 as i32
        {
            let tile = map.at((ray_pos.0 as usize, ray_pos.1 as usize));
            if *tile == Tile::Occupied {
                num_occupied += 1;
                break;
            } else if *tile == Tile::Empty {
                break;
            }

            move_ray(&mut ray_pos, *dir);
        }
    }

    num_occupied
}

fn step2(original: &Map<Tile>) -> Result<Map<Tile>, AocError> {
    let mut map = original.clone();

    for pos in map.iter_positions() {
        if *map.at(pos) == Tile::Floor {
            continue;
        }

        let num_occupied = get_num_occupied_2(original, (pos.0 as i32, pos.1 as i32));

        if num_occupied == 0 {
            *map.at_mut(pos) = Tile::Occupied;
        } else if num_occupied > 4 {
            *map.at_mut(pos) = Tile::Empty;
        }
    }

    Ok(map)
}

#[aoc_generator(day11)]
fn generator(input: &str) -> Result<Map<Tile>, AocError> {
    Map::from_str(input)
}

#[aoc(day11, part1)]
fn part01(input: &Map<Tile>) -> Result<usize, AocError> {
    Ok(fix(step1, input.clone())?
        .tiles
        .iter()
        .filter(|t| **t == Tile::Occupied)
        .count())
}

#[aoc(day11, part2)]
fn part02(input: &Map<Tile>) -> Result<usize, AocError> {
    Ok(fix(step2, input.clone())?
        .tiles
        .iter()
        .filter(|t| **t == Tile::Occupied)
        .count())
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT_1: &str = r"L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL";

    const BUILD_INPUT_2: &str = r"#.##.##.##
    #######.##
    #.#.#..#..
    ####.##.##
    #.##.##.##
    #.#####.##
    ..#.#.....
    ##########
    #.######.#
    #.#####.##";

    #[test]
    fn test_step_1() {
        let mut input = super::generator(BUILD_INPUT_2).unwrap();
        for _ in 0..4 {
            input = super::step1(&input).unwrap();

            println!("\n{}\n", input)
        }
    }

    #[test]
    fn test1101() {
        let input = super::generator(BUILD_INPUT_1).unwrap();
        let res = super::part01(&input);

        println!("{:?}", res);
    }

    #[test]
    fn test_get_num_occupied_2() {
        let input = super::generator(BUILD_INPUT_2).unwrap();
        let res = super::get_num_occupied_2(&input, (1, 0));
        println!("{}", input.at((1, 0)));
        println!("{}", res);
    }

    #[test]
    fn test1102() {
        let mut input = super::generator(BUILD_INPUT_1).unwrap();
        for _ in 0..6 {
            input = super::step2(&input).unwrap();

            println!("{}\n", input)
        }
    }
}
