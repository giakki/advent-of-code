/** Correct results:
 * - 336
 * - 2620
 */
use std::collections::{HashMap, HashSet};

#[derive(Clone, Eq, Hash, PartialEq)]
struct Point {
    d: [i64; 4],
}

#[derive(Clone, Copy)]
enum Dimensions {
    Three,
    Four,
}

struct NeighborsBitsetIterator {
    base: [i64; 4],
    base_mask: u32,
    submask: u32,
}

impl NeighborsBitsetIterator {
    fn correct_submask(&mut self) {
        while self.has_invalid_submask() {
            self.submask = (self.submask - 1) & self.base_mask;
        }
    }

    fn has_invalid_submask(&self) -> bool {
        for i in 0_u32..4 {
            if (self.submask >> (2 * i)) & 0b11 == 0b11 {
                return true;
            }
        }

        false
    }
}

impl Iterator for NeighborsBitsetIterator {
    type Item = Point;

    fn next(&mut self) -> Option<Point> {
        self.correct_submask();

        if self.submask == 0 {
            None
        } else {
            let mut d = self.base;
            for (i, el) in d.iter_mut().enumerate() {
                let diff = match (self.submask >> (2 * i)) & 0b11 {
                    0 => 0,
                    1 => -1,
                    2 => 1,
                    _ => return None, // Shpuld retrn an error
                };

                *el += diff;
            }
            self.submask = (self.submask - 1) & self.base_mask;

            Some(Point { d })
        }
    }
}

impl Point {
    const fn neighbors(&self, dimensions: Dimensions) -> NeighborsBitsetIterator {
        let base_mask: u32 = match dimensions {
            Dimensions::Three => 0b0011_1111,
            Dimensions::Four => 0b1111_1111,
        };

        NeighborsBitsetIterator {
            base: self.d,
            submask: base_mask,
            base_mask,
        }
    }
}

fn life_step(cells: &HashSet<Point>, dimensions: Dimensions) -> HashSet<Point> {
    let mut next_generation = HashSet::new();
    let mut inactive_neighbors: HashMap<Point, usize> = HashMap::new();

    for cube in cells {
        let mut active_neighbors = 0usize;

        for neighbor in cube.neighbors(dimensions) {
            if cells.contains(&neighbor) {
                active_neighbors += 1;
            } else {
                *inactive_neighbors.entry(neighbor).or_insert(0) += 1;
            }
        }

        if active_neighbors == 2 || active_neighbors == 3 {
            next_generation.insert(cube.clone());
        }
    }

    for (cube, count) in inactive_neighbors {
        if count == 3 {
            next_generation.insert(cube.clone());
        }
    }

    next_generation
}

fn generator(input: &str) -> HashSet<Point> {
    input
        .lines()
        .enumerate()
        .flat_map(|(y, line)| {
            line.bytes()
                .enumerate()
                .filter_map(move |(x, cell)| match cell {
                    b'#' => Some(Point {
                        d: [x as i64, y as i64, 0, 0],
                    }),
                    _ => None,
                })
        })
        .collect()
}

#[aoc(day17, part1)]
fn part01(input: &str) -> usize {
    let mut cells = generator(input);

    for _ in 0..6 {
        cells = life_step(&cells, Dimensions::Three);
    }

    cells.len()
}

#[aoc(day17, part2)]
fn part02(input: &str) -> usize {
    let mut cells = generator(input);

    for _ in 0..6 {
        cells = life_step(&cells, Dimensions::Four);
    }

    cells.len()
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r".#.
..#
###";

    #[test]
    fn test1701() {
        let res = super::part01(BUILD_INPUT);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test1702() {
        let res = super::part02(BUILD_INPUT);

        println!("-- {:?} --", res)
    }
}
