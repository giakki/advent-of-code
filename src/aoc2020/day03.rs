/**
 * Correct results:
 * - 223
 * - 3517401300
 */
use crate::common::errors::AocError;
use crate::common::map::Map;
use crate::common::{chr::FromChar, point::PointOps};
use std::str::FromStr;

#[derive(Clone, Copy, PartialEq)]
enum Tile {
    Snow,
    Tree,
}

impl FromChar for Tile {
    type Err = AocError;

    fn from_char(c: &char) -> Result<Self, Self::Err> {
        match c {
            '.' => Ok(Self::Snow),
            '#' => Ok(Self::Tree),
            _ => Err(AocError::ParseError(format!("Unknown char: {c}"))),
        }
    }
}

#[aoc_generator(day3)]
fn generator(input: &str) -> Result<Map<Tile>, AocError> {
    Map::from_str(input)
}

fn walk_slope(map: &Map<Tile>, slope: (usize, usize)) -> usize {
    let mut pos = (0, 0);
    let mut count = 0;

    while pos.y() < map.height() {
        if map.at(pos) == &Tile::Tree {
            count += 1;
        }
        pos.set_x((pos.x() + slope.0) % map.width());
        pos.set_y(pos.y() + slope.1);
    }

    count
}

#[aoc(day3, part1)]
fn part01(input: &Map<Tile>) -> usize {
    walk_slope(input, (3, 1))
}

#[aoc(day3, part2)]
fn part02(input: &Map<Tile>) -> usize {
    let slopes = vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];

    slopes
        .into_iter()
        .map(|slope| walk_slope(input, slope))
        .product()
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r"..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#";

    #[test]
    fn test0301() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::part01(&input);

        println!("-- {} --", res)
    }

    #[test]
    fn test0302() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::part02(&input);

        println!("-- {} --", res)
    }
}
