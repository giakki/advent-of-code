/** Correct results:
* - 179
* - 18925
*/
use crate::common::parsers::parse_int;
use crate::common::{errors::AocError, parsers::input_parser};
use lazy_static::lazy_static;
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{alpha1, char},
    combinator::{map, map_res, value},
    multi::separated_list0,
    sequence::{delimited, preceded, terminated, tuple},
    IResult,
};
use std::collections::{HashMap, HashSet};
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::Mutex;

type Color<'a> = (&'a str, &'a str);
type Contained<'a> = (usize, Color<'a>);
type Children = Vec<(usize, usize)>;

lazy_static! {
    static ref COUNTER: AtomicUsize = AtomicUsize::new(1);
    static ref COLOR_MAP: Mutex<HashMap<String, usize>> = {
        let mut m = HashMap::new();
        m.insert("shinygold".to_owned(), 0);
        Mutex::new(m)
    };
}

fn color_code(name: String) -> Result<usize, AocError> {
    let mut lock = COLOR_MAP.try_lock();
    lock.as_mut().map_or(Err(AocError::Unspecified), |mutex| {
        Ok(*mutex
            .entry(name)
            .or_insert_with(|| COUNTER.fetch_add(1, Ordering::SeqCst)))
    })
}

fn parse_color(input: &str) -> IResult<&str, Color> {
    tuple((terminated(alpha1, char(' ')), alpha1))(input)
}

fn parse_contains(input: &str) -> IResult<&str, Option<Contained<'_>>> {
    let middle = || preceded(char(' '), parse_color);

    alt((
        map(
            terminated(tuple((value(1, char('1')), middle())), tag(" bag")),
            Option::Some,
        ),
        map(
            terminated(tuple((parse_int, middle())), tag(" bags")),
            Option::Some,
        ),
        value(Option::None, tag("no other bags")),
    ))(input)
}

fn parse_line(input: &str) -> IResult<&str, (usize, Children)> {
    map_res(
        tuple((
            parse_color,
            delimited(
                tag(" bags contain "),
                separated_list0(tag(", "), parse_contains),
                char('.'),
            ),
        )),
        |(p, cs)| -> Result<(usize, Children), AocError> {
            let parent = color_code(p.0.to_owned() + p.1).map_err(|_| AocError::Unspecified)?;
            let children = cs
                .iter()
                .filter_map(|z| {
                    if let Some((c0, c1)) = z {
                        color_code(c1.0.to_owned() + c1.1)
                            .map_or(Some(Err(AocError::Unspecified)), |code| {
                                Some(Ok((*c0, code)))
                            })
                    } else {
                        None
                    }
                })
                .collect::<Result<Children, AocError>>()?;

            Ok((parent, children))
        },
    )(input)
}

#[aoc_generator(day07)]
fn generator(input: &str) -> Result<Vec<(usize, Children)>, AocError> {
    input_parser(separated_list0(char('\n'), parse_line))(input)
}

#[aoc(day07, part1)]
fn part01(input: &[(usize, Children)]) -> usize {
    let mut map: HashMap<usize, Vec<usize>> = HashMap::new();

    for (parent, children) in input {
        for child in children {
            let parents = map.entry(child.1).or_default();
            parents.push(*parent);
        }
    }

    let mut stack = vec![0];
    let mut solutions = HashSet::new();

    while let Some(color) = stack.pop() {
        if let Some(parents) = map.get(&color) {
            for parent in parents {
                if !solutions.contains(parent) {
                    solutions.insert(parent);
                    stack.push(*parent);
                }
            }
        }
    }

    solutions.len()
}

#[aoc(day07, part2)]
fn part02(input: &[(usize, Children)]) -> usize {
    let mut map: HashMap<usize, Children> = HashMap::new();

    for (parent, children) in input {
        let parent = map.entry(*parent).or_default();
        parent.extend(children.clone());
    }

    let mut stack = vec![(0, 1, 1)];
    let mut solution = 0;

    while let Some((color, count, mul)) = stack.pop() {
        solution += count * mul;

        if let Some(children) = map.get(&color) {
            for child in children {
                stack.push((child.1, child.0, count * mul));
            }
        }
    }

    solution - 1
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT_1: &str = r"light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.";

    const BUILD_INPUT_2: &str = r"shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.";

    #[test]
    fn test07_gen() {
        let res = super::generator(BUILD_INPUT_1);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test0701() {
        let input = super::generator(BUILD_INPUT_1).unwrap();
        let res = super::part01(&input);

        println!("-- {} --", res)
    }

    #[test]
    fn test0702() {
        let input = super::generator(BUILD_INPUT_2).unwrap();
        let res = super::part02(&input);

        println!("-- {} --", res)
    }
}
