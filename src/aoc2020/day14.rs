/** Correct results:
 * - 15514035145260
 * - 3926790061594
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser},
};
use itertools::Itertools;
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{char, digit1},
    combinator::{map, value},
    multi::many0,
    sequence::{delimited, preceded, separated_pair},
    IResult,
};
use std::collections::HashMap;

#[derive(Debug, Clone, Copy)]
enum Qubit {
    Zero,
    One,
    Floating,
}

#[derive(Debug, Default, Clone, Copy)]
struct Mask {
    zeros: u64,
    ones: u64,
}

impl Mask {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        map(
            preceded(
                tag("mask = "),
                many0(alt((
                    value(Qubit::Zero, char('0')),
                    value(Qubit::One, char('1')),
                    value(Qubit::Floating, char('X')),
                ))),
            ),
            |qubits| {
                let (zeros, ones) = qubits.into_iter().fold((0, 0), |(zeros, ones), qubit| {
                    let zeros = zeros << 1;
                    let ones = ones << 1;
                    match qubit {
                        Qubit::Floating => (zeros, ones + 1),
                        Qubit::One => (zeros + 1, ones + 1),
                        Qubit::Zero => (zeros, ones),
                    }
                });

                Self { zeros, ones }
            },
        )(s)
    }

    /**
     * Inefficient - just for demonstrating powerset
     */
    pub fn floating_addresses(&self, base_address: u64) -> impl Iterator<Item = u64> {
        // Floating bits are those that are neither ones nor zeros.
        let floating = self.zeros ^ self.ones;
        // Get the fixed bits of the address.
        let base_address = (base_address | self.ones) & !floating;

        (0..36)
            .filter_map(move |idx| {
                let bit = 1_u64 << idx;
                if floating & bit == 0 {
                    None
                } else {
                    Some(bit)
                }
            })
            .powerset()
            .map(move |mask_bits| {
                let mask = mask_bits
                    .into_iter()
                    .fold(0, |mask, mask_bit| mask | mask_bit);

                base_address | mask
            })
    }
}

#[derive(Debug)]
enum Command {
    SetMask(Mask),
    Write(u64, u64),
}

impl Command {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        alt((map(Mask::parse, Self::SetMask), Self::parse_store))(s)
    }

    fn parse_store(s: &str) -> IResult<&str, Self> {
        let (s, c) = separated_pair(
            delimited(tag("mem["), digit1, char(']')),
            tag(" = "),
            digit1,
        )(s)?;

        Ok((s, Self::Write(c.0.parse().unwrap(), c.1.parse().unwrap())))
    }
}

#[aoc_generator(day14)]
fn input_generator(input: &str) -> Result<Vec<Command>, AocError> {
    input_parser(by_lines(Command::parse))(input)
}

#[aoc(day14, part1)]
fn part1(input: &[Command]) -> u64 {
    let mut memory = HashMap::new();
    let mut current_mask = Mask::default();

    for command in input {
        match command {
            Command::SetMask(mask) => current_mask = *mask,
            Command::Write(address, value) => {
                memory.insert(address, (value | current_mask.zeros) & current_mask.ones);
            }
        }
    }
    memory.values().sum()
}

#[aoc(day14, part2)]
fn part2(input: &[Command]) -> u64 {
    let mut memory = HashMap::new();
    let mut current_mask = Mask::default();

    for command in input {
        match command {
            Command::SetMask(mask) => current_mask = *mask,
            Command::Write(address, value) => {
                for floating in current_mask.floating_addresses(*address) {
                    memory.insert(floating, *value);
                }
            }
        }
    }
    memory.values().copied().sum()
}
