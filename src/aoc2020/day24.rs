/** Correct results:
* - 282
* - 3445
*/
use crate::common::{errors::AocError, parsers::input_parser, point::Point};
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::char,
    combinator::value,
    multi::{many0, separated_list0},
    IResult,
};
use std::collections::HashMap;

const HEX_NEIGHBORS: &[Point<i32>] = &[(1, 0), (-1, 0), (0, 1), (0, -1), (1, -1), (-1, 1)];

#[derive(Clone, Copy, Debug)]
enum HexDirection {
    E,
    SE,
    SW,
    W,
    NW,
    NE,
}

impl HexDirection {
    const fn to_point(self) -> Point<i32> {
        match self {
            Self::E => (1, 0),
            Self::W => (-1, 0),
            Self::NE => (0, 1),
            Self::NW => (-1, 1),
            Self::SE => (1, -1),
            Self::SW => (0, -1),
        }
    }
}

fn parse_hex_direction(s: &str) -> IResult<&str, HexDirection> {
    alt((
        value(HexDirection::E, char('e')),
        value(HexDirection::W, char('w')),
        value(HexDirection::NE, tag("ne")),
        value(HexDirection::NW, tag("nw")),
        value(HexDirection::SE, tag("se")),
        value(HexDirection::SW, tag("sw")),
    ))(s)
}

#[aoc_generator(day24)]
fn generator(input: &str) -> Result<Vec<Vec<HexDirection>>, AocError> {
    input_parser(separated_list0(char('\n'), many0(parse_hex_direction)))(input)
}

fn walk(path: &[HexDirection]) -> Point<i32> {
    let mut pos = (0, 0);

    for dir in path {
        let diff = dir.to_point();
        pos.0 += diff.0;
        pos.1 += diff.1;
    }

    pos
}

fn life(black_tiles: &[Point<i32>]) -> Vec<Point<i32>> {
    black_tiles
        .iter()
        // Fo each black tile and its neighbors, collect its neighbors
        .fold(HashMap::new(), |mut neighbors, (x0, y0)| {
            neighbors.entry((*x0, *y0)).or_insert((0, true)).1 = true;

            for (x1, y1) in HEX_NEIGHBORS {
                neighbors.entry((x0 + x1, y0 + y1)).or_insert((0, false)).0 += 1;
            }

            neighbors
        })
        .into_iter()
        .filter_map(|(tile, (num_neighbors, is_black))| {
            #[allow(clippy::nonminimal_bool)]
            if (is_black && (num_neighbors == 1 || num_neighbors == 2))
                || (!is_black && num_neighbors == 2)
            {
                Some(tile)
            } else {
                None
            }
        })
        .collect()
}

#[aoc(day24, part1)]
fn part01(input: &[Vec<HexDirection>]) -> usize {
    let mut tiles = HashMap::new();

    for path in input {
        let p = tiles.entry(walk(path)).or_insert(false);
        *p = !*p;
    }

    tiles.values().filter(|v| **v).count()
}

#[aoc(day24, part2)]
fn part02(input: &[Vec<HexDirection>]) -> usize {
    let mut tiles = HashMap::new();

    for path in input {
        let p = tiles.entry(walk(path)).or_insert(false);
        *p = !*p;
    }

    let mut black_tiles = tiles
        .into_iter()
        .filter_map(|(k, v)| if v { Some(k) } else { None })
        .collect::<Vec<_>>();
    for _ in 0..100 {
        black_tiles = life(&black_tiles);
    }

    black_tiles.len()
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r"sesenwnenenewseeswwswswwnenewsewsw
neeenesenwnwwswnenewnwwsewnenwseswesw
seswneswswsenwwnwse
nwnwneseeswswnenewneswwnewseswneseene
swweswneswnenwsewnwneneseenw
eesenwseswswnenwswnwnwsewwnwsene
sewnenenenesenwsewnenwwwse
wenwwweseeeweswwwnwwe
wsweesenenewnwwnwsenewsenwwsesesenwne
neeswseenwwswnwswswnw
nenwswwsewswnenenewsenwsenwnesesenew
enewnwewneswsewnwswenweswnenwsenwsw
sweneswneswneneenwnewenewwneswswnese
swwesenesewenwneswnwwneseswwne
enesenwswwswneneswsenwnewswseenwsese
wnwnesenesenenwwnenwsewesewsesesew
nenewswnwewswnenesenwnesewesw
eneswnwswnwsenenwnwnwwseeswneewsenese
neswnwewnwnwseenwseesewsenwsweewe
wseweeenwnesenwwwswnew";

    #[test]
    fn test24_gen() {
        let input = super::generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test2401() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::part01(&input);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test2402() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::part02(&input);

        println!("-- {:?} --", res)
    }
}
