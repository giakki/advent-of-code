/** Correct results:
* - 828
* - 565
*/
use crate::common::errors::AocError;
use crate::common::{chr::FromChar, parsers::input_parser};
use itertools::Itertools;
use nom::{
    branch::alt,
    character::complete::char,
    combinator::map_res,
    multi::{many0, separated_list0},
};
use std::convert::TryFrom;

struct Seat {
    row: usize,
    col: usize,
}

impl Seat {
    pub const fn id(&self) -> usize {
        8 * self.row + self.col
    }
}

impl TryFrom<Vec<char>> for Seat {
    type Error = AocError;

    fn try_from(chars: Vec<char>) -> Result<Self, Self::Error> {
        let bspfied = chars
            .iter()
            .map(BspDir::from_char)
            .collect::<Result<Vec<_>, _>>()?;
        let (row_descriptor, col_descriptor) = (&bspfied[..7], &bspfied[7..]);

        Ok(Self {
            row: bsp(row_descriptor),
            col: bsp(col_descriptor),
        })
    }
}

#[derive(Debug)]
enum BspDir {
    Bottom,
    Top,
}

impl FromChar for BspDir {
    type Err = AocError;

    fn from_char(c: &char) -> Result<Self, Self::Err> {
        match c {
            'F' | 'L' => Ok(Self::Bottom),
            'B' | 'R' => Ok(Self::Top),
            _ => Err(AocError::ParseError(format!("Invalid BspDir: {c}"))),
        }
    }
}

fn bsp(chars: &[BspDir]) -> usize {
    let mut min = 0;
    let mut max = 1 << chars.len();

    for c in chars {
        let next_value = (min >> 1) + (max >> 1);
        match c {
            BspDir::Top => min = next_value,
            BspDir::Bottom => max = next_value,
        }
    }

    min
}

fn generator(input: &str) -> Result<Vec<Seat>, AocError> {
    input_parser(separated_list0(
        char('\n'),
        map_res(
            many0(alt((char('F'), char('L'), char('B'), char('R')))),
            Seat::try_from,
        ),
    ))(input)
}

#[aoc(day5, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    let lines = generator(input)?;
    lines.iter().map(Seat::id).max().ok_or(AocError::NoSolution)
}

#[aoc(day5, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    let lines = generator(input)?;

    let mut all_seats: Vec<usize> = lines.iter().map(Seat::id).collect();
    all_seats.sort_unstable();

    all_seats
        .iter()
        .tuple_windows()
        .find_map(|(a, b)| if *b - *a == 2 { Some(a + 1) } else { None })
        .ok_or(AocError::NoSolution)
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r"FBFBBFFRLR
BFFFBBFRRR
FFFBBBFRRR
BBFFBBFRLL";

    #[test]
    fn test0501() {
        let res = super::part01(BUILD_INPUT);

        println!("-- {:?} --", res)
    }

    #[test]
    fn test0502() {
        let res = super::part02(BUILD_INPUT);

        println!("-- {:?} --", res)
    }
}
