/** Correct results:
 * - 0
 * - 0
 */
use crate::common::errors::AocError;

#[aoc_generator(day0)]
fn generator(input: &str) -> Result<Vec<usize>, AocError> {
    Ok(Vec::new())
}

#[aoc(day0, part1)]
fn part01(input: &[usize]) -> usize {
    0
}

#[aoc(day0, part2)]
fn part02(input: &[usize]) -> usize {
    0
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"";

    #[test]
    fn test00_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test0001() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 0);
    }

    #[test]
    fn test0002() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, 0);
    }
}
