/** Correct results:
 * - 104439
 * - 701
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use lazy_static::lazy_static;
use ndarray::{s, Array2};
use nom::{
    bytes::streaming::tag,
    character::complete::char,
    combinator::map,
    sequence::{preceded, tuple},
    IResult,
};
use regex::Regex;
use std::collections::HashSet;
use std::str::FromStr;

#[derive(Debug, PartialEq)]
struct Claim {
    pub id: usize,
    pub left: usize,
    pub top: usize,
    pub width: usize,
    pub height: usize,
}

impl FromStr for Claim {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref PARSE_REGEX: Regex =
                Regex::new(r"#(\d+) @ (\d+),(\d+): (\d+)x(\d+)").unwrap();
        }

        let matches = PARSE_REGEX.captures(s).ok_or("No match")?;

        Ok(Self {
            id: matches[1].parse::<usize>()?,
            left: matches[2].parse::<usize>()?,
            top: matches[3].parse::<usize>()?,
            width: matches[4].parse::<usize>()?,
            height: matches[5].parse::<usize>()?,
        })
    }
}

fn parse_line(s: &str) -> IResult<&str, Claim> {
    map(
        tuple((
            preceded(char('#'), parse_int),
            preceded(tag(" @ "), parse_int),
            preceded(char(','), parse_int),
            preceded(tag(": "), parse_int),
            preceded(char('x'), parse_int),
        )),
        |ts| Claim {
            id: ts.0,
            left: ts.1,
            top: ts.2,
            width: ts.3,
            height: ts.4,
        },
    )(s)
}

#[aoc_generator(day3)]
fn generator(input: &str) -> Result<Vec<Claim>, AocError> {
    input_parser(by_lines(parse_line))(input)
}

#[aoc(day3, part1)]
fn part01(input: &[Claim]) -> usize {
    let mut matrix = Array2::<u8>::zeros((1000, 1000));

    for claim in input {
        let start_row = claim.top;
        let end_row = start_row + claim.height;
        let start_col = claim.left;
        let end_col = start_col + claim.width;

        let slice = s![start_row..end_row, start_col..end_col];
        for cell in &mut matrix.slice_mut(slice) {
            *cell += 1;
        }
    }

    matrix.iter().filter(|cell| **cell > 1).count()
}

#[aoc(day3, part2)]
fn part02(input: &[Claim]) -> String {
    let mut matrix = Array2::<usize>::zeros((1000, 1000));
    let mut solutions: HashSet<usize> = input.iter().map(|claim| claim.id).collect();

    for claim in input {
        let start_row = claim.top;
        let end_row = start_row + claim.height;
        let start_col = claim.left;
        let end_col = start_col + claim.width;

        let slice = s![start_row..end_row, start_col..end_col];
        for cell in &mut matrix.slice_mut(slice) {
            if *cell == 0 {
                *cell = claim.id;
            } else {
                solutions.remove(&claim.id);
                solutions.remove(cell);
            }
        }
    }

    assert!(solutions.len() == 1);

    solutions
        .iter()
        .next()
        .map_or_else(|| panic!("No solution."), ToString::to_string)
}
