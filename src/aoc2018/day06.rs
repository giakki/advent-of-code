/** Correct results:
 * - 3989
 * - 49715
 */
use crate::common::errors::AocError;
use std::cmp;
use std::collections::HashSet;
use std::str;

#[aoc_generator(day6)]
fn generator(input: &str) -> Result<Vec<(i64, i64)>, AocError> {
    input
        .lines()
        .map(|line| {
            let chars = line.as_bytes();
            let comma = chars
                .iter()
                .position(|&c| c == b',')
                .ok_or_else(|| AocError::ParseError(format!("Missing comma: {line}")))?;

            let a = str::from_utf8(&chars[0..comma])?.parse::<i64>()?;
            let b = str::from_utf8(&chars[(comma + 2)..])?.parse::<i64>()?;

            Ok((a, b))
        })
        .collect::<Result<Vec<_>, AocError>>()
}

#[aoc(day6, part1)]
fn part01(input: &[(i64, i64)]) -> Result<usize, AocError> {
    let max_indexes = input.iter().fold((0, 0), |(acc_x, acc_y), (x, y)| {
        (cmp::max(acc_x, *x), cmp::max(acc_y, *y))
    });

    let width = (max_indexes.0) + 1;
    let height = (max_indexes.1) + 1;
    let mut sums = vec![0; input.len() + 1];
    let mut infinites = HashSet::new();
    infinites.insert(0);

    for y in 0..height {
        for x in 0..width {
            let mut min_distance = (width * height) + 1;
            let mut min_point = 0;

            for (i, point) in input.iter().enumerate() {
                let distance = (point.0 - x).abs() + (point.1 - y).abs();
                if distance == min_distance {
                    min_point = 0;
                }
                if distance < min_distance {
                    min_point = i + 1;
                    min_distance = distance;
                }
            }

            if y == 0 || y == height - 1 || x == 0 || x == width - 1 {
                infinites.insert(min_point);
            }

            sums[min_point] += 1;
        }
    }

    sums.iter()
        .enumerate()
        .filter_map(|(i, sum)| {
            if infinites.contains(&i) {
                None
            } else {
                Some(*sum)
            }
        })
        .max()
        .ok_or(AocError::NoSolution)
}

#[aoc(day6, part2)]
fn part02(input: &[(i64, i64)]) -> i64 {
    let max_indexes = input.iter().fold((0, 0), |(acc_x, acc_y), (x, y)| {
        (cmp::max(acc_x, *x), cmp::max(acc_y, *y))
    });

    let width = (max_indexes.0) + 1;
    let height = (max_indexes.1) + 1;

    let mut sum = 0_i64;
    for y in 0..height {
        for x in 0..width {
            let total_distance = input
                .iter()
                .map(|point| (point.0 - x).abs() + (point.1 - y).abs())
                .sum::<i64>();

            if total_distance < 10000 {
                sum += 1;
            }
        }
    }

    sum
}
