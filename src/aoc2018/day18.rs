/**
 *
 */
use ndarray::{s, Array2};
use std::collections::VecDeque;
use std::error::Error;
use std::fmt;

#[derive(Clone, Debug, Eq, PartialEq)]
enum Tile {
    Empty,
    Forest,
    Lumberyard,
}

impl Tile {
    fn try_from(byte: u8) -> Result<Self, Box<dyn Error>> {
        match byte {
            b'.' => Ok(Self::Empty),
            b'|' => Ok(Self::Forest),
            b'#' => Ok(Self::Lumberyard),
            _ => Err(Box::from("No parse.")),
        }
    }
}

impl fmt::Display for Tile {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let out = match self {
            Self::Empty => ".",
            Self::Forest => "|",
            Self::Lumberyard => "#",
        };

        write!(f, "{out}")
    }
}

type Grid = Array2<Tile>;

const INPUT_SIZE: usize = 50;
const GRID_SIZE: usize = INPUT_SIZE + 2;

fn tick(grid: &Grid) -> Grid {
    let mut next = grid.to_owned();

    for y in 1..GRID_SIZE - 1 {
        for x in 1..GRID_SIZE - 1 {
            let tile = &grid[(x, y)];
            let window = grid.slice(s![x - 1..=x + 1, y - 1..=y + 1]);

            match tile {
                Tile::Empty => {
                    if window
                        .into_iter()
                        .filter(|cell| **cell == Tile::Forest)
                        .count()
                        >= 3
                    {
                        next[(x, y)] = Tile::Forest;
                    }
                }
                Tile::Forest => {
                    if window
                        .into_iter()
                        .filter(|cell| **cell == Tile::Lumberyard)
                        .count()
                        >= 3
                    {
                        next[(x, y)] = Tile::Lumberyard;
                    }
                }
                Tile::Lumberyard => {
                    if window
                        .into_iter()
                        .filter(|cell| **cell == Tile::Lumberyard)
                        .count()
                        == 1
                        || window.iter().filter(|cell| **cell == Tile::Forest).count() == 0
                    {
                        next[(x, y)] = Tile::Empty;
                    }
                }
            }
        }
    }

    next
}

fn solution(grid: &Grid) -> usize {
    let (wooded, lumberyards) = grid.into_iter().fold((0, 0), |acc, next| match next {
        Tile::Empty => acc,
        Tile::Forest => (acc.0 + 1, acc.1),
        Tile::Lumberyard => (acc.0, acc.1 + 1),
    });

    wooded * lumberyards
}

#[aoc_generator(day18)]
fn generator(input: &str) -> Result<Grid, Box<dyn Error>> {
    let shape = (GRID_SIZE, GRID_SIZE);
    let mut grid = Grid::from_elem(shape, Tile::Empty);

    let mut view = grid.slice_mut(s![1..GRID_SIZE, 1..GRID_SIZE]);

    for (y, line) in input.lines().enumerate() {
        for (x, byte) in line.as_bytes().iter().enumerate() {
            view[(x, y)] = Tile::try_from(*byte)?;
        }
    }

    Ok(grid)
}

#[aoc(day18, part1)]
fn part01(input: &Grid) -> usize {
    let mut grid = input.to_owned();

    for _ in 0..10 {
        grid = tick(&grid);
    }

    solution(&grid)
}

#[aoc(day18, part2)]
fn part02(input: &Grid) -> usize {
    let mut grid = input.to_owned();
    let mut last_200 = VecDeque::with_capacity(200);

    while last_200.len() < 200 {
        grid = tick(&grid);
        last_200.push_back(solution(&grid));
    }

    let mut period_heuristic = (0, 0);
    for n in 200..1_000_000_000 {
        let has_repetitions = last_200
            .iter()
            .skip(1)
            .take(100)
            .position(|n| *n == last_200[0])
            .and_then(|pos| {
                let mut i0 = 0;
                let mut i1 = pos + 1;
                while i1 < last_200.len() {
                    if last_200[i0] != last_200[i1] {
                        return None;
                    }
                    i0 += 1;
                    i1 += 1;
                }

                Some(pos + 1)
            });

        if let Some(period) = has_repetitions {
            period_heuristic = (period, n);
            break;
        }

        grid = tick(&grid);
        last_200.pop_front();
        last_200.push_back(solution(&grid));
    }

    let (period, mut generation) = period_heuristic;
    let npos = 1_000_000_000 % period;
    while generation % period != npos {
        grid = tick(&grid);
        generation += 1;
    }

    solution(&grid)
}
