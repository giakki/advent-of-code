/** Correct results:
 * - 5478
 * - qyzphxoiseldjrntfygvdmanu
 */
#[aoc(day2, part1)]
fn part01(input: &str) -> u32 {
    let mut twos = 0;
    let mut threes = 0;

    for line in input.lines() {
        let mut bytes = Vec::from(line.as_bytes());
        bytes.sort_unstable();

        let mut buckets = [0; 26];
        for byte in bytes {
            buckets[(byte - b'a') as usize] += 1;
        }

        if buckets.iter().any(|&n| n == 2) {
            twos += 1;
        }

        if buckets.iter().any(|&n| n == 3) {
            threes += 1;
        }
    }

    twos * threes
}

#[aoc(day2, part2)]
fn part02(input: &str) -> String {
    for (i, a) in input.lines().enumerate() {
        for b in input.lines().skip(i + 1) {
            let different_characters = a
                .as_bytes()
                .iter()
                .zip(b.as_bytes().iter())
                .filter(|(c1, c2)| c1 != c2)
                .count();

            if different_characters == 1 {
                return a
                    .chars()
                    .zip(b.chars())
                    .filter_map(|(c1, c2)| if c1 == c2 { Some(c1) } else { None })
                    .collect();
            }
        }
    }

    panic!("No solution.")
}
