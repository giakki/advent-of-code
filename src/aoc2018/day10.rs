extern crate ndarray;
extern crate open;
extern crate regex;

use ndarray::Array;
use regex::Regex;
use std::fs::OpenOptions;
use std::io::Write;

#[derive(Clone, Debug)]
struct Point {
    pub p_x: i32,
    pub p_y: i32,
    pub v_x: i32,
    pub v_y: i32,
}

#[aoc_generator(day10)]
fn generator(input: &str) -> Result<Vec<Point>, Box<dyn std::error::Error>> {
    let regex =
        Regex::new(r"position=< *([\-\d]+), *([\-\d]+)> velocity=< *([\-\d]+), *([\-\d]+)>")?;

    input
        .lines()
        .map(|line| {
            let captures = regex.captures(line).ok_or("")?;

            let p_x = captures[1].parse::<i32>()?;
            let p_y = captures[2].parse::<i32>()?;
            let v_x = captures[3].parse::<i32>()?;
            let v_y = captures[4].parse::<i32>()?;

            Ok(Point { p_x, p_y, v_x, v_y })
        })
        .collect::<Result<Vec<Point>, Box<dyn std::error::Error>>>()
}

fn get_bounding_box_area(points: &[Point]) -> (usize, usize) {
    let (min_x, min_y, max_x, max_y) = get_min_max(points);

    let width = max_x - min_x + 1;
    let height = max_y - min_y + 1;

    (width as usize, height as usize)
}

fn get_min_max(points: &[Point]) -> (i32, i32, i32, i32) {
    points.iter().fold(
        (i32::max_value(), i32::max_value(), 0, 0),
        |(min_x, min_y, max_x, max_y), point| {
            (
                std::cmp::min(min_x, point.p_x),
                std::cmp::min(min_y, point.p_y),
                std::cmp::max(max_x, point.p_x),
                std::cmp::max(max_y, point.p_y),
            )
        },
    )
}

fn show_last_state(points: &[Point]) -> Result<String, Box<dyn std::error::Error>> {
    let (min_x, min_y, _, _) = get_min_max(points);
    let (width, height) = get_bounding_box_area(points);
    let mut text = Array::from_elem((height, width), '0');

    println!("({min_x}, {min_y}): {width}x{height}");

    for point in points {
        let x = (point.p_x - min_x) as usize;
        let y = (point.p_y - min_y) as usize;
        text[(y, x)] = '1';
    }

    let mut image = String::with_capacity(width * height * 2);
    image.push_str(&format!("P1\n{width} {height}\n"));
    for row in text.outer_iter() {
        for character in row {
            image.push_str(&format!("{character}"));
        }
        image += "\n";
    }

    let mut dir = std::env::temp_dir();
    dir.push("day10.pbm");
    let mut file = OpenOptions::new().create(true).write(true).open(&dir)?;
    file.write_all(image.as_bytes())?;

    open::that(dir)?;

    Ok(image)
}

#[aoc(day10, part1)]
fn part01(points: &[Point]) -> Result<u32, Box<dyn std::error::Error>> {
    let (min_x, min_y, _, _) = get_min_max(points);
    let mut last_state: Vec<Point> = points
        .iter()
        .map(|point| Point {
            p_x: point.p_x + min_x.abs(),
            p_y: point.p_y + min_y.abs(),
            v_x: point.v_x,
            v_y: point.v_y,
        })
        .collect();

    let mut last_area = {
        let (width, height) = get_bounding_box_area(&last_state);
        width * height
    };

    loop {
        let next_points: Vec<Point> = last_state
            .iter()
            .map(|p| Point {
                p_x: p.p_x + p.v_x,
                p_y: p.p_y + p.v_y,
                v_x: p.v_x,
                v_y: p.v_y,
            })
            .collect();

        let (width, height) = get_bounding_box_area(&next_points);
        if width * height < last_area {
            last_area = width * height;
            last_state = next_points;
        } else {
            show_last_state(&last_state)?;
            return Ok(0);
        }
    }
}

#[aoc(day10, part2)]
fn part02(points: &[Point]) -> u32 {
    let (min_x, min_y, _, _) = get_min_max(points);
    let mut last_state: Vec<Point> = points
        .iter()
        .map(|point| Point {
            p_x: point.p_x + min_x.abs(),
            p_y: point.p_y + min_y.abs(),
            v_x: point.v_x,
            v_y: point.v_y,
        })
        .collect();

    let mut last_area = {
        let (width, height) = get_bounding_box_area(&last_state);
        width * height
    };

    let mut i = 0;
    loop {
        let next_points: Vec<Point> = last_state
            .iter()
            .map(|p| Point {
                p_x: p.p_x + p.v_x,
                p_y: p.p_y + p.v_y,
                v_x: p.v_x,
                v_y: p.v_y,
            })
            .collect();

        let (width, height) = get_bounding_box_area(&next_points);
        if width * height < last_area {
            last_area = width * height;
            last_state = next_points;
            i += 1;
        } else {
            return i;
        }
    }
}
