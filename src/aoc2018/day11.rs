extern crate ndarray;
extern crate rayon;

use crate::common::errors::AocError;
use ndarray::s;
use ndarray::{Array, Dim};
use rayon::prelude::*;

const fn power_level(x: usize, y: usize, serial: usize) -> i8 {
    let rack_id = x + 10;
    let mut level = rack_id * y + serial;
    level *= rack_id;
    level = (level / 100) % 10;
    (level - 5) as i8
}

fn max_power_level(cells: &Array<i8, Dim<[usize; 2]>>, size: usize) -> (usize, usize, i8) {
    let mut max_sum = (0, 0, i8::min_value());
    for x in 0..(300 - size) {
        for y in 0..(300 - size) {
            let slice = s![x..x + size, y..y + size];
            let sum = cells.slice(slice).iter().sum();
            if sum > max_sum.2 {
                max_sum = (x, y, std::cmp::max(sum, max_sum.2));
            }
        }
    }

    (max_sum.0, max_sum.1, max_sum.2)
}

#[aoc(day11, part1)]
fn part01(input: &str) -> Result<String, AocError> {
    let serial = input.parse::<usize>()?;
    let cells = Array::from_shape_fn((300, 300), |(x, y)| power_level(x + 1, y + 1, serial));

    let solution = max_power_level(&cells, 3);
    Ok(format!("{},{}", solution.0 + 1, solution.1 + 1))
}

#[aoc(day11, part2)]
fn part02(input: &str) -> Result<String, AocError> {
    let serial = input.parse::<usize>()?;
    let cells = Array::from_shape_fn((300, 300), |(x, y)| power_level(x + 1, y + 1, serial));

    let solution = (1..300_usize)
        .into_par_iter()
        .map(|size| (max_power_level(&cells, size), size))
        .max_by_key(|((_, _, power), _)| *power)
        .map(|((x, y, _), size)| (x, y, size))
        .ok_or(AocError::NoSolution)?;

    Ok(format!("{},{},{}", solution.0, solution.1, solution.2))
}
