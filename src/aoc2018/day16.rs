/** Correct results:
 * - 614
 * - 656
 */
use crate::common::chr::FromAsciiDigit;
use crate::common::errors::AocError;
use crate::common::opcode::{Instruction, OpCode};
use std::collections::{HashMap, HashSet};

struct State {
    id: usize,
    instruction: Instruction,
    before: [usize; NUM_REGISTERS],
    after: [usize; NUM_REGISTERS],
}

const NUM_REGISTERS: usize = 4;

const ALL_OPCODES: [OpCode; 16] = [
    OpCode::Addi,
    OpCode::Addr,
    OpCode::Bani,
    OpCode::Banr,
    OpCode::Bori,
    OpCode::Borr,
    OpCode::Eqir,
    OpCode::Eqri,
    OpCode::Eqrr,
    OpCode::Gtir,
    OpCode::Gtri,
    OpCode::Gtrr,
    OpCode::Muli,
    OpCode::Mulr,
    OpCode::Seti,
    OpCode::Setr,
];

const A_OPCODES: [OpCode; 7] = [
    OpCode::Addi,
    OpCode::Bani,
    OpCode::Bori,
    OpCode::Eqri,
    OpCode::Gtri,
    OpCode::Muli,
    OpCode::Setr,
];
const B_OPCODES: [OpCode; 2] = [OpCode::Eqir, OpCode::Gtir];
const X_OPCODES: [OpCode; 6] = [
    OpCode::Addr,
    OpCode::Banr,
    OpCode::Borr,
    OpCode::Eqrr,
    OpCode::Gtrr,
    OpCode::Mulr,
];

fn get_possible_matches(instruction: &Instruction) -> Vec<Instruction> {
    let mut possibilities = HashSet::with_capacity(ALL_OPCODES.len());
    possibilities.insert(&OpCode::Seti);

    if instruction.a < NUM_REGISTERS {
        possibilities.extend(&A_OPCODES);
    }
    if instruction.b < NUM_REGISTERS {
        possibilities.extend(&B_OPCODES);
    }
    if instruction.a < NUM_REGISTERS && instruction.b < NUM_REGISTERS {
        possibilities.extend(&X_OPCODES);
    }

    possibilities
        .into_iter()
        .map(|opcode| Instruction {
            opcode: *opcode,
            a: instruction.a,
            b: instruction.b,
            c: instruction.c,
        })
        .collect()
}

fn find_opcodes(
    mut matches: HashMap<usize, HashSet<&OpCode>>,
) -> Result<HashMap<usize, OpCode>, AocError> {
    let mut results = HashMap::new();

    while results.len() < ALL_OPCODES.len() {
        let (k, v) = {
            let (k, v) = matches
                .iter()
                .find_map(|(k, v)| {
                    if v.len() == 1 {
                        v.iter().next().map(|values| (k, values))
                    } else {
                        None
                    }
                })
                .ok_or(AocError::Unspecified)?;
            results.insert(*k, **v);
            (*k, *v)
        };
        matches
            .values_mut()
            .for_each(|values| values.retain(|o| *o != v));
        matches.remove(&k);
    }

    Ok(results)
}

#[aoc_generator(day16)]
fn generator(input: &str) -> Result<(Vec<State>, Vec<[usize; 4]>), AocError> {
    let mut states = Vec::new();

    let mut iter = input.lines();
    loop {
        let before = iter.next().ok_or("No parse.")?.as_bytes();
        let arg_line = iter.next().ok_or("No parse.")?;

        if before.is_empty() && arg_line.is_empty() {
            break;
        }

        let after = iter.next().ok_or("No parse.")?.as_bytes();

        let arguments = arg_line.split(' ').collect::<Vec<_>>();

        states.push(State {
            before: [
                usize::from_ascii_digit(before[9]),
                usize::from_ascii_digit(before[12]),
                usize::from_ascii_digit(before[15]),
                usize::from_ascii_digit(before[18]),
            ],
            id: arguments[0].parse()?,
            instruction: Instruction {
                opcode: OpCode::Undefined,
                a: arguments[1].parse()?,
                b: arguments[2].parse()?,
                c: arguments[3].parse()?,
            },
            after: [
                usize::from_ascii_digit(after[9]),
                usize::from_ascii_digit(after[12]),
                usize::from_ascii_digit(after[15]),
                usize::from_ascii_digit(after[18]),
            ],
        });

        iter.next().ok_or("No parse.")?;
    }

    let mut instructions = Vec::new();
    for line in iter {
        let instruction = line.split(' ').collect::<Vec<_>>();
        instructions.push([
            instruction[0].parse()?,
            instruction[1].parse()?,
            instruction[2].parse()?,
            instruction[3].parse()?,
        ]);
    }

    Ok((states, instructions))
}

#[aoc(day16, part1)]
fn part01(input: &(Vec<State>, Vec<[usize; 4]>)) -> usize {
    let mut solution = 0;

    for state in &input.0 {
        let mut matches = 0;
        for instruction in get_possible_matches(&state.instruction) {
            let mut state_copy = state.before;
            instruction.run(&mut state_copy);
            if state_copy == state.after {
                matches += 1;
            }
        }

        if matches >= 3 {
            solution += 1;
        }
    }

    solution
}

#[aoc(day16, part2)]
fn part02(input: &(Vec<State>, Vec<[usize; 4]>)) -> Result<usize, AocError> {
    let mut opcode_matches = HashMap::new();
    for state in &input.0 {
        opcode_matches
            .entry(state.id)
            .and_modify(|set: &mut HashSet<&OpCode>| {
                set.retain(|opcode| {
                    get_possible_matches(&state.instruction)
                        .into_iter()
                        .filter(|i| {
                            let mut state_copy = state.before;
                            i.run(&mut state_copy);
                            state_copy == state.after
                        })
                        .any(|p| p.opcode == **opcode)
                });
            })
            .or_insert_with(|| ALL_OPCODES.iter().collect::<HashSet<_>>());
    }

    let opcodes = find_opcodes(opcode_matches)?;
    let instructions = input.1.iter().map(|i| Instruction {
        opcode: opcodes[&i[0]],
        a: i[1],
        b: i[2],
        c: i[3],
    });

    let mut state = [0, 0, 0, 0];
    for instruction in instructions {
        instruction.run(&mut state);
    }

    Ok(state[0])
}
