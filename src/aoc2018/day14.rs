/** Correct results:
 * - 1631191756
 * - 20219475
 */
use crate::common::{chr::FromAsciiDigit, errors::AocError};

#[aoc(day14, part1)]
fn part01(raw_input: &str) -> Result<String, AocError> {
    let input = raw_input.parse()?;
    let num_recipes = input + 11;
    let mut recipes = Vec::with_capacity(num_recipes);

    recipes.push(3);
    recipes.push(7);

    let mut elf_1 = 0;
    let mut elf_2 = 1;
    while recipes.len() < num_recipes {
        let sum = recipes[elf_1] + recipes[elf_2];
        if sum > 9 {
            recipes.push(sum / 10);
        }
        recipes.push(sum % 10);

        elf_1 = (1 + elf_1 + recipes[elf_1]) % recipes.len();
        elf_2 = (1 + elf_2 + recipes[elf_2]) % recipes.len();
    }

    Ok(recipes[input..input + 10]
        .iter()
        .map(ToString::to_string)
        .collect())
}

#[aoc(day14, part2)]
fn part02(raw_input: &str) -> Result<usize, AocError> {
    let input = raw_input.parse::<usize>()?;
    let num_recipes = input + 11;
    let mut recipes = Vec::with_capacity(num_recipes);

    recipes.push(3);
    recipes.push(7);

    let quest = raw_input
        .as_bytes()
        .iter()
        .map(|c| usize::from_ascii_digit(*c))
        .collect::<Vec<_>>();
    let mut elf_1 = 0;
    let mut elf_2 = 1;
    loop {
        let sum = recipes[elf_1] + recipes[elf_2];
        if sum > 9 {
            recipes.push(sum / 10);
        }

        if recipes.ends_with(&quest) {
            return Ok(recipes.len() - quest.len());
        }

        recipes.push(sum % 10);

        elf_1 = (1 + elf_1 + recipes[elf_1]) % recipes.len();
        elf_2 = (1 + elf_2 + recipes[elf_2]) % recipes.len();

        if recipes.ends_with(&quest) {
            return Ok(recipes.len() - quest.len());
        }
    }
}
