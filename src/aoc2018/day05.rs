/** Correct results:
 * - 9686
 * - 5524
 */
use crate::common::errors::AocError;

fn react<I>(polymers: I, size_hint: usize) -> usize
where
    I: Iterator<Item = char>,
{
    let mut solution: Vec<char> = Vec::with_capacity(size_hint);

    for character in polymers {
        if let Some(last) = solution.last().copied() {
            if last.to_ascii_lowercase() == character.to_ascii_lowercase() && last != character {
                solution.pop();
            } else {
                solution.push(character);
            }
        } else {
            solution.push(character);
        }
    }

    solution.len()
}

#[aoc(day5, part1)]
fn part01(input: &str) -> usize {
    react(input.chars(), input.len())
}

#[aoc(day5, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    let mut polymers = input
        .chars()
        .map(|c| c.to_ascii_lowercase())
        .collect::<Vec<_>>();
    polymers.sort_unstable();
    polymers.dedup();

    polymers
        .into_iter()
        .map(|polymer| {
            react(
                input.chars().filter(|c| c.to_ascii_lowercase() != polymer),
                input.len(),
            )
        })
        .min()
        .ok_or(AocError::NoSolution)
}
