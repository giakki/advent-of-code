/**
 *
 */
extern crate ndarray;

use crate::common::errors::AocError;
use ndarray::Array2;
use std::fmt;
use std::fmt::{Display, Formatter, Write};

#[derive(Clone, Copy, Debug, PartialEq)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    pub const fn clockwise(dir: Self) -> Self {
        match dir {
            Self::Up => Self::Right,
            Self::Right => Self::Down,
            Self::Down => Self::Left,
            Self::Left => Self::Up,
        }
    }
    pub const fn counterclockwise(dir: Self) -> Self {
        match dir {
            Self::Up => Self::Left,
            Self::Left => Self::Down,
            Self::Down => Self::Right,
            Self::Right => Self::Up,
        }
    }
}

#[derive(Debug)]
struct Cart {
    x: usize,
    y: usize,
    id: usize,
    direction: Direction,
    next_turn: Option<Direction>,
    crashed: bool,
}

impl Cart {
    pub fn crash(&mut self) {
        self.crashed = true;
    }

    pub const fn has_crashed(&self) -> bool {
        self.crashed
    }

    pub const fn id(&self) -> usize {
        self.id
    }

    pub const fn new(id: usize, x: usize, y: usize, direction: Direction) -> Self {
        Self {
            x,
            y,
            id,
            crashed: false,
            direction,
            next_turn: Some(Direction::Left),
        }
    }

    pub const fn pos(&self) -> (usize, usize) {
        (self.x, self.y)
    }

    pub fn tick(&mut self, map: &Array2<u8>) {
        if self.crashed {
            return;
        }

        let (dx, dy) = match self.direction {
            Direction::Up => (-1, 0),
            Direction::Down => (1, 0),
            Direction::Left => (0, -1),
            Direction::Right => (0, 1),
        };

        self.x = (self.x as i32 + dx) as usize;
        self.y = (self.y as i32 + dy) as usize;

        let underlying = map[(self.x, self.y)];
        if underlying == b'\\' || underlying == b'/' {
            self.turn(underlying);
        } else if underlying == b'+' {
            self.turn_intersection();
        }
    }

    pub fn try_from(id: usize, x: usize, y: usize, input: u8) -> Option<(Self, u8)> {
        let dir = match input {
            b'^' => Some(Direction::Up),
            b'v' => Some(Direction::Down),
            b'<' => Some(Direction::Left),
            b'>' => Some(Direction::Right),
            _ => None,
        };

        dir.map(|direction| {
            let underlying = if direction == Direction::Left || direction == Direction::Right {
                b'-'
            } else {
                b'|'
            };
            (Self::new(id, x, y, direction), underlying)
        })
    }

    fn turn(&mut self, dir: u8) {
        #[allow(clippy::nonminimal_bool)]
        if (self.direction == Direction::Up && dir == b'/')
            || (self.direction == Direction::Down && dir == b'/')
            || (self.direction == Direction::Left && dir == b'\\')
            || (self.direction == Direction::Right && dir == b'\\')
        {
            self.direction = Direction::clockwise(self.direction);
        } else {
            self.direction = Direction::counterclockwise(self.direction);
        }
    }

    fn turn_intersection(&mut self) {
        match self.next_turn {
            Some(Direction::Left) => {
                self.direction = Direction::counterclockwise(self.direction);
                self.next_turn = None;
            }
            Some(Direction::Right) => {
                self.direction = Direction::clockwise(self.direction);
                self.next_turn = Some(Direction::Left);
            }
            _ => {
                self.next_turn = Some(Direction::Right);
            }
        }
    }
}

impl Display for Cart {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        if self.crashed {
            return f.write_char('X');
        }

        let out = match self.direction {
            Direction::Down => 'v',
            Direction::Left => '<',
            Direction::Right => '>',
            Direction::Up => '^',
        };

        f.write_char(out)
    }
}

#[derive(Debug)]
struct Mine {
    carts: Vec<Cart>,
    map: Array2<u8>,
}

impl Mine {
    pub fn check_collisions(&mut self, collision_pos: (usize, usize)) -> bool {
        self.carts
            .iter()
            .filter(|cart| !cart.has_crashed() && cart.pos() == collision_pos)
            .count()
            > 1
    }

    pub fn count_carts(&self) -> usize {
        self.carts.iter().filter(|cart| !cart.has_crashed()).count()
    }

    pub fn next_cart_position(&self) -> Option<(usize, usize)> {
        self.carts
            .iter()
            .find(|cart| !cart.has_crashed())
            .map(Cart::pos)
    }

    fn move_cart(&mut self, id: usize) -> Option<(usize, usize)> {
        let new_pos = {
            if let Some(cart) = self.carts.iter_mut().find(|cart| cart.id() == id) {
                cart.tick(&self.map);
                Some(cart.pos())
            } else {
                None
            }
        };

        new_pos.and_then(|pos| {
            if self.check_collisions(pos) {
                self.carts
                    .iter_mut()
                    .filter(|cart| cart.pos() == pos)
                    .for_each(Cart::crash);
                Some(pos)
            } else {
                None
            }
        })
    }

    fn tick(&mut self) -> Option<Vec<(usize, usize)>> {
        self.carts.sort_by_key(Cart::pos);

        let cart_ids = self.carts.iter().map(Cart::id).collect::<Vec<_>>();
        let mut collisions: Option<Vec<(usize, usize)>> = None;
        for id in cart_ids {
            if let Some(collision) = self.move_cart(id) {
                if let Some(ref mut vec) = collisions {
                    vec.push(collision);
                } else {
                    collisions = Some(vec![collision]);
                }
            }
        }

        collisions
    }

    fn try_from(input: &str) -> Result<Self, AocError> {
        let height = input.lines().count();
        let width = input
            .lines()
            .map(str::len)
            .max()
            .ok_or(AocError::Unspecified)?;
        let tmp_map = input
            .lines()
            .map(|line| {
                line.as_bytes()
                    .iter()
                    .chain(std::iter::once(&b' ').cycle())
                    .take(width)
                    .copied()
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();
        let shape = (height, width);

        let mut carts = Vec::new();
        let map = Array2::<u8>::from_shape_fn(shape, |(x, y)| {
            if let Some((cart, underlying)) = Cart::try_from(carts.len(), x, y, tmp_map[x][y]) {
                carts.push(cart);
                underlying
            } else {
                tmp_map[x][y]
            }
        });

        Ok(Self { carts, map })
    }
}

impl Display for Mine {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let mut copy = self.map.to_owned();
        for cart in &self.carts {
            copy[(cart.x, cart.y)] = format!("{cart}").as_bytes()[0];
        }

        copy.outer_iter().try_for_each(|row| {
            writeln!(
                f,
                "{}",
                String::from_utf8_lossy(&row.iter().copied().collect::<Vec<_>>())
            )
        })
    }
}

#[aoc(day13, part1)]
fn part01(input: &str) -> Result<String, AocError> {
    let mut mine = Mine::try_from(input)?;

    loop {
        if let Some(collisions) = mine.tick() {
            return Ok(format!("{},{}", collisions[0].1, collisions[0].0));
        }
    }
}

#[aoc(day13, part2)]
fn part02(input: &str) -> Result<String, AocError> {
    let mut mine = Mine::try_from(input)?;

    while mine.count_carts() > 1 {
        mine.tick();
    }

    mine.next_cart_position()
        .ok_or(AocError::NoSolution)
        .map(|(x, y)| format!("{y},{x}"))
}
