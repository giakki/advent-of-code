use crate::common::errors::AocError;
use std::collections::VecDeque;

#[aoc_generator(day9)]
fn generator(input: &str) -> Result<Box<(usize, usize)>, Box<dyn std::error::Error>> {
    let num_players = input.split(' ').next().ok_or("")?.parse::<usize>()?;
    let num_marbles = input.split(' ').nth(6).ok_or("")?.parse::<usize>()?;

    Ok(Box::new((num_players, num_marbles)))
}

fn play(num_players: usize, num_marbles: usize) -> Result<usize, AocError> {
    let mut board = VecDeque::with_capacity(num_marbles);
    let mut players = vec![0; num_players];
    board.push_back(0);

    for marble in 1..=num_marbles {
        if marble % 23 == 0 {
            players[marble % num_players] += marble;
            for _ in 0..7 {
                let back = board.pop_back().ok_or(AocError::Unspecified)?;
                board.push_front(back);
            }
            players[marble % num_players] += board.pop_front().ok_or(AocError::Unspecified)?;
        } else {
            for _ in 0..2 {
                let front = board.pop_front().ok_or(AocError::Unspecified)?;
                board.push_back(front);
            }
            board.push_front(marble);
        }
    }

    Ok(*players.iter().max().ok_or(AocError::Unspecified)?)
}

#[aoc(day9, part1)]
fn part01(input: &(usize, usize)) -> Result<usize, AocError> {
    play(input.0, input.1)
}

#[aoc(day9, part2)]
fn part02(input: &(usize, usize)) -> Result<usize, AocError> {
    play(input.0, input.1 * 100)
}
