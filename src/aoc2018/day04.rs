/** Correct results:
 * - 8421
 * - 83359
 */
use crate::common::errors::AocError;
use std::collections::HashMap;

#[derive(Debug)]
enum Command {
    Switch(usize),
    Start(usize),
    End(usize),
}

#[derive(Clone)]
struct GuardInfo {
    pub id: usize,
    pub schedule: [usize; 60],
    pub total_minutes: usize,
}

impl GuardInfo {
    pub const fn default(id: usize) -> Self {
        Self {
            id,
            schedule: [0; 60],
            total_minutes: 0,
        }
    }
}

fn spy_on_guards(input: &str) -> Result<HashMap<usize, GuardInfo>, AocError> {
    let mut lines = input.lines().collect::<Vec<_>>();
    lines.sort_unstable();

    let commands = lines.iter().map(|line| {
        let min_cmd = &line[15..17];
        let str_cmd = &line[25..];

        if str_cmd.starts_with('#') {
            str_cmd
                .chars()
                .skip(1)
                .take_while(|c| *c >= '0' && *c <= '9')
                .collect::<String>()
                .parse::<usize>()
                .map(Command::Switch)
        } else if str_cmd.starts_with('a') {
            min_cmd.parse::<usize>().map(Command::Start)
        } else {
            min_cmd.parse::<usize>().map(Command::End)
        }
    });

    let mut guards: HashMap<usize, GuardInfo> = HashMap::new();
    let mut current_guard_id = 0;
    let mut current_start: usize = 0;

    for command in commands {
        match command? {
            Command::End(end) => {
                if let Some(current_guard) = guards.get_mut(&current_guard_id) {
                    let minutes_asleep = end - current_start;
                    for minute in &mut current_guard.schedule[current_start..end] {
                        *minute += 1;
                    }

                    current_guard.total_minutes += minutes_asleep;
                    current_start = 0;
                } else {
                    return Err(AocError::Generic(format!(
                        "Cannot get guard {current_guard_id}"
                    )));
                }
            }
            Command::Start(start) => {
                current_start = start;
            }
            Command::Switch(id) => {
                current_guard_id = guards
                    .entry(id)
                    .or_insert_with(|| GuardInfo::default(id))
                    .id;
            }
        }
    }

    Ok(guards)
}

#[aoc(day4, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    let guards = spy_on_guards(input)?;

    let most_asleep_guard = guards
        .values()
        .max_by(|a, b| a.total_minutes.cmp(&b.total_minutes))
        .ok_or(AocError::NoSolution)?;

    let most_asleep_minute = most_asleep_guard
        .schedule
        .iter()
        .enumerate()
        .max_by(|a, b| a.1.cmp(b.1))
        .ok_or(AocError::NoSolution)?
        .0;

    Ok(most_asleep_guard.id * most_asleep_minute)
}

#[aoc(day4, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    let guards = spy_on_guards(input)?;

    let most_asleep_guard = guards
        .values()
        .max_by(|a, b| {
            a.schedule
                .iter()
                .max()
                //TODO: Unpanic
                .unwrap()
                .cmp(b.schedule.iter().max().unwrap())
        })
        .ok_or(AocError::NoSolution)?;

    let most_asleep_minute = most_asleep_guard
        .schedule
        .iter()
        .enumerate()
        .max_by(|a, b| a.1.cmp(b.1))
        .ok_or(AocError::NoSolution)?
        .0;

    Ok(most_asleep_guard.id * most_asleep_minute)
}
