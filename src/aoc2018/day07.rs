/** Correct results:
 * - GKPTSLUXBIJMNCADFOVHEWYQRZ
 * - 920
 */
use std::collections::{HashMap, HashSet};
use std::error::Error;
use std::str::FromStr;

/// A map from step to all of its required dependency steps. The set of
/// required dependency sets may be empty.
type RequiredFor = HashMap<Step, HashSet<Step>>;

type Step = char;

#[aoc_generator(day7)]
fn generator(input: &str) -> Result<Box<RequiredFor>, Box<dyn Error>> {
    let mut deps: Vec<Dependency> = vec![];
    for line in input.lines() {
        let dep = line.parse()?;
        deps.push(dep);
    }

    let mut required_for: RequiredFor = HashMap::new();
    for dep in deps {
        required_for
            .entry(dep.step)
            .or_default()
            .insert(dep.required);
        required_for.entry(dep.required).or_default();
    }

    Ok(Box::new(required_for))
}

#[aoc(day7, part1)]
fn part01(required_for: &RequiredFor) -> String {
    let mut taken: HashSet<Step> = HashSet::new();
    let mut order: Vec<Step> = vec![];
    let mut next: Vec<Step> = vec![];
    loop {
        find_next_steps(required_for, &taken, &taken, &mut next);
        let next_step = match next.pop() {
            None => break,
            Some(next_step) => next_step,
        };
        taken.insert(next_step);
        order.push(next_step);
    }

    order.iter().copied().rev().collect()
}

#[aoc(day7, part2)]
fn part02(required_for: &RequiredFor) -> i32 {
    let mut workers = Workers::new(5);
    let mut assigned: HashSet<Step> = HashSet::new();
    let mut done: HashSet<Step> = HashSet::new();
    let mut order: Vec<Step> = vec![];
    let mut next: Vec<Step> = vec![];

    let mut seconds = 0;
    loop {
        workers.run_one_step(&mut order, &mut done);

        find_next_steps(required_for, &assigned, &done, &mut next);
        if next.is_empty() && workers.all_idle() {
            break;
        }
        for worker in workers.available() {
            let next_step = match next.pop() {
                None => break,
                Some(next_step) => next_step,
            };
            assigned.insert(next_step);
            workers.work_on(worker, next_step);
        }
        seconds += 1;
    }

    seconds
}

/// Populate `next_stack` with next steps such that the steps are sorted in
/// reverse lexicographically with no duplicates.
///
/// Steps in `taken` are never added to the stack.
///
/// Steps in `done` signify which steps have already been completed. Only steps
/// with all dependencies completed will be put on to the stack.
fn find_next_steps(
    required_for: &RequiredFor,
    taken: &HashSet<Step>,
    done: &HashSet<Step>,
    next_stack: &mut Vec<Step>,
) {
    for (&step, dependencies) in required_for {
        if taken.contains(&step) {
            continue;
        }
        if dependencies.iter().all(|s| done.contains(s)) {
            next_stack.push(step);
        }
    }
    next_stack.sort_unstable();
    next_stack.dedup();
}

/// Workers manages the simulation of a fixed size worker pool. This tracks
/// the status of each worker, whether idle or active. When active, we record
/// how much and what work remains until that worker is idle again.
#[derive(Debug)]
struct Workers {
    status: Vec<Status>,
}

type WorkerID = usize;

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum Status {
    Idle,
    Working { step: Step, remaining: u32 },
}

impl Workers {
    fn new(count: usize) -> Self {
        Self {
            status: vec![Status::Idle; count],
        }
    }

    fn available(&self) -> Vec<WorkerID> {
        let mut available = vec![];
        for (worker, &status) in self.status.iter().enumerate() {
            if status == Status::Idle {
                available.push(worker);
            }
        }
        available
    }

    fn all_idle(&self) -> bool {
        self.status.iter().all(|s| *s == Status::Idle)
    }

    fn work_on(&mut self, worker: WorkerID, step: Step) {
        let status = &mut self.status[worker];
        assert!(*status == Status::Idle, "worker {worker} is not available");

        let remaining = (step as u32) - u32::from(b'A') + 1 + 60;
        *status = Status::Working { step, remaining }
    }

    /// Run one step in the simulation. Workers that have finished their work
    /// are transitioned to idle status.
    fn run_one_step(&mut self, order: &mut Vec<Step>, done: &mut HashSet<Step>) {
        for worker in 0..self.status.len() {
            let mut is_done = false;
            match self.status[worker] {
                Status::Idle => {}
                Status::Working {
                    step,
                    ref mut remaining,
                } => {
                    *remaining -= 1;
                    if *remaining == 0 {
                        is_done = true;
                        order.push(step);
                        done.insert(step);
                    }
                }
            }
            if is_done {
                self.status[worker] = Status::Idle;
            }
        }
    }
}

#[derive(Clone, Copy, Debug)]
struct Dependency {
    step: Step,
    required: Step,
}

impl FromStr for Dependency {
    type Err = Box<dyn Error>;

    fn from_str(s: &str) -> Result<Self, Box<dyn Error>> {
        let bytes = s.as_bytes();

        Ok(Self {
            step: bytes[5] as Step,
            required: bytes[36] as Step,
        })
    }
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r"Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin.";

    #[test]
    fn test0701() {
        let input = super::generator(BUILD_INPUT).unwrap();
        let res = super::part01(&input);

        println!("{:?}", res);
    }
}
