#![allow(dead_code)]
use crate::common::point::Point;
use crate::common::{errors::AocError, point::PointOps};
use ndarray::Array2;
use std::cmp;
use std::collections::{BinaryHeap, HashSet};

type Faction = u8;

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
struct Unit {
    faction: Faction,
    position: Point<usize>,
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum Tile {
    Empty,
    Wall,
    Unit(Faction),
}

impl Tile {
    pub const fn is_enemy(self, of: Faction) -> bool {
        if let Self::Unit(faction) = self {
            faction != of
        } else {
            false
        }
    }
}

impl From<&u8> for Tile {
    fn from(byte: &u8) -> Self {
        match byte {
            b'.' => Self::Empty,
            b'#' => Self::Wall,
            b => Self::Unit(*b),
        }
    }
}

impl From<&Tile> for u8 {
    fn from(tile: &Tile) -> Self {
        match tile {
            Tile::Empty => b'.',
            Tile::Wall => b'#',
            Tile::Unit(u) => *u,
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum DijkstraTile {
    NotVisited,
    Blocked,
    Visited(usize),
}

impl DijkstraTile {
    pub const fn is_visitable(&self) -> bool {
        matches!(self, Self::NotVisited)
    }

    pub const fn is_visited(&self) -> bool {
        matches!(self, Self::Visited(_))
    }
}

impl From<&Tile> for DijkstraTile {
    fn from(other: &Tile) -> Self {
        match other {
            Tile::Empty => Self::NotVisited,
            Tile::Wall | Tile::Unit(_) => Self::Blocked,
        }
    }
}

#[derive(Eq, PartialEq)]
struct DijkstraNode {
    pub value: usize,
    pub position: Point<usize>,
}

impl DijkstraNode {
    pub const fn new(value: usize, position: Point<usize>) -> Self {
        Self { value, position }
    }
}

impl Ord for DijkstraNode {
    fn cmp(&self, other: &Self) -> cmp::Ordering {
        self.value
            .cmp(&other.value)
            .reverse()
            .then(self.position.cmp_reading(&other.position))
    }
}

impl PartialOrd for DijkstraNode {
    fn partial_cmp(&self, other: &Self) -> Option<cmp::Ordering> {
        Some(self.cmp(other))
    }
}

type TileMap<T> = Array2<T>;

struct Game {
    map: TileMap<Tile>,
    units: Vec<Unit>,
}

impl Game {
    pub fn new(map: TileMap<Tile>, units: Vec<Unit>) -> Self {
        Self { map, units }
    }

    pub fn show(&self) {
        for row in self.map.outer_iter() {
            println!(
                "{}",
                String::from_utf8_lossy(&row.iter().map(u8::from).collect::<Vec<_>>())
            );
        }
    }

    pub fn tick(&mut self) -> Result<(), AocError> {
        self.units
            .sort_by(|a, b| a.position.cmp_reading(&b.position));

        for unit in self.units.clone() {
            if !self.can_attack(&unit) {
                self.try_move(&unit)?;
            }

            self.try_attack(&unit);
        }

        Ok(())
    }

    fn can_attack(&self, unit: &Unit) -> bool {
        let neighboring_enemies = unit
            .position
            .neighbors()
            .iter()
            .filter(|p| self.map[(p.x(), p.y())].is_enemy(unit.faction))
            .count();

        neighboring_enemies > 0
    }

    fn try_attack(&mut self, _: &Unit) {}

    fn try_move(&mut self, unit: &Unit) -> Result<(), AocError> {
        let mut path_map = self.map.map(DijkstraTile::from);
        let mut queue: BinaryHeap<DijkstraNode> =
            Self::visitable_neighbors(&path_map, &unit.position, 0)
                .into_iter()
                .collect();
        while !queue.is_empty() {
            let next = queue.pop().ok_or(AocError::Unspecified)?;
            path_map[(next.position.x(), next.position.y())] = DijkstraTile::Visited(next.value);
            queue.extend(Self::visitable_neighbors(
                &path_map,
                &next.position,
                next.value,
            ));
        }

        println!(
            "Paths from unit at {},{}:",
            unit.position.y(),
            unit.position.x()
        );
        for row in path_map.outer_iter() {
            println!(
                "{}",
                row.iter()
                    .map(|tile| match tile {
                        DijkstraTile::Blocked => "#".to_string(),
                        DijkstraTile::NotVisited => ".".to_string(),
                        DijkstraTile::Visited(value) => value.to_string(),
                    })
                    .collect::<String>()
            );
        }
        println!();

        let targets = self
            .units
            .iter()
            .filter(|u| u.faction != unit.faction)
            .flat_map(|u| {
                u.position
                    .neighbors()
                    .iter()
                    .filter(|p| path_map[(p.x(), p.y())].is_visited())
                    .copied()
                    .collect::<Vec<_>>()
            })
            .collect::<HashSet<_>>();

        println!(
            "Targets for unit at {},{}: {:?}",
            unit.position.x(),
            unit.position.y(),
            targets
        );

        panic!();
    }

    fn visitable_neighbors(
        map: &TileMap<DijkstraTile>,
        position: &Point<usize>,
        value: usize,
    ) -> Vec<DijkstraNode> {
        position
            .neighbors()
            .iter()
            .filter_map(|pos| {
                if map[(pos.x(), pos.y())] == DijkstraTile::NotVisited {
                    Some(DijkstraNode::new(value + 1, *pos))
                } else {
                    None
                }
            })
            .collect()
    }
}

#[aoc_generator(day15)]
fn generator(input: &str) -> Result<Game, AocError> {
    let width = input.lines().next().ok_or("No parse")?.len();
    let height = input.lines().count();
    let tiles = input
        .lines()
        .flat_map(|line| line.as_bytes().iter().map(Tile::from))
        .collect();
    let map = TileMap::<Tile>::from_shape_vec((height, width), tiles)?;
    let units = map
        .indexed_iter()
        .filter_map(|(xy, tile)| {
            if let Tile::Unit(faction) = tile {
                Some(Unit {
                    faction: *faction,
                    position: xy,
                })
            } else {
                None
            }
        })
        .collect();

    Ok(Game::new(map, units))
}

#[cfg(test)]
mod tests {

    const BUILD_INPUT: &str = r"#######
#E..G.#
#...#.#
#.G.#G#
#######";

    #[test]
    fn map_new() {
        let mut game = super::generator(BUILD_INPUT).unwrap();

        game.show();
        game.tick().unwrap();
    }
}
