/**
 *
 */
use crate::common::errors::AocError;
use lazy_static::lazy_static;
use regex::Regex;
use std::cmp;
use std::convert::TryFrom;
use std::ops::{Index, IndexMut};

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum Faction {
    Host,
    Infection,
}

impl TryFrom<usize> for Faction {
    type Error = AocError;

    fn try_from(other: usize) -> Result<Self, Self::Error> {
        match other {
            0 => Ok(Self::Host),
            1 => Ok(Self::Infection),
            _ => Err(AocError::Generic("Logic error".to_string())),
        }
    }
}

#[derive(Clone, Copy, Debug)]
enum AttackType {
    Fire = 0,
    Cold = 1,
    Slashing = 2,
    Bludgeoning = 3,
    Radiation = 4,
}

impl TryFrom<&str> for AttackType {
    type Error = AocError;

    fn try_from(other: &str) -> Result<Self, Self::Error> {
        match other {
            "fire" => Ok(Self::Fire),
            "cold" => Ok(Self::Cold),
            "slashing" => Ok(Self::Slashing),
            "bludgeoning" => Ok(Self::Bludgeoning),
            "radiation" => Ok(Self::Radiation),
            _ => Err(AocError::ParseError("No parse".to_string())),
        }
    }
}

#[derive(Clone, Copy, Debug)]
enum Effectiveness {
    Immune,
    Normal,
    Weak,
}

impl TryFrom<&str> for Effectiveness {
    type Error = AocError;

    fn try_from(other: &str) -> Result<Self, Self::Error> {
        match other {
            "immune" => Ok(Self::Immune),
            "normal" => Ok(Self::Normal),
            "weak" => Ok(Self::Weak),
            _ => Err(AocError::ParseError("No parse".to_string())),
        }
    }
}

#[derive(Clone, Debug)]
struct Resistances(pub [Effectiveness; 5]);

impl Resistances {
    pub const fn default() -> Self {
        Self([
            Effectiveness::Normal,
            Effectiveness::Normal,
            Effectiveness::Normal,
            Effectiveness::Normal,
            Effectiveness::Normal,
        ])
    }
}

impl Index<AttackType> for Resistances {
    type Output = Effectiveness;

    fn index(&self, index: AttackType) -> &Self::Output {
        let idx = match index {
            AttackType::Fire => 0,
            AttackType::Cold => 1,
            AttackType::Slashing => 2,
            AttackType::Bludgeoning => 3,
            AttackType::Radiation => 4,
        };

        &self.0[idx]
    }
}

impl IndexMut<AttackType> for Resistances {
    fn index_mut(&mut self, index: AttackType) -> &mut Self::Output {
        let idx = match index {
            AttackType::Fire => 0,
            AttackType::Cold => 1,
            AttackType::Slashing => 2,
            AttackType::Bludgeoning => 3,
            AttackType::Radiation => 4,
        };

        &mut self.0[idx]
    }
}

type GroupId = (Faction, u32);

#[derive(Clone, Debug)]
struct Group {
    id: GroupId,
    units: u32,
    hit_points: u32,
    attack_damage: u32,
    initiative: u32,
    resistances: Resistances,
    attack_type: AttackType,
}

impl Group {
    pub const fn calculate_losses(&self, damage: u32) -> u32 {
        damage / self.hit_points
    }

    pub fn calculate_damage_against(&self, other: &Self) -> u32 {
        match other.effectiveness(self.attack_type) {
            Effectiveness::Immune => 0,
            Effectiveness::Normal => self.effective_power(),
            Effectiveness::Weak => self.effective_power() * 2,
        }
    }

    pub fn cmp_target_selection(&self, other: &Self) -> cmp::Ordering {
        self.effective_power()
            .cmp(&other.effective_power())
            .then(self.initiative.cmp(&other.initiative))
            .reverse()
    }

    pub fn effectiveness(&self, attack_type: AttackType) -> Effectiveness {
        self.resistances[attack_type]
    }

    pub const fn effective_power(&self) -> u32 {
        self.units * self.attack_damage
    }

    pub fn is_same(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

fn parse_resistances(input: &str) -> Result<Resistances, AocError> {
    lazy_static! {
        static ref RES_REGEX: Regex = Regex::new(r"(immune|weak) to ([\w, ]+)").unwrap();
    }

    Ok(input
        .split(';')
        .map(|res| {
            let caps = RES_REGEX.captures(res).ok_or("")?;
            let eff = Effectiveness::try_from(&caps[1])?;
            let types = caps[2]
                .split(", ")
                .map(AttackType::try_from)
                .collect::<Result<_, _>>()?;

            Ok((eff, types))
        })
        .collect::<Result<Vec<(Effectiveness, Vec<AttackType>)>, AocError>>()?
        .iter()
        .fold(Resistances::default(), |mut res, (eff, types)| {
            for t in types {
                res[*t] = *eff;
            }

            res
        }))
}

#[aoc_generator(day24)]
fn generator(input: &str) -> Result<Vec<Group>, AocError> {
    lazy_static! {
        static ref GROUP_REGEX: Regex = Regex::new(r"^(\d+) units each with (\d+) hit points (\([\w ,;]+\) )?with an attack that does (\d+) (\w+) damage at initiative (\d+)").unwrap();
    }
    input
        .split("\n\n")
        .enumerate()
        .flat_map(|(army_id, army)| {
            army.lines()
                .skip(1)
                .enumerate()
                .map(move |(unit_id, line)| {
                    let faction = Faction::try_from(army_id)?;
                    let id = unit_id as u32 + 1;
                    GROUP_REGEX
                        .captures(line)
                        .map(|caps| {
                            let units = caps[1].parse::<u32>()?;
                            let hit_points = caps[2].parse::<u32>()?;
                            let resistances = caps.get(3).map_or_else(
                                || Ok(Resistances::default()),
                                |cap| parse_resistances(cap.as_str()),
                            )?;
                            let attack_damage = caps[4].parse::<u32>()?;
                            let attack_type = AttackType::try_from(&caps[5])?;
                            let initiative = caps[6].parse::<u32>()?;

                            Ok(Group {
                                id: (faction, id),
                                units,
                                hit_points,
                                resistances,
                                attack_damage,
                                attack_type,
                                initiative,
                            })
                        })
                        .ok_or("")?
                })
        })
        .collect::<Result<Vec<Group>, AocError>>()
}

fn target_selection_phase(groups: &[Group]) -> Vec<(GroupId, GroupId)> {
    let mut target_selections = Vec::new();
    for group in groups {
        let target = groups
            .iter()
            .filter_map(|target| {
                if target.id.0 == group.id.0
                    || target_selections
                        .iter()
                        .any(|(_, tgt): &(&Group, &Group)| tgt.is_same(target))
                {
                    return None;
                }

                let damage = group.calculate_damage_against(target);
                Some((target, damage))
            })
            .filter(|(_, dmg)| *dmg > 0)
            .max_by_key(|sel| (sel.1, sel.0.effective_power(), sel.0.initiative));

        if let Some((tgt, _)) = target {
            target_selections.push((group, tgt));
        }
    }

    target_selections.sort_by(|(g1, _), (g2, _)| g1.initiative.cmp(&g2.initiative).reverse());

    target_selections
        .iter()
        .map(|(att, def)| (att.id, def.id))
        .collect()
}

fn attack_phase(groups: &mut [Group], attacks: Vec<(GroupId, GroupId)>) -> Result<u32, AocError> {
    let mut units_lost = 0;

    for (att_id, def_id) in attacks {
        let damage = groups
            .iter()
            .find(|g| g.id == att_id)
            .and_then(|att| {
                groups
                    .iter()
                    .find(|g| g.id == def_id)
                    .map(|def| att.calculate_damage_against(def))
            })
            .ok_or(AocError::NoSolution)?;

        let def = groups
            .iter_mut()
            .find(|g| g.id == def_id)
            .ok_or(AocError::NoSolution)?;

        let losses = cmp::min(def.units, def.calculate_losses(damage));
        def.units -= losses;
        units_lost += losses;
    }

    Ok(units_lost)
}

fn run_infection(mut groups: Vec<Group>) -> Result<Option<(Faction, u32)>, AocError> {
    while groups.iter().any(|g| g.id.0 != groups[0].id.0) {
        groups.sort_by(Group::cmp_target_selection);
        let target_selections = target_selection_phase(&groups);
        let units_lost = attack_phase(&mut groups, target_selections)?;

        if units_lost == 0 {
            return Ok(None);
        }

        groups.retain(|g| g.units > 0);
    }

    Ok(Some((groups[0].id.0, groups.iter().map(|g| g.units).sum())))
}

#[aoc(day24, part1)]
fn part01(input: &[Group]) -> Result<u32, AocError> {
    let groups = input.to_vec();

    Ok(run_infection(groups)?.ok_or(AocError::NoSolution)?.1)
}

#[aoc(day24, part2)]
fn part02(input: &[Group]) -> Result<u32, AocError> {
    let mut min = 0;
    loop {
        let mut groups = input.to_vec();
        for group in &mut groups {
            if group.id.0 == Faction::Host {
                group.attack_damage += min;
            }
        }

        if let Some((winner, units)) = run_infection(groups)? {
            if winner == Faction::Host {
                return Ok(units);
            }
        }

        min += 1;
    }
}
