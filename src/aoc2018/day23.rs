/**
 *
 */
use crate::common::errors::AocError;
use crate::common::vec::Vec3;
use lazy_static::lazy_static;
use regex::Regex;

#[aoc_generator(day23)]
fn generator(input: &str) -> Result<Vec<(Vec3, u32)>, AocError> {
    lazy_static! {
        static ref REGEX: Regex =
            Regex::new(r"^pos=<([\-\d]+),([\-\d]+),([\-\d]+)>, r=(\d+)$").unwrap();
    }

    input
        .lines()
        .map(|line| {
            let caps = REGEX.captures(line).ok_or("")?;

            let x = caps[1].parse::<i32>()?;
            let y = caps[2].parse::<i32>()?;
            let z = caps[3].parse::<i32>()?;
            let r = caps[4].parse::<u32>()?;

            Ok((Vec3::new(x, y, z), r))
        })
        .collect()
}

#[aoc(day23, part1)]
fn part01(input: &[(Vec3, u32)]) -> usize {
    input.iter().max_by_key(|(_, k)| k).map_or(0, |drone| {
        input
            .iter()
            .filter(|d| d.0.manhattan_distance(&drone.0) <= drone.1)
            .count()
    })
}

#[aoc(day23, part2)]
fn part02(input: &[(Vec3, u32)]) -> Result<u32, AocError> {
    let mut min_x = input
        .iter()
        .map(|(d, _)| d.x)
        .min()
        .ok_or_else(|| AocError::ParseError("Missing: min_x".to_owned()))?;
    let mut max_x = input
        .iter()
        .map(|(d, _)| d.x)
        .max()
        .ok_or_else(|| AocError::ParseError("Missing: max_x".to_owned()))?;
    let mut min_y = input
        .iter()
        .map(|(d, _)| d.y)
        .min()
        .ok_or_else(|| AocError::ParseError("Missing: min_y".to_owned()))?;
    let mut max_y = input
        .iter()
        .map(|(d, _)| d.y)
        .max()
        .ok_or_else(|| AocError::ParseError("Missing: max_y".to_owned()))?;
    let mut min_z = input
        .iter()
        .map(|(d, _)| d.z)
        .min()
        .ok_or_else(|| AocError::ParseError("Missing: min_z".to_owned()))?;
    let mut max_z = input
        .iter()
        .map(|(d, _)| d.z)
        .max()
        .ok_or_else(|| AocError::ParseError("Missing: max_z".to_owned()))?;

    let mut range = 1;
    while range < max_x - min_x {
        range *= 2;
    }

    let mut best = (Vec3::origin(), 0);
    loop {
        let mut target_count = 0;

        for x in (min_x..=max_x).step_by(range as usize) {
            for y in (min_y..=max_y).step_by(range as usize) {
                for z in (min_z..=max_z).step_by(range as usize) {
                    let pos = Vec3::new(x, y, z);
                    let distance_from_origin = pos.manhattan_distance(&Vec3::origin());
                    let count = input
                        .iter()
                        .filter(|(drone, r)| {
                            (drone.manhattan_distance(&pos) as i32 - *r as i32) / range <= 0
                        })
                        .count();

                    if count > target_count
                        || (count == target_count && distance_from_origin < best.1)
                    {
                        target_count = count;
                        best = (pos, distance_from_origin);
                    }
                }
            }
        }

        if range == 1 {
            break;
        }

        min_x = best.0.x - range;
        max_x = best.0.x + range;
        min_y = best.0.y - range;
        max_y = best.0.y + range;
        min_z = best.0.z - range;
        max_z = best.0.z + range;

        range /= 2;
    }

    Ok(best.1)
}
