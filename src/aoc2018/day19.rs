/** Correct results:
 * - 1728
 * - 18200448
 */
use crate::common::chr::FromAsciiDigit;
use crate::common::errors::AocError;
use crate::common::opcode::{Instruction, OpCode};

#[aoc_generator(day19)]
fn generator(input: &str) -> Result<(usize, Vec<Instruction>), AocError> {
    let ip = usize::from_ascii_digit(
        input
            .lines()
            .next()
            .ok_or_else(|| AocError::ParseError("Missing input".to_owned()))?
            .as_bytes()[4],
    );

    let instructions = input
        .lines()
        .skip(1)
        .map(|line| {
            let parts = line.split(' ').collect::<Vec<&str>>();
            let opcode = OpCode::from(parts[0]);
            let a = parts[1].parse::<usize>()?;
            let b = parts[2].parse::<usize>()?;
            let c = parts[3].parse::<usize>()?;

            Ok(Instruction { opcode, a, b, c })
        })
        .collect::<Result<Vec<Instruction>, AocError>>()?;

    Ok((ip, instructions))
}

#[aoc(day19, part1)]
fn part01((ip, instructions): &(usize, Vec<Instruction>)) -> usize {
    let mut registers = [0, 0, 0, 0, 0, 0];
    while registers[*ip] < instructions.len() {
        let current_instruction = &instructions[registers[*ip]];
        current_instruction.run(&mut registers);
        registers[*ip] += 1;
    }

    registers[0]
}

#[aoc(day19, part2)]
fn part02((ip, instructions): &(usize, Vec<Instruction>)) -> usize {
    let mut num_instr: usize = 0;

    let mut registers = [1, 0, 0, 0, 0, 0];
    while registers[*ip] < instructions.len() {
        let current_instruction = &instructions[registers[*ip]];
        current_instruction.run(&mut registers);
        registers[*ip] += 1;

        num_instr += 1;
        if num_instr % 1_000_000_000 == 0 {
            println!("{num_instr}: {registers:?}");
        }
    }

    registers[0]
}

#[cfg(test)]
mod tests {

    const EXAMPLE_INPUT_0: &str = r"#ip 0
seti 5 0 1
seti 6 0 2
addi 0 1 0
addr 1 2 3
setr 1 0 0
seti 8 0 4
seti 9 0 5";

    #[test]
    fn parse() {
        let zaz = super::generator(EXAMPLE_INPUT_0);
        println!("{:?}", zaz);
    }

    #[test]
    fn part1() {
        let zaz = super::generator(EXAMPLE_INPUT_0).unwrap();
        println!("{:?}", super::part01(&zaz));
    }

    #[test]
    fn part2() {
        let zaz = super::generator(EXAMPLE_INPUT_0).unwrap();
        println!("{:?}", super::part02(&zaz));
    }
}
