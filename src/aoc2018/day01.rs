/** Correct results:
 * - 505
 * - 72330
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use std::collections::HashSet;

#[aoc_generator(day1)]
fn generator(input: &str) -> Result<Vec<i32>, AocError> {
    input_parser(by_lines(parse_int))(input)
}

#[aoc(day1, part1)]
fn part01(input: &[i32]) -> i32 {
    input.iter().sum()
}

#[aoc(day1, part2)]
fn part02(input: &[i32]) -> i32 {
    let mut frequency_set = HashSet::new();
    frequency_set.insert(0);

    let mut current_frequency = 0;
    for frequency in input.iter().cycle() {
        current_frequency += frequency;

        if !frequency_set.insert(current_frequency) {
            break;
        }
    }

    current_frequency
}
