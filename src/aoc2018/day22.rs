use crate::common::point::Point;
/**
 *
 */
use crate::common::{errors::AocError, point::PointOps};
use ndarray::Array2;
use pathfinding::prelude::dijkstra;

#[derive(Clone, Debug)]
enum Tile {
    Rocky,
    Wet,
    Narrow,
}

impl From<usize> for Tile {
    fn from(other: usize) -> Self {
        match other % 3 {
            0 => Self::Rocky,
            1 => Self::Wet,
            2 => Self::Narrow,
            _ => unreachable!(),
        }
    }
}

#[aoc_generator(day22)]
fn generator(input: &str) -> Result<(usize, Point<usize>), AocError> {
    let mut lines = input.lines();

    let depth =
        String::from_utf8_lossy(&lines.next().ok_or("")?.as_bytes()[7..]).parse::<usize>()?;

    let mut position = lines
        .next()
        .ok_or("")?
        .split(' ')
        .nth(1)
        .ok_or("")?
        .split(',')
        .map(str::parse);
    let y = position.next().ok_or("")??;
    let x = position.next().ok_or("")??;

    Ok((depth, (x, y)))
}

fn build_map(depth: usize, target: &Point<usize>) -> Array2<usize> {
    let width = target.x() + 15;
    let height = target.y() + 15;

    let mut cave = Array2::<usize>::from_elem((width, height), 0);

    for (x, cell) in cave.row_mut(0).indexed_iter_mut() {
        *cell = ((x * 16807) + depth) % 20183;
    }
    for (y, cell) in cave.column_mut(0).indexed_iter_mut() {
        *cell = ((y * 48271) + depth) % 20183;
    }

    for x in 1..width {
        for y in 1..height {
            cave[(x, y)] = ((cave[(x - 1, y)] * cave[(x, y - 1)]) + depth) % 20183;
        }
    }

    let mut erosions = cave.mapv(|t| t % 3);
    erosions[(0, 0)] = 0;
    erosions[*target] = 0;

    erosions
}

#[aoc(day22, part1)]
fn part01(input: &(usize, Point<usize>)) -> usize {
    build_map(input.0, &input.1).sum()
}

const NEITHER: usize = 1;
const TORCH: usize = 2;
const GEAR: usize = 4;

const ALLOWED: [usize; 3] = [TORCH + GEAR, NEITHER + GEAR, NEITHER + TORCH];

#[aoc(day22, part2)]
fn part02(input: &(usize, Point<usize>)) -> Result<usize, AocError> {
    let map = build_map(input.0, &input.1);
    let tx = input.1.x();
    let ty = input.1.y();
    let max_x = input.1.x() + 14;
    let max_y = input.1.y() + 14;

    let (_, cost) = dijkstra(
        &((0, 0), TORCH),
        |&(xy, equipment)| {
            xy.cardinal_neighbors_checked(max_x, max_y)
                .iter()
                .map(|p| (p.x(), p.y()))
                .filter(|&(nx, ny)| ALLOWED[map[(nx, ny)]] & equipment == equipment)
                .map(|(nx, ny)| (((nx, ny), equipment), 1))
                .chain(std::iter::once(((xy, ALLOWED[map[xy]] - equipment), 7)))
                .collect::<Vec<_>>()
        },
        |&((x, y), equipment)| x == tx && y == ty && equipment == TORCH,
    )
    .ok_or(AocError::NoSolution)?;

    Ok(cost)
}
