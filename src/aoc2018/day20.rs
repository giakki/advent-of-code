/**
 *
 */
use crate::common::errors::AocError;
use std::collections::HashMap;

#[derive(Debug)]
enum Token {
    Direction(i32, i32),
    Open,
    Close,
    Branch,
}

impl Token {
    pub fn try_from(byte: u8) -> Result<Self, AocError> {
        match byte {
            b'N' => Ok(Self::Direction(0, -1)),
            b'S' => Ok(Self::Direction(0, 1)),
            b'W' => Ok(Self::Direction(-1, 0)),
            b'E' => Ok(Self::Direction(1, 0)),
            b'(' | b'^' => Ok(Self::Open),
            b')' | b'$' => Ok(Self::Close),
            b'|' => Ok(Self::Branch),
            _ => Err(AocError::ParseError("Invalid token".to_string())),
        }
    }
}

#[aoc_generator(day20)]
fn generator(input: &str) -> Result<Vec<Token>, AocError> {
    input.as_bytes()[1..(input.len() - 1)]
        .iter()
        .copied()
        .map(Token::try_from)
        .collect()
}

fn build_distances(input: &[Token]) -> Result<Vec<usize>, AocError> {
    let mut positions = Vec::with_capacity(input.len());
    let mut distances = HashMap::new();

    let mut current = (0, 0);
    let mut prev = current;

    for token in input {
        match token {
            Token::Direction(dx, dy) => {
                current.0 += dx;
                current.1 += dy;

                let prev_distance_plus_one = distances.get(&prev).copied().unwrap_or_default() + 1;

                distances
                    .entry(current)
                    .and_modify(|distance| {
                        *distance = std::cmp::min(*distance, prev_distance_plus_one);
                    })
                    .or_insert(prev_distance_plus_one);
            }
            Token::Branch => {
                current = *positions.last().ok_or(AocError::Unspecified)?;
            }
            Token::Open => {
                positions.push(current);
            }
            Token::Close => {
                current = positions.pop().ok_or(AocError::Unspecified)?;
            }
        };

        prev = current;
    }

    Ok(distances.values().copied().collect())
}

#[aoc(day20, part1)]
fn part01(input: &[Token]) -> Result<usize, AocError> {
    Ok(build_distances(input)?.into_iter().max().unwrap_or(0))
}

#[aoc(day20, part2)]
fn part02(input: &[Token]) -> Result<usize, AocError> {
    Ok(build_distances(input)?
        .into_iter()
        .filter(|d| *d >= 1000)
        .count())
}
