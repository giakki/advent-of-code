use std::error::Error;

#[derive(Debug)]
struct Node {
    pub children: Vec<Node>,
    pub metadata: Vec<usize>,
}

impl Node {
    fn iter(&self) -> NodeIterator {
        NodeIterator {
            nodes_to_visit: vec![self],
        }
    }

    fn value(&self) -> usize {
        if self.children.is_empty() {
            self.metadata.iter().sum()
        } else {
            self.metadata
                .iter()
                .filter_map(|m| {
                    if *m != 0 && *m <= self.children.len() {
                        Some(self.children[m - 1].value())
                    } else {
                        None
                    }
                })
                .sum()
        }
    }
}

struct NodeIterator<'a> {
    nodes_to_visit: Vec<&'a Node>,
}

impl<'a> Iterator for NodeIterator<'a> {
    type Item = &'a Node;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(next) = self.nodes_to_visit.pop() {
            for node in &next.children {
                self.nodes_to_visit.push(node);
            }
            Some(next)
        } else {
            None
        }
    }
}

#[aoc_generator(day8)]
fn generator(input: &str) -> Result<Box<Node>, Box<dyn Error>> {
    let mut iter = input.split(' ');

    Ok(Box::new(create_node(&mut iter)?))
}

fn create_node(iter: &mut dyn Iterator<Item = &str>) -> Result<Node, Box<dyn Error>> {
    let num_children = iter.next().ok_or("")?.parse::<usize>()?;
    let num_metadata = iter.next().ok_or("")?.parse::<usize>()?;

    let mut children = Vec::new();
    for _ in 0..num_children {
        children.push(create_node(iter)?);
    }

    let mut metadata = Vec::new();
    for _ in 0..num_metadata {
        metadata.push(iter.next().ok_or("")?.parse::<usize>()?);
    }

    Ok(Node { children, metadata })
}

#[aoc(day8, part1)]
fn part01(input: &Node) -> usize {
    let mut sum = 0_usize;
    for node in input.iter() {
        sum += node.metadata.iter().sum::<usize>();
    }

    sum
}

#[aoc(day8, part2)]
fn part02(input: &Node) -> usize {
    input.value()
}
