/**
 * --- Day 12: Subterranean Sustainability ---
 * The year 518 is significantly more underground than your history books implied. Either that, or you've arrived in a vast cavern network
 *   under the North Pole.
 *
 * After exploring a little, you discover a long tunnel that contains a row of small pots as far as you can see to your left and right. A
 *  few of them contain plants - someone is trying to grow things in these geothermally-heated caves.
 *
 * The pots are numbered, with 0 in front of you. To the left, the pots are numbered -1, -2, -3, and so on; to the right, 1, 2, 3.... Your
 *   puzzle input contains a list of pots from 0 to the right and whether they do (#) or do not (.) currently contain a plant, the initial
 *   state. (No other pots currently contain plants.) For example, an initial state of #..##.... indicates that pots 0, 3, and 4 currently
 *   contain plants.
 *
 * Your puzzle input also contains some notes you find on a nearby table: someone has been trying to figure out how these plants spread to
 *   nearby pots. Based on the notes, for each generation of plants, a given pot has or does not have a plant based on whether that pot
 *   (and the two pots on either side of it) had a plant in the last generation. These are written as LLCRR => N, where L are pots to the
 *   left, C is the current pot being considered, R are the pots to the right, and N is whether the current pot will have a plant in the
 *   next generation. For example:
 *
 * A note like ..#.. => . means that a pot that contains a plant but with no plants within two pots of it will not have a plant in it during
 *   the next generation.
 * A note like ##.## => . means that an empty pot with two plants on each side of it will remain empty in the next generation.
 * A note like .##.# => # means that a pot has a plant in a given generation if, in the previous generation, there were plants in that pot,
 *   the one immediately to the left, and the one two pots to the right, but not in the ones immediately to the right and two to the left.
 * It's not clear what these plants are for, but you're sure it's important, so you'd like to make sure the current configuration of plants
 *   is sustainable by determining what will happen after 20 generations.
 *
 * For example, given the following input:
 *
 * initial state: #..#.#..##......###...###
 *
 * ...## => #
 * ..#.. => #
 * .#... => #
 * .#.#. => #
 * .#.## => #
 * .##.. => #
 * .#### => #
 * #.#.# => #
 * #.### => #
 * ##.#. => #
 * ##.## => #
 * ###.. => #
 * ###.# => #
 * ####. => #
 *
 * For brevity, in this example, only the combinations which do produce a plant are listed. (Your input includes all possible combinations.)
 *   Then, the next 20 generations will look like this:
 *
 * 1         2         3
 * 0         0         0         0
 * 0: ...#..#.#..##......###...###...........
 * 1: ...#...#....#.....#..#..#..#...........
 * 2: ...##..##...##....#..#..#..##..........
 * 3: ..#.#...#..#.#....#..#..#...#..........
 * 4: ...#.#..#...#.#...#..#..##..##.........
 * 5: ....#...##...#.#..#..#...#...#.........
 * 6: ....##.#.#....#...#..##..##..##........
 * 7: ...#..###.#...##..#...#...#...#........
 * 8: ...#....##.#.#.#..##..##..##..##.......
 * 9: ...##..#..#####....#...#...#...#.......
 * 10: ..#.#..#...#.##....##..##..##..##......
 * 11: ...#...##...#.#...#.#...#...#...#......
 * 12: ...##.#.#....#.#...#.#..##..##..##.....
 * 13: ..#..###.#....#.#...#....#...#...#.....
 * 14: ..#....##.#....#.#..##...##..##..##....
 * 15: ..##..#..#.#....#....#..#.#...#...#....
 * 16: .#.#..#...#.#...##...#...#.#..##..##...
 * 17: ..#...##...#.#.#.#...##...#....#...#...
 * 18: ..##.#.#....#####.#.#.#...##...##..##..
 * 19: .#..###.#..#.#.#######.#.#.#..#.#...#..
 * 20: .#....##....#####...#######....#.#..##.
 *
 * The generation is shown along the left, where 0 is the initial state. The pot numbers are shown along the top, where 0 labels the center
 *   pot, negative-numbered pots extend to the left, and positive pots extend toward the right. Remember, the initial state begins at pot 0,
 *   which is not the leftmost pot used in this example.
 *
 * After one generation, only seven plants remain. The one in pot 0 matched the rule looking for ..#.., the one in pot 4 matched the rule
 *   looking for .#.#., pot 9 matched .##.., and so on.
 *
 * In this example, after 20 generations, the pots shown as # contain plants, the furthest left of which is pot -2, and the furthest right
 *   of which is pot 34. Adding up all the numbers of plant-containing pots after the 20th generation produces 325.
 *
 * After 20 generations, what is the sum of the numbers of all pots which contain a plant?
 *
 * Your puzzle answer was 3241.
 *
 * The first half of this puzzle is complete! It provides one gold star: *
 *
 * --- Part Two ---
 * You realize that 20 generations aren't enough. After all, these plants will need to last another 1500 years to even reach your timeline,
 *   not to mention your future.
 *
 * After fifty billion (50000000000) generations, what is the sum of the numbers of all pots which contain a plant?
 */
extern crate bytecount;

use crate::common::errors::AocError;

fn hash_cell(cell: &[u8]) -> usize {
    cell.iter().enumerate().fold(0, |hash, (i, byte)| {
        if *byte == b'#' {
            hash | 1 << (cell.len() - 1 - i)
        } else {
            hash
        }
    })
}

fn run_next_generation(rules: &[u8; 32], state: &[u8]) -> Vec<u8> {
    let mut next_state = Vec::with_capacity(state.len() + 2);

    next_state.extend_from_slice(b"...");
    for i in 2..state.len() - 2 {
        next_state.push(rules[hash_cell(&state[i - 2..=i + 2])]);
    }
    next_state.extend_from_slice(b"...");

    next_state
}

fn generations_equal(gen_0: &[u8], gen_1: &[u8]) -> bool {
    gen_0
        .iter()
        .skip_while(|p| **p == b'.')
        .zip(gen_1.iter().skip_while(|p| **p == b'.'))
        .all(|(a, b)| a == b)
}

fn generator(input: &str) -> Result<(Vec<u8>, [u8; 32]), AocError> {
    let mut line_iter = input.lines();

    let initial_state = [b'.', b'.', b'.']
        .iter()
        .chain(
            line_iter
                .next()
                .ok_or_else(|| AocError::ParseError("Invalid input".to_owned()))?
                .as_bytes()[15..]
                .iter(),
        )
        .chain([b'.', b'.', b'.'].iter())
        .copied()
        .collect();

    let mut buckets = [b'.'; 32];
    for line in line_iter.skip(1) {
        let hash = hash_cell(&line.as_bytes()[..5]);
        buckets[hash] = line.as_bytes()[9];
    }

    Ok((initial_state, buckets))
}

fn calculate_pot_value(state: &[u8], gen: i64) -> i64 {
    state
        .iter()
        .enumerate()
        .filter_map(|(i, pot)| {
            if *pot == b'#' {
                Some((i as i64) - gen - 3)
            } else {
                None
            }
        })
        .sum::<i64>()
}

#[aoc(day12, part1)]
fn part01(input: &str) -> Result<String, AocError> {
    let (mut state, rules) = generator(input)?;

    for _ in 0..20 {
        let next_state = run_next_generation(&rules, &state);

        state = next_state;
    }

    Ok(calculate_pot_value(&state, 20).to_string())
}

#[aoc(day12, part2)]
fn part02(input: &str) -> Result<String, AocError> {
    let (mut state, rules) = generator(input)?;

    let mut generation = 0;
    loop {
        let next_state = run_next_generation(&rules, &state);

        if generations_equal(&state, &next_state) {
            break;
        }

        state = next_state;
        generation += 1;
    }

    let pot_value = calculate_pot_value(&state, generation);
    let num_pots = bytecount::count(&state, b'#') as i64;

    Ok((pot_value + (50_000_000_000_i64 - generation) * num_pots).to_string())
}
