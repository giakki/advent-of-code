/** Correct results:
 * - 4139586
 * - 1800151
 */
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_binary_u32},
};
use itertools::Itertools;
use nom::{character::complete::char, multi::separated_list0};

#[aoc_generator(day3)]
fn generator(input: &str) -> Result<(usize, Vec<u32>), AocError> {
    let numbers = input_parser(separated_list0(char('\n'), parse_binary_u32))(input)?;
    let bits = input.lines().next().ok_or(AocError::Input)?.chars().count();

    Ok((bits, numbers))
}

fn find_most_common_bits(bits: usize, numbers: &[u32]) -> Vec<(u32, u32)> {
    let mut bins = vec![(0, 0); bits];

    for num in numbers {
        let mut mask: u32 = 1 << (bits - 1);
        for bin in &mut bins {
            if num & mask == 0 {
                bin.0 += 1;
            } else {
                bin.1 += 1;
            }

            mask >>= 1;
        }
    }

    bins
}

#[aoc(day3, part1)]
fn part01(input: &(usize, Vec<u32>)) -> Result<u32, AocError> {
    let bins = find_most_common_bits(input.0, &input.1);

    let gamma_str = bins
        .into_iter()
        .map(|(a, b)| if a > b { '0' } else { '1' })
        .join("");

    let epsilon_str = gamma_str
        .chars()
        .map(|c| if c == '0' { '1' } else { '0' })
        .join("");

    Ok(u32::from_str_radix(&gamma_str, 2)? * u32::from_str_radix(&epsilon_str, 2)?)
}

#[aoc(day3, part2)]
fn part02((bits, numbers): &(usize, Vec<u32>)) -> Result<u32, AocError> {
    let mut ls_rating = numbers.iter().copied().collect_vec();

    for i in 0..32 {
        let bins = find_most_common_bits(*bits, &ls_rating);
        let mask: u32 = 1 << (bits - 1 - i);

        ls_rating.retain(|n| {
            if bins[i].0 > bins[i].1 {
                *n & mask == 0
            } else {
                *n & mask != 0
            }
        });
        if ls_rating.len() == 1 {
            break;
        }
    }

    let mut o2g_rating = numbers.iter().copied().collect_vec();
    for i in 0..32 {
        let bins = find_most_common_bits(*bits, &o2g_rating)
            .into_iter()
            .collect::<Vec<_>>();
        let mask: u32 = 1 << (bins.len() - 1 - i);

        o2g_rating.retain(|n| {
            if bins[i].0 <= bins[i].1 {
                *n & mask == 0
            } else {
                *n & mask != 0
            }
        });
        if o2g_rating.len() == 1 {
            break;
        }
    }

    ls_rating
        .first()
        .and_then(|ls| o2g_rating.first().map(|o2g| *o2g * *ls))
        .ok_or(AocError::NoSolution)
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = r"00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010";

    #[test]
    fn test03_generator() {
        let input = generator(INPUT).unwrap();
        assert_eq!(
            (5, vec![4, 30, 22, 23, 21, 15, 7, 28, 16, 25, 2, 10]),
            input
        );
    }

    #[test]
    fn test0301() {
        let input = generator(INPUT).unwrap();
        assert_eq!(part01(&input).unwrap(), 198);
    }

    #[test]
    fn test0302() {
        let input = generator(INPUT).unwrap();
        assert_eq!(part02(&input).unwrap(), 230);
    }
}
