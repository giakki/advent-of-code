/** Correct results:
 * - 1713
 * - 1734
 */
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use itertools::Itertools;
use nom::{character::complete::char, multi::separated_list1};

#[aoc_generator(day1)]
fn generator(input: &str) -> Result<Vec<i32>, AocError> {
    input_parser(separated_list1(char('\n'), parse_int))(input)
}

#[aoc(day1, part1)]
fn part01(input: &[i32]) -> usize {
    input.windows(2).filter(|w| w[1] > w[0]).count()
}
#[aoc(day1, part2)]
fn part02(input: &[i32]) -> usize {
    input
        .windows(3)
        .map(|w| w.iter().sum::<i32>())
        .tuple_windows::<(_, _)>()
        .filter(|(a, b)| b > a)
        .count()
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = r"199
200
208
210
200
207
240
269
260
263";

    #[test]
    fn test0101() {
        let input = generator(INPUT).unwrap();
        assert_eq!(7, part01(&input));
    }

    #[test]
    fn test0102() {
        let input = generator(INPUT).unwrap();
        assert_eq!(5, part02(&input));
    }
}
