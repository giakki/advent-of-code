/** Correct results:
 * - 360761
 * - 1632779838045
 */
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use nom::{character::complete::char, multi::separated_list0};
use std::collections::VecDeque;

fn group_fishes(fishes: &[usize]) -> VecDeque<usize> {
    let mut fishes_by_age = VecDeque::from(vec![0; 9]);

    for fish_age in fishes {
        fishes_by_age[*fish_age] += 1;
    }

    fishes_by_age
}

#[aoc_generator(day6)]
fn generator(input: &str) -> Result<Vec<usize>, AocError> {
    input_parser(separated_list0(char(','), parse_int))(input)
}

#[aoc(day6, part1)]
fn part01(input: &[usize]) -> usize {
    let mut fishes_by_age = group_fishes(input);

    for _ in 0..80 {
        fishes_by_age.rotate_left(1);
        fishes_by_age[6] += fishes_by_age[8];
    }

    fishes_by_age.into_iter().sum()
}

#[aoc(day6, part2)]
fn part02(input: &[usize]) -> usize {
    let mut fishes_by_age = group_fishes(input);

    for _ in 0..256 {
        fishes_by_age.rotate_left(1);
        fishes_by_age[6] += fishes_by_age[8];
    }

    fishes_by_age.into_iter().sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"3,4,3,1,2";

    #[test]
    fn test06_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test0601() {
        let input = generator(BUILD_INPUT).unwrap();

        assert_eq!(part01(&input), 5934);
    }

    #[test]
    fn test0602() {
        let input = generator(BUILD_INPUT).unwrap();

        assert_eq!(part02(&input), 26984457539);
    }
}
