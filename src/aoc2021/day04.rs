/** Correct results:
 * - 21607
 * - 19012
 */
use itertools::Itertools;
use ndarray::Array2;
use nom::{
    character::complete::{char, multispace1, space0},
    combinator::map,
    multi::{separated_list0, separated_list1},
    sequence::{preceded, terminated, tuple},
    IResult,
};

use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};

#[derive(Clone)]
struct BingoTile {
    n: u32,
    marked: bool,
}

#[derive(Clone)]
struct BingoBoard(Array2<BingoTile>);

impl BingoBoard {
    pub fn new(vec: Vec<u32>) -> Result<Self, AocError> {
        let arr = Array2::from_shape_vec((5, 5), vec)?.mapv(|n| BingoTile { n, marked: false });

        Ok(Self(arr))
    }

    pub fn set(&mut self, n: u32) {
        for tile in &mut self.0 {
            if tile.n == n {
                tile.marked = true;
            }
        }
    }

    pub fn is_solved(&self) -> bool {
        for row in self.0.rows() {
            let mut solved = true;
            for element in row {
                if !element.marked {
                    solved = false;
                }
            }
            if solved {
                return true;
            }
        }
        for col in self.0.columns() {
            let mut solved = true;
            for element in col {
                if !element.marked {
                    solved = false;
                }
            }
            if solved {
                return true;
            }
        }

        false
    }

    pub fn score(&self) -> u32 {
        self.0
            .fold(0, |acc, tile| if tile.marked { acc } else { acc + tile.n })
    }
}

fn parse_numbers(s: &str) -> IResult<&str, Vec<u32>> {
    terminated(separated_list0(char(','), parse_int), multispace1)(s)
}

fn parse_board_row(s: &str) -> IResult<&str, Vec<u32>> {
    separated_list1(char(' '), preceded(space0, parse_int))(s)
}

fn parse_board(s: &str) -> IResult<&str, Vec<u32>> {
    map(separated_list1(char('\n'), parse_board_row), |v| {
        v.into_iter().flatten().collect_vec()
    })(s)
}

fn parse_boards(s: &str) -> IResult<&str, Vec<Vec<u32>>> {
    separated_list0(multispace1, parse_board)(s)
}

#[aoc_generator(day4)]
fn generator(input: &str) -> Result<(Vec<u32>, Vec<Vec<u32>>), AocError> {
    input_parser(tuple((parse_numbers, parse_boards)))(input)
}

#[aoc(day4, part1)]
fn part01((numbers, in_boards): &(Vec<u32>, Vec<Vec<u32>>)) -> Result<u32, AocError> {
    let mut boards = in_boards
        .iter()
        .map(|v| BingoBoard::new(v.clone()))
        .collect::<Result<Vec<_>, _>>()?;

    for number in numbers {
        for board in &mut boards {
            board.set(*number);
            if board.is_solved() {
                return Ok(board.score() * number);
            }
        }
    }

    Err(AocError::NoSolution)
}

#[aoc(day4, part2)]
fn part02((numbers, in_boards): &(Vec<u32>, Vec<Vec<u32>>)) -> Result<u32, AocError> {
    let mut boards = in_boards
        .iter()
        .map(|v| BingoBoard::new(v.clone()))
        .collect::<Result<Vec<_>, _>>()?;

    for number in numbers {
        for i in 0..boards.len() {
            boards[i].set(*number);
            if boards.iter().all(BingoBoard::is_solved) {
                return Ok(boards[i].score() * number);
            }
        }
    }

    Err(AocError::NoSolution)
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = r"7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7";

    #[test]
    fn test04_generator() {
        let input = generator(INPUT).unwrap();
        assert_eq!(input.0.len(), 27);
        assert_eq!(input.1.len(), 3);
    }

    #[test]
    fn test0401() {
        let input = generator(INPUT).unwrap();
        assert_eq!(part01(&input).unwrap(), 4512);
    }

    #[test]
    fn test0402() {
        let input = generator(INPUT).unwrap();
        assert_eq!(part02(&input).unwrap(), 1924);
    }
}
