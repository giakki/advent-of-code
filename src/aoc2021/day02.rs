/** Correct results:
 * - 1924923
 * - 1982495697
 */
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use nom::{
    character::complete::{alpha1, char},
    combinator::map_res,
    multi::separated_list0,
    sequence::{terminated, tuple},
};
#[derive(Debug, PartialEq, Eq)]
enum Command {
    Forward(i32),
    Down(i32),
    Up(i32),
}

impl Command {
    pub fn from_parsed(dir: &str, amount: i32) -> Result<Self, AocError> {
        match dir {
            "forward" => Ok(Self::Forward(amount)),
            "down" => Ok(Self::Down(amount)),
            "up" => Ok(Self::Up(amount)),
            _ => Err(AocError::ParseError(format!(
                "Cannot parse command '{dir} {amount}'",
            ))),
        }
    }
}

#[aoc_generator(day2)]
fn generator(input: &str) -> Result<Vec<Command>, AocError> {
    input_parser(separated_list0(
        char('\n'),
        map_res(
            tuple((terminated(alpha1, char(' ')), parse_int)),
            |(dir, amount)| Command::from_parsed(dir, amount),
        ),
    ))(input)
}

#[aoc(day2, part1)]
fn part01(input: &[Command]) -> i32 {
    let mut pos = (0, 0);

    for command in input {
        match command {
            Command::Forward(n) => pos.0 += n,
            Command::Down(n) => pos.1 += n,
            Command::Up(n) => pos.1 -= n,
        }
    }

    pos.0 * pos.1
}

#[aoc(day2, part2)]
fn part02(input: &[Command]) -> i32 {
    let mut pos = (0, 0);
    let mut aim = 0;

    for command in input {
        match command {
            Command::Forward(n) => {
                pos.0 += n;
                pos.1 += aim * n;
            }
            Command::Down(n) => aim += n,
            Command::Up(n) => aim -= n,
        }
    }

    pos.0 * pos.1
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test02_generator() {
        let input = generator(
            r"forward 5
down 5
forward 8
up 3
down 8
forward 2",
        )
        .unwrap();

        assert_eq!(
            input,
            vec![
                Command::Forward(5),
                Command::Down(5),
                Command::Forward(8),
                Command::Up(3),
                Command::Down(8),
                Command::Forward(2),
            ]
        );
    }

    #[test]
    fn test0201() {
        let input = generator(
            r"forward 5
down 5
forward 8
up 3
down 8
forward 2",
        )
        .unwrap();
        assert_eq!(150, part01(&input));
    }

    #[test]
    fn test0202() {
        let input = generator(
            r"forward 5
down 5
forward 8
up 3
down 8
forward 2",
        )
        .unwrap();
        assert_eq!(900, part02(&input));
    }
}
