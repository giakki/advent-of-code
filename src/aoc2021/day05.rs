/** Correct results:
 * - 7438
 * - 21406
 */
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_point_comma},
    point::{Point, PointOps},
};
use ndarray::{s, Array2};
use nom::{
    bytes::complete::tag,
    character::complete::char,
    multi::separated_list0,
    sequence::{terminated, tuple},
};
use std::cmp::Ordering;

type Line = (Point<usize>, Point<usize>);

fn build_board(lines: &[Line]) -> Array2<usize> {
    let max_x = lines
        .iter()
        .fold(0, |max, l| max.max(usize::max(l.0.x(), l.1.x())))
        + 1;
    let max_y = lines
        .iter()
        .fold(0, |max, l| max.max(usize::max(l.0.y(), l.1.y())))
        + 1;

    Array2::<usize>::zeros((max_y, max_x))
}

fn draw_axis_line(board: &mut Array2<usize>, line: &Line) {
    let start_y = usize::min(line.0.y(), line.1.y());
    let end_y = usize::max(line.0.y(), line.1.y());
    let start_x = usize::min(line.0.x(), line.1.x());
    let end_x = usize::max(line.0.x(), line.1.x());

    board
        .slice_mut(s![start_y..=end_y, start_x..=end_x])
        .iter_mut()
        .for_each(|n| *n += 1);
}

fn draw_diagonal_line(board: &mut Array2<usize>, line: &Line) {
    let mut iter = line.0;
    while iter.x() != line.1.x() && iter.y() != line.1.y() {
        board[(iter.y(), iter.x())] += 1;

        match iter.x().cmp(&line.1.x()) {
            Ordering::Less => iter.0 += 1,
            Ordering::Greater => iter.0 -= 1,
            Ordering::Equal => {}
        }

        match iter.y().cmp(&line.1.y()) {
            Ordering::Less => iter.1 += 1,
            Ordering::Greater => iter.1 -= 1,
            Ordering::Equal => {}
        }
    }
    board[(iter.y(), iter.x())] += 1;
}

#[aoc_generator(day5)]
fn generator(input: &str) -> Result<Vec<Line>, AocError> {
    input_parser(separated_list0(
        char('\n'),
        tuple((
            terminated(parse_point_comma, tag(" -> ")),
            parse_point_comma,
        )),
    ))(input)
}

#[aoc(day5, part1)]
fn part01(input: &[Line]) -> usize {
    let mut board = build_board(input);

    for line in input {
        if line.0.x() == line.1.x() || line.0.y() == line.1.y() {
            draw_axis_line(&mut board, line);
        }
    }

    board.fold(0, |sum, n| if *n > 1 { sum + 1 } else { sum })
}

#[aoc(day5, part2)]
fn part02(input: &[Line]) -> usize {
    let mut board = build_board(input);

    for line in input {
        if line.0.x() == line.1.x() || line.0.y() == line.1.y() {
            draw_axis_line(&mut board, line);
        } else {
            draw_diagonal_line(&mut board, line);
        }
    }

    board.fold(0, |sum, n| if *n > 1 { sum + 1 } else { sum })
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2";

    #[test]
    fn test05_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test0501() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 5);
    }

    #[test]
    fn test0502() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, 12);
    }
}
