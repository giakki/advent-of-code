/** Correct results:
 * - 36838
 * - 83029436920891
 */
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
    point::PointOps,
};
use nom::{
    bytes::complete::tag,
    character::complete::char,
    combinator::map,
    multi::separated_list0,
    sequence::{preceded, separated_pair, terminated, tuple},
    IResult,
};

#[derive(Debug)]
struct Arcade {
    a: (i32, i32),
    b: (i32, i32),
    prize: (i32, i32),
}

impl Arcade {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        map(
            tuple((
                terminated(
                    preceded(
                        tag("Button A: X+"),
                        separated_pair(parse_int, tag(", Y+"), parse_int),
                    ),
                    char('\n'),
                ),
                terminated(
                    preceded(
                        tag("Button B: X+"),
                        separated_pair(parse_int, tag(", Y+"), parse_int),
                    ),
                    char('\n'),
                ),
                preceded(
                    tag("Prize: X="),
                    separated_pair(parse_int, tag(", Y="), parse_int),
                ),
            )),
            |(a, b, prize)| Self { a, b, prize },
        )(s)
    }
}

#[aoc_generator(day13)]
fn generator(input: &str) -> Result<Vec<Arcade>, AocError> {
    input_parser(separated_list0(tag("\n\n"), Arcade::parse))(input)
}

#[aoc(day13, part1)]
fn part01(input: &[Arcade]) -> i32 {
    let mut result = 0;

    for arcade in input {
        let determinant = arcade.a.x() * arcade.b.y() - arcade.a.y() * arcade.b.x();
        let a = (arcade.prize.x() * arcade.b.y() - arcade.prize.y() * arcade.b.x()) / determinant;
        let b = (arcade.a.x() * arcade.prize.y() - arcade.a.y() * arcade.prize.x()) / determinant;
        if (
            arcade.a.x() * a + arcade.b.x() * b,
            arcade.a.y() * a + arcade.b.y() * b,
        ) == (arcade.prize.x(), arcade.prize.y())
        {
            result += a * 3 + b;
        }
    }

    result
}

#[aoc(day13, part2)]
fn part02(input: &[Arcade]) -> isize {
    let mut result = 0;

    for arcade in input {
        let a = (arcade.a.x() as isize, arcade.a.y() as isize);
        let b = (arcade.b.x() as isize, arcade.b.y() as isize);
        let prize = (
            arcade.prize.x() as isize + 10_000_000_000_000,
            arcade.prize.y() as isize + 10_000_000_000_000,
        );

        let determinant = a.x() * b.y() - a.y() * b.x();
        let mat_a = (prize.x() * b.y() - prize.y() * b.x()) / determinant;
        let mat_b = (a.x() * prize.y() - a.y() * prize.x()) / determinant;
        if (a.x() * mat_a + b.x() * mat_b, a.y() * mat_a + b.y() * mat_b) == (prize.x(), prize.y())
        {
            result += mat_a * 3 + mat_b;
        }
    }

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"Button A: X+94, Y+34
Button B: X+22, Y+67
Prize: X=8400, Y=5400

Button A: X+26, Y+66
Button B: X+67, Y+21
Prize: X=12748, Y=12176

Button A: X+17, Y+86
Button B: X+84, Y+37
Prize: X=7870, Y=6450

Button A: X+69, Y+23
Button B: X+27, Y+71
Prize: X=18641, Y=10279";

    #[test]
    fn test13_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test1301() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 480);
    }

    #[test]
    fn test1302() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, 0);
    }
}
