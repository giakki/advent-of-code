/** Correct results:
 * - 1461806
 * - 887932
 */
use crate::common::{map::Map, point::PointOps};
use itertools::Itertools;

#[aoc_generator(day12)]
fn generator(input: &str) -> Map<u8> {
    let tiles = input
        .lines()
        .map(|line| line.bytes().collect_vec())
        .collect_vec();

    Map::from_vec2(&tiles)
}

#[aoc(day12, part1)]
fn part01(input: &Map<u8>) -> usize {
    let mut map = input.clone();
    let mut result = 0;

    for pos in map.iter_positions() {
        let tile = *map.at(pos);
        if tile == u8::default() {
            continue;
        }

        let mut area = 0;
        let mut perimeter = 0;
        let mut stack = vec![pos];
        while let Some(n_pos) = stack.pop() {
            let n_tile = map.at_mut(n_pos);
            if *n_tile != tile {
                continue;
            }
            *n_tile = u8::default();
            area += 1;

            let neighbors_of_same_type = n_pos
                .cardinal_neighbors_checked(map.width() - 1, map.height() - 1)
                .into_iter()
                .filter(|n| *input.at(*n) == tile)
                .collect_vec();

            perimeter += 4 - neighbors_of_same_type.len();
            stack.extend(neighbors_of_same_type);
        }

        result += area * perimeter;
    }

    result
}

#[aoc(day12, part2)]
fn part02(input: &Map<u8>) -> usize {
    let mut map = input.clone();
    let mut result = 0;

    for pos in map.iter_positions() {
        let tile = *map.at(pos);
        if tile == u8::default() {
            continue;
        }

        let mut area = 0;
        let mut sides = 0;
        let mut stack = vec![pos];
        while let Some(n_pos) = stack.pop() {
            let n_tile = map.at_mut(n_pos);
            if *n_tile != tile {
                continue;
            }
            *n_tile = u8::default();
            area += 1;

            let neighbors_of_same_type = n_pos
                .cardinal_neighbors_checked(map.width() - 1, map.height() - 1)
                .into_iter()
                .filter(|n| *input.at(*n) == tile)
                .collect_vec();

            let ipos = (n_pos.x() as isize, n_pos.y() as isize);
            sides += [
                [
                    (ipos.0, ipos.1 - 1),
                    (ipos.0 + 1, ipos.1 - 1),
                    (ipos.0 + 1, ipos.1),
                ],
                [
                    (ipos.0 + 1, ipos.1),
                    (ipos.0 + 1, ipos.1 + 1),
                    (ipos.0, ipos.1 + 1),
                ],
                [
                    (ipos.0, ipos.1 + 1),
                    (ipos.0 - 1, ipos.1 + 1),
                    (ipos.0 - 1, ipos.1),
                ],
                [
                    (ipos.0 - 1, ipos.1),
                    (ipos.0 - 1, ipos.1 - 1),
                    (ipos.0, ipos.1 - 1),
                ],
            ]
            .into_iter()
            .filter(|[a, b, c]| {
                (input.at_checked(*a) != tile && input.at_checked(*c) != tile)
                    || (input.at_checked(*a) == tile
                        && input.at_checked(*c) == tile
                        && input.at_checked(*b) != tile)
            })
            .count();

            stack.extend(neighbors_of_same_type);
        }

        result += area * sides;
    }

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"RRRRIICCFF
RRRRIICCCF
VVRRRCCFFF
VVRCCCJFFF
VVVVCJJCFE
VVIVCCJJEE
VVIIICJJEE
MIIIIIJJEE
MIIISIJEEE
MMMISSJEEE";

    #[test]
    fn test12_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test1201() {
        let input = generator(BUILD_INPUT);
        let res = part01(&input);

        assert_eq!(res, 1930);
    }

    #[test]
    fn test1202() {
        let input = generator(BUILD_INPUT);
        let res = part02(&input);

        assert_eq!(res, 1206);
    }
}
