/** Correct results:
 * - 6337921897505
 * - 6362722604045
 */
use crate::common::errors::AocError;
use std::iter;

fn generator(input: &str) -> Result<Vec<Option<usize>>, AocError> {
    let mut disk_map = Vec::with_capacity(input.len());

    for (id, chunk) in input.as_bytes().chunks(2).enumerate() {
        match chunk {
            [file, free_space] => {
                disk_map.extend(iter::repeat(Some(id)).take((file - b'0') as usize));
                disk_map.extend(iter::repeat(None).take((free_space - b'0') as usize));
            }
            [file] => disk_map.extend(iter::repeat(Some(id)).take((file - b'0') as usize)),
            _ => return Err(AocError::Input),
        }
    }

    Ok(disk_map)
}

#[aoc(day9, part1)]
fn part01(input: &str) -> Result<usize, AocError> {
    let mut disk_map = generator(input)?;

    let mut block_iter = (0..disk_map.len()).rev();
    'outer: for free_space_idx in 0..disk_map.len() {
        if disk_map[free_space_idx].is_some() {
            continue;
        }

        for block_idx in block_iter.by_ref() {
            if block_idx <= free_space_idx {
                break 'outer;
            }

            if disk_map[block_idx].is_none() {
                continue;
            }

            disk_map[free_space_idx] = disk_map[block_idx];
            disk_map[block_idx] = None;
            break;
        }
    }

    Ok(disk_map
        .into_iter()
        .enumerate()
        .fold(0, |acc, (id, block)| block.map_or(acc, |n| acc + id * n)))
}

#[aoc(day9, part2)]
fn part02(input: &str) -> Result<usize, AocError> {
    let mut disk_map = generator(input)?;

    let mut block_iter = (0..disk_map.len()).rev().peekable();
    while let Some(block_idx_end) = block_iter.next() {
        if disk_map[block_idx_end].is_none() {
            continue;
        }

        let mut block_size = 1;
        while let Some(block_idx_start) = block_iter.peek() {
            if disk_map[block_idx_end] != disk_map[*block_idx_start] {
                break;
            }
            block_size += 1;
            block_iter.next();
        }

        if block_idx_end.saturating_sub(block_size) == 0 {
            break;
        }

        let mut free_space_iter = (0..disk_map.len()).peekable();
        while let Some(free_space_idx_start) = free_space_iter.next() {
            if free_space_idx_start > block_idx_end - block_size {
                break;
            }
            if disk_map[free_space_idx_start].is_some() {
                continue;
            }
            let mut free_space_size = 1;
            while let Some(free_space_idx_end) = free_space_iter.peek() {
                if disk_map[*free_space_idx_end].is_some() {
                    break;
                }
                free_space_size += 1;
                free_space_iter.next();
            }
            if free_space_size < block_size {
                continue;
            }
            for i in 0..block_size {
                disk_map[free_space_idx_start + i] = disk_map[block_idx_end - i];
                disk_map[block_idx_end - i] = None;
            }
            break;
        }
    }

    Ok(disk_map
        .into_iter()
        .enumerate()
        .fold(0, |acc, (id, block)| block.map_or(acc, |n| acc + id * n)))
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"2333133121414131402";
    const BUILD_INPUT_1: &str = r"12345";

    #[test]
    fn test09_gen() {
        let input = generator(BUILD_INPUT_1);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test0901() {
        let res = part01(BUILD_INPUT).unwrap();

        assert_eq!(res, 1928);
    }

    #[test]
    fn test0902() {
        let res = part02(BUILD_INPUT).unwrap();

        assert_eq!(res, 2858);
    }
}
