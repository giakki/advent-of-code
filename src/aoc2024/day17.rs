/** Correct results:
 * - 1,2,3,1,3,2,5,3,1
 * - 105706277661082
 */
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use itertools::Itertools;
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::char,
    combinator::map,
    multi::separated_list1,
    sequence::{preceded, separated_pair, terminated, tuple},
    IResult,
};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct Registers {
    a: usize,
    b: usize,
    c: usize,
}

impl Registers {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        map(
            tuple((
                terminated(preceded(tag("Register A: "), parse_int), char('\n')),
                terminated(preceded(tag("Register B: "), parse_int), char('\n')),
                terminated(preceded(tag("Register C: "), parse_int), char('\n')),
            )),
            |(a, b, c)| Self { a, b, c },
        )(s)
    }

    pub fn get_combo(&self, value: u8) -> usize {
        match value {
            0..=3 => value as usize,
            4 => self.a,
            5 => self.b,
            6 => self.c,
            _ => panic!("Invalid register value: {value}"),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Instruction {
    Adv(u8),
    Bxl(u8),
    Bst(u8),
    Jnz(u8),
    Bxc(u8),
    Out(u8),
    Bdv(u8),
    Cdv(u8),
}

impl Instruction {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        alt((
            map(
                separated_pair(char('0'), char(','), parse_int),
                |(_, op)| Self::Adv(op),
            ),
            map(
                separated_pair(char('1'), char(','), parse_int),
                |(_, op)| Self::Bxl(op),
            ),
            map(
                separated_pair(char('2'), char(','), parse_int),
                |(_, op)| Self::Bst(op),
            ),
            map(
                separated_pair(char('3'), char(','), parse_int),
                |(_, op)| Self::Jnz(op),
            ),
            map(
                separated_pair(char('4'), char(','), parse_int),
                |(_, op)| Self::Bxc(op),
            ),
            map(
                separated_pair(char('5'), char(','), parse_int),
                |(_, op)| Self::Out(op),
            ),
            map(
                separated_pair(char('6'), char(','), parse_int),
                |(_, op)| Self::Bdv(op),
            ),
            map(
                separated_pair(char('7'), char(','), parse_int),
                |(_, op)| Self::Cdv(op),
            ),
        ))(s)
    }

    pub const fn unparse(self) -> [u8; 2] {
        match self {
            Self::Adv(op) => [0, op],
            Self::Bxl(op) => [1, op],
            Self::Bst(op) => [2, op],
            Self::Jnz(op) => [3, op],
            Self::Bxc(op) => [4, op],
            Self::Out(op) => [5, op],
            Self::Bdv(op) => [6, op],
            Self::Cdv(op) => [7, op],
        }
    }
}

fn run_program(mut state: Registers, program: &[Instruction]) -> Vec<u8> {
    let mut result = Vec::new();
    let mut pp = 0;
    while pp < program.len() {
        match program[pp] {
            Instruction::Adv(op) => state.a >>= state.get_combo(op) as u32,
            Instruction::Bxl(op) => state.b ^= op as usize,
            Instruction::Bst(op) => state.b = state.get_combo(op) % 8,
            Instruction::Jnz(op) => {
                if state.a != 0 {
                    pp = op as usize;
                    continue;
                }
            }
            Instruction::Bxc(_) => state.b ^= state.c,
            Instruction::Out(op) => result.push((state.get_combo(op) % 8) as u8),
            Instruction::Bdv(op) => state.b = state.a >> (state.get_combo(op) as u32),
            Instruction::Cdv(op) => state.c = state.a >> (state.get_combo(op) as u32),
        }

        pp += 1;
    }

    result
}

#[aoc_generator(day17)]
fn generator(input: &str) -> Result<(Registers, Vec<Instruction>), AocError> {
    input_parser(separated_pair(
        Registers::parse,
        char('\n'),
        preceded(
            tag("Program: "),
            separated_list1(char(','), Instruction::parse),
        ),
    ))(input)
}

#[aoc(day17, part1)]
fn part01((registers, program): &(Registers, Vec<Instruction>)) -> String {
    run_program(*registers, program)
        .into_iter()
        .map(|c| c.to_string())
        .join(",")
}

#[aoc(day17, part2)]
fn part02((_, program): &(Registers, Vec<Instruction>)) -> Result<usize, AocError> {
    let desired_ouput = program
        .iter()
        .flat_map(|i| i.unparse().into_iter().map(|op| op as usize))
        .collect_vec();

    let mut stack = vec![0];
    for target in desired_ouput.iter().rev() {
        let mut new_stack = Vec::new();
        for item in stack {
            for i in 0..8 {
                /*
                 * 2,4,1,5,7,5,1,6,0,3,4,3,5,5,3,0
                 *
                 * 2,4 -> B = A % 8
                 * 1,5 -> B = B ^ 5
                 * 7,5 -> C = A >> B
                 * 1,6 -> B = B ^ 6
                 * 0,3 -> A = A >> 3   <-- !!!
                 * 4,3 -> B = B ^ C
                 * 5,5 -> Out(B % 8)
                 * 3,0 -> Jnz(A, 0)
                 *
                 * ====================
                 *
                 * ((((A%8)^5)^6)^(A>>((A%8)^5))) % 8
                 *
                 */
                let a = (item << 3_usize) + i;

                let result = ((((a % 8) ^ 5) ^ 6) ^ (a >> ((a % 8) ^ 5))) % 8;

                if result == *target {
                    new_stack.push(a);
                }
            }
        }
        stack = new_stack;
    }

    stack.iter().min().copied().ok_or(AocError::NoSolution)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT_1: &str = r"Register A: 729
Register B: 0
Register C: 0

Program: 0,1,5,4,3,0";

    #[test]
    fn test17_gen() {
        let input = generator(BUILD_INPUT_1);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test1701() {
        let input = generator(BUILD_INPUT_1).unwrap();
        let res = part01(&input);

        assert_eq!(res, "4,6,3,5,6,3,5,2,1,0");
    }
}
