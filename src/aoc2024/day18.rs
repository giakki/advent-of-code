/** Correct results:
 * - 336
 * - 24,30
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
    point::{Point, PointOps},
};
use ndarray::{s, Array2};
use nom::{character::complete::char, sequence::separated_pair};

fn fall_slice(bytes: &[Point<usize>], map_width: usize) -> Array2<bool> {
    let mut map = Array2::from_elem((map_width + 2, map_width + 2), false);
    map.slice_mut(s![1..=map_width, 1..=map_width])
        .assign(&Array2::from_elem((map_width, map_width), true));

    for byte in bytes {
        map[(byte.y() + 1, byte.x() + 1)] = false;
    }

    map
}

fn fall_until(mut map: Array2<bool>, bytes: &[Point<usize>]) -> Result<String, AocError> {
    for byte in bytes {
        map[(byte.y() + 1, byte.x() + 1)] = false;

        if escape(&map).is_none() {
            return Ok(format!("{},{}", byte.0, byte.1));
        }
    }

    Err(AocError::NoSolution)
}

fn escape(map: &Array2<bool>) -> Option<(Vec<Point<usize>>, usize)> {
    let end = (map.dim().x() - 2, map.dim().y() - 2);

    pathfinding::directed::astar::astar(
        &(1, 1),
        |point| {
            point
                .neighbors()
                .into_iter()
                .filter_map(|n| map[n].then_some((n, 1)))
        },
        |res| res.manhattan_distance(&end),
        |res| *res == end,
    )
}

#[aoc_generator(day18)]
fn generator(input: &str) -> Result<Vec<Point<usize>>, AocError> {
    input_parser(by_lines(separated_pair(parse_int, char(','), parse_int)))(input)
}

#[aoc(day18, part1)]
fn part01(input: &[Point<usize>]) -> Result<usize, AocError> {
    let map = fall_slice(&input[..1024], 71);
    let (_, length) = escape(&map).ok_or(AocError::NoSolution)?;

    Ok(length)
}

#[aoc(day18, part2)]
fn part02(input: &[Point<usize>]) -> Result<String, AocError> {
    let map = fall_slice(&input[..1024], 71);
    fall_until(map, &input[1024..])
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"5,4
4,2
4,5
3,0
2,1
6,3
2,4
1,5
0,6
3,3
2,6
5,1
1,2
5,5
2,5
6,5
1,4
0,4
6,4
1,1
6,1
1,0
0,5
1,6
2,0";

    #[test]
    fn test18_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test1801() {
        let input = generator(BUILD_INPUT).unwrap();

        let map = fall_slice(&input[..12], 7);
        let (_, res) = escape(&map).unwrap();

        assert_eq!(res, 22);
    }

    #[test]
    fn test1802() {
        let input = generator(BUILD_INPUT).unwrap();
        let map = fall_slice(&[], 7);

        let res = fall_until(map, &input).unwrap();

        assert_eq!(res, "6,1");
    }
}
