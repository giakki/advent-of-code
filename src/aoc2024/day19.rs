/** Correct results:
 * - 260
 * - 639963796864990
 */
use crate::common::{errors::AocError, parsers::input_parser};
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::char,
    combinator::map,
    multi::{many1, separated_list1},
    sequence::separated_pair,
    IResult,
};
use std::collections::HashMap;

type Input = (Vec<Vec<Color>>, Vec<Vec<Color>>);

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
enum Color {
    White,
    Blue,
    Black,
    Red,
    Green,
}

impl Color {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        alt((
            map(tag("w"), |_| Self::White),
            map(tag("u"), |_| Self::Blue),
            map(tag("b"), |_| Self::Black),
            map(tag("r"), |_| Self::Red),
            map(tag("g"), |_| Self::Green),
        ))(s)
    }
}

fn can_make<'a>(
    towel: &'a [Color],
    patterns: &'_ [Vec<Color>],
    cache: &'_ mut HashMap<&'a [Color], usize>,
) -> usize {
    if towel.is_empty() {
        return 1;
    }

    if let Some(n) = cache.get(towel) {
        return *n;
    }

    let mut possibilities = 0;
    for pattern in patterns {
        if towel.starts_with(pattern) {
            possibilities += can_make(&towel[pattern.len()..], patterns, cache);
        }
    }

    cache.insert(towel, possibilities);
    possibilities
}

#[aoc_generator(day19)]
fn generator(input: &str) -> Result<Input, AocError> {
    input_parser(separated_pair(
        separated_list1(tag(", "), many1(Color::parse)),
        tag("\n\n"),
        separated_list1(char('\n'), many1(Color::parse)),
    ))(input)
}

#[aoc(day19, part1)]
fn part01((patterns, towels): &Input) -> usize {
    towels
        .iter()
        .filter_map(|t| {
            let res = can_make(t, patterns, &mut HashMap::new());

            (res > 0).then_some(true)
        })
        .count()
}

#[aoc(day19, part2)]
fn part02((patterns, towels): &Input) -> usize {
    towels
        .iter()
        .map(|t| can_make(t, patterns, &mut HashMap::new()))
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"r, wr, b, g, bwu, rb, gb, br

brwrr
bggr
gbbr
rrbgbr
ubwu
bwurrg
brgr
bbrgwb";

    #[test]
    fn test19_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test1901() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 6);
    }

    #[test]
    fn test1902() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, 16);
    }
}
