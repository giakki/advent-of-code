/** Correct results:
 * - 217812
 * - 259112729857522
 */
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use nom::{character::complete::space1, multi::separated_list1};
use std::collections::HashMap;

#[aoc_generator(day11)]
fn generator(input: &str) -> Result<Vec<usize>, AocError> {
    input_parser(separated_list1(space1, parse_int))(input)
}

pub fn blink(num: usize, num_iterations: u32, cache: &mut HashMap<(usize, u32), usize>) -> usize {
    if num_iterations == 0 {
        return 1;
    }

    if let Some(&memo) = cache.get(&(num, num_iterations)) {
        return memo;
    }

    if num == 0 {
        let result = blink(1, num_iterations - 1, cache);
        cache.insert((num, num_iterations), result);
        return result;
    }

    let num_digits = num.ilog10() + 1;
    if num_digits % 2 != 0 {
        return blink(num * 2024, num_iterations - 1, cache);
    }

    let div = 10_usize.pow(num_digits / 2);
    let fst = num / div;
    let snd = num % div;
    let result = blink(fst, num_iterations - 1, cache) + blink(snd, num_iterations - 1, cache);
    cache.insert((num, num_iterations), result);
    result
}

#[aoc(day11, part1)]
fn part01(input: &[usize]) -> usize {
    input
        .iter()
        .fold(0, |acc, x| acc + blink(*x, 25, &mut HashMap::new()))
}

#[aoc(day11, part2)]
fn part02(input: &[usize]) -> usize {
    input
        .iter()
        .fold(0, |acc, x| acc + blink(*x, 75, &mut HashMap::new()))
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"125 17";

    #[test]
    fn test11_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test1101() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 55312);
    }

    #[test]
    fn test1102() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, 0);
    }
}
