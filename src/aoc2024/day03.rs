/** Correct results:
 * - 187833789
 * - 94455185
 */
use crate::common::{
    errors::AocError,
    parsers::{input_parser, parse_int},
};
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{anychar, char},
    combinator::map,
    multi::many0,
    sequence::{delimited, separated_pair},
    IResult,
};

#[derive(Debug)]
struct MulInstruction {
    pub a: u32,
    pub b: u32,
}

impl MulInstruction {
    pub fn parse(input: &str) -> IResult<&str, Self> {
        let (s, (a, b)) = delimited(
            tag("mul("),
            separated_pair(parse_int, char(','), parse_int),
            char(')'),
        )(input)?;

        Ok((s, Self { a, b }))
    }
}

#[derive(Debug)]
enum Instruction {
    Mul(MulInstruction),
    Do,
    Dont,
}

impl Instruction {
    pub fn parse(s: &str) -> IResult<&str, Self> {
        alt((
            map(MulInstruction::parse, Self::Mul),
            map(tag("do()"), |_| Self::Do),
            map(tag("don't()"), |_| Self::Dont),
        ))(s)
    }
}

#[aoc_generator(day3)]
fn generator(input: &str) -> Result<Vec<Instruction>, AocError> {
    input_parser(map(
        many0(alt((map(Instruction::parse, Some), map(anychar, |_| None)))),
        |tokens| tokens.into_iter().flatten().collect(),
    ))(input)
}

#[aoc(day3, part1)]
fn part01(input: &[Instruction]) -> u32 {
    input
        .iter()
        .map(|instruction| match instruction {
            Instruction::Mul(mul) => mul.a * mul.b,
            Instruction::Do | Instruction::Dont => 0,
        })
        .sum()
}

#[aoc(day3, part2)]
fn part02(input: &[Instruction]) -> u32 {
    input
        .iter()
        .fold(
            (true, 0),
            |(is_enabled, sum), instruction| match instruction {
                Instruction::Mul(mul) => {
                    if is_enabled {
                        (true, sum + mul.a * mul.b)
                    } else {
                        (false, sum)
                    }
                }
                Instruction::Do => (true, sum),
                Instruction::Dont => (false, sum),
            },
        )
        .1
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT_1: &str =
        r"xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))";

    const BUILD_INPUT_2: &str =
        r"xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))";

    #[test]
    fn test03_gen() {
        let input = generator(BUILD_INPUT_1);
        println!("-- {:?} --", input);

        let input = generator(BUILD_INPUT_2);
        println!("-- {:?} --", input);
    }

    #[test]
    fn test0301() {
        let input = generator(BUILD_INPUT_1).unwrap();
        let res = part01(&input);

        assert_eq!(res, 161);
    }

    #[test]
    fn test0302() {
        let input = generator(BUILD_INPUT_2).unwrap();
        let res = part02(&input);

        assert_eq!(res, 48);
    }
}
