/** Correct results:
 * - 564
 * - 604
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use itertools::Itertools;
use nom::{character::complete::space1, multi::separated_list1};

#[aoc_generator(day2)]
fn generator(input: &str) -> Result<Vec<Vec<u32>>, AocError> {
    input_parser(by_lines(separated_list1(space1, parse_int)))(input)
}

fn is_safe_1(report: &[u32]) -> Result<bool, AocError> {
    let sign;

    if let [a, b, ..] = report {
        sign = a.cmp(b);
    } else {
        return Err(AocError::Input);
    }

    Ok(report.iter().tuple_windows().all(|(a, b)| {
        if a.cmp(b) != sign {
            return false;
        }
        let diff = a.max(b) - a.min(b);
        diff > 0 && diff < 4
    }))
}

fn is_safe_2(report: &[u32]) -> Result<bool, AocError> {
    if is_safe_1(report)? {
        return Ok(true);
    }

    for i in 0..report.len() {
        let dampened = report
            .iter()
            .take(i)
            .chain(report.iter().skip(i + 1))
            .copied()
            .collect::<Vec<_>>();
        if is_safe_1(&dampened)? {
            return Ok(true);
        }
    }

    Ok(false)
}

#[aoc(day2, part1)]
fn part01(input: &[Vec<u32>]) -> Result<u32, AocError> {
    let mut result = 0;

    for report in input {
        if is_safe_1(report)? {
            result += 1;
        }
    }

    Ok(result)
}

#[aoc(day2, part2)]
fn part02(input: &[Vec<u32>]) -> Result<u32, AocError> {
    let mut result = 0;

    for report in input {
        if is_safe_2(report)? {
            result += 1;
        }
    }

    Ok(result)
}

#[cfg(test)]
mod tests {
    use std::u32;

    use super::*;

    const BUILD_INPUT: &str = r"7 6 4 2 1
1 2 7 8 9
9 7 6 2 1
1 3 2 4 5
8 6 4 4 1
1 3 6 7 9";

    #[test]
    fn test02_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test0201() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input).unwrap_or(u32::MAX);

        assert_eq!(res, 2);
    }

    #[test]
    fn test0202() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input).unwrap_or(u32::MAX);

        assert_eq!(res, 4);
    }
}
