/** Correct results:
 * - 652
 * - 1432
 */
use crate::common::{
    direction::Direction,
    errors::AocError,
    point::{Point, PointOps},
};
use itertools::Itertools;
use std::collections::HashSet;

#[derive(Debug)]
struct InputView {
    input: Vec<u8>,
    width: usize,
    height: usize,
}

impl InputView {
    pub fn from(input: &str) -> Result<Self, AocError> {
        let widths = input.lines().map(str::len).collect_vec();
        if widths
            .iter()
            .any(|line| line != widths.first().unwrap_or(&0))
        {
            return Err(AocError::Input);
        }

        Ok(Self {
            input: input.bytes().collect(),
            height: widths.len(),
            width: widths.into_iter().max().unwrap_or(0),
        })
    }

    pub fn at(&self, (x, y): Point<isize>) -> u8 {
        if (x < 0 || x >= self.width as isize) || (y < 0 || y >= self.height as isize) {
            return u8::MAX;
        }

        self.input[y as usize * (self.width + 1) + x as usize]
    }

    pub fn trailheads(&self) -> impl Iterator<Item = (isize, isize)> + use<'_> {
        (0..self.width as isize)
            .cartesian_product(0..self.height as isize)
            .filter(move |(x, y)| self.at((*x, *y)) == b'0')
    }
}

fn walk(
    pos: Point<isize>,
    input: &InputView,
    reachables: &mut Option<HashSet<Point<isize>>>,
) -> usize {
    let height = input.at(pos);
    if height == b'9' {
        if let Some(rs) = reachables {
            if rs.contains(&pos) {
                return 0;
            }
            rs.insert(pos);
            return 1;
        }

        return 1;
    }

    [
        Direction::Up,
        Direction::Right,
        Direction::Down,
        Direction::Left,
    ]
    .into_iter()
    .filter_map(|dir| {
        let next_pos = pos.moved(1, &dir);

        let next_height = input.at(next_pos);
        if next_height != height + 1 {
            return None;
        }

        Some(walk(next_pos, input, reachables))
    })
    .sum()
}

#[aoc_generator(day10)]
fn generator(input: &str) -> Result<InputView, AocError> {
    InputView::from(input)
}

#[aoc(day10, part1)]
fn part01(input: &InputView) -> usize {
    input
        .trailheads()
        .map(|t| walk(t, input, &mut Some(HashSet::new())))
        .sum()
}

#[aoc(day10, part2)]
fn part02(input: &InputView) -> usize {
    input.trailheads().map(|t| walk(t, input, &mut None)).sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"89010123
78121874
87430965
96549874
45678903
32019012
01329801
10456732";

    #[test]
    fn test10_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test1001() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 36);
    }

    #[test]
    fn test1002() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, 81);
    }
}
