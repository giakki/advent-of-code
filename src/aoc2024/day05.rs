/** Correct results:
 * - 6949
 * - 4145
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use itertools::Itertools;
use nom::{
    bytes::complete::tag, character::complete::char, combinator::map, multi::separated_list1,
    sequence::separated_pair,
};
use std::collections::HashMap;

#[derive(Debug)]
struct Input {
    pub rules: HashMap<u32, Vec<u32>>,
    pub updates: Vec<Vec<u32>>,
}

impl Input {
    pub fn new(rules_vec: Vec<(u32, u32)>, updates: Vec<Vec<u32>>) -> Self {
        let mut rules = HashMap::with_capacity(rules_vec.len());
        for (fst, snd) in rules_vec {
            rules.entry(fst).or_insert_with(Vec::new).push(snd);
        }

        Self { rules, updates }
    }
}

#[aoc_generator(day5)]
fn generator(input: &str) -> Result<Input, AocError> {
    input_parser(map(
        separated_pair(
            by_lines(separated_pair(parse_int, char('|'), parse_int)),
            tag("\n\n"),
            by_lines(separated_list1(char(','), parse_int)),
        ),
        |(rules, updates)| Input::new(rules, updates),
    ))(input)
}

fn is_update_valid(update: &[u32], rules: &HashMap<u32, Vec<u32>>) -> bool {
    for (idx_fst, fst) in update.iter().enumerate() {
        if idx_fst >= update.len() - 1 {
            continue;
        }
        let mut valid_page = true;
        'check_valid_page: for snd in update.iter().skip(idx_fst + 1) {
            if let Some(rule) = rules.get(snd) {
                if rule.contains(fst) {
                    valid_page = false;
                    break 'check_valid_page;
                }
            }
        }

        if !valid_page {
            return false;
        }
    }

    true
}

#[aoc(day5, part1)]
fn part01(input: &Input) -> u32 {
    let mut valid_updates_score = 0;
    for update in &input.updates {
        if is_update_valid(update, &input.rules) {
            valid_updates_score += update.get(update.len() / 2).unwrap_or(&0);
        }
    }

    valid_updates_score
}

#[aoc(day5, part2)]
fn part02(input: &Input) -> u32 {
    input
        .updates
        .iter()
        .filter(|update| !is_update_valid(update, &input.rules))
        .map(|update| {
            update
                .iter()
                .sorted_unstable_by(|a, b| {
                    if let Some(rule) = input.rules.get(a) {
                        if rule.contains(b) {
                            return std::cmp::Ordering::Less;
                        }
                    } else if let Some(rule) = input.rules.get(b) {
                        if rule.contains(a) {
                            return std::cmp::Ordering::Greater;
                        }
                    } else {
                        // noop
                    }
                    std::cmp::Ordering::Equal
                })
                .nth(update.len() / 2)
                .unwrap_or(&0)
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"47|53
97|13
97|61
97|47
75|29
61|13
75|53
29|13
97|29
53|29
61|53
97|53
61|29
47|13
75|47
97|75
47|61
75|61
47|29
75|13
53|13

75,47,61,53,29
97,61,53,29,13
75,29,13
75,97,47,61,53
61,13,29
97,13,75,29,47";

    #[test]
    fn test05_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test0501() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 143);
    }

    #[test]
    fn test0502() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, 123);
    }
}
