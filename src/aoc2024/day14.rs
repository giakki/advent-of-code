/** Correct results:
 * - 220971520
 * - 6355
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int, parse_signed_int},
    point::PointOps,
};
use nom::combinator::map;
use nom::{
    bytes::complete::tag,
    character::complete::char,
    sequence::{preceded, separated_pair},
    IResult,
};
use std::collections::HashSet;

#[derive(Debug, Clone)]
struct Robot {
    position: (i32, i32),
    velocity: (i32, i32),
}

impl Robot {
    pub fn parse(input: &str) -> IResult<&str, Self> {
        map(
            separated_pair(
                preceded(tag("p="), separated_pair(parse_int, char(','), parse_int)),
                tag(" v="),
                separated_pair(parse_signed_int, char(','), parse_signed_int),
            ),
            |(initial_pos, velocity)| Self {
                position: initial_pos,
                velocity,
            },
        )(input)
    }
}

#[aoc_generator(day14)]
fn generator(input: &str) -> Result<Vec<Robot>, AocError> {
    input_parser(by_lines(Robot::parse))(input)
}

#[aoc(day14, part1)]
fn part01(input: &[Robot]) -> usize {
    let width = 101;
    let height = 103;
    let mut robots = input.to_vec();

    for _ in 0..100 {
        for robot in &mut robots {
            robot.position = (
                (robot.position.x() + robot.velocity.x()).rem_euclid(width),
                (robot.position.y() + robot.velocity.y()).rem_euclid(height),
            );
        }
    }

    let mut quadrants = [0, 0, 0, 0];
    for robot in robots {
        let x = robot.position.x();
        let y = robot.position.y();

        if x < width / 2 && y < height / 2 {
            quadrants[0] += 1;
        } else if x > width / 2 && y < height / 2 {
            quadrants[1] += 1;
        } else if x < width / 2 && y > height / 2 {
            quadrants[2] += 1;
        } else if x > width / 2 && y > height / 2 {
            quadrants[3] += 1;
        } else {
            // noop
        }
    }

    quadrants.iter().product()
}

#[aoc(day14, part2)]
fn part02(input: &[Robot]) -> Result<usize, AocError> {
    let width = 101_i32;
    let height = 103_i32;
    let mut robots = input.to_vec();
    let mut set = HashSet::with_capacity(robots.len());

    for i in 0.. {
        for robot in &mut robots {
            robot.position = (
                (robot.position.x() + robot.velocity.x()).rem_euclid(width),
                (robot.position.y() + robot.velocity.y()).rem_euclid(height),
            );
        }

        set.clear();
        set.extend(robots.iter().map(|robot| robot.position));
        if set.len() == robots.len() {
            return Ok(i + 1);
        }
    }

    Err(AocError::NoSolution)
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"p=0,4 v=3,-3
p=6,3 v=-1,-3
p=10,3 v=-1,2
p=2,0 v=2,-1
p=0,0 v=1,3
p=3,0 v=-2,-2
p=7,6 v=-1,-3
p=3,0 v=-1,-2
p=9,3 v=2,3
p=7,3 v=-1,2
p=2,4 v=2,-3
p=9,5 v=-3,-3";

    #[test]
    fn test14_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test1401() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 21);
    }
}
