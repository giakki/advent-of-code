/** Correct results:
 * - 252
 * - 839
 */
use crate::common::point::{Point, PointOps};
use itertools::Itertools;
use std::collections::{HashMap, HashSet};

#[derive(Debug)]
struct Map {
    pub locations: HashMap<u8, Vec<Point<usize>>>,
    pub width: usize,
    pub height: usize,
}

#[aoc_generator(day8)]
fn generator(input: &str) -> Map {
    let mut locations = HashMap::new();
    let height = input.lines().count();
    let mut width = 0;

    for (y, row) in input.lines().enumerate() {
        width = width.max(row.len());
        for (x, b) in row.bytes().enumerate() {
            if b.is_ascii_alphanumeric() {
                locations.entry(b).or_insert(Vec::new()).push((x, y));
            }
        }
    }

    Map {
        locations,
        width,
        height,
    }
}

#[aoc(day8, part1)]
fn part01(input: &Map) -> usize {
    let mut antinodes = HashSet::new();

    for locations in input.locations.values() {
        for (fst, snd) in locations.iter().tuple_combinations() {
            let (a, b) = if fst.cmp_reading(snd) == std::cmp::Ordering::Less {
                (fst, snd)
            } else {
                (snd, fst)
            };

            let dx = a.x() as isize - b.x() as isize;
            let dy = a.y() as isize - b.y() as isize;

            let points = [
                (a.x() as isize + dx, a.y() as isize + dy),
                (a.x() as isize + dx, a.y() as isize + dy),
                (b.x() as isize - dx, b.y() as isize - dy),
                (b.x() as isize - dx, b.y() as isize - dy),
            ]
            .into_iter()
            .filter(|(x, y)| {
                *x >= 0
                    && *x < input.width as isize
                    && *y >= 0
                    && *y < input.height as isize
                    && (*x as usize, *y as usize) != *a
                    && (*x as usize, *y as usize) != *b
            })
            .map(|(x, y)| (x as usize, y as usize));

            antinodes.extend(points);
        }
    }

    antinodes.len()
}

#[aoc(day8, part2)]
fn part02(input: &Map) -> usize {
    let mut antinodes = HashSet::new();

    for locations in input.locations.values() {
        for (fst, snd) in locations.iter().tuple_combinations() {
            let (a, b) = if fst.cmp_reading(snd) == std::cmp::Ordering::Less {
                (fst, snd)
            } else {
                (snd, fst)
            };
            let dx = a.x() as isize - b.x() as isize;
            let dy = a.y() as isize - b.y() as isize;

            let mut iter = (a.x() as isize, a.y() as isize);
            while iter.x() < input.width as isize
                && iter.y() < input.height as isize
                && iter.x() >= 0
                && iter.y() >= 0
            {
                antinodes.insert((iter.x() as usize, iter.y() as usize));
                iter = (iter.x() + dx, iter.y() + dy);
            }

            iter = (a.x() as isize, a.y() as isize);
            while iter.x() < input.width as isize
                && iter.y() < input.height as isize
                && iter.x() >= 0
                && iter.y() >= 0
            {
                antinodes.insert((iter.x() as usize, iter.y() as usize));
                iter = (iter.x() - dx, iter.y() - dy);
            }
        }
    }

    antinodes.len()
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"............
........0...
.....0......
.......0....
....0.......
......A.....
............
............
........A...
.........A..
............
............";

    #[test]
    fn test08_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test0801() {
        let input = generator(BUILD_INPUT);
        let res = part01(&input);

        assert_eq!(res, 14);
    }

    #[test]
    fn test0802() {
        let input = generator(BUILD_INPUT);
        let res = part02(&input);

        assert_eq!(res, 34);
    }
}
