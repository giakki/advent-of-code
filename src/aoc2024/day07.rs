/** Correct results:
 * - 1708857123053
 * - 189207836795655
 */
use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use nom::{
    bytes::complete::tag, character::complete::char, multi::separated_list1,
    sequence::separated_pair,
};

fn can_evaluate(result: usize, operands: &[usize], has_concat: bool) -> bool {
    if operands.len() == 1 {
        return result == operands[0];
    }

    let last = if let Some(last) = operands.last() {
        *last
    } else {
        return false;
    };
    let rest = &operands[..operands.len() - 1];

    let can_mul = if result % last == 0 {
        can_evaluate(result / last, rest, has_concat)
    } else {
        false
    };

    let can_concat = if has_concat {
        let num_digits = 10_usize.pow(1 + last.checked_ilog10().unwrap_or_default());
        if (result.wrapping_sub(last)) % num_digits == 0 {
            can_evaluate((result.wrapping_sub(last)) / num_digits, rest, has_concat)
        } else {
            false
        }
    } else {
        false
    };

    let can_add = can_evaluate(result.wrapping_sub(last), rest, has_concat);

    can_mul || can_concat || can_add
}

#[aoc_generator(day7)]
fn generator(input: &str) -> Result<Vec<(usize, Vec<usize>)>, AocError> {
    input_parser(by_lines(separated_pair(
        parse_int,
        tag(": "),
        separated_list1(char(' '), parse_int),
    )))(input)
}

#[aoc(day7, part1)]
fn part01(input: &[(usize, Vec<usize>)]) -> usize {
    input
        .iter()
        .filter_map(|(result, operands)| can_evaluate(*result, operands, false).then_some(result))
        .sum()
}

#[aoc(day7, part2)]
fn part02(input: &[(usize, Vec<usize>)]) -> usize {
    input
        .iter()
        .filter_map(|(result, operands)| can_evaluate(*result, operands, true).then_some(result))
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"190: 10 19
3267: 81 40 27
83: 17 5
156: 15 6
7290: 6 8 6 15
161011: 16 10 13
192: 17 8 14
21037: 9 7 18 13
292: 11 6 16 20";

    #[test]
    fn test07_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test0701() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 3749);
    }

    #[test]
    fn test0702() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, 11387);
    }
}
