/** Correct results:
 * - 4580
 * - 1480
 */
use crate::common::{
    direction::Direction,
    errors::AocError,
    map::Map,
    point::{Point, PointOps},
};
use ndarray::Array2;
use rayon::iter::{ParallelBridge, ParallelIterator};
use std::iter;

#[derive(Debug, Clone, Copy)]
enum Tile {
    NotVisited,
    Visited([bool; 4]),
    Obstacle,
    Outside,
}

impl TryFrom<u8> for Tile {
    type Error = AocError;

    fn try_from(byte: u8) -> Result<Self, AocError> {
        match byte {
            b'#' => Ok(Self::Obstacle),
            b'.' | b'^' | b'>' | b'v' | b'<' => Ok(Self::NotVisited),
            b' ' => Ok(Self::Outside),
            _ => Err(AocError::ParseError(format!("Cannot parse tile '{byte}'",))),
        }
    }
}

#[derive(Debug)]
struct Lab {
    map: Map<Tile>,
    guard_pos: Point<usize>,
    guard_dir: Direction,
}

enum RunResult {
    Outside,
    Loop,
}

impl Lab {
    fn run(&mut self) -> RunResult {
        loop {
            let dir_idx = match self.guard_dir {
                Direction::Up => 0,
                Direction::Right => 1,
                Direction::Down => 2,
                Direction::Left => 3,
            };

            match self.map.at_mut(self.guard_pos) {
                Tile::NotVisited => {
                    let mut visits = [false; 4];
                    visits[dir_idx] = true;

                    *self.map.at_mut(self.guard_pos) = Tile::Visited(visits);
                }
                Tile::Visited(ref mut visited) => {
                    visited[dir_idx] = true;
                }
                _ => {
                    panic!("Cannot visit tile");
                }
            }

            let next_pos = self.guard_pos.moved(1, &self.guard_dir);

            match self.map.at(next_pos) {
                Tile::NotVisited => {
                    self.guard_pos = next_pos;
                }
                Tile::Visited(dirs) => {
                    if dirs[dir_idx] {
                        return RunResult::Loop;
                    }
                    self.guard_pos = next_pos;
                }
                Tile::Obstacle => {
                    self.guard_dir = self.guard_dir.rotate_cw();
                }
                Tile::Outside => {
                    return RunResult::Outside;
                }
            }
        }
    }
}

#[aoc_generator(day6)]
fn generator(input: &str) -> Result<Lab, AocError> {
    let width = input.lines().next().ok_or(AocError::Input)?.len();
    let height = input.lines().count();

    let tiles = iter::repeat(b' ')
        .take(width + 2)
        .chain(
            input
                .lines()
                .flat_map(|line| iter::once(b' ').chain(line.bytes()).chain(iter::once(b' '))),
        )
        .chain(iter::repeat(b' ').take(width + 2))
        .map(Tile::try_from)
        .collect::<Result<Vec<_>, AocError>>()?;

    let (guard_pos, guard_dir) = input
        .bytes()
        .filter(|tile| *tile != b'\n')
        .enumerate()
        .find_map(|(idx, tile)| {
            let x = (idx % width) + 1;
            let y = (idx / width) + 1;

            if let Ok(dir) = Direction::try_from_arrow(&tile) {
                return Some(((x, y), dir));
            }
            None
        })
        .ok_or(AocError::Input)?;

    Ok(Lab {
        map: Map::from_ndarray(
            Array2::from_shape_vec((width + 2, height + 2), tiles)?.reversed_axes(),
        ),
        guard_pos,
        guard_dir,
    })
}

#[aoc(day6, part1)]
fn part01(input: &Lab) -> usize {
    let mut lab = Lab {
        map: input.map.clone(),
        guard_pos: input.guard_pos,
        guard_dir: input.guard_dir,
    };

    let _ = lab.run();

    lab.map
        .iter_positions()
        .filter(|pos| {
            if let Tile::Visited(_) = lab.map.at(*pos) {
                return true;
            }
            false
        })
        .count()
}

#[aoc(day6, part2)]
fn part02(input: &Lab) -> usize {
    let mut visited_lab = Lab {
        map: input.map.clone(),
        guard_pos: input.guard_pos,
        guard_dir: input.guard_dir,
    };

    let _ = visited_lab.run();

    visited_lab
        .map
        .iter_positions()
        .par_bridge()
        .filter(|pos| {
            if matches!(visited_lab.map.at(*pos), Tile::NotVisited) {
                return false;
            }
            if *pos == input.guard_pos {
                return false;
            }

            let mut lab = Lab {
                map: input.map.clone(),
                guard_pos: input.guard_pos,
                guard_dir: input.guard_dir,
            };

            *lab.map.at_mut(*pos) = Tile::Obstacle;
            if matches!(lab.run(), RunResult::Loop) {
                return true;
            }

            false
        })
        .count()
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"....#.....
.........#
..........
..#.......
.......#..
..........
.#..^.....
........#.
#.........
......#...";

    #[test]
    fn test06_gen() {
        let input = generator(BUILD_INPUT).unwrap();

        println!("{:?}", input)
    }

    #[test]
    fn test0601() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 41);
    }

    #[test]
    fn test0602() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, 6);
    }
}
