/** Correct results:
 * - 2578
 * - 1972
 */
use crate::common::{errors::AocError, point::PointOps};
use ndarray::{Array, Array2};
use std::iter;

const DIRECTIONS: [(i32, i32); 8] = [
    (1, 0),
    (-1, 0),
    (0, 1),
    (0, -1),
    (1, 1),
    (-1, 1),
    (1, -1),
    (-1, -1),
];
const SEARCH: [(i32, u8); 4] = [(0, b'X'), (1, b'M'), (2, b'A'), (3, b'S')];

#[aoc_generator(day4)]
fn generator(input: &str) -> Result<Array2<u8>, AocError> {
    let width = input.lines().next().ok_or(AocError::Input)?.len();
    let height = input.lines().count();

    let iter = iter::repeat(b'.')
        .take(width + 2)
        .chain(
            input
                .lines()
                .flat_map(|line| iter::once(b'.').chain(line.bytes()).chain(iter::once(b'.'))),
        )
        .chain(iter::repeat(b'.').take(width + 2));

    Array::from_iter(iter)
        .into_shape_with_order((width + 2, height + 2))
        .map_err(|_| AocError::Input)
}

#[aoc(day4, part1)]
fn part01(input: &Array2<u8>) -> usize {
    input
        .indexed_iter()
        .map(|(p, _)| {
            let pos = (p.x() as i32, p.y() as i32);

            DIRECTIONS
                .iter()
                .filter(|dir| {
                    SEARCH.iter().all(|(step, expected)| {
                        let next = (
                            (pos.x() + step * dir.x()) as usize,
                            (pos.y() + step * dir.y()) as usize,
                        );
                        input[next] == *expected
                    })
                })
                .count()
        })
        .sum()
}

#[aoc(day4, part2)]
fn part02(input: &Array2<u8>) -> usize {
    input
        .windows((3, 3))
        .into_iter()
        .filter(|window| {
            if window[(1, 1)] != b'A' {
                return false;
            }

            let top_left = window[(0, 0)];
            let top_right = window[(2, 0)];
            let bottom_left = window[(0, 2)];
            let bottom_right = window[(2, 2)];

            ((top_left == b'M' && bottom_right == b'S')
                || (top_left == b'S' && bottom_right == b'M'))
                && ((top_right == b'M' && bottom_left == b'S')
                    || (top_right == b'S' && bottom_left == b'M'))
        })
        .count()
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"MMMSXXMASM
MSAMXMSMSA
AMXSXMAAMM
MSAMASMSMX
XMASAMXAMM
XXAMMXXAMA
SMSMSASXSS
SAXAMASAAA
MAMMMXMMMM";

    #[test]
    fn test04_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test0401() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 18);
    }

    #[test]
    fn test0402() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, 9);
    }
}
