/** Correct results:
 * - 1426855
 * - 1404917
 */
use crate::common::{
    chr::FromChar,
    direction::Direction,
    errors::AocError,
    map::Map,
    point::{Point, PointOps},
};
use itertools::Itertools;
use ndarray::Array2;
use std::{collections::HashSet, str::FromStr};

type Input = (Map<Tile>, Vec<Direction>, Point<usize>);

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Tile {
    Wall,
    Empty,
    Box,
    BigBoxL,
    BigBoxR,
    Robot,
}

impl FromChar for Tile {
    type Err = AocError;

    fn from_char(c: &char) -> Result<Self, Self::Err> {
        match c {
            '#' => Ok(Self::Wall),
            '.' => Ok(Self::Empty),
            'O' => Ok(Self::Box),
            '@' => Ok(Self::Robot),
            _ => Err(AocError::ParseError(format!("Cannot parse tile '{c}'"))),
        }
    }
}

fn run_robot(map: &mut Map<Tile>, path: &[Direction], mut robot_pos: Point<usize>) {
    let mut pos_stack = Vec::new();
    let mut to_move = HashSet::new();
    'outer: for direction in path {
        pos_stack.clear();
        pos_stack.push(robot_pos);
        to_move.clear();

        while let Some(pos) = pos_stack.pop() {
            if !to_move.insert(pos) {
                continue;
            }
            let next_pos = pos.moved(1, direction);
            match map.at(next_pos) {
                Tile::Wall => continue 'outer,
                Tile::Box => pos_stack.push(next_pos),
                Tile::BigBoxL => pos_stack.extend([next_pos, (next_pos.x() + 1, next_pos.y())]),
                Tile::BigBoxR => pos_stack.extend([next_pos, (next_pos.x() - 1, next_pos.y())]),
                _ => continue,
            }
        }
        while !to_move.is_empty() {
            for pos in to_move.iter().copied().collect_vec() {
                let next_pos = pos.moved(1, direction);
                if !to_move.contains(&next_pos) {
                    *map.at_mut(next_pos) = *map.at(pos);
                    *map.at_mut(pos) = Tile::Empty;
                    to_move.remove(&pos);
                }
            }
        }
        robot_pos.move_by(1, direction);
    }
}

#[aoc_generator(day15)]
fn generator(input: &str) -> Result<Input, AocError> {
    if let [m, p] = input.split("\n\n").collect_vec().as_slice() {
        let mut map = Map::from_str(m)?;

        let path = p
            .chars()
            .filter(|c| *c != '\n')
            .map(|c| Direction::from_arrow(&c))
            .collect::<Result<Vec<_>, _>>()?;

        let robot_pos = map
            .iter_positions()
            .find(|pos| *map.at(*pos) == Tile::Robot)
            .ok_or(AocError::Input)?;
        *map.at_mut(robot_pos) = Tile::Empty;

        Ok((map, path, robot_pos))
    } else {
        Err(AocError::ParseError("Invalid input".to_owned()))
    }
}

#[aoc(day15, part1)]
fn part01((input_map, path, robot_pos): &Input) -> usize {
    let mut map = input_map.clone();
    run_robot(&mut map, path, *robot_pos);

    map.iter_positions()
        .filter_map(|pos| match map.at(pos) {
            Tile::Box => Some(100 * pos.y() + pos.x()),
            _ => None,
        })
        .sum::<usize>()
}

#[aoc(day15, part2)]
fn part02((initial_map, path, robot_pos): &Input) -> usize {
    let (mut map, robot_pos_n) = {
        let mut new_map = Map::from_ndarray(Array2::from_elem(
            (initial_map.width() * 2, initial_map.height()),
            Tile::Empty,
        ));

        for pos in initial_map.iter_positions() {
            let new_pos = (pos.x() * 2, pos.y());
            match initial_map.at(pos) {
                Tile::Box => {
                    *new_map.at_mut(new_pos) = Tile::BigBoxL;
                    *new_map.at_mut((new_pos.x() + 1, new_pos.y())) = Tile::BigBoxR;
                }
                other => {
                    *new_map.at_mut(new_pos) = *other;
                    *new_map.at_mut((new_pos.x() + 1, new_pos.y())) = *other;
                }
            }
        }
        (new_map, (robot_pos.x() * 2, robot_pos.y()))
    };

    run_robot(&mut map, path, robot_pos_n);

    map.iter_positions()
        .filter_map(|pos| match map.at(pos) {
            Tile::BigBoxL => Some(100 * pos.y() + pos.x()),
            Tile::Wall | Tile::Empty | Tile::Box | Tile::BigBoxR | Tile::Robot => None,
        })
        .sum::<usize>()
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"##########
#..O..O.O#
#......O.#
#.OO..O.O#
#..O@..O.#
#O#..O...#
#O..O..O.#
#.OO.O.OO#
#....O...#
##########

<vv>^<v^>v>^vv^v>v<>v^v<v<^vv<<<^><<><>>v<vvv<>^v^>^<<<><<v<<<v^vv^v>^
vvv<<^>^v^^><<>>><>^<<><^vv^^<>vvv<>><^^v>^>vv<>v<<<<v<^v>^<^^>>>^<v<v
><>vv>v^v^<>><>>>><^^>vv>v<^^^>>v^v^<^^>v^^>v^<^v>v<>>v^v^<v>v^^<^^vv<
<<v<^>>^^^^>>>v^<>vvv^><v<<<>^^^vv^<vvv>^>v<^^^^v<>^>vvvv><>>v^<<^^^^^
^><^><>>><>^^<<^^v>>><^<v>^<vv>>v>>>^v><>^v><<<<v>>v<v<v>vvv>^<><<>^><
^>><>^v<><^vvv<^^<><v<<<<<><^v<<<><<<^^<v<^^^><^>>^<v^><<<^>>^v<v^v<v^
>^>>^v>vv>^<<^v<>><<><<v<<v><>v<^vv<<<>^^v^>^^>>><<^v>>v^v><^^>>^<>vv^
<><^^>^^^<><vvvvv^v<v<<>^v<v>v<<^><<><<><<<^^<<<^<<>><<><^^^>^^<>^>v<>
^^>vv<^v^v<vv>^<><v<^v>^^^>>>^^vvv^>vvv<>>>^<^>>>>>^<<^v>^vvv<>^<><<v>
v^^>>><<^^<>>^v^<v^vv<>v^<<>^<^v^v><^<<<><<^<v><v<>vv>>v><v^<vv<>v^<<^";

    #[test]
    fn test15_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test1501() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 10092);
    }

    #[test]
    fn test1502() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, 9021);
    }
}
