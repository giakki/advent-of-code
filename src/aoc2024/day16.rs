/** Correct results:
 * - 106512
 * - 563
 */
use crate::common::{
    chr::FromChar,
    direction::Direction,
    errors::AocError,
    map::Map,
    point::{Point, PointOps},
};
use itertools::Itertools;
use pathfinding::prelude::{astar_bag, AstarSolution};
use std::{collections::HashSet, str::FromStr};

type Input = (Map<Tile>, Point<usize>, Point<usize>);

type Solution = Result<(AstarSolution<((usize, usize), Direction)>, usize), AocError>;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Tile {
    Wall,
    Empty,
    Start,
    End,
}

impl FromChar for Tile {
    type Err = AocError;

    fn from_char(c: &char) -> Result<Self, Self::Err> {
        match c {
            '#' => Ok(Self::Wall),
            '.' => Ok(Self::Empty),
            'S' => Ok(Self::Start),
            'E' => Ok(Self::End),
            _ => Err(AocError::ParseError(format!("Cannot parse tile '{c}'"))),
        }
    }
}

fn solve((map, start, end): &Input) -> Solution {
    astar_bag(
        &(*start, Direction::Right),
        |(pos, dir)| {
            [
                (1, *dir),
                (1001, dir.rotate_cw()),
                (1001, dir.rotate_ccw()),
                (2001, dir.flip()),
            ]
            .into_iter()
            .filter_map(|(points, next_dir)| {
                let next_pos = pos.moved(1, &next_dir);
                match map.at(next_pos) {
                    Tile::Empty => Some(((next_pos, next_dir), points)),
                    Tile::Wall | Tile::Start | Tile::End => None,
                }
            })
            .collect_vec()
        },
        |(pos, _)| pos.manhattan_distance(end),
        |(pos, _)| pos == end,
    )
    .ok_or(AocError::NoSolution)
}

#[aoc_generator(day16)]
fn generator(input: &str) -> Result<Input, AocError> {
    let mut map = Map::from_str(input)?;

    let start = map
        .tiles
        .indexed_iter()
        .find(|(_, tile)| **tile == Tile::Start)
        .map(|(pos, _)| pos)
        .ok_or(AocError::Input)?;
    *map.at_mut(start) = Tile::Empty;

    let end = map
        .tiles
        .indexed_iter()
        .find(|(_, tile)| **tile == Tile::End)
        .map(|(pos, _)| pos)
        .ok_or(AocError::Input)?;
    *map.at_mut(end) = Tile::Empty;

    Ok((map, start, end))
}

#[aoc(day16, part1)]
fn part01(input: &Input) -> Result<usize, AocError> {
    let (_, cost) = solve(input)?;

    Ok(cost)
}

#[aoc(day16, part2)]
fn part02(input: &Input) -> Result<usize, AocError> {
    let (solution, _) = solve(input)?;

    let best_tiles = solution
        .into_iter()
        .flat_map(|path| path.into_iter().map(|(pos, _)| pos))
        .collect::<HashSet<_>>();

    Ok(best_tiles.len())
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT_1: &str = r"###############
#.......#....E#
#.#.###.#.###.#
#.....#.#...#.#
#.###.#####.#.#
#.#.#.......#.#
#.#.#####.###.#
#...........#.#
###.#.#####.#.#
#...#.....#.#.#
#.#.#.###.#.#.#
#.....#...#.#.#
#.###.#.#.#.#.#
#S..#.....#...#
###############";

    const BUILD_INPUT_2: &str = r"#################
#...#...#...#..E#
#.#.#.#.#.#.#.#.#
#.#.#.#...#...#.#
#.#.#.#.###.#.#.#
#...#.#.#.....#.#
#.#.#.#.#.#####.#
#.#...#.#.#.....#
#.#.#####.#.###.#
#.#.#.......#...#
#.#.###.#####.###
#.#.#...#.....#.#
#.#.#.#####.###.#
#.#.#.........#.#
#.#.#.#########.#
#S#.............#
#################";

    #[test]
    fn test16_gen() {
        let input = generator(BUILD_INPUT_1);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test1601() {
        let input = generator(BUILD_INPUT_1).unwrap();
        let res = part01(&input).unwrap();
        assert_eq!(res, 7036);

        let input = generator(BUILD_INPUT_2).unwrap();
        let res = part01(&input).unwrap();
        assert_eq!(res, 11048);
    }

    #[test]
    fn test1602() {
        let input = generator(BUILD_INPUT_1).unwrap();
        let res = part02(&input).unwrap();
        assert_eq!(res, 45);

        let input = generator(BUILD_INPUT_2).unwrap();
        let res = part02(&input).unwrap();
        assert_eq!(res, 64);
    }
}
