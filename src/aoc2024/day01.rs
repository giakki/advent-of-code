/** Correct results:
 * - 2970687
 * - 23963899
 */
use std::collections::HashMap;

use crate::common::{
    errors::AocError,
    parsers::{by_lines, input_parser, parse_int},
};
use itertools::Itertools;
use nom::{character::complete::space1, sequence::separated_pair, IResult};

fn parse_line(s: &str) -> IResult<&str, (u32, u32)> {
    separated_pair(parse_int, space1, parse_int)(s)
}

#[aoc_generator(day1)]
fn generator(input: &str) -> Result<Vec<(u32, u32)>, AocError> {
    input_parser(by_lines(parse_line))(input)
}

#[aoc(day1, part1)]
fn part01(input: &[(u32, u32)]) -> u32 {
    let left = input.iter().map(|(a, _)| a).sorted();
    let right = input.iter().map(|(_, b)| b).sorted();

    left.zip(right).map(|(l, r)| l.max(r) - l.min(r)).sum()
}

#[aoc(day1, part2)]
fn part02(input: &[(u32, u32)]) -> usize {
    let mut similarities = HashMap::with_capacity(input.len());

    for (a, _) in input {
        let entry = similarities.entry(a).or_insert((0, 0));
        if entry.0 != 0 {
            *entry = (entry.0, entry.1 + entry.0);
        } else {
            let occurrences = input.iter().filter(|(_, b)| a == b).count();
            let similarity = *a as usize * occurrences;
            *entry = (similarity, similarity);
        }
    }

    similarities.values().map(|(_, total)| total).sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    const BUILD_INPUT: &str = r"3   4
4   3
2   5
1   3
3   9
3   3";

    #[test]
    fn test01_gen() {
        let input = generator(BUILD_INPUT);

        println!("-- {:?} --", input)
    }

    #[test]
    fn test0101() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part01(&input);

        assert_eq!(res, 11);
    }

    #[test]
    fn test0102() {
        let input = generator(BUILD_INPUT).unwrap();
        let res = part02(&input);

        assert_eq!(res, 31);
    }
}
